import React from 'react';

import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  AsyncStorage,
  View,
  TouchableWithoutFeedback,
  StatusBar,
  TextInput,
  SafeAreaView,
  Keyboard,
  KeyboardAvoidingView,
  ScrollView
} from 'react-native';

import { WebBrowser } from 'expo';
import DeviceInfo from 'react-native-device-info'; //Todo
//https://github.com/rebeccahughes/react-native-device-info
import { SIGN_IN } from "../graphqlQueries";
//import { Mutation } from "react-apollo";
import { Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';
//import LoginMutation from '../components/LoginMutation';
import { graphql } from 'react-apollo';

import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import { Formik } from "formik";
import * as Yup from "yup";
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";

import storage from '../helper/storage';

const MyInput = compose(handleTextInput, withNextInputAutoFocusInput)(TextField);

const Form = withNextInputAutoFocusForm(ScrollView, {
  submitAfterLastInput: false
});

const initialState = {
  email: "ronak@complitech.us",
  password: "12345678"

  // email: "ashwin@complitech.net",
  // password: "testing"

  //  email: "raj@complitech.net",
  //  password: "12345678"

  // email: "nikhil@complitech.net",
  // password: "12345678"
};

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .required()
    .email(),
  password: Yup.string()
    .required()
    .min(6, "Too short"),
  star: Yup.boolean()
    .required()
    .oneOf([true])
});


class LogInScreen extends React.Component {
  static navigationOptions = {
    title: '',
    header: null
  };

  constructor(props) {
    super(props);
    this.state = { ...initialState };
  }

  render() {
    console.log("loginscreen1 cll");
    let { email, password, error } = this.state;
    return (
      // <Mutation mutation={SIGN_IN} variables={{ email, password }}>
      //  {(login, { error, data, loading }) => {
      //       // if (loading) return "Loading...";
      //       if (error) console.log(error.message);

      //       return (
      <View style={styles.container}>
        <SafeAreaView style={styles.container}>
          <StatusBar barStyle="light-content" />
          <KeyboardAvoidingView behavior='padding' style={styles.container}>
            <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>
              <View style={styles.container}>
                <View style={styles.logoContainer}>
                  <Image style={styles.logo}
                    source={require('../assets/images/logo.png')}>
                  </Image>
                </View>
                <Formik
                  onSubmit={values => alert(JSON.stringify(values, null, 2))}
                  validationSchema={validationSchema}
                  initialValues={{ star: true }}>
                  <Form style={styles.infoContainer} keyboardShouldPersistTaps='always'>
                    <MyInput
                      //style={styles.input}
                      label="Email"
                      name="email"
                      type="email"
                      id="email"
                      placeholder="Email"
                      value={this.state.email}
                      onChangeText={(value)=> this.onEmailChange(value)}
                      placeholderTextColor='rgba(255,255,255,0.8)'
                      keyboardType='email-address'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtPassword.focus()}
                    />
                    <MyInput
                      //style={styles.input}
                      label="Password"
                      name="password"
                      type="password"
                      placeholder="Password"
                      placeholderTextColor='rgba(255,255,255,0.8)'
                      returnKeyType='go'
                      value={this.state.password}
                      onChangeText={(value) => this.onPasswordChange(value)}
                      secureTextEntry={true}
                      autoCorrect={false}
                      ref={"txtPassword"}
                    />

                    {/* <LoginMutation email={email} password={password} /> */}
                    <TouchableOpacity onPress={this._gotoForgetPassword}>
                      <Text style={styles.buttonForgotContainer}>Forgot your password?</Text>
                    </TouchableOpacity>
                    <Button raised
                      buttonStyle={styles.buttonContainer}
                      icon={{ name: 'fingerprint' }}
                      title='SIGN IN'
                      onPress={this._signInAsync} />

                    <Button raised
                      buttonStyle={styles.buttonSignupContainer}
                      icon={{ name: 'fingerprint' }}
                      title='SIGN UP'
                      onPress={this._gotoSignupScreen} />

                  </Form>
                </Formik>
              </View>
            </TouchableWithoutFeedback>
          </KeyboardAvoidingView>
        </SafeAreaView>

      </View>

      // );}}
      // </Mutation>
    );
  }

  onEmailChange = email => {
    
 //   const email = e.target.value;
    console.log(email);
    this.setState(email => ({
      email
    }));
  };
  onPasswordChange = password => {
    //const password = e.target.value;
    this.setState(password => ({
      password
    }));
  };
  _gotoSignupScreen = async () => {
    this.props.navigation.navigate('SignupScreen')
  };
  _signInAsync = async () => {
    //await AsyncStorage.setItem('userToken', 'abc');
    //this.props.navigation.navigate('App');
    this.props.
      signIn(this.state.email, this.state.password)
      .then(({ data }) => {
        console.log("new token eeee: " + JSON.stringify(data));
        if (data) {
         AsyncStorage.setItem('token', data.signIn.authentication_token)
          .then(() => {
            this.props.navigation.navigate('App');
          })


          AsyncStorage.setItem('account', JSON.stringify(data.signIn.user.account))
            .then(() => {
              console.log('It was saved successfully yeeeeeeeee')
            })
            .catch(() => {
              console.log('There was an error saving the account')
            })


          console.log("dsfwwwww" , data.signIn.user.account)
          //AsyncStorage.setItem('account', JSON.stringify(data.signIn));
         // this._storageTest(data)
         this._storeageUser(data)

          
        }
      }).catch((error) =>{
        console.log('my error :'+ error);
      })
  };

  _storeageUser = async (data) => {

    storage.save({
      key: 'user', // Note: Do not use underscore("_") in key!
      data: data.signIn.user,
     
      // if expires not specified, the defaultExpires will be applied instead.
      // if set to null, then it will never expire.
      //expires: 1000 * 3600
      expires: null
    });
  }

  _storageTest = async (data) => {

    await AsyncStorage.setItem('account', JSON.stringify(data.signIn.user.account))
            .then(() => {
              console.log('It was saved successfully')
            })
            .catch(() => {
              console.log('There was an error saving the account')
            })
  }


  // _signInAsync = async(login)   => {
  //   console.log('here we go');
  //   login().then(({ data: { signIn: { authentication_token } } }) => {
  //      console.log(authentication_token);
  //     // localStorage.setItem("token", authentication_token);
  //     // this.resetState();
  //     // this.props.history.push("/dashboard");
  //     console.log('before store token');
  //     AsyncStorage.setItem('token', authentication_token);
  //     this.props.navigation.navigate('App');  
  //   });

  // };

  _gotoForgetPassword = async () => {
    this.props.navigation.navigate('ForgetPasswordScreen')
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(255, 255, 255)',
    flexDirection: 'column'
  },
  logoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  logo: {
    width: 266,
    height: 50
  },
  title: {
    color: '#f7c744',
    fontSize: 18,
    textAlign: 'center',
    marginTop: 5,
    opacity: 0.9
  },
  infoContainer: {
    flexDirection: 'column',
    flex: 1,
    // position: 'absolute',
    // left: 0,
    // right: 0,
    bottom: 0,
    height: 200,
    //padding: 30,
    padding: 10,
    //backgroundColor: 'red'
  },
  input: {
    height: 40,
    backgroundColor: 'rgba(0,0,0,0.2)',
    color: '#fff',
    marginBottom: 20,
    paddingHorizontal: 10
  },
  containerButton: {
    flexDirection: 'row',
    backgroundColor: 'red',
  },
  buttonContainer: {
    backgroundColor: '#373f51',
    marginTop: 15,
  },
  buttonForgotContainer: {
    marginTop: 15,
  },
  buttonSignupContainer: {
    marginTop: 30,
    backgroundColor: '#423231',
  },

  buttonText: {
    textAlign: 'center',
    color: 'rgb(32,53,70)',
    fontWeight: 'bold',
    fontSize: 18
  }
});

export default graphql(SIGN_IN,
  {
    props: ({ mutate }) => ({
      signIn: (email, password) => mutate({ variables: { email, password } })
    }),

  }
)(LogInScreen)
