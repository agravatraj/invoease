import React from "react";

import {
  StyleSheet,
  AsyncStorage,
  View,
  SafeAreaView,
  StatusBar,
  Keyboard,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Image,
  TextInput,
  ScrollView,
  Alert,
  Text,
  TouchableOpacity,
  Animated,
  TouchableHighlight,
  SectionList
} from "react-native";
import { Button, List, ListItem } from "react-native-elements";

import { Query } from "react-apollo";
import { GET_TAXES } from "../graphqlQueries";

import { CREATE_PRODUCT, GET_PRODUCTS } from "../graphqlQueries";
import { graphql } from "react-apollo";

import DateTimePicker from "react-native-modal-datetime-picker";

import { UPDATE_PRODUCT } from "../graphqlQueries";
import SimplePicker from "react-native-simple-picker";

import {
  handleTextInput,
  withNextInputAutoFocusForm,
  withNextInputAutoFocusInput
} from "react-native-formik";
import { Formik } from "formik";
import * as Yup from "yup";
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";
import { Dropdown } from "react-native-material-dropdown";
import { createFilter } from "react-native-search-filter";

var FloatingLabel = require("react-native-floating-labels");

const KEYS_TO_FILTERS = ["name"];

//import DoneButton from 'react-native-keyboard-done-button';
const math = require("mathjs");

import SwitchToggle from "react-native-switch-toggle";
var listSelectItemTax = [];

const MyInput = compose(
  handleTextInput,
  withNextInputAutoFocusInput
)(TextField);

const options = [
  "Bank Transfer",
  "PayPal",
  "Stripe",
  "Credit Card",
  "Debit Card",
  "Check",
  "Cash",
  "Other"
];
const labels = [
  "Bank Transfer",
  "PayPal",
  "Stripe",
  "Credit Card",
  "Debit Card",
  "Check",
  "Cash",
  "Other"
];

const Form = withNextInputAutoFocusForm(ScrollView, {
  submitAfterLastInput: false
});

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .required()
    .email(),
  password: Yup.string()
    .required()
    .min(6, "Too short"),
  confirmPassword: Yup.string().oneOf(
    [Yup.ref("password")],
    "Passwords do not match"
  ),
  star: Yup.boolean()
    .required()
    .oneOf([true])
});

const initialState = {
  id: "",
  name: "",
  description: "",
  price: "",
  quantity: "",
  inventory: "25",
  account_id: "",
  isNodeSelected: false,
  isFocusNodeSelected: false,
  focusNodes: [],
  selectedSwitches: [],
  selectedTaxes:0,
  isFocusNodeFilled: false,
  tax1_id: 0,
  tax2_id: 0
};

class ProductAddScreen extends React.Component {
  // static navigationOptions = {
  //   title: 'Add Product',
  //   headerRight: <TouchableOpacity onPress={this._productAddAsync}><View style={{marginRight:20}}><Text style={{color: '#000', fontSize: 18,}}>Save</Text></View></TouchableOpacity>
  // };

  static navigationOptions = ({ navigation }) => {
    return {
      title: "Add Product",
      headerTitle: "Add Product",
      headerRight: (
        <TouchableOpacity onPress={navigation.getParam("productAddAsync")}>
          <View style={{ marginRight: 20 }}>
            <Text style={{ color: "#000", fontSize: 18 }}>Save</Text>
          </View>
        </TouchableOpacity>
      )
    };
  };

  onPress1 = () => {
    this.setState({ switchOn1: !this.state.switchOn1 });
  };
  onPress2 = () => {
    this.setState({ switchOn2: !this.state.switchOn2 });
  };
  onPress3 = () => {
    this.setState({ switchOn3: !this.state.switchOn3 });
  };
  onPress4 = () => {
    this.setState({ switchOn4: !this.state.switchOn4 });
  };

  constructor(props) {
    super(props);

    this.state = {
      ...initialState,
      switchOn1: false,
      switchOn2: false,
      switchOn4: false,
      valueArray: [],
      comeFromCreateInvoice: this.props.navigation.state.params
        .comeFromCreateInvoice
        ? this.props.navigation.state.params.comeFromCreateInvoice
        : "false",
      taxes: [],
      dropdownValue: "",
      mydropdowndata: []
    };

    listSelectItemTax = [];
    this.index = 0;
    this.animatedValue = new Animated.Value(0);

    this._retrieveAccountData();
  }

  componentDidMount() {}

  _setScreenState() {
    console.log(
      "sdf",
      this.props.navigation.state.params.comeFromCreateInvoice
    );
  }

  UNSAFE_componentWillMount() {
    const { navigation } = this.props;
    const item = navigation.getParam("item", "NO-ITEM");

    this._setScreenState();

    // if (item !== null)
    // {
    //   this.setState({ id: item.id.toString() })
    //   this.setState({ name: item.name })
    //   this.setState({ price: item.price.toString() })
    //   this.setState({ quantity: item.quantity.toString() })
    //   this.setState({ inventory: item.inventory.toString() })
    //   this.setState({ account_id: item.account_id.toString() })

    //   const isFromUpdate = navigation.getParam('isRecordUpdate', 'NO-UPDATE-ITEM');
    //   console.log(isFromUpdate)
    // }

    this.props.navigation.setParams({ productAddAsync: this._productAddAsync });

    console.log("In User Data Function");

    storage
      .load({
        key: "user",
        autoSync: true,
        syncInBackground: true,

        // you can pass extra params to the sync method
        // see sync example below
        syncParams: {
          extraFetchOptions: {
            // blahblah
          },
          someFlag: true
        }
      })
      .then(ret => {
        // found data go to then()
        console.log("we found data");
        console.log("first", ret.first_name);
        console.log("last", ret.last_name);
        console.log(ret.account[0].name);
        console.log(ret.account[0].phone);
        console.log("ret =", ret);
        console.log("taxes =[=[= ", ret.taxes);
        this.setState({ taxes: ret.taxes });
      })
      .catch(err => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case "NotFoundError":
            // TODO;

            break;
          case "ExpiredError":
            // TODO
            break;
        }
      });
  }

  handleOnNavigateBack = item => {
    listSelectItemTax.push(item);
    console.log("ProductAdd Screen &&&&&&&&&&&& ", listSelectItemTax);
    storage.save({
      key: "selectItemTax", // Note: Do not use underscore("_") in key!
      data: listSelectItemTax,

      // if expires not specified, the defaultExpires will be applied instead.
      // if set to null, then it will never expire.
      expires: null //1000 * 3600
    });
    console.log("handleOnNavigateBack call product list");
    console.log(listSelectItemTax);

    this.setState({ newSubtotal: Number(0.0) });

    console.log("listSelectItemTax length", listSelectItemTax.length);
    subtotalCount = 0.0;

    listSelectItemTax.map((item, key) => {
      console.log(
        "Q * P",
        math.eval(Number(item.quantity) * Number(item.unit_cost))
      );
      //subtotalCount += math.eval(Number(item.quantity) * Number(item.price));
      subtotalCount += Number(item.quantity) * Number(item.unit_cost);
      console.log("subtotal", subtotalCount);
    });
    console.log("subtotal count", subtotalCount);

    this.setState({ newSubtotal: Number(subtotalCount) });
    console.log(this.state.newSubtotal);

    this.addMore();
  };

  gotoAddTax = () => {
    console.log("In ADD ProductScreen screen");
    this.props.navigation.navigate("AddTax", {
      onNavigateBack: this.handleOnNavigateBack.bind(this)
    });
  };

  addMore = () => {
    console.log("Add More Call");
    this.animatedValue.setValue(0);

    let newlyAddedValue = { index: this.index };

    console.log(newlyAddedValue);

    storage
      .load({
        key: "selectItemTax",

        autoSync: true,

        syncInBackground: true,

        syncParams: {
          extraFetchOptions: {
            // blahblah
          },
          someFlag: true
        }
      })
      .then(ret => {
        // found data go to then()
        console.log("we found data Invoease Detail");
        console.log(ret);

        this.setState(
          { disabled: true, valueArray: [...this.state.valueArray, ret] },
          () => {
            Animated.timing(this.animatedValue, {
              toValue: 1,
              duration: 500,
              useNativeDriver: true
            }).start(() => {
              this.index = this.index + 1;
              this.setState({ disabled: false });

              //console.log(...this.state.valueArray)
            });
          }
        );

        //console.log(ret.states);

        // var mystates = []
        // ret.states.map(state => mystates.push(state.name) );
        // this.setState({ stateLabel : mystates, stateOptions: mystates  });
      })
      .catch(err => {
        // any exception including data not found
        // goes to catch()
        console.log("error home" + err);
        console.warn(err.message);
        switch (err.name) {
          case "NotFoundError":
            // TODO;
            break;
          case "ExpiredError":
            // TODO
            break;
        }
      });
  };

  _handleSwitch = (value, index, item) => {
    //this.state.isFocusNodeSelected = true;
    console.log(value);
    console.log(index);
    console.log("item", item);
    console.log("item title", item.name);
    console.log("item tax", item.rate);

    if (this.state.selectedSwitches.length > 1 && value === true) {
      console.log("Removing index at 0 index from an array.");
      this.state.focusNodes[this.state.selectedSwitches[0]] = false;

      this.state.selectedSwitches.splice(0, 1);
      this.state.selectedSwitches.push(index);

      this.state.selectedTaxes.splice(0, 1);
      this.state.selectedTaxes.push(item.id);

      this.state.focusNodes[index] = true;

      this.setState({ focusNodes: this.state.focusNodes });

      console.log("New focus nodes ____", this.state.focusNodes);
    } else if (this.state.selectedSwitches.length > 1 && value === false) {
      this.state.focusNodes[index] = value;
      this.setState({ focusNodes: this.state.focusNodes });
    } else {
      this.state.selectedSwitches.push(index);
      this.state.focusNodes[index] = value;
      this.state.selectedTaxes.push(item.id);
      this.setState({ focusNodes: this.state.focusNodes });
    }
    console.log(
      "selectedSwitches length =",
      this.state.selectedSwitches.length
    );
    console.log("Selected switches", this.state.selectedSwitches);
    //this.setState({ name: value.name })
    //console.log(this.state.isFocusNodeSelected);
  };

  render() {
    let {
      name,
      description,
      price,
      quantity,
      inventory,
      account_id
    } = this.state;

    const animationValue = this.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: [-59, 0]
    });
    console.log("value array Taxes => ", JSON.stringify(this.state.taxes));

    // let newArray = this.state.taxes.map((item, key) => {
    //   console.log("newArray Tax => ", JSON.stringify(item));

    //   //'task' => 'TEST Devloper','description' => 'TEST Development','rate' => '100.00','tax_name' => 'C-GST','tax' => '10.00','qty' => '1.00','total' => '110.00'

    //   if ((key) == this.index) {
    //     return (
    //       <Animated.View key={key} style={[styles.viewHolder1, { opacity: this.animatedValue, transform: [{ translateY: animationValue }] }]}>
    //         {/* <Text style={styles.text}>Row {item.index}</Text> */}

    //         <View style={styles.payamounttitle}>
    //           <View style={{ flex: 3, flexDirection: 'column' }}>
    //             <Text style={styles.amounttitle}>{item.name}</Text>
    //             <Text style={styles.tax}>{item.rate}%</Text>
    //           </View>
    //           <View style={{ flex: 1, flexDirection: 'column' }}>
    //             <SwitchToggle
    //               switchOn={this.state.switchOn1}
    //               onPress={this.onPress1}
    //             />
    //           </View>
    //         </View>
    //       </Animated.View>
    //     );
    //   }
    //   else {
    //     return (
    //       <View key={key} style={styles.payamounttitle}>
    //         {/* <Text style={styles.text}>Row {item.index}</Text>  1 at 1.00,00 */}

    //         <View style={{ flex: 3, flexDirection: 'column' }}>
    //           <Text style={styles.amounttitle}>{item.name}</Text>
    //           <Text style={styles.tax}>{item.rate}%</Text>
    //         </View>
    //         <View style={{ flex: 1, flexDirection: 'column' }}>
    //           <SwitchToggle
    //             switchOn={this.state.switchOn1}
    //             onPress={this.onPress1}
    //           />
    //         </View>
    //       </View>
    //     );
    //   }
    // });

    return (
      <SafeAreaView style={styles.container}>
        <KeyboardAvoidingView behavior="padding" style={styles.container}>
          <StatusBar barStyle="light-content" />
          <ScrollView style={styles.container}>
            <TouchableWithoutFeedback
              style={styles.container}
              onPress={Keyboard.dismiss}
            >
              <View style={styles.container}>
                <Formik
                  onSubmit={values => alert(JSON.stringify(values, null, 2))}
                  validationSchema={validationSchema}
                  initialValues={{ star: true }}
                >
                  {/* <Form style={styles.payamounttitle}>                    
                      <Text style={styles.paytext}>Item Name</Text>
                      <TextInput
                        style={styles.paytitle}
                        value={this.state.name}
                        onChangeText={(name) => this.setState({ name })}
                        placeholder="Design Consulting"
                      />
                      <View style={styles.payamounttitle}>
                        <TextInput
                          style={styles.amount}
                          value={this.state.description}
                          onChangeText={(description) => this.setState({ description })}
                          placeholder="Description"
                        />
                      </View>
                      <View style={{ flexDirection: 'row' }}>
                        <View style={styles.paymenttype}>
                          <Text style={styles.amounttitle}>Rate</Text>
                          <View>
                            <TextInput
                              style={styles.rateamount}
                              value={this.state.price}
                              onChangeText={(price) => this.setState({ price })}
                              keyboardType="number-pad"
                              returnKeyType='done'
                              placeholder="Rate"
                            />
                          </View>
                        </View>
                        <View style={styles.paymentdate}>
                          <Text style={styles.amounttitle}>Quantity</Text>
                          <View style={{ flexWrap: 'wrap', alignItems: 'flex-start', flexDirection: 'row', }}>
                            <TouchableOpacity>
                              <TextInput
                                style={styles.amount}
                                value={this.state.quantity}
                                onChangeText={(quantity) => this.setState({ quantity })}
                                placeholder="Quantity"
                                returnKeyType='done'
                                keyboardType="number-pad"
                              />
                            </TouchableOpacity>
                          </View>
                        </View>
                      </View>
                      <Text style={styles.paytext}>Taxes</Text>
                      <View style={styles.payamounttitle}>
                        {this.renderTaxes()}
                      </View>
                      <View style={styles.payamounttitle}>
                        <Text style={styles.lineTotalTitle}>Line Total</Text>
                        <Text style={styles.amount}>{ Number(this.state.price) * Number(this.state.quantity)}</Text>
                      </View>
                      <TouchableOpacity activeOpacity={0.8} disabled={this.state.disabled} onPress={this.gotoAddTax}>
                        <View>
                          <Text h3 style={styles.addTaxes}>Add Taxes</Text>
                        </View>
                      </TouchableOpacity>
                      <Button raised
                        buttonStyle={styles.buttonContainer}
                        icon={{ name: 'fingerprint' }}
                        title='SUBMIT'
                        onPress={this._productAddAsync} />
                    </Form> */}

                  <Form
                    style={styles.infoContainer}
                    keyboardShouldPersistTaps="always"
                  >
                    <View style={styles.payamounttitle}>
                      <FloatingLabel
                        labelStyle={styles.labelInput}
                        inputStyle={styles.input}
                        style={styles.formInput}
                        value={this.state.name}
                        onChangeText={name => this.setState({ name })}
                        onBlur={this.onBlur}
                      >
                        Item Name
                      </FloatingLabel>
                    </View>
                    <View style={styles.payamounttitle}>
                      <FloatingLabel
                        labelStyle={styles.labelInput}
                        inputStyle={styles.input}
                        style={styles.formInput}
                        value={this.state.description}
                        onChangeText={description =>
                          this.setState({ description })
                        }
                        onBlur={this.onBlur}
                      >
                        Description
                      </FloatingLabel>
                    </View>

                    <View style={{ flexDirection: "row" }}>
                      <View style={styles.paymenttype}>
                        <FloatingLabel
                          labelStyle={styles.labelInput}
                          inputStyle={styles.input}
                          style={styles.formInput}
                          value={this.state.price}
                          onChangeText={price => this.setState({ price })}
                          keyboardType="number-pad"
                          returnKeyType="done"
                          onBlur={this.onBlur}
                        >
                          Rate
                        </FloatingLabel>
                      </View>
                      <View style={styles.paymentdate}>
                        <FloatingLabel
                          labelStyle={styles.labelInput}
                          inputStyle={styles.input}
                          style={styles.formInput}
                          value={this.state.quantity}
                          onChangeText={quantity => this.setState({ quantity })}
                          returnKeyType="done"
                          keyboardType="number-pad"
                          onBlur={this.onBlur}
                        >
                          Quantity
                        </FloatingLabel>
                      </View>
                    </View>

                    <View>
                      {/* <Text style={styles.paytaxdesc}>Taxes</Text> */}
                      {/* {this.renderTaxes()} */}
                      {this.renderTaxesDropdown()}
                      {/* {this.dropdown()} */}
                    </View>

                    {/*<View style={styles.payamounttitle}>
                        <FloatingLabel
                          labelStyle={styles.labelInput}
                          inputStyle={styles.input}
                          style={styles.formInput}
                          onBlur={this.onBlur}>
                          
                          Line Total
                        </FloatingLabel>
                        <Text style={styles.amount}>{ Number(this.state.price) * Number(this.state.quantity)}</Text>
                      </View>*/}

                    <View>
                      <View
                        style={{
                          flex: 1,
                          flexDirection: "column",
                          paddingHorizontal: 15
                        }}
                      >
                        <Text style={styles.tax}>Line Total</Text>
                        <Text style={styles.amount}>
                          {math.eval(
                            Number(this.state.quantity) *
                              Number(this.state.price)
                          )}
                        </Text>
                      </View>
                    </View>
                    <View style={{backgroundColor:'#999',height:0.5, marginTop:5}}></View>

                    <TouchableOpacity
                      activeOpacity={0.8}
                      disabled={this.state.disabled}
                      onPress={this.gotoAddTax}
                    >
                      <View>
                        <Text h3 style={styles.addTaxes}>
                          Add Taxes
                        </Text>
                      </View>
                    </TouchableOpacity>
                    {/*<Button raised
                        buttonStyle={styles.buttonContainer}
                        icon={{ name: 'fingerprint' }}
                        title='SUBMIT'
                        onPress={this._productAddAsync} />*/}
                  </Form>
                </Formik>
              </View>
            </TouchableWithoutFeedback>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }

  renderTaxes() {
    return (
      <View styles={{ marginTop: 0, marginBottom: 0 }}>
        <List
          containerStyle={{
            marginTop: 0,
            marginLeft: 0,
            paddingLeft: 0,
            borderTopWidth: 0
          }}
        >
          <Query query={GET_TAXES}>
            {({ loading, error, data }) => {
              if (loading) return <Text>Loading...</Text>;
              if (error) {
                return <Text>Error :)</Text>;
              }
              if (this.state.isFocusNodeFilled == false) {
                this.state.focusNodes = [];
                this.state.selectedSwitches = [];
                this.state.selectedTaxes = [];
                data.taxes.map(({ item, i }) => [
                  this.state.focusNodes.push(false),
                  (this.state.isFocusNodeFilled = true)
                ]);
              } else {
              }
              console.log(this.state.focusNodes);
              console.log("this is data", data);

              return <Text></Text>;
              return (
                <View styles={{ marginTop: 0 }}>
                  {data.taxes.map((item, i) => (
                    <TouchableOpacity>
                      <ListItem
                        containerStyle={{
                          paddingTop: 0,
                          marginTop: 0,
                          borderWidth: 0,
                          borderColor: "#bbb",
                          borderStyle: "solid",
                          borderBottomColor: "#bbb",
                          borderBottomWidth: StyleSheet.hairlineWidth,
                          borderTopColor: "#fff",
                          borderTopWidth: StyleSheet.hairlineWidth
                        }}
                        key={i}
                        title={item.name}
                        subtitle={item.rate}
                        switchButton
                        hideChevron
                        switched={this.state.focusNodes[i]}
                        switchonTrackColor={"#00BCD4"}
                        onSwitch={value => {
                          console.log("value ---", value, i, item);
                          this._handleSwitch(value, i, item);
                        }}
                      />
                    </TouchableOpacity>
                  ))}
                </View>
              );
            }}
          </Query>
        </List>
      </View>
    );
  }

  renderTaxesDropdown() {
    console.log("renderTaxesDropdown called");
    return (
      <View styles={{ marginTop: 0, marginBottom: 0 }}>
        <List
          containerStyle={{
            marginTop: 0,
            marginLeft: 0,
            paddingLeft: 0,
            borderTopWidth: 0
          }}
        >
          <Query query={GET_TAXES}>
            {({ loading, error, data }) => {
              if (loading) return <Text>Loading...</Text>;
              if (error) {
                return <Text>Error :)</Text>;
              } else {
              }

              console.log(this.state.focusNodes);
              console.log("this is data", data);
              this.state.mydropdowndata = data.taxes;
              console.log("mydata", this.state.mydropdowndata);
              return <View>
                {
                  this.dropdown()
                }
                </View>;
            }}
          </Query>
        </List>
      </View>
    );
  }

  dropdown() {
    console.log("dropdown called");
    let data = this.state.mydropdowndata;
    console.log("dataaaaa---->",data);
    //this for loop is used for copy the name field object of taxes array into value field because drop down fiels takes value from only value field
    var i;
    for (i = 0; i < data.length; i++) 
    {
          data[i].value = data[i]["name"] + " (" + data[i]["rate"] + "%)";
      //  delete data[i].name;
    }
    return (
      <Dropdown
        label="    Select Tax"
        data={data}
        style={{marginLeft:12}}
        //onChangeText={(value)=>{ this.setState({dropdownValue:value})}}
        //onChangeText={(value,index,data)=>{alert(data),console.log("dropdown dataa- >",data[index].id)}}
        onChangeText={(value,index,data)=>
          {this.setState({selectedTaxes:data[index].id})} }
      />
    );
  }

  _navigateBack(input) {
    this.props.navigation.state.params.onNavigateBack(input);
    this.props.navigation.pop();
  }

  goBack() {
    this.props.navigation.state.params.onNavigateBack();
    this.props.navigation.pop();
  }

  _productAddAsync = async () => {
    if (this.state.name == "") {
      Alert.alert("Please enter item name.");
      return;
    }

    if (this.state.description == "") {
      Alert.alert("Please enter description.");
      return;
    }

    if (this.state.rate == "") {
      Alert.alert("Please enter rate.");
      return;
    }

    if (this.state.quantity == "") {
      Alert.alert("Please enter quantity.");
      return;
    }

    var input = {
      name: this.state.name,
      description: this.state.description,
      unit_cost: Number(this.state.price),
      quantity: Number(this.state.quantity),
      tax1_id: this.state.selectedTaxes? Number(this.state.selectedTaxes) : 0,
      //tax2_id: this.state.selectedTaxes[0] ? this.state.selectedTaxes[0] : 0
    };

    console.log("input -------->",input)

    if (this.state.comeFromCreateInvoice == "true") {
      console.log("IF executed...");
      this.props.navigation.state.params.onNavigateBack(input);
      this.props.navigation.pop();
    } else {
      console.log("ELSE Executed...");
      console.log("tax1", this.state.selectedTaxes[0]);
      console.log("tax2", this.state.selectedTaxes[1]);

      //  var input = {"name":this.state.name, "description": this.state.description,"price":this.state.price,
      //      "quantity": this.state.quantity,"tax1_id":this.state.selectedTaxes[0]?this.state.selectedTaxes[0]:"","tax2_id":this.state.selectedTaxes[1]?this.state.selectedTaxes[1]:""};

      console.log(
        "abc ======>",
        this.state.name,
        this.state.description,
        Number(this.state.price),
        Number(this.state.quantity),
        Number(this.state.inventory),
        Number(this.state.selectedTaxes),
        Number(this.state.selectedTaxes),
        Number(this.state.account_id),
        Number(this.state.tax1_id)
      );

      this.props
        .create_product(
          this.state.name,
          this.state.description,
          Number(this.state.price),
          Number(this.state.quantity),
          Number(this.state.inventory),
          Number(this.state.selectedTaxes),
          Number(this.state.selectedTaxes),
          Number(this.state.account_id)
        )
        //create_product(this.state.name, this.state.description, 10, 5,1, 1)
        .then(({ data }) => {
          console.log("create product : " + JSON.stringify(data));
          if (data) {
            //this.props.navigation.state.params.onNavigateBack(input)
            //this.props.navigation.pop()

            Alert.alert(
              "Invoease",
              "Product added successfully.",
              [
                { text: "OK", onPress: () => this.goBack() }
                //{ text: 'OK', onPress: () => this._navigateBack(input) },
              ],
              { cancelable: false }
            );
          }
        });
    }
  };

  _productUpdateAsync = async () => {
    console.log("this state =====> ", this.state);

    this.props
      .update_product(
        Number(this.state.id),
        this.state.name,
        this.state.description,
        Number(this.state.price),
        Number(this.state.quantity),
        Number(this.state.inventory),
        Number(this.state.account_id)
      )
      .then(({ data }) => {
        console.log("update product : " + JSON.stringify(data));
        if (data) {
          Alert.alert(
            "Invoease",
            "Product update successfully.",
            [{ text: "OK", onPress: () => _navigateBack(input) }],
            { cancelable: false }
          );
        }
      });
  };

  _retrieveAccountData = async () => {
    try {
      const value = await AsyncStorage.getItem("account");
      if (value !== null) {
        // We have data!!
        console.log("We have data!");
        console.log(JSON.parse(value));
        let data = JSON.parse(value);
        console.log(data[0]);
        console.log(" >", data[0].id);
        this.setState({
          account_id: data[0].id
        });
        console.log("Account ID");
        console.log(this.state);
      } else {
        console.log("No Account records foundddddd");
      }
    } catch (error) {
      // Error retrieving data
    }
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgb(255, 255, 255)"
  },
  logoContainer: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1
  },
  logo: {
    width: 266,
    height: 50
  },
  title: {
    color: "#F7C744",
    fontSize: 18,
    textAlign: "center",
    marginTop: 5,
    opacity: 0.9
  },
  infoContainer: {
    flexDirection: "column",
    flex: 1
  },
  input: {
    // fontSize: 18,
    // borderColor: '#ccc',
    // borderWidth: 1,
    // borderStyle: 'solid',
    // height: 30,
  },
  buttonContainer: {
    backgroundColor: "#1FA2FF",
    marginTop: 15
  },
  buttonText: {
    textAlign: "center",
    color: "rgb(32,53,70)",
    fontWeight: "bold",
    fontSize: 18
  },
  paytitle: {
    color: "#333",
    fontSize: 26,
    fontWeight: "600",
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 15
  },
  paytext: {
    color: "#999",
    fontSize: 18,
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 15
  },
  payamounttitle: {
    borderWidth: 0.5,
    borderColor: "#ccc",
    backgroundColor: "#fff",
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 0
  },
  amounttitle: {
    color: "#ccc",
    fontSize: 16
  },
  amount: {
    marginTop: 5,
    color: "#444",
    fontSize: 21,
    paddingLeft: 0
  },
  paymenttype: {
    width: "50%",
    borderWidth: 0.5,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 0.5,
    borderColor: "#ccc",
    backgroundColor: "#fff",
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 0
  },
  paymentdate: {
    width: "50%",
    borderWidth: 0.5,
    borderTopWidth: 0,
    borderBottomWidth: 0.5,
    borderColor: "#ccc",
    backgroundColor: "#fff",
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 0
  },

  labelInput: {
    color: "#ccc",
    fontSize: 15,
    paddingLeft: 15
  },
  formInput: {
    // borderBottomWidth: 1,
    paddingLeft: 5,
    borderColor: "#ccc"
  },
  input: {
    borderWidth: 0
  },
  addTaxes: {
    color: "skyblue",
    fontSize: 18,
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 10,
    paddingLeft: 10,
    textAlign: "center"
  },
  payswitch: {
    backgroundColor: "orange"
  },
  rateamount: {
    color: "#999",
    fontSize: 20
  },
  tax: {
    marginTop: 5,
    color: "#ccc",
    fontSize: 17,
    flexWrap: "wrap"
  },
  lineTotal: {
    borderWidth: 0,
    paddingTop: 15,
    paddingRight: 15,
    paddingLeft: 15,
    paddingBottom: 15
  },
  lineTotalTitle: {
    marginLeft: 10
  },
  paytaxdesc: {
    paddingRight: 15,
    paddingLeft: 15,
    color: "#ccc",
    fontSize: 16
  }
});

// export default graphql(UPDATE_PRODUCT,
//   {
//     props: ({ mutate }) => ({
//       update_product: (id, name, description, price, quantity, inventory, account_id) => mutate({ variables: { id, name, description, price, quantity, inventory, account_id } })
//     }),

//   }
// )(ProductAddScreen)

export default graphql(CREATE_PRODUCT, {
  props: ({ mutate }) => ({
    // create_product: (name, description, price, quantity, inventory, account_id) => mutate({ variables: { name, description, price, quantity, inventory, account_id } })
    create_product: (
      name,
      description,
      price,
      quantity,
      inventory,
      tax1_id,
      tax2_id,
      account_id
    ) =>
      mutate({
        variables: {
          name,
          description,
          price,
          quantity,
          inventory,
          tax1_id,
          tax2_id,
          account_id
        }
      })
  }),
  options: {
    refetchQueries: [
      {
        query: GET_PRODUCTS
      }
    ]
  }
})(ProductAddScreen);
