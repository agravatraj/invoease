import React from 'react';

import {View, Text, StyleSheet, TextInput, ScrollView,Dimensions,TouchableOpacity} from 'react-native';
import { CheckBox, Input, Button, ListItem, SearchBar, Divider, Header, ButtonGroup, Card } from 'react-native-elements';
import ActionButton from 'react-native-action-button';
// import Icon from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/Ionicons';
import IconFont from 'react-native-vector-icons/FontAwesome';

export default class InvoiceView extends React.Component {
    

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'app.json',
      headerTitle: 'Invoice Detail',
      headerRight: <TouchableOpacity onPress={navigation.getParam('gotoEditInvoiceScreen')}><View style={{marginRight:20}}><IconFont name={'edit'} size={22} color={'#373f51'} style={{paddingRight:10}}/></View></TouchableOpacity>
    };
  };

  constructor(props){
    super(props);
    console.disableYellowBox = true;
    this.state = {
      myInvoice : this.props.navigation.state.params.myInvoice,
      // myInvoiceLines : this.state.myInvoice.invoice_lines,
      myInvoiceNumber : this.props.navigation.state.params.invoiceNumber,
      newPaid_amt : 0.0,
    };

    
    console.log("this.state.myInvoice.--- ",this.state.myInvoice)
    
    
  }

  UNSAFE_componentWillMount() {
    this.props.navigation.setParams({ gotoEditInvoiceScreen: this._gotoEditInvoiceScreen });
  }

  _gotoEditInvoiceScreen = async () => {
    console.log("go to n edit invoice screen.......",this.state.myInvoice)
    this.props.navigation.navigate('EditInvoiceScreen',{editInvoice:this.state.myInvoice})
  };

    
  handleOnNavigateBackFromAddPayment = (paidAmt) => {
    
    this.setState({ newPaid_amt: paidAmt });
  }

  _gotoAddPaymentScreen = async () => {
    this.props.navigation.navigate('AddPayment',{myInvoice:this.state.myInvoice , invoiceId : this.state.myInvoice.id , 
      onNavigateBack: this.handleOnNavigateBackFromAddPayment.bind(this)})
  };

  

  _gotoInvoiceEmailScreen = async () => {
    //console.log("this.state.myInvoice" , this.state.myInvoice)
     this.props.navigation.navigate('InvoiceEmail',{myInvoice:this.state.myInvoice})
  };

  _gotoNotificationScreen = async () => {
    console.log("heyyyyyy",this.state.myInvoice.id)
    this.props.navigation.navigate('InvoiceNotification',{invoiceId:this.state.myInvoice.id})
  };
  
  render() {    
    return (
    <View>
    
    <ScrollView>
      <View style={{flex: 1, backgroundColor:'#fff', paddingLeft: 10, paddingRight: 10, position: 'relative'}}>
        <View style={styles.logo}>
          <Text h3 style={styles.mainamountdue}>Amount Due ({this.state.myInvoice.client[0].currency[0].symbol})</Text>
          <Text h1 style={styles.mainamount}>{this.state.myInvoice.due_amount.toFixed(2)}</Text>
        </View>

        <View style={{flex: 1, flexDirection: 'row'}}>
         
          <View style={{width: '55%',}}>
            <View style={styles.description}>
              <View style={{marginTop: 20, flex:1}}>
                <Text style={styles.welcome1}>Billed To</Text>
                <Text h3 style={styles.text} style={{ fontSize: 16, color: 'black' }}> {this.state.myInvoice.client[0].company_name}</Text>
                <Text h3 style={styles.text} style={{ fontSize: 16, color: 'black' }}> {this.state.myInvoice.address1}</Text>
                <Text h3 style={styles.text} style={{ fontSize: 16, color: 'black' }}> {this.state.myInvoice.address2}</Text>
                <Text h3 style={styles.text} style={{ fontSize: 16, color: 'black' }}> {this.state.myInvoice.city}</Text>
                <Text h3 style={styles.text} style={{ fontSize: 16, color: 'black' }}> {this.state.myInvoice.country}</Text>
              </View>
            </View>
          </View>

          <View style={{width: '5%',}} />
          
          <View style={{width: '40%'}}>
            <View style={styles.description}>
              <View style={{marginTop: 20, flex:1}}>
                <Text style={styles.welcome2}>Invoice Number</Text>
                <Text style={{fontSize: 16, textAlign: 'right', color: 'black'}}>
                  {this.state.myInvoice.invoice_number}
                </Text>
              </View>
            </View>        

            <View style={{marginTop: 10}}>
              <Text style={styles.welcome}>Date of Issue</Text>
              <Text style={{fontSize: 16, textAlign: 'right', color: 'black'}}>
                {this.state.myInvoice.inv_date}
              </Text>
            </View>

            <View style={{marginTop: 10}}>
              <Text style={styles.welcome}>Due Date</Text>
              <Text style={{fontSize: 16, textAlign: 'right', color: 'black'}}>
                {this.state.myInvoice.inv_due_date}
              </Text>
            </View>
          </View>

        </View>

        <View style={{borderBottomColor: 'green', borderBottomWidth: 3, marginBottom: 10, marginTop: 20}}></View>
        <View style={styles.description}>
          <View style={{flex:1}}>
            <Text style={styles.welcome1}> Description </Text>
          </View>
          <View style={{flex:1}}>
            <Text style={styles.welcome3}> Tax </Text>
          </View>
          <View style={{flex:1}}>
            <Text style={styles.welcome2}> Line Total </Text> 
          </View>
        </View>

        <View>
          {
            this.state.myInvoice.invoice_lines.map((item, i) => (
              this.renderItem(item)
              // { this.renderItem(item)}
            ))
          }
          </View>

          {/* <View style={{flex:1}}>
            <Text style={styles.descriptiontext1}> Web Services </Text>
            <Text h5 style={styles.unitprice}> 1 at 1.00,00 </Text>
          </View>
          <View style={{flex:1}}>
            <Text style={styles.descriptiontext2}> 1.100,00 </Text>
          </View> */}


        <View style={{flex: 1, flexDirection: 'row', borderBottomColor: '#999', borderBottomWidth: 1, paddingBottom: 10}}>
          <View style={{width: '30%',}} />

          <View style={{width: '70%',}} >
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{width: '60%',}}>
                <View style={{marginTop: 10}}>
                  <Text style={{fontSize: 16, textAlign: 'right'}}>Subtotal</Text>
                </View>
              </View>
              <View style={{width: '40%',}}>
                <View style={{marginTop: 10}}>
                  <Text style={{fontSize: 16, textAlign: 'right', color: 'black'}}>
                  {this.state.myInvoice.client[0].currency[0].symbol}{this.state.myInvoice.amount}
                  </Text>
                </View>
              </View>
            </View>

            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{width: '60%',}}>
                <View style={{marginTop: 10}}>
                  <Text style={{fontSize: 16, textAlign: 'right'}}>Discount</Text>
                </View>
              </View>
              <View style={{width: '40%',}}>
                <View style={{marginTop: 10}}>
                  <Text style={{fontSize: 16, textAlign: 'right', color: 'black'}}>
                  {
                    100
                    //this.state.myInvoice.discount_amount != '' && this.state.myInvoice.client[0].currency[0].symbol + this.state.myInvoice.discount_amount
                  }
                  </Text>
                </View>
              </View>
            </View>

            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{width: '60%',}}>
                <View style={{marginTop: 10}}>
                  <Text style={{fontSize: 16, textAlign: 'right'}}>Tax</Text>
                </View>
              </View>
              <View style={{width: '40%',}}>
                <View style={{marginTop: 10}}>
                  <Text style={{fontSize: 16, textAlign: 'right', color: 'black'}}>
                    60
                  </Text>
                </View>
              </View>
            </View>

          </View>
        </View>

        <View style={{flex: 1, flexDirection: 'row', borderBottomColor: '#999', borderBottomWidth: 2, paddingBottom: 10}}>
          <View style={{width: '30%',}} />

          <View style={{width: '70%',}} >
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{width: '60%',}}>
                <View style={{marginTop: 10}}>
                  <Text style={{fontSize: 16, textAlign: 'right'}}>Total</Text>
                </View>
              </View>
              <View style={{width: '40%',}}>
                <View style={{marginTop: 10}}>
                  <Text style={{fontSize: 16, textAlign: 'right', color: 'black'}}>
                  {
                    1060
                    //this.state.myInvoice.client[0].currency[0].symbol}{this.state.myInvoice.total_invoice_amount
                  }
                  </Text>
                </View>
              </View>
            </View>

            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{width: '60%',}}>
                <View style={{marginTop: 0}}>
                  <Text style={{fontSize: 16, textAlign: 'right'}}>Amount Paid</Text>
                </View>
              </View>
              <View style={{width: '40%',}}>
                <View style={{marginTop: 0}}>
                  <Text style={{fontSize: 16, textAlign: 'right', color: 'black'}}>
                  {this.state.myInvoice.client[0].currency[0].symbol}{Number(this.state.myInvoice.paid_amount) + Number(this.state.newPaid_amt)}
                    </Text>
                </View>
              </View>
            </View>            
          </View>
        </View>

        <View style={{flex: 1, flexDirection: 'row', paddingBottom: 15}}>
          <View style={{width: '25%',}} />

          <View style={{width: '75%',}}>
            <View style={{flex: 1, flexDirection: 'row'}}>
              <View style={{width: '62%',}}>
                <View style={{marginTop: 10}}>
                  <Text style={{fontSize: 16, color: 'green', textAlign: 'right'}}>Amount Due ({this.state.myInvoice.client[0].currency[0].symbol})</Text>
                </View>
              </View>
              <View style={{width: '38%',}}>
                <View style={{marginTop: 10}}>
                  <Text style={{fontSize: 16, fontWeight: 'bold', textAlign: 'right', color: 'black'}}>
                  {1060
                    //this.state.myInvoice.client[0].currency[0].symbol}{Number(this.state.myInvoice.due_amount) - Number(this.state.newPaid_amt)
                    }
                  </Text>
                </View>
              </View>
            </View>            
          </View>
        </View>

        <View style={{flex: 1, flexDirection: 'row', paddingBottom: 15}}>
          <View style={{width: '100%',}}>                        
            <View style={{marginTop: 10}}>
              <Text style={{fontSize: 16, color: 'green', textAlign: 'left'}}>Notes</Text>
            </View>
            <View style={{marginTop: 10}}>
              <Text style={{fontSize: 17, textAlign: 'left', color: 'black'}}>
                {this.state.myInvoice.notes}
              </Text>
            </View>
          </View>          
        </View>

        <View style={{flex: 1, flexDirection: 'row', paddingBottom: 15}}>
          <View style={{width: '80%'}}>                        
            <View style={{marginTop: 10}}>
              <Text style={{fontSize: 16, color: 'green', textAlign: 'left'}}>Terms</Text>
            </View>
            <View style={{marginTop: 10}}>
              <Text style={{fontSize: 17, textAlign: 'left', color: 'black'}}>
                {this.state.myInvoice.terms}
              </Text>
            </View>
            {/* <View style={{marginTop: 25}}>
              <Text style={{fontSize: 16, color: 'red', textAlign: 'left', fontSize: 19}}>Delete</Text>
            </View> */}
          </View>

        </View>

      </View>
    </ScrollView>
    
    <View>
      
    </View>
      {/* <View style={{width: '100%', textAlign: 'right', justifyContent: 'center', position: 'absolute', bottom: 100, right: 0 }}> */}
        {/* Rest of the app comes ABOVE the action button component !*/}
        <ActionButton buttonColor="rgba(231,76,60,1)">
         <ActionButton.Item buttonColor='#9b59b6' title="Add Payment" onPress={() => {this._gotoAddPaymentScreen()}}>
           <Icon name="md-add" style={styles.actionButtonIcon} size={24} color={'#fff'} />
         </ActionButton.Item>
         <ActionButton.Item buttonColor='#3498db' title="Send Invoice" onPress={() => {this._gotoInvoiceEmailScreen()}}>
         <Icon name="md-done-all" style={styles.actionButtonIcon} size={24} color={'#fff'} />
         </ActionButton.Item>
         <ActionButton.Item buttonColor='#1abc9c' title="Notifications" onPress={() => {this._gotoNotificationScreen()}}>
           <Icon name="md-notifications-off" style={styles.actionButtonIcon} size={24} color={'#fff'} />
         </ActionButton.Item>
         <ActionButton.Item buttonColor='#1abc9c' title="Delete Invoice" onPress={() => {}}>
           <Icon name="md-trash" style={styles.actionButtonIcon} size={24} color={'#fff'} />
         </ActionButton.Item>
        </ActionButton>
        {/* </View> */}
      </View>
    );
  }

  renderItem(item) 
  {
    console.log("renderItem called and this is item--> ",item)
    
    return(
    <View style={styles.description} key={item.name}>
      <View style={{flex:1}}>
        <Text style={styles.descriptiontext1}> {item.name} </Text>
        <Text h5 style={styles.unitprice}> {item.quantity} at {item.currency_symbol}{item.unit_cost} </Text>
      </View>
      <View style={{flex:1}}>
        <Text style={styles.descriptiontext3}> {item.tax1_name} </Text>
        <Text h5 style={styles.unitprice1}> {item.line_tax_amount} </Text>
      </View>
      <View style={{flex:1}}>
        <Text style={styles.descriptiontext2}> {item.currency_symbol}{Number(item.line_total) + Number(item.line_tax_amount)} </Text>
      </View>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingLeft: 10,
    paddingRight: 10,
  },
  logo: {
   flex: 1, 
   marginTop: 10,
   marginLeft: 'auto', 
   marginRight: 'auto', 
   textAlign: 'center',
   justifyContent: 'center'
  },
  h5: {
    color: '#666',
    fontSize: 13,
  },
  text: {
    flex: 1,
    color: '#333',
    fontSize: 15,
  },
  welcome: {
    fontSize: 15,
    textAlign: 'right',
    marginTop: 10,
    marginBottom: 5,
    color: 'green',
  },
  input: {
    height: 50,
    borderColor: '#f00',
    borderWidth: 1,
    borderBottomColor: '#fff',
    color: '#333',
  },
  description: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 0,
    flexWrap: 'wrap',
  },
  welcome1: {
    fontSize: 15,
    textAlign: 'left',
    alignSelf: 'stretch',
    marginTop: 10,
    marginBottom: 5,
    color: 'green',
  },
  welcome2: {
    fontSize: 15,
    textAlign: 'right',
    alignSelf: 'stretch',
    marginTop: 10,
    marginBottom: 5,
    color: 'green',
  },
  welcome3: {
    fontSize: 15,
    textAlign: 'center',
    alignSelf: 'stretch',
    marginTop: 10,
    marginBottom: 5,
    color: 'green',
  },
  descriptiontext1: {
    fontSize: 17,
    textAlign: 'left',
    marginTop: 25,
    marginBottom: 5,
    color: '#333',
  },
  descriptiontext2: {
    fontSize: 17,
    textAlign: 'right',
    marginTop: 25,
    marginBottom: 5,
    color: '#333',
  },
  descriptiontext3: {
    fontSize: 17,
    textAlign: 'center',
    marginTop: 25,
    marginBottom: 5,
    color: '#333',
  },
  unitprice: {
    fontSize: 15,
    textAlign: 'left',
    alignSelf: 'stretch',
    marginBottom: 5,
    color: '#666',
  },
  unitprice1: {
    fontSize: 15,
    textAlign: 'center',
    alignSelf: 'stretch',
    marginBottom: 5,
    color: '#666',
  },
  mainamountdue: {
    fontSize: 15,
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 5,
    color: 'green',
  },
  mainamount: {
    fontSize: 34,
    textAlign: 'center',
    marginBottom: 5,
    color: '#333',
  },
  addline: {
    flex: 1,
    padding: 15,
    color: '#333',
    fontSize: 15,
    textAlign: 'left',
  },
  discount:{
    fontSize: 16,
    textAlign: 'right',
    color: 'blue',
    marginTop: 5,
    marginBottom: 0
  }
});