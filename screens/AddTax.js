import React from 'react';

import { StyleSheet, AsyncStorage, Alert, View, SafeAreaView, StatusBar, Keyboard, Text, KeyboardAvoidingView, TouchableWithoutFeedback, Image, TextInput,ScrollView, TouchableOpacity } from 'react-native';
import { Button } from 'react-native-elements'

import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import { Formik } from "formik";
import * as Yup from "yup";
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";

import { CREATE_TAXES } from "../graphqlQueries";
import { GET_TAXES } from "../graphqlQueries";

import { graphql } from 'react-apollo';
import DoneButton from 'react-native-keyboard-done-button';

var FloatingLabel = require('react-native-floating-labels');

const math = require('mathjs')
const MyInput = compose(handleTextInput, withNextInputAutoFocusInput)(TextField);

const Form = withNextInputAutoFocusForm(ScrollView, {
  submitAfterLastInput: false
});

const validationSchema = Yup.object().shape({
  name: Yup.string().required(),
  email: Yup.string()
    .required()
    .email(),
  password: Yup.string()
    .required()
    .min(6, "Too short"),
  confirmPassword: Yup.string().oneOf(
      [Yup.ref('password')],
      'Passwords do not match',
    ),
  star: Yup.boolean()
    .required()
    .oneOf([true])
});

const initialState = {
  name: "",
  rate: "",
  number: "",
  compound: "",
  account_id: ""
};

class AddTax extends React.Component {
  // static navigationOptions = {
  //   title: 'Add Tax',
  //   headerRight: <TouchableOpacity onPress={this._CreateTaxes}><View style={{marginRight:20}}><Text style={{color: '#000', fontSize: 18,}}>Save</Text></View></TouchableOpacity>
  // };

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Add Tax',
      headerTitle: 'Add Tax',
      headerRight: <TouchableOpacity onPress={navigation.getParam('CreateTaxes')}><View style={{marginRight:20}}><Text style={{color: '#000', fontSize: 18,}}>Save</Text></View></TouchableOpacity>
    };
  };

  constructor(props) {
    super(props);
    this.state = { ...initialState };
    console.log("ADD TAXES")
    this._retrieveAccountData()
  }

  UNSAFE_componentWillMount(){
    this.props.navigation.setParams({ CreateTaxes: this._CreateTaxes });    
  }

  _retrieveAccountData = async () => {
    try {
      const value = await AsyncStorage.getItem('account');
      if (value !== null) {
        // We have data!!
        console.log('We have data! Add TAXES');
        let data = JSON.parse(value)
        this.setState({
            account_id:data[0].id 
        }) 
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView style={styles.container}>
          <StatusBar barStyle="light-content" />
          <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={Platform.select({ios: 0, android: 80})} style={styles.container}>
            <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>
              <View style={styles.container}>
                {/* <View style={styles.logoContainer}>
                  <Image style={styles.logo}
                    source={require('../assets/images/logo.png')}>
                  </Image>
                </View> */}
                <Formik
                  onSubmit={values => alert(JSON.stringify(values, null, 2))}
                  initialValues={{ star: true }}>
                  <Form style={styles.infoContainer} keyboardShouldPersistTaps='always'>
                    
                    {/* <MyInput 
                      style={styles.input}
                      color="#333"
                      label="Name"
                      labelFontSize="16"
                      name="name"
                      type="name"
                      id="name"
                      placeholder="Name"
                      placeholderTextColor='rgba(255,255,255,0.8)'
                      value={this.state.name}
                      onChangeText={(name) => this.setState({ name })}
                      keyboardType='default'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtlanguage.focus()}
                      ref={"txtName"}
                    />
                    <DoneButton
                      title="Next!"  
                      doneStyle={{ color: 'green' }}  
                    />
                    <MyInput 
                      style={styles.input}
                      color="#333"
                      label="Rate"
                      labelFontSize="16"
                      name="rate"
                      type="rate"
                      id="rate"
                      placeholder="Rate"
                      placeholderTextColor='rgba(255,255,255,0.8)'
                      value={this.state.rate}
                      onChangeText={(rate) => this.setState({ rate })}
                      keyboardType='default'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtlanguage.focus()}
                      ref={"txtRate"}
                    />
                    <DoneButton
                      title="Next!"   
                      doneStyle={{ color: 'green' }} 
                    />
                    <MyInput 
                      style={styles.input}
                      label="Number"
                      color="#333"
                      name="number"
                      labelFontSize="16"
                      type="number"
                      id="number"
                      placeholder="Number"
                      placeholderTextColor='rgba(255,255,255,0.8)'
                      value={this.state.number}
                      onChangeText={(number) => this.setState({ number })}
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtCompound.focus()}
                      ref={"txtNumber"}
                    />

                    <DoneButton
                      title="Next!"  
                      doneStyle={{ color: 'green' }} 
                    />
                    <MyInput 
                      style={styles.input}
                      color="#333"
                      labelFontSize="16"
                      label="Compound"
                      name="compound"
                      type="compound"
                      placeholder="Compound"
                      placeholderTextColor='rgba(255,255,255,0.8)'
                      value={this.state.compound}
                      onChangeText={(compound) => this.setState({ compound })}
                      keyboardType='numeric'
                      returnKeyType='go'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtZipCode.focus()}
                      ref={"txtCompound"}
                    />
                    <DoneButton
                      title="Next!"  
                      doneStyle={{ color: 'green' }} 
                    /> */}

                    
                    <View style={styles.paymenttype}>
                      <FloatingLabel
                        labelStyle={styles.labelInput}
                        inputStyle={styles.input}
                        style={styles.formInput}
                        value={this.state.name}
                        onChangeText={(name) => this.setState({ name })}
                        keyboardType='default'
                        returnKeyType='next'
                        autoCorrect={false}
                        onSubmitEsiting={() => this.refs.txtlanguage.focus()}
                        ref={"txtName"}
                        onBlur={this.onBlur}>

                        Tax Name
                      </FloatingLabel>
                    </View>
                    <View style={styles.payamounttitle}>
                      <FloatingLabel
                        labelStyle={styles.labelInput}
                        inputStyle={styles.input}
                        style={styles.formInput}
                        value={this.state.rate}
                        onChangeText={(rate) => this.setState({ rate })}
                        keyboardType='numeric'
                        returnKeyType='next'
                        autoCorrect={false}
                        onSubmitEsiting={() => this.refs.txtlanguage.focus()}
                        ref={"txtRate"}
                        onBlur={this.onBlur}>

                        Rate
                      </FloatingLabel>
                    </View>

                    <View style={styles.payamounttitle}>
                      <FloatingLabel
                        labelStyle={styles.labelInput}
                        inputStyle={styles.input}
                        style={styles.formInput}
                        value={this.state.number}
                        onChangeText={(number) => this.setState({ number })}
                        returnKeyType='next'
                        autoCorrect={false}
                        onSubmitEsiting={() => this.refs.txtCompound.focus()}
                        ref={"txtNumber"}
                        onBlur={this.onBlur}>

                        Number
                      </FloatingLabel>
                    </View>

                    <View style={styles.payamounttitle}>
                      <FloatingLabel
                        labelStyle={styles.labelInput}
                        inputStyle={styles.input}
                        style={styles.formInput}
                        value={this.state.compound}
                        onChangeText={(compound) => this.setState({ compound })}
                        keyboardType='numeric'
                        returnKeyType='go'
                        autoCorrect={false}
                        onSubmitEsiting={() => this.refs.txtZipCode.focus()}
                        ref={"txtCompound"}
                        onBlur={this.onBlur}>

                        Compound
                      </FloatingLabel>
                    </View>

                    {/*<Button raised
                      buttonStyle={styles.buttonContainer}
                      title='SUBMIT'
                      onPress={this._CreateTaxes} />*/}

                  </Form>
                </Formik>
              </View>
            </TouchableWithoutFeedback>
          </KeyboardAvoidingView>
        </SafeAreaView>

      </View>
    );
  }
  
  _goBack()
  {
    this.props.navigation.state.params.onNavigateBack()
    this.props.navigation.pop()
  }

  _CreateTaxes = async () => {
    
    console.log(this.state);


    if (this.state.name == '') {
      Alert.alert("Please enter Name.");
      return
    }

    if (this.state.rate == '') {
      Alert.alert("Please enter rate.");
      return
    }

    if (this.state.number == '') {
      Alert.alert("Please enter Number.");
      return
    }

    if (this.state.compound == '') {
      Alert.alert("Please enter compound.");
      return
    }

        this.props.
        create_taxes(this.state.name, Number(this.state.rate), this.state.number,Number(this.state.compound), Number(this.state.account_id))
        //create_taxes(this.state.name, Number(this.state.rate), Number(this.state.account_id))
      .then(({ data }) => {
        console.log("create tax : " + JSON.stringify(data.createTax.Tax));
        if (data) {
          Alert.alert(
            'Invoease',
            'Tax added successfully.',
            [
              {text: 'OK', onPress: () => this._goBack()},
            ],
            {cancelable: false},
          );
        }
      })
  };
}

const styles = StyleSheet.create({
  container: {
        flex: 1,
        backgroundColor: 'rgb(255, 255, 255)',
        flexDirection: 'column'
    },
    logoContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    logo: {
        width: 266,
        height: 50
    },
    title: {
        color: '#f7c744',
        fontSize: 18,
        textAlign: 'center',
        marginTop: 5,
        opacity: 0.9
    },
    infoContainer: {
        flexDirection: 'column',
        flex: 1,
    },
    buttonContainer: {
        backgroundColor: '#1FA2FF',
        marginTop: 15,
    },
    buttonText: {
        textAlign: 'center',
        color: 'rgb(32,53,70)',
        fontWeight: 'bold',
        fontSize: 18
    },

    labelInput: {
      color: '#ccc',
      fontSize: 15,
      paddingLeft: 15,
    },
    formInput: {    
      borderBottomWidth: 1, 
      paddingLeft: 5,
      borderColor: '#ccc',       
    },
    input: {
      borderWidth: 0
    }
});

export default graphql(CREATE_TAXES,
  {
    props: ({ mutate }) => ({
      // create_taxes: (name, rate, number, compound, account_id) => 
      // mutate({ variables: {name, rate, number, compound, account_id } })

      create_taxes: (name, rate, number, compound, account_id) => 
      mutate({ variables: {name, rate, number, compound ,account_id } })
    }),
    options: {
      refetchQueries: [{
          query:GET_TAXES
      }
      ],
    }
  }
)(AddTax)
