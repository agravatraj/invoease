import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  AsyncStorage,
  View,
  Dimensions
} from 'react-native';

import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator
} from 'react-native-indicators';

import { WebBrowser } from 'expo';

import { List, ListItem, Button, SearchBar } from 'react-native-elements';
import IconFont from 'react-native-vector-icons/FontAwesome';

import { Query } from "react-apollo";
import { GET_INVOICE } from "../graphqlQueries";

import SearchInput, { createFilter } from  'react-native-search-filter';
const KEYS_TO_FILTERS = ['client.company_name'];

import { NavigationEvents } from 'react-navigation';
import { graphql } from 'react-apollo';
import { DESTORY_CLIENT } from "../graphqlQueries";

class EstimateAddScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Estimates',
      headerTitle: 'Estimates',
      headerRight: <TouchableOpacity onPress={navigation.getParam('createEstimateAsync')}><View style={{marginRight:20}}><IconFont name={'plus'} size={22} color={'#373f51'} style={{paddingRight:10}}/></View></TouchableOpacity>
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      searchTerm: '',
      newArrInvoice: [],
      ascending:true,
      newChecked:[],
      isRefreshing: false,
      filteredInvoices:[],
    }
  }

  onRefresh() {
    this.setState({isRefreshing: true});
    
    // Simulate fetching data from the server
    setTimeout(() => {
      this.setState({isRefreshing: false});
    }, 5000);
  }

  handleOnNavigateBackFromSortBy = (checked) => {
    this.state.newChecked = checked
    console.log("ascending" , this.state.ascending )
    console.log(checked)
    if(checked != '')
    {
      console.log("selected name",checked[0].name)
    sortName = ''
    
    console.log("arrinv ===> ", arrInvoice)
    

    if(checked[0].name == "Client")
    {
        if (this.state.ascending)
        {
          arrInvoice.sort((a, b) => (a.client[0].company_name > b.client[0].company_name) ? 1 : -1)
          sortArrInvoice = arrInvoice.sort((a, b) => (a.client[0].company_name > b.client[0].company_name) ? 1 : -1)
        }
        else{
          arrInvoice.sort((b , a) => (a.client[0].company_name > b.client[0].company_name) ? 1 : -1)
          sortArrInvoice = arrInvoice.sort((b , a) => (a.client[0].company_name > b.client[0].company_name) ? 1 : -1)
        }
        this.setState({newArrInvoice: sortArrInvoice})
    }
    else if(checked[0].name == "Invoice Number")
    {
      if (this.state.ascending)
      {
        arrInvoice.sort((a, b) => (a.invoice_number > b.invoice_number) ? 1 : -1)
        sortArrInvoice = arrInvoice.sort((a, b) => (a.invoice_number > b.invoice_number) ? 1 : -1)
      }
      else{
        arrInvoice.sort((b , a) => (a.invoice_number > b.invoice_number) ? 1 : -1)
        sortArrInvoice = arrInvoice.sort((b , a) => (a.invoice_number > b.invoice_number) ? 1 : -1)
      }
        this.setState({newArrInvoice: sortArrInvoice})
    }
    else if(checked[0].name == "Status")
    {
      if (this.state.ascending)
      {
        arrInvoice.sort((a, b) => (a.status > b.status) ? 1 : -1)
        sortArrInvoice = arrInvoice.sort((a, b) => (a.status > b.status) ? 1 : -1)
      }
      else{
        arrInvoice.sort((b , a) => (b.status > a.status) ? 1 : -1)
        sortArrInvoice = arrInvoice.sort((b , a) => (a.status > b.status) ? 1 : -1)
      } 
        this.setState({newArrInvoice: sortArrInvoice})
        
    }
    else if(checked[0].name == "Amount")
    {
      if (this.state.ascending)
      {
        // console.log("arrinvoice www= ",arrInvoice)
        // sortArrInvoice = arrInvoice.sort(function(obj1, obj2) {
        //   // Ascending: first age less than the previous
        //   return obj1.outstanding_amount - obj2.outstanding_amount;
        // });
        arrInvoice.sort((a, b) => (a.outstanding_amount > b.outstanding_amount) ? 1 : -1)
        sortArrInvoice = arrInvoice.sort((a, b) => (a.outstanding_amount > b.outstanding_amount) ? 1 : -1)
       
      }
      else
      {
        // console.log("arrinvoice wwwe= ",arrInvoice)
        // sortArrInvoice = arrInvoice.sort(function(obj2, obj1) {
        //   // Ascending: first age less than the previous
        //   return obj1.outstanding_amount - obj2.outstanding_amount;
        // });

        arrInvoice.sort((b , a) => (a.outstanding_amount > b.outstanding_amount) ? 1 : -1)
        sortArrInvoice = arrInvoice.sort((b , a) => (a.outstanding_amount > b.outstanding_amount) ? 1 : -1)
      }
        this.setState({newArrInvoice: sortArrInvoice})
    }
    else if(checked[0].name == "Date of Issue")
    {
      if (this.state.ascending)
      {
        arrInvoice.sort((a, b) => (a.created_at > b.created_at) ? 1 : -1)
        sortArrInvoice = arrInvoice.sort((a, b) => (a.created_at > b.created_at) ? 1 : -1)
      }
      else{
        arrInvoice.sort((b , a) => (a.created_at > b.created_at) ? 1 : -1)
        sortArrInvoice = arrInvoice.sort((b , a) => (a.created_at > b.created_at) ? 1 : -1)
      }
        this.setState({newArrInvoice: sortArrInvoice})
    }
    else if(checked[0].name == "Last Updated")
    {
      if (this.state.ascending)
      {
        arrInvoice.sort((a, b) => (a.updated_at > b.updated_at) ? 1 : -1)
        sortArrInvoice = arrInvoice.sort((a, b) => (a.updated_at > b.updated_at) ? 1 : -1)
      }
      else{
        arrInvoice.sort((b , a) => (a.updated_at > b.updated_at) ? 1 : -1)
        sortArrInvoice = arrInvoice.sort((b , a) => (a.updated_at > b.updated_at) ? 1 : -1)
      }
        this.setState({newArrInvoice: sortArrInvoice})
    }
    }
  }

  _gotoSortScreen = async () => {
    this.props.navigation.navigate('SortBy',{
      onNavigateBack: this.handleOnNavigateBackFromSortBy.bind(this)})
  };

  _createEstimateAsync = async () => {
    this.props.navigation.navigate('EstimateCreateScreen',{selectClient:""})
  };

  _changeFilter = async (i) => 
  {
    console.log("change filter .ascending = ",this.state.ascending)
    this.state.ascending = !this.state.ascending
    console.log("change filter .ascending = ",this.state.ascending)
    this.handleOnNavigateBackFromSortBy(this.state.newChecked)
  }

  UNSAFE_componentWillMount() {
   this.props.navigation.setParams({ createEstimateAsync: this._createEstimateAsync });
  }

  componentWillUnmount() {  
    this.setState({
      arrInvoice: arrInvoice
    });
    console.log("ds" , this.state.arrInvoice)
  }

  searchUpdated(term) {
    this.setState({ searchTerm: term })
  }

  handleOnNavigateBackFromRecommended()
  {
    this.setState({isRefreshing: false});
  }

  render() {
    console.log("Home Screen Render method calleedddddd");
  
    return (
      <View style={styles.container}>

        <NavigationEvents
          onDidFocus={payload => this.handleOnNavigateBackFromRecommended()}
        />
        
        <SearchInput 
          onChangeText={(term) => { this.searchUpdated(term) }} 
          style={styles.searchInput}
          placeholder="Search invoices"
          clearIcon={<IconFont name={'search'} size={20} color={'#373f51'} style={{paddingRight:5}}/>}
          returnKeyType="search"
          />
            <View style={{flexDirection: 'row', backgroundColor: '#f7f7f7', position: 'relative', padding: 10}}>
              <View style={{flex: 3, flexDirection: 'row'}}>
                <Text style={{marginTop: 1, fontSize: 16}}>Sorted by</Text><Button title='Date of Issue' buttonStyle={styles.registerButton} color={'#0d83dd'} onPress={this._gotoSortScreen} />
              </View>
              <TouchableOpacity onPress={() => this._changeFilter()}>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <IconFont name={'long-arrow-up'} size={17} color={'#0d83dd'} style={{}}/>
                <IconFont name={'long-arrow-down'} size={17} color={'#cdd4d9'} style={{marginTop: 5}}/>
              </View>
              </TouchableOpacity>
            </View>
                <ScrollView style={{flex: 1}}>      
                   <View style={{backgroundColor: '#ffffff'}}>
                                <Query query={GET_INVOICE}>
                                  {({ loading, error, data }) => {
                                    if (loading) return <BarIndicator color='#4235cc' style={styles.indicator} />;
                                    console.log("error found sdf dre", error)
                                    
                                    if (error) {
                                      console.log("GET_INVOICE API called")
                                      console.log("TEST ERR =>"+ error.graphQLErrors.map(x => x.message));
                                      return <Text>Error :)</Text>;
                                    }
                                  
                                  arrInvoice = data.invoices  
                                 
                                  if (loading){
                                    console.log("its loading state")
                                  } 

                                  if (this.state.newArrInvoice && this.state.newArrInvoice.length > 0)
                                  {
                                    console.log("newArrInvoice Found")
                                    filteredInvoices = this.state.newArrInvoice.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))
                                    console.log("number of invoice count:",filteredInvoices.length)
                                    //filteredInvoices = this.state.newArrInvoice 
                                  }
                                  else {
                                    console.log("newArrInvoice Blanck")
                                    if(data && data.invoices)
                                    {
                                      filteredInvoices = data.invoices.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))
                                    }
                                 
                                    console.log("number of invoice count:",filteredInvoices.length)
                                  }
                                 
                                  return (
                                    
                                    <View>
                                      {
                                        filteredInvoices.map((item, i)  => (
                                          // <TouchableOpacity onPress={() => this._gotoInvoiceDetailScreen(i)} key={i}>  
                                          <TouchableOpacity onPress={() => console.log("Estimate detail screen....")} key={i}>  
                                          {
                                            this.renderItem(item)}
                                          </TouchableOpacity>
                                        ))                                    
                                      }
                                    </View>)
                                }}
                              </Query>
                      </View>
                </ScrollView>
      </View>
    );
  }

  render1() {
    return (

      <View style={styles.container}>
        <SearchInput 
          onChangeText={(term) => { this.searchUpdated(term) }} 
          style={styles.searchInput}
          placeholder="Search invoices"
          clearIcon={<IconFont name={'search'} size={20} color={'#373f51'} style={{paddingRight:5}}/>}
          returnKeyType="search"
          />

            <ScrollView>

            <View style={{flex: 1, flexDirection: 'row', backgroundColor: '#f7f7f7', position: 'absolute', top: 60, padding: 10}}>
              <View style={{flex: 3.7, flexDirection: 'row'}}>
                <Text style={{marginTop: 1, fontSize: 16}}>Sorted by</Text><Button title='Date of Issue' buttonStyle={styles.registerButton} color={'#0d83dd'} onPress={this._gotoSortScreen} />
              </View>
              <TouchableOpacity onPress={() => this._changeFilter()}>
              <View style={{flex: 0.3, flexDirection: 'row'}}>
                <IconFont name={'long-arrow-up'} size={17} color={'#0d83dd'} style={{}}/>
                <IconFont name={'long-arrow-down'} size={17} color={'#cdd4d9'} style={{marginTop: 5}}/>
              </View>
              </TouchableOpacity>
            </View>

          <ScrollView style={{marginTop:52}}>      
            
            <View style={{flex: 1}}>
                  <Query query={GET_INVOICE}>
                    {({ loading, error, data }) => {
                      if (loading) return <BarIndicator color='#4235cc' style={styles.indicator} />;
                      console.log("error found sdf dre", error)
                      if (error) {
                        console.log("GET_INVOICE API called")
                        console.log("TEST ERR =>"+ error.graphQLErrors.map(x => x.message));
                        return <Text>Error :)</Text>;
                      }
                      if(data){
                      arrInvoice = data.invoices  
                      
                      if (this.state.newArrInvoice && this.state.newArrInvoice.length > 0)
                      {
                        console.log("newArrInvoice Found")
                        filteredInvoices = this.state.newArrInvoice.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))
                        //filteredInvoices = this.state.newArrInvoice 
                      }
                      else{
                        console.log("newArrInvoice Blanck")
                        filteredInvoices = data.invoices.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))
                      }
                    }
                      return (

                        <View>
                          {
                            filteredInvoices.map((item, i)  => (
                              <TouchableOpacity onPress={() => this._gotoInvoiceDetailScreen(i)} key={i}>  
                              {
                                this.renderItem(item)}
                              </TouchableOpacity>
                             // ))
                            ))
                          }
                        </View>)
                    }}
                  </Query>            
        </View>
    </ScrollView>
  </ScrollView>
</View>   
    );
  }
  
  invoiceBorderStyle = function(color) {
    return {
      width: '4%',
      borderLeftWidth: 8,
      borderColor:color
    }
  }
  invoiceStatusStyle = function(color){
    return{
      fontSize: 18,
    textAlign: 'left',
    fontWeight: 'bold',
    color: color,
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 5
    }
  }

  renderItem(item) {
    // console.log("df", item)
    
    var borderColor =  '#ccc';
    switch(item.status)
    {
        case '0':
        borderColor =  '#ccc';
        break;

        case '1':
        borderColor =  'green';
        break;
        case '2':
        borderColor =  '#42F3';
        break;
        case '3':
        
        borderColor =  '#f6df92';
        break;
        case '4':
        borderColor = '#b5dc79';
        default:
        borderColor =  '#ccc';
       

    }
    
    return(
    <View style={styles.pendingBox}>          
    
    <View style={this.invoiceBorderStyle(borderColor)}>
    </View>
      
      <View style={{width: '48%', justifyContent: 'center'}}>
        <View style={styles.vmiddle}>
          
          <Text style={styles.pendingTitle}>{item.client[0].company_name}</Text>
          <Text style={styles.pendingMemo}>#{item.invoice_number}</Text>
          <Text style={this.invoiceStatusStyle(borderColor)}>{item.invoice_status}</Text>
        </View>
      </View>
      <View style={{width: '48%', justifyContent: 'center'}}>
        <View style={styles.vmiddle}>									
          <Text style={styles.pendingDate}>{item.inv_date}</Text>
          {/* <Text style={styles.pendingAmount}>{item.client[0].currency[0].symbol}{item.outstanding_amount}.00</Text> */}
          <Text style={styles.pendingAmount}>{item.client[0].currency[0].symbol}{Number(item.outstanding_amount).toFixed(2)}</Text>  
        </View>
    </View>
</View>
    );
  }

  _maybeRenderDevelopmentModeWarning() {
    if (__DEV__) {
      const learnMoreButton = (
        <Text onPress={this._handleLearnMorePress} style={styles.helpLinkText}>
          Learn more
        </Text>
      );

      return (
        <Text style={styles.developmentModeText}>
          Development mode is enabled, your app will be slower but you can use useful development
          tools. {learnMoreButton}
        </Text>
      );
    } else {
      return (
        <Text style={styles.developmentModeText}>
          You are not in development mode, your app will run at full speed.
        </Text>
      );
    }
  }

  _handleLearnMorePress = () => {
    WebBrowser.openBrowserAsync('https://docs.expo.io/versions/latest/guides/development-mode');
  };

  _handleHelpPress = () => {
    WebBrowser.openBrowserAsync(
      'https://docs.expo.io/versions/latest/guides/up-and-running.html#can-t-see-your-changes'
    );
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  indicator:{
    flex: 1,
    alignItems:'flex-start',
    justifyContent:'center',
    marginTop: 50,
},
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
  searchInput: {
    paddingTop:1,
    marginTop: 10,
    paddingLeft:10,
    marginLeft: 10,
    marginRight: 10,
    borderColor: '#CCC',
    height:40,
    borderWidth: 1,
    backgroundColor: 'white',
    borderRadius: 5,
  },
  // searchInput:{
  //   marginTop:10,
  //   paddingTop:10,
  //   paddingBottom:5,
  //   marginLeft:10,
  //   marginRight:10,
  //   borderColor: '#CCC',
  //   borderWidth: 1,
  //   backgroundColor: 'lightgray',
  //   borderRadius:5,
  // },
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  pendingBox:{
    flex: 1,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#ccc'
  },
  pendingleftBorder: {
    width: '4%',
    borderLeftWidth: 8,
    borderColor: '#f6df92'
  },
  pendingTitle: {   
    fontSize: 18,
    textAlign: 'left',
    color: '#333',
    marginTop: 10,
    marginBottom: 5,
    marginLeft: 5
  },
  pendingMemo: {   
    fontSize: 15,
    textAlign: 'left',
    color: '#ccc',
    marginTop: 10,
    marginBottom: 5,
    marginLeft: 5
  },
  pendingStatus: {   
    fontSize: 18,
    textAlign: 'left',
    fontWeight: 'bold',
    color: '#f6df92',
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 5
  },
  pendingDate: {
    fontSize: 14,
    textAlign: 'right',
    color: '#999',
    marginTop: 10,
    marginRight: 10
  },
  pendingAmount: {
    fontSize: 18,
    textAlign: 'right',
    color: '#333',
    marginBottom: 10,
    marginRight: 10
  },
  vmiddle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  createleftBorder: {
    width: '4%',
    borderLeftWidth: 8
  },
  createStatus:{
    fontSize: 18,
    textAlign: 'left',
    fontWeight: 'bold',
    color: '#c4cdd3',
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 5
  },
  successleftBorder:{
    width: '4%',
    borderLeftWidth: 8,
    borderColor: '#b5dc79'
  },
  successStatus:{
    fontSize: 18,
    textAlign: 'left',
    fontWeight: 'bold',
    color: '#b5dc79',
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 5
  },
  registerButton: {
    padding: 0,
    marginTop: 2,
    marginLeft: -12,
    backgroundColor: '#f7f7f7'
  }
});
export default EstimateAddScreen;



