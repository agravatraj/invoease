import React from 'react';
import { StyleSheet, View, Image, Text, SafeAreaView, Keyboard, KeyboardAvoidingView,ImageBackground, StatusBar, TouchableWithoutFeedback, TextInput, ScrollView,TouchableOpacity , Alert} from 'react-native';
import { Button } from 'react-native-elements'

import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import { Formik } from "formik";
import * as Yup from "yup";
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";

import { FORGOT_PASSWORD } from "../graphqlQueries";
import { graphql } from 'react-apollo';

import Icon from 'react-native-vector-icons/FontAwesome';

const MyInput = compose(handleTextInput, withNextInputAutoFocusInput)(TextField);

const Form = withNextInputAutoFocusForm(ScrollView, {
  submitAfterLastInput: false
});

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .required()
    .email(),
  password: Yup.string()
    .required()
    .min(6, "Too short"),
  star: Yup.boolean()
    .required()
    .oneOf([true])
});

const initialState = {
  email: "prashant@complitech.net",
};
class ForgetPasswordScreen extends React.Component {

  static navigationOptions = {
    title: 'Forgot Password',
    headerTitle: 'Forgot Password',
    header: null,
    //   header: ({ state }) => ({
    //     left: <Button title={"Save"} onPress={state.params.handleBack} />
    // })
  };

  _back = async () => {
    this.props.navigation.goBack();
  };

  constructor(props) {
    super(props);
    this.state = { ...initialState };
  }

  componentDidMount() {

    const params = {
      right: (
        <Button
          onPress={() => this._back}
        >
        </Button>
      ),
    };
    this.props.navigation.setParams(params);
  }
  render() {
    return (
     // <ImageBackground style={styles.imgBackground} source={require('../assets/images/login_bg.png')} resizeMode='cover'>
      <View style={styles.container}>
        <SafeAreaView style={styles.container}>
          <StatusBar barStyle="light-content" />
          
          <KeyboardAvoidingView behavior='padding' style={styles.container}>
            <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>
              <View style={styles.container}>
                <View style={styles.logoContainer}>
                  <Image style={styles.logo}
                    source={require('../assets/images/logo.png')}>
                  </Image>
                </View>

                <View>
                  <Text h2 style={styles.loginText}>Forgot Password?</Text>
                </View>

                <View style={styles.infoContainer}>
                  <View style={styles.searchSection}>
                    <Icon raised style={styles.searchIcon} name='envelope' type='font-awesome' color='#000' size={20} />
                    <TextInput
                      style={styles.input}
                      placeholder="Email"
                      value={this.state.email}
                      onChangeText={(email) => this.setState({ email })}
                      underlineColorAndroid="transparent"
                    />
                  </View>
                  <View style={styles.actionButon}>
                  <TouchableOpacity onPress={this._forgotPasswordAsync} >
                    <Icon raised style={styles.actionRight} name='repeat' type='font-awesome' color='#fff' size={22} />
                  </TouchableOpacity>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: 'row', marginTop: 50 }}>
                  <Button title="Login" buttonStyle={styles.registerButton} color={'#2089dc'} onPress={() => this.props.navigation.pop()} />
                </View>

              </View>
            </TouchableWithoutFeedback>
          </KeyboardAvoidingView>
          
        </SafeAreaView>
      </View>
     // </ImageBackground>
    );
  }

  _forgotPasswordAsync = async () => {
    console.log("forgot password api called")
    if (this.state.email == '') {
      Alert.alert("Please enter email.");
      return
    }


    this.props.
      forgot_password(this.state.email)
      .then(({ data}) => {
        
        if(data.forgotPassword.errors)
        {
          Alert.alert(
            'Invoease',
            data.forgotPassword.errors,
            [
              
            ],
            {cancelable: false},
          );
        }
    
        else if ({ data }) {
            console.log("Data found")
    
            Alert.alert(
              'Invoease',
              data.forgotPassword.message,
              [
                {text: 'OK', onPress: () => this.props.navigation.pop()},
              ],
              {cancelable: false},
            );
        }
      }).catch((error) => {
        console.log('my error :' + error);
        Alert.alert(
          'Invoease',
          error.message,
          [
            
          ],
          {cancelable: false},
        );
      })
      };


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column'
  },
  imgBackground: {
    flex: 1
  },
  logoContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    width: 266,
    height: 50
  },
  title: {
    color: '#f7c744',
    fontSize: 18,
    textAlign: 'center',
    marginTop: 5,
    opacity: 0.9
  },
  infoContainer: {
    elevation: 4,
    borderRadius: 50,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    backgroundColor: '#fff',
    width: '90%',
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowRadius: 7
  },
  // input: {
  //   height: 40,
  //   backgroundColor: 'rgba(0,0,0,0.2)',
  //   color: '#fff',
  //   marginBottom: 20,
  //   paddingHorizontal: 10
  // },
  containerButton: {
    flexDirection: 'row'
  },
  buttonContainer: {
    backgroundColor: '#373f51',
    marginTop: 15,
  },
  buttonForgotContainer: {
    marginTop: 15,
  },
  buttonSignupContainer: {
    marginTop: 30,
    backgroundColor: '#423231',
  },
  buttonText: {
    textAlign: 'center',
    color: 'rgb(32,53,70)',
    fontWeight: 'bold',
    fontSize: 18
  },
  searchSection: {
    flexDirection: 'row',
    borderTopColor: 'transparent',
    borderRightColor: 0,
    borderLeftColor: 0,
    borderBottomColor: 0,
    borderColor: '#ccc',
    borderWidth: 1
  },
  searchIcon: {
    padding: 15,
    position: 'relative'
  },
  input: {
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    color: '#424242',
  },
  imgBackground: {
    flex: 1
  },
  actionButon: {
    position: 'absolute',
    top: 1,
    right: -20,
    borderRadius: 100,
    backgroundColor: '#2089dc',
    width: 50,
    height: 50,
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 15,
    textAlign: 'center'
  },
  actionRight: {
    width: '100%'
  },
  forgotTxt: {
    width: '100%',
    color: '#666',
    textAlign: 'right',
    paddingTop: 20,
    paddingRight: 20,
    fontSize: 15
  },
  loginButton: {
    fontSize: 20,
    paddingHorizontal: 25,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowRadius: 7,
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
    marginRight: -15,
    backgroundColor: '#fff',
  },
  registerButton: {
    elevation: 3,
    // fontSize: 20,
    paddingHorizontal: 25,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowRadius: 7,
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    marginLeft: -15,
    backgroundColor: '#fff',
  },
  loginText: {
    fontSize: 32,
    color: '#333',
    fontWeight: 'bold',
    width: '100%',
    textAlign: 'center',
    marginBottom: 20
  }
});

export default graphql(FORGOT_PASSWORD,
  {
    props: ({ mutate }) => ({
      forgot_password: (email) => mutate({ variables: { email } })
    }),

  }
)(ForgetPasswordScreen)