import React from 'react';
import { Platform, ScrollView, StyleSheet, Text, View, TouchableOpacity, Image, Alert } from 'react-native';
import { SearchBar, List, ListItem } from 'react-native-elements'
import InvoiceDetail from './InvoiceDetail';
import { Query } from "react-apollo";
import { GET_SERVICES } from "../graphqlQueries";
import { DESTORY_SERVICE } from "../graphqlQueries";
import { graphql } from 'react-apollo';

import IconFont from 'react-native-vector-icons/FontAwesome';

import Swipeout from 'react-native-swipeout';

import SearchInput, { createFilter } from 'react-native-search-filter';
const KEYS_TO_FILTERS = ['name', 'hours', 'price'];

var temp=0;

class ServicesScreen extends React.Component {

  //arrayholder = [{"title":"Taylor Swift","artist":"Taylor Swift","url":"https://www.amazon.com/Taylor-Swift/dp/B0014I4KH6","image":"https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Fearless","artist":"Taylor Swift","url":"https://www.amazon.com/Fearless-Enhanced-Taylor-Swift/dp/B001EYGOEM","image":"https://images-na.ssl-images-amazon.com/images/I/51qmhXWZBxL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Speak Now","artist":"Taylor Swift","url":"https://www.amazon.com/Speak-Now-Taylor-Swift/dp/B003WTE886","image":"https://images-na.ssl-images-amazon.com/images/I/51vlGuX7%2BFL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Red","artist":"Taylor Swift","url":"https://www.amazon.com/Red-Taylor-Swift/dp/B008XNZMOU","image":"https://images-na.ssl-images-amazon.com/images/I/41j7-7yboXL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"1989","artist":"Taylor Swift","url":"https://www.amazon.com/1989-Taylor-Swift/dp/B00MRHANNI","image":"https://images-na.ssl-images-amazon.com/images/I/717DWgRftmL._SX522_.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"}];
  //state = { invoices: [{"title":"Taylor Swift","artist":"Taylor Swift","url":"https://www.amazon.com/Taylor-Swift/dp/B0014I4KH6","image":"https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Fearless","artist":"Taylor Swift","url":"https://www.amazon.com/Fearless-Enhanced-Taylor-Swift/dp/B001EYGOEM","image":"https://images-na.ssl-images-amazon.com/images/I/51qmhXWZBxL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Speak Now","artist":"Taylor Swift","url":"https://www.amazon.com/Speak-Now-Taylor-Swift/dp/B003WTE886","image":"https://images-na.ssl-images-amazon.com/images/I/51vlGuX7%2BFL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Red","artist":"Taylor Swift","url":"https://www.amazon.com/Red-Taylor-Swift/dp/B008XNZMOU","image":"https://images-na.ssl-images-amazon.com/images/I/41j7-7yboXL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"1989","artist":"Taylor Swift","url":"https://www.amazon.com/1989-Taylor-Swift/dp/B00MRHANNI","image":"https://images-na.ssl-images-amazon.com/images/I/717DWgRftmL._SX522_.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"}]};

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Services',
      headerTitle: 'Services',
      headerRight: <TouchableOpacity onPress={navigation.getParam('gotoAddService')}><View style={{ marginRight: 20 }}><IconFont name={'plus'} size={22} color={'#373f51'} style={{ paddingRight: 10 }} /></View></TouchableOpacity>
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      searchTerm: '',
      rowItem: null,
      rowID: '',
      filteredServices: [],
      flag: false
    }
  }

  handleOnNavigateBack = (item) => {
    console.log("handleOnNavigateBack" , filteredServices.length);
    // if (filteredServices.length == 0){
      // this.setState({isNoLabelHidden : true})
       this.setState({flag : false})
    // }
  }

  _gotoAddService = async () => {
    // this.props.navigation.navigate(‘AddService’)
    this.props.navigation.navigate('AddService', {
      onNavigateBack: this.handleOnNavigateBack.bind(this)
    });
  }

  searchUpdated(term) {
    this.setState({ searchTerm: term })
  }

  _gotoEditServiceScreen = async (i) => {

    this.props.navigation.navigate('EditService', { selectService: filteredServices[i] })
  };

  deleteRow(item) {
    //console.log(filteredServices)
    //console.log(JSON.stringify(item))
    console.log('Select ROW :' + JSON.stringify(this.state.rowItem))

    Alert.alert(
      'Are you sure?',
      'You want to delete this service',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK', onPress: () => this.deleteService()
        },
      ],
      { cancelable: false },
    );
  }

  deleteService() {

    console.log(JSON.stringify(this.state.rowItem.id))
    this.setState({flag:false})           

    this.props.
      destroy_service(this.state.rowItem.id)
      .then(({ data }) => {
        console.log("Delete service : " + JSON.stringify(data));
        if (data) {

          Alert.alert(
            'Invoease',
            'Service destory successfully.',
            [
              { text: 'OK' },
            ],
            { cancelable: false },
          );

        }
      })

  };

  checkEmpty()
  {
    this.setState({flag : true})
  }
  renderServices() {
console.log("render services method called")
    return (

      <View>
        <SearchInput
          onChangeText={(term) => { this.searchUpdated(term) }}
          style={styles.searchInput}
          placeholder="Search Services"
          clearIcon={<IconFont name={'search'} size={20} color={'#373f51'} style={{ paddingRight: 5 }} />}
          returnKeyType="search"
        />
        <List containerStyle={{ marginBottom: 20 }}>
          <Query query={GET_SERVICES}>
            {({ loading, error, data }) => {
              if (loading) return <Text>Loading...</Text>;
              console.log("GET_SERVICES METHOD CALLEEDDDD")
              if (error) {
                console.log("TEST ERR =>"+ error.graphQLErrors.map(x => x.message));
                return <Text>Error :)</Text>;
              }

              if (data.services.length == 0) 
             {
               this.checkEmpty()
             }

              // this.setState({flag:false})

              // (data.services.length==0)? this.setState({flag:false}):this.setState({flag:true})
             

              console.log("array has value")

              console.log("get services api cccccc",
                data.services.map(({ name, price, hours }) => [
                  name, price, hours
                ])
              );


              //  console.log("this is data"+data)
              // console.log(
              //   data.clients.map(({ company_name, contact } ) => [
              //     company_name, contact =>{return '${contact.first_name} ${contact.last_name}'}
              //   ])
              // );


              filteredServices = data.services.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))

              let swipeBtns = [{
                text: 'Delete',
                backgroundColor: 'red',
                // underlayColor: 'rgba(0, 0, 0, 1, 0.6)',
                onPress: () => { this.deleteRow(filteredServices[this.state.rowID]) }
              }];


              return (
                <View>
                  {
                    filteredServices.map((item, i) => (
                      <Swipeout right={swipeBtns}
                        key={i}
                        autoClose={true}
                        backgroundColor='transparent'
                        rowID={i}
                        onOpen={(i) => {
                          console.log('VALUE OF ITEM :' + i)
                          this.setState({ rowItem: item })
                        }}>

                        <TouchableOpacity onPress={() => this._gotoEditServiceScreen(i)}>

                          <ListItem
                            key={item.id}
                            title={item.name}
                            // subtitle={item.price}
                            subtitle={item.unit_cost + ' per ' + item.hours + ' hours'}

                          />

                        </TouchableOpacity>
                      </Swipeout>
                    ))
                  }
                </View>)
            }}
          </Query>
        </List>
      </View>
    )
  }

  handleSearch = (text) => {
    console.log("text", text)

    const newData = this.arrayholder.filter(invoice => {
      const itemData = '${invoices.artist.toUpperCase()} ${invoices.title.toUpperCase()}';
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });

    this.setState({ invoices: newData });
  };


  UNSAFE_componentWillMount() {
    console.log(`ServicesScreen screen ` + JSON.stringify(this.props.navigation));
    console.log(this.props.navigation)
    this.props.navigation.setParams({ gotoAddService: this._gotoAddService });
  }

  UNSAFE_componentDidMount() {
    
    console.log("component Did mount caleee")
    
  }

  render() {
    console.log("serveces screen render method called")
    return (
      <View>
        {
          (this.state.flag==true
            ?
            <View style={{ height: 200, backgroundColor: '#EAE9EF', marginTop: 25 }}>
            <Image source={require('../assets/images/service.png')}
              style={{ width: 290, height: 61, position: 'absolute', right: 28, top: 5 }}></Image>
          </View>
            :
            <ScrollView>
              {/* <SearchBar lightTheme placeholder='Type Here...' onChangeText={this.handleSearch}/>
             {this.renderServices()} */}
              {/* {(flag==false)?this.renderServices():<Text>hello</Text>} */}
              {this.renderServices()}
              {/* <Text>hello</Text> */}
            </ScrollView>
           
          )}
      </View>

    );
  }


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },

  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  searchInput: {
    paddingTop: 1,
    marginTop: 10,
    paddingLeft: 10,
    marginLeft: 10,
    marginRight: 10,
    borderColor: '#CCC',
    height: 40,
    borderWidth: 1,
    backgroundColor: 'white',
    borderRadius: 5,
  },
});

export default graphql(DESTORY_SERVICE,
  {
    props: ({ mutate }) => ({
      destroy_service: (id) =>
        mutate({ variables: { id } })
    }),
    options: {
      refetchQueries: [{
        query: GET_SERVICES
      }
      ],
    }

  }
)(ServicesScreen)