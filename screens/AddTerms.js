import React from 'react';

import { StyleSheet, AsyncStorage, View, SafeAreaView, StatusBar, Keyboard, KeyboardAvoidingView, TouchableWithoutFeedback, Image, TextInput,ScrollView, TouchableOpacity, Text , Alert } from 'react-native';
import { Button } from 'react-native-elements'

import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import { Formik } from "formik";
import * as Yup from "yup";
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";

import { CREATE_TERMS } from "../graphqlQueries";
import { GET_TERMS } from "../graphqlQueries";

import { graphql } from 'react-apollo';

var FloatingLabel = require('react-native-floating-labels');

const MyInput = compose(handleTextInput, withNextInputAutoFocusInput)(TextField);

const Form = withNextInputAutoFocusForm(ScrollView, {
  submitAfterLastInput: false
});

const validationSchema = Yup.object().shape({
  name: Yup.string().required(),
  email: Yup.string()
    .required()
    .email(),
  password: Yup.string() 
    .required()
    .min(6, "Too short"),
  confirmPassword: Yup.string().oneOf(
      [Yup.ref('password')],
      'Passwords do not match',
    ),
  star: Yup.boolean()
    .required()
    .oneOf([true])
});

const initialState = {
  name: "",
  account_id: ""
};

class AddTerms extends React.Component {
  // static navigationOptions = {
  //   title: 'Add Terms',
  //   headerRight: <TouchableOpacity onPress={this._CreateTerms}><View style={{marginRight:20}}><Text style={{color: '#000', fontSize: 18,}}>Save</Text></View></TouchableOpacity>
  // };

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Add Terms',
      headerTitle: 'Add Terms',
      headerRight: <TouchableOpacity onPress={navigation.getParam('CreateTerms')}><View style={{marginRight:20}}><Text style={{color: '#000', fontSize: 18,}}>Save</Text></View></TouchableOpacity>
    };
  };

  constructor(props) {
    super(props);
    this.state = { ...initialState };
    console.log("ADD TERMS")
    this._retrieveAccountData()
  }

  UNSAFE_componentWillMount(){
    this.props.navigation.setParams({ CreateTerms: this._CreateTerms });    
  }

  _retrieveAccountData = async () => {
    try {
      const value = await AsyncStorage.getItem('account');
      if (value !== null) {
        // We have data!!
        console.log('We have data! Add TERMS');
        let data = JSON.parse(value)
        this.setState({
            account_id:data[0].id 
        }) 
      }
    } catch (error) {
      // Error retrieving data
    }
  };


  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView style={styles.container}>
          <StatusBar barStyle="light-content" />
          <KeyboardAvoidingView behavior='padding' style={styles.container}>
            <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>
              <View style={styles.container}>
                <Formik
                  onSubmit={values => alert(JSON.stringify(values, null, 2))}
                  validationSchema={validationSchema}
                  initialValues={{ star: true }}>
                  <Form style={styles.infoContainer} keyboardShouldPersistTaps='always'>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.name}
                      onChangeText={(name) => this.setState({ name })}
                      keyboardType='default'
                      returnKeyType='go'
                      autoCorrect={false}
                      ref={"txtName"}
                      onBlur={this.onBlur}>

                      Add Term
                    </FloatingLabel>
                    {/*<Button raised
                      buttonStyle={styles.buttonContainer}
                     // icon={{ name: 'fingerprint' }}
                      title='Save'
                      onPress={this._CreateTerms} />*/}
                  </Form>
                </Formik>
              </View>
            </TouchableWithoutFeedback>
          </KeyboardAvoidingView>
        </SafeAreaView>

      </View>
    );
  }
  goBack()
  {
    this.props.navigation.state.params.onNavigateBack()
    this.props.navigation.pop()
  }
  _CreateTerms = async () => {
    
    console.log(this.state.name);

    if (this.state.name == '') {
      Alert.alert("Please enter Term.");
      return
    }

        this.props.
        create_terms(this.state.name, Number(this.state.account_id))
        .then(({ data }) => {
        console.log("create terms : " + JSON.stringify(data));
        if (data) {
          Alert.alert(
            'Invoease',
            'Terms added successfully.',
            [
              {text: 'OK', onPress: () => this.goBack("okkk")},
            ],
            {cancelable: false},
          );
        }
      })
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(255, 255, 255)',
    flexDirection: 'column'
  },
  logoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    //backgroundColor: 'red',
  },
  logo: {
    width: 266,
    height: 50
  },
  title: {
    color: '#f7c744',
    fontSize: 18,
    textAlign: 'center',
    marginTop: 5,
    opacity: 0.9
  },
  infoContainer: {
    flexDirection: 'column',
    flex: 1
  },
  input: {
    height: 40,
    backgroundColor: 'rgba(0,0,0,0.2)',
    color: '#fff',
    marginBottom: 20,
    paddingHorizontal: 10
  },
  buttonContainer: {
    backgroundColor: '#1FA2FF',
    marginTop: 15,
  },
  buttonText: {
    textAlign: 'center',
    color: 'rgb(32,53,70)',
    fontWeight: 'bold',
    fontSize: 18
  },
  labelInput: {
    color: '#ccc',
    fontSize: 15,
    paddingLeft: 15,
  },
  formInput: {    
    borderBottomWidth: 1, 
    paddingLeft: 5,
    borderColor: '#ccc',       
  },
  input: {
    borderWidth: 0
  }
});

export default graphql(CREATE_TERMS,
  {
    props: ({ mutate }) => ({
      create_terms: (name, account_id) => 
      mutate({ variables: {name, account_id } })
    }),
    options: {
      refetchQueries: [{
          query:GET_TERMS
      }
      ],
    }
  }
)(AddTerms)