import React from 'react';

import {
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  AsyncStorage,
  View,
  TouchableWithoutFeedback,
  StatusBar,
  TextInput, 
  SafeAreaView,
  Keyboard,
  KeyboardAvoidingView,
  Alert,
  ScrollView
} from 'react-native';

import { LinearGradient } from 'expo-linear-gradient';
import { Query } from "react-apollo";

import { WebBrowser } from 'expo';
import DeviceInfo from 'react-native-device-info'; //Todo
//https://github.com/rebeccahughes/react-native-device-info
import { SIGN_IN, GET_COUNTRIES, GET_TIMEZONES } from "../graphqlQueries";
//import { Mutation } from "react-apollo";
import { Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';
//import * as Icon from '@expo/vector-icons'
//import { createUploadLink } from 'apollo-upload-client'
//import LoginMutation from '../components/LoginMutation';
import { graphql } from 'react-apollo';


import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import { Formik } from "formik";
import * as Yup from "yup";
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";

import { GET_LANGUAGES } from "../graphqlQueries";

import { GET_CURRENCIES } from "../graphqlQueries";

import ApolloClient from "apollo-boost";
import { apiConfig } from "../config/apiConfig";
import { InMemoryCache } from 'apollo-cache-inmemory';

const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

const MyInput = compose(handleTextInput, withNextInputAutoFocusInput)(TextField);

const Form = withNextInputAutoFocusForm(ScrollView, {
  submitAfterLastInput: false
});

const client = new ApolloClient( apiConfig.DEV_API_CONFIG);

const initialState = {
  
  email: "prashant@complitech.net",
  password: "asdfasdf" 

  // email: "ashwin@complitech.net",
  // password: "testing"

  // email: "pratik@complitech.net", 
  // password: "12345678"
  
  // email: "nikhil@complitech.net",
  // password: "12345678"

  //  email: "admin@invoease.com",
  //  password: "12345678"

  // email: "rajagravat@complitech.net",
  // password: "12345678"

};

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .required()
    .email(),
  password: Yup.string()
    .required()
    .min(6, "Too short"),
  star: Yup.boolean()
    .required()
    .oneOf([true])
});


class LogInScreen extends React.Component {
  static navigationOptions = {
    title: '',
    header: null
  };

  constructor(props) {
    super(props);
    this.state = { ...initialState };
  }

  render() {
    console.log("loginscreen c")
    let { email, password, error } = this.state;
    return (
      // <Mutation mutation={SIGN_IN} variables={{ email, password }}>
      //  {(login, { error, data, loading }) => {
      //       // if (loading) return "Loading...";
      //       if (error) console.log(error.message); 

      //       return (

      <ImageBackground style={styles.imgBackground} source={require('../assets/images/login_bg.png')} resizeMode='cover'>
        <View style={styles.container}> 
          <View style={styles.logoContainer}>
            <Image style={styles.logo} source={require('../assets/images/logo.png')}></Image>
          </View>

          {/* <View style={{flex: 1, flexDirection: 'row', alignSelf: 'flex-end', marginTop: 50}}>
          <Button title="Login" buttonStyle={styles.loginButton} color={'#2089dc'} />
        </View> */}

          <View>
            <Text h2 style={styles.loginText}>Login</Text>
          </View>

          <View style={styles.infoContainer}>
            <View style={styles.searchSection}>
              <Icon raised style={styles.searchIcon} name='user' type='font-awesome' color='#000' size={20} />
              <TextInput
                style={styles.input}
                placeholder="Username"
                underlineColorAndroid="transparent"

                value={this.state.email}
                onChangeText={(value) => this.onEmailChange(value)}
                keyboardType='email-address'
                returnKeyType='next'
                autoCorrect={false}
                onSubmitEsiting={() => this.refs.txtPassword.focus()}
              />
            </View>
            <View style={{ height: 1, backgroundColor: '#eee' }}></View>
            <View style={styles.searchSection}>
              <Icon raised style={styles.searchIcon} name='lock' type='font-awesome' color='#000' size={20} />
              <TextInput
                style={styles.input}
                placeholder="Password"
                underlineColorAndroid="transparent"

                returnKeyType='go'
                value={this.state.password}
                onChangeText={(value) => this.onPasswordChange(value)}
                secureTextEntry={true}
                autoCorrect={false}
                ref={"txtPassword"}
              />
            </View>
            {/* <View style={{height: 1, backgroundColor: '#eee'}}></View>
          <View style={styles.searchSection}>
            <Icon raised style={styles.searchIcon} name='envelope' type='font-awesome' color='#000' size={20} />
            <TextInput
                style={styles.input}
                placeholder="Email"
                onChangeText={(searchString) => {this.setState({searchString})}}
                underlineColorAndroid="transparent"
            />
          </View> */}
            {/* <View style={styles.actionButon}>
            <Icon raised style={styles.actionRight} name='chevron-right' type='font-awesome' color='#fff' size={17} />
          </View> */}
            <LinearGradient colors={['#0dc7ca', '#1ddab7', '#1eea9d']} style={styles.actionButon}>
              <View>
              <TouchableOpacity onPress={this._signInAsync}>
                  <Icon raised style={styles.actionRight} name='check' type='font-awesome' color='#fff' size={22} />
                </TouchableOpacity>
              </View>
            </LinearGradient>
          </View>

          <View>
            <TouchableOpacity onPress={this._gotoForgetPassword}>
              <Text h4 style={styles.forgotTxt}>Forgot ?</Text>
            </TouchableOpacity>
          </View>

          <View style={{ flex: 1, flexDirection: 'row', marginTop: 50 }}>
            <Button title="Register" buttonStyle={styles.registerButton} color={'#2089dc'} onPress={this._gotoSignupScreen} />
          </View>

          {/* <View style={styles.content} >
          <View style={styles.background} >
            <Image style={styles.image} source={require('../assets/images/3.jpeg')} />
          </View>
        </View> */}

        </View>
      </ImageBackground>
    );
  }

  onEmailChange = txt => {

    //   const email = e.target.value;
    console.log(txt);
    this.setState({email:txt});
  };
  onPasswordChange = txt => {
    //const password = e.target.value;
    this.setState({password:txt});
  };

  _gotoSignupScreen = async () => {
    this.props.navigation.navigate('SignupScreen')
  };

  _myTestFunc = async(abc)=> {

     await AsyncStorage.setItem('token', abc);
     console.log("my token = "+await AsyncStorage.getItem('token'))
     this.props.navigation.navigate('App');
   
     // await AsyncStorage.setItem('token', 'raj');
    // console.log("token of raj"+await AsyncStorage.getItem('token')

            // .then(() => {
            //   console.log('STORED TOKEN : ' +await AsyncStorage.getItem('token'));
            //   console.log("Token set to AsyncStorage")

             
            // })
  }

  _signInAsync = async () => 
  {
    console.log("emailllllll",this.state.email)
    if (reg.test(this.state.email) === false) {
      Alert.alert("Please enter valid Email.");
      return
    }

    if (this.state.password.length < 6) {
      Alert.alert("Please enter minimum six character in Password.");
      return
    }

    //  await AsyncStorage.setItem('userToken', 'bhai bhai');
    //  console.log("token of raj"+await AsyncStorage.getItem('userToken'))

    this.props.navigation.navigate('App');
     this.props.
      signIn(this.state.email, this.state.password)
      .then(({ data }) =>
       {
        if (data) {
          console.log("its started.....")
          console.log("authentication_tokendata.signIn." , data.signIn.authentication_token);
          this._myTestFunc(data.signIn.authentication_token)
          //  AsyncStorage.setItem('token', "raj")
          //   .then(() => {
          //     console.log('STORED TOKEN : ' + JSON.stringify( AsyncStorage.getItem('token')));
          //     console.log("Token set to AsyncStorage")

          //     this.props.navigation.navigate('App');
          //   })

          
           AsyncStorage.setItem('account', JSON.stringify(data.signIn.user.account))
          .then(() => {
            console.log('It was saved successfully yeeeeeeeee')
          })
          .catch(() => {
            console.log('There was an error saving the account')
          })

          //AsyncStorage.setItem('account', JSON.stringify(data.signIn));
          // this._storageTest(data)
          //this._storeLanguages()
          this.fetchLanguagesAPI()
          this.fetchCurrenciesAPI()
          this.fetchCountriesAPI()
          this.fetchTimezoneAPI()
          this._storeageUser(data)


        }
      }).catch((error) => {
        console.log('my error :' + error);
      })
  };

  fetchCountriesAPI = () => {
    client.query({query:GET_COUNTRIES
      }).
    then((response) => {
      //console.log(response.data);
      storage.save({
        key: 'totalCountries', // Note: Do not use underscore("_") in key!
        data: response.data,
        expires: 10000 * 3600
      });
    }).catch((err) => {
      console.log('errr : ', err);
    })
  }

  fetchTimezoneAPI = () => {
    client.query({query:GET_TIMEZONES
      }).
    then((response) => {
      // console.log(response.data);
      storage.save({
        key: 'totalTimezones', // Note: Do not use underscore("_") in key!
        data: response.data,
        expires: 10000 * 3600
      });
    }).catch((err) => {
      console.log('errr : ', err);
    })
  }

  fetchCurrenciesAPI = () => {
    client.query({query:GET_CURRENCIES
      }).
    then((response) => {
      // console.log(response.data);
      storage.save({
        key: 'totalCurrencies', // Note: Do not use underscore("_") in key!
        data: response.data,
        expires: 10000 * 3600
      });
    }).catch((err) => {
      console.log('errr : ', err);
    })
  }

  fetchLanguagesAPI = () => {
    client.query({query:GET_LANGUAGES
      }).
    then((response) => {
      // console.log(response.data);
      storage.save({
        key: 'totalLanguages', // Note: Do not use underscore("_") in key!
        data: response.data,
        expires: 10000 * 3600
      });
    }).catch((err) => {
      console.log('errr : ', err);
    })
  }

  _storeageUser = async (data) => {
    storage.save({
      key: 'usersdata', // Note: Do not use underscore("_") in key!
      data: data.signIn.user,

      // if expires not specified, the defaultExpires will be applied instead.
      // if set to null, then it will never expire.
      expires: 1000 * 3600
    });
  }

  _storeLanguages()
  {
    console.log("store language called");
    <View>
    <Query query={GET_LANGUAGES}>
      {({ loading, error, data }) => {
        console.log("API Response")
        if (loading) return <BarIndicator color='#4235cc' style={styles.indicator} />;
        console.log("error found sdf dre", error)
        if (error) {
          console.log("GET_INVOICE API called")
          console.log("TEST ERR =>"+ error.graphQLErrors.map(x => x.message));
          return <Text>Error :)</Text>;
        }
        if(data){
          //console.log("languages  = ",data)
        }}}
      </Query>
    </View>
  }

  _storageTest = async (data) => {

    await AsyncStorage.setItem('account', JSON.stringify(data.signIn.user.account))
      .then(() => {
        console.log('It was saved successfully')
      })
      .catch(() => {
        console.log('There was an error saving the account')
      })
  }


  // _signInAsync = async(login)   => {
  //   console.log('here we go');
  //   login().then(({ data: { signIn: { authentication_token } } }) => {
  //      console.log(authentication_token);
  //     // localStorage.setItem("token", authentication_token);
  //     // this.resetState();
  //     // this.props.history.push("/dashboard");
  //     console.log('before store token');
  //     AsyncStorage.setItem('token', authentication_token);
  //     this.props.navigation.navigate('App');  
  //   });

  // };

  _gotoForgetPassword = async () => {
    this.props.navigation.navigate('ForgetPasswordScreen')
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#fdfdfd',
    flexDirection: 'column'
  },
  logoContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    width: 202,
    height: 38
  },
  title: {
    color: '#f7c744',
    fontSize: 18,
    textAlign: 'center',
    marginTop: 5,
    opacity: 0.9
  },
  infoContainer: {
    borderRadius: 50,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    backgroundColor: '#fff',
    width: '90%',
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowRadius: 7,
  },
  // input: {
  //   height: 40,
  //   backgroundColor: 'rgba(0,0,0,0.2)',
  //   color: '#fff',
  //   marginBottom: 20,
  //   paddingHorizontal: 10
  // },
  containerButton: {
    flexDirection: 'row'
  },
  buttonContainer: {
    backgroundColor: '#373f51',
    marginTop: 15,
  },
  buttonForgotContainer: {
    marginTop: 15,
  },
  buttonSignupContainer: {
    marginTop: 30,
    backgroundColor: '#423231',
  },
  buttonText: {
    textAlign: 'center',
    color: 'rgb(32,53,70)',
    fontWeight: 'bold',
    fontSize: 18
  },
  searchSection: {
    flexDirection: 'row',
    borderTopColor: 'transparent',
    borderRightColor: 0,
    borderLeftColor: 0,
    borderBottomColor: 0,
    borderColor: '#ccc',
    borderWidth: 1
  },
  searchIcon: {
    padding: 15,
    position: 'relative'
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    color: '#424242',
  },
  imgBackground: {
    flex: 1
  },
  actionButon: {
    position: 'absolute',
    top: '22%',
    right: -20,
    borderRadius: 25,
    backgroundColor: '#2089dc',
    borderColor: 0,
    width: 50,
    height: 50,
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 15,
    //textAlign: 'center',
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowRadius: 3,
  },
  actionRight: {
    width: '100%'
  },
  forgotTxt: {
    width: '100%',
    color: '#666',
    textAlign: 'right',
    paddingTop: 20,
    paddingRight: 35,
    fontSize: 15
  },
  loginButton: {
    fontSize: 20,
    paddingHorizontal: 25,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowRadius: 7,
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
    marginRight: -15,
    backgroundColor: '#fff',
  },
  registerButton: {
    //fontSize: 20,
    paddingHorizontal: 25,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowRadius: 7,
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    marginLeft: -15,
    backgroundColor: '#fff',
  },
  loginText: {
    fontSize: 32,
    color: '#333',
    fontWeight: 'bold',
    width: '100%',
    textAlign: 'center',
    marginBottom: 20,
    marginTop: 20
  },
  content: {
    alignSelf: 'center',
    marginTop: 100,
    width: 200,
    overflow: 'hidden', // for hide the not important parts from circle
    margin: 10,
    height: 100,
  },
  background: { // this shape is a circle 
    borderRadius: 400, // border borderRadius same as width and height
    width: 400,
    height: 400,
    marginLeft: -100, // reposition the circle inside parent view
    position: 'absolute',
    bottom: 0, // show the bottom part of circle
    overflow: 'hidden', // hide not important part of image
  },
  image: {
    height: 100, // same width and height for the container
    width: 200,
    position: 'absolute', // position it in circle
    bottom: 0, // position it in circle
    marginLeft: 100, // center it in main view same value as marginLeft for circle but positive
  }
});

export default graphql(SIGN_IN,
  {
    props: ({ mutate }) => ({
      signIn: (email, password) => mutate({ variables: { email, password } })
    }),

  }
)(LogInScreen)
