import React from 'react';
import { Platform, ScrollView, StyleSheet, Text, View,TouchableOpacity,Image,TextInput } from 'react-native';
import { SearchBar, List, ListItem } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';

import { ADD_PAYMENT_FOR_INVOICE } from "../graphqlQueries";
import { GET_INVOICE } from "../graphqlQueries";

import { graphql } from 'react-apollo';

import { AsyncStorage, Alert, SafeAreaView, StatusBar, Keyboard, KeyboardAvoidingView, TouchableWithoutFeedback } from 'react-native';
import { Button } from 'react-native-elements';

import { Formik, Field } from "formik";
import SimplePicker from 'react-native-simple-picker';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";
import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import * as Yup from "yup";
import IconFont from 'react-native-vector-icons/FontAwesome';

import ReactNativePickerModule from 'react-native-picker-module';

var FloatingLabel = require('react-native-floating-labels');

const Form = withNextInputAutoFocusForm(ScrollView, {
  submitAfterLastInput: false
});
import makeInput, {
  KeyboardModal,
  withPickerValues
} from "react-native-formik";

const MyInput = compose(handleTextInput, withNextInputAutoFocusInput)(TextField);

const PaymentOptions = ['Bank Transfer', 'PayPal', 'Stripe', 'Credit Card', 'Debit Card', 'Check','Cash','Other'];

const validationSchema = Yup.object().shape({  
});

const initialState = {
  amount: "0.0",
  date: "",
  payment_method: "1",
  notes: "2",
  invoice_id: "",
  first_name: "pratik"
};

class AddPayment extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Add Payment',
      headerTitle: 'Add Payment',
      headerRight: <TouchableOpacity onPress={navigation.getParam('addPaymentInAsync')}><View style={{marginRight:20}}><Text style={{color: '#000', fontSize: 18,}}>Save</Text></View></TouchableOpacity>
    };
  };

  goBack(amount)
  {
    console.log('Selected amount :', amount);
    this.props.navigation.state.params.onNavigateBack(amount)
    this.props.navigation.pop()
  }
  UNSAFE_componentWillMount() {
    this.props.navigation.setParams({ addPaymentInAsync: this._addPaymentInAsync });
  }

  _addPaymentInAsync = async () => {
    console.log("idm" , this.state.myInvoice.id);    
  
    this.props.
    add_payment_for_invoice(Number(this.state.amount), this.state.selectedDate, this.state.payment_method, 
   this.state.notes, Number(this.state.myInvoice.id))
  .then(({ data }) => {
    console.log("Add payment : " + JSON.stringify(data));
    console.log("heloo",data.AddPayment.errors)

    if (data.AddPayment.errors!==null) {
      console.log("eror comes in add payment")
      Alert.alert(
        'Invoease',
        ''+data.AddPayment.errors,
        [
          {text: 'OK', onPress: () => {}},
        ],
        {cancelable: false},
      );
    }
    else {
      Alert.alert(
        'Invoease',
        'Add payment successfully.',
        [
          {text: 'OK', onPress: () => this.goBack(this.state.amount)},
        ],
        {cancelable: false},
      );
  
    }
  })
  };

  _gotoAddProduct = async () => {
    console.log("add product clicked")
    this.props.navigation.navigate('ProductAddScreen')
  };

  constructor(props) {
    super(props)

    var today = new Date();
    var todayDay =today.getDate();
    var todayMonth = today.getMonth() + 1;
    var todayYear = today.getFullYear();

    this.state = { ...initialState ,
      selectedIndex: 1,
      //selectedState: 'Cash',
      isDateTimePickerVisible: false,
      selectDay: todayDay,
      selectMonth: todayMonth,
      selectYear: todayYear,
      selectedDate: todayMonth+'/'+todayDay+'/'+todayYear,
      //selectedDate: '03/10/1990',
      notes:"",
      myInvoice : this.props.navigation.state.params.myInvoice,
      
      selectedPayment:'Credit Card',
      selectedPaymentID: '',
      payment:'',
      filteredPayment:[
        'Bank Transfer', 'PayPal', 'Stripe', 'Credit Card', 'Debit Card', 'Check','Cash','Other'
      ],

      selectedValue: null,
    };
  }
  
  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = (date) => {
    console.log('A date has been picked: ', date);

    var Day = date.getDate()
    var Month = date.getMonth() + 1
    var Year = date.getFullYear()

    console.log('A date has been picked: '+ Day);
    console.log('A date has been picked: '+ Month);
    console.log('A date has been picked: '+ Year);

    if (Day < 10)
    {
      Day = '0' + Day
    }
    if (Month < 10)
    {
      Month = '0' + Month
    }
    
    this.setState({ selectMonth: Month })
    this.setState({ selectDay: Day })
    this.setState({ selectYear: Year })
    this.setState({ selectedDate: Month+"/"+Day+"/"+Year })
    this._hideDateTimePicker();
  };

  render() {
    return (
     <View style={styles.container}>

      <Formik
        onSubmit={values => alert(JSON.stringify(values, null, 2))}
        validationSchema={validationSchema}
        initialValues={{ star: true }}>
        <Form style={styles.infoContainer}>
      
      <Text style={styles.paytitle}> {this.state.myInvoice.due_amount} <Text style={styles.paytext}> oustanding</Text></Text>
      <View style={styles.payamounttitle}>
        <FloatingLabel
          labelStyle={styles.labelInput}
          inputStyle={styles.input}
          style={styles.formInput}
          value={this.state.amount}
          onChangeText={(amount) => this.setState({amount})}
          onBlur={this.onBlur}
          keyboardType ={'numbers-and-punctuation'}
          >
          Payment Amount
        </FloatingLabel>
      </View>
      <View style={{flexDirection: 'row'}}>
        <View style={styles.paymenttype}>
          {/*<Text style={styles.amounttitle}>Payment Type</Text>
          <View>
            <Text style={styles.amount} onPress={() => { this.refs.picker2.show(); }}>{this.state.selectedState}</Text>
          </View>
          <SimplePicker
            ref={'picker2'}
            options={options}
            labels={labels}
            itemStyle={{
              color: '#444',
              fontSize: 21
            }}
            onSubmit={(option) => {
              this.setState({ selectedState: option, });
              this.setState({ State: option, });
            }}
          />*/}

          <Text style={styles.amounttitle}>Payment Type</Text>
          <View>
            <Text style={styles.pickerTitle} onPress={() => {this.paymentRef.show()}}>{this.state.selectedPayment}</Text>
            <ReactNativePickerModule
              pickerRef={e => this.paymentRef = e}
              value={this.state.PaymentLabels}
              title={"Select a payment"}
              items={this.state.filteredPayment}
              onValueChange={(PaymentOptions) => {
                this.setState({ selectedPayment: PaymentOptions, payment: PaymentOptions});
              }}
            />
          </View>
        </View>
        <View style={styles.paymentdate}>
          <Text style={styles.amounttitle}>Payment Date</Text>
          <View style={{ flexWrap: 'wrap', alignItems: 'flex-start', flexDirection: 'row', }}>
            <TouchableOpacity onPress={this._showDateTimePicker}>
              <Text style={styles.amount}>{this.state.selectDay}/{this.state.selectMonth}/{this.state.selectYear}</Text>
            </TouchableOpacity>
            <DateTimePicker isVisible={this.state.isDateTimePickerVisible} onConfirm={this._handleDatePicked} onCancel={this._hideDateTimePicker} />
          </View>
        </View>
      </View>      
      <View style={styles.payamounttitle}>
        <FloatingLabel
          labelStyle={styles.labelInput}
          inputStyle={styles.input}
          style={styles.formInput}
          onChangeText={(notes) => this.setState({notes})}
          value={this.state.notes}
          onBlur={this.onBlur}>

          Payment Notes (Optional)
        </FloatingLabel>
      </View>
      </Form>
    </Formik>    
     </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f8f9'
  },
  paytitle: {
    color: '#333',
    fontSize: 26,
    fontWeight: '600',
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 15
  },
  paytext: {
    color: '#999',
    fontSize: 18,
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 5
  },
  payamounttitle: {
    borderWidth: 1,
    borderColor: '#ccc',
    backgroundColor: '#fff',
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 5,
  },
  amounttitle: {
    color: '#ccc',
    fontSize: 16
  },
  amount: {
    marginTop: 5,
    color: '#444',
    fontSize: 21
  },
  paymenttype: {
    width: '50%',
    borderWidth: 1,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 0,
    borderColor: '#ccc',
    backgroundColor: '#fff',
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 15
  },
  paymentdate: {
    width: '50%',
    borderWidth: 1,
    borderTopWidth: 0,
    borderBottomWidth: 0,
    borderColor: '#ccc',
    backgroundColor: '#fff',
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 15
  },

  labelInput: {
    color: '#ccc',
    fontSize: 15,
    paddingLeft: 10
  },
  formInput: {    
    // borderBottomWidth: 1, 
    paddingLeft: 0,
    borderColor: '#ccc'
  },
  input: {
    borderWidth: 0
  },

  pickerTitle: {
    marginTop: 5,
    color: '#444',
    fontSize: 21
  }
});

export default graphql(ADD_PAYMENT_FOR_INVOICE,
  {
    props: ({ mutate }) => ({
      add_payment_for_invoice: (amount, date, payment_method, notes, invoice_id) => mutate({ variables: {
            amount, date, payment_method, notes, invoice_id } })
    }),

    options: {
      refetchQueries: [{
          query:GET_INVOICE
      }
      ],
    }

  }
)(AddPayment)