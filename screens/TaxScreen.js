import React from 'react';
import { Platform, ScrollView, StyleSheet, Text, View, TouchableOpacity, Image, Alert } from 'react-native';
import { SearchBar, List, ListItem } from 'react-native-elements'
import InvoiceDetail from './InvoiceDetail';
import { Query } from "react-apollo";
import { GET_TAXES } from "../graphqlQueries";
import { DESTORY_TAX } from "../graphqlQueries";
import { graphql } from 'react-apollo';

import Swipeout from 'react-native-swipeout';

import IconFont from 'react-native-vector-icons/FontAwesome';
import SearchInput, { createFilter } from 'react-native-search-filter';
const KEYS_TO_FILTERS = ['name'];

class TaxScreen extends React.Component {

  //arrayholder = [{"title":"Taylor Swift","artist":"Taylor Swift","url":"https://www.amazon.com/Taylor-Swift/dp/B0014I4KH6","image":"https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Fearless","artist":"Taylor Swift","url":"https://www.amazon.com/Fearless-Enhanced-Taylor-Swift/dp/B001EYGOEM","image":"https://images-na.ssl-images-amazon.com/images/I/51qmhXWZBxL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Speak Now","artist":"Taylor Swift","url":"https://www.amazon.com/Speak-Now-Taylor-Swift/dp/B003WTE886","image":"https://images-na.ssl-images-amazon.com/images/I/51vlGuX7%2BFL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Red","artist":"Taylor Swift","url":"https://www.amazon.com/Red-Taylor-Swift/dp/B008XNZMOU","image":"https://images-na.ssl-images-amazon.com/images/I/41j7-7yboXL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"1989","artist":"Taylor Swift","url":"https://www.amazon.com/1989-Taylor-Swift/dp/B00MRHANNI","image":"https://images-na.ssl-images-amazon.com/images/I/717DWgRftmL._SX522_.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"}];
  //state = { invoices: [{"title":"Taylor Swift","artist":"Taylor Swift","url":"https://www.amazon.com/Taylor-Swift/dp/B0014I4KH6","image":"https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Fearless","artist":"Taylor Swift","url":"https://www.amazon.com/Fearless-Enhanced-Taylor-Swift/dp/B001EYGOEM","image":"https://images-na.ssl-images-amazon.com/images/I/51qmhXWZBxL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Speak Now","artist":"Taylor Swift","url":"https://www.amazon.com/Speak-Now-Taylor-Swift/dp/B003WTE886","image":"https://images-na.ssl-images-amazon.com/images/I/51vlGuX7%2BFL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Red","artist":"Taylor Swift","url":"https://www.amazon.com/Red-Taylor-Swift/dp/B008XNZMOU","image":"https://images-na.ssl-images-amazon.com/images/I/41j7-7yboXL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"1989","artist":"Taylor Swift","url":"https://www.amazon.com/1989-Taylor-Swift/dp/B00MRHANNI","image":"https://images-na.ssl-images-amazon.com/images/I/717DWgRftmL._SX522_.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"}]};

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Tax',
      headerTitle: 'Tax',
      headerRight: <TouchableOpacity onPress={navigation.getParam('gotoAddTax')}><View style={{ marginRight: 20 }}><IconFont name={'plus'} size={22} color={'#373f51'} style={{ paddingRight: 10 }} /></View></TouchableOpacity>
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      searchTerm: '',
      rowItem: null,
      rowID: '',
      flag: false
    }
  }

  handleOnNavigateBack = () => 
  {
    console.log("handleOnNavigateBack" , filteredTaxes.length);
       this.setState({flag : false})
  }

  _gotoAddTax = async () => {
    // this.props.navigation.navigate('AddTax')
    this.props.navigation.navigate('AddTax', {
      onNavigateBack: this.handleOnNavigateBack.bind(this)
    });
  };

  searchUpdated(term) {
    this.setState({ searchTerm: term })
  }

  _gotoEditTaxScreen = async (i) => {

    this.props.navigation.navigate('EditTax', { selectTax: filteredTaxes[i] })
  };

  deleteRow(item) {
    //console.log(filteredServices)
    //console.log(JSON.stringify(item))
    console.log('Select ROW :' + JSON.stringify(this.state.rowItem))

    Alert.alert(
      'Are you sure?',
      'You want to delete this Tax',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK', onPress: () => this.deleteTAX()
        },
      ],
      { cancelable: false },
    );
  }

  deleteTAX() {  

    console.log("deleted row number",JSON.stringify(this.state.rowItem.id))

    this.props.destroy_tax(this.state.rowItem.id).then(({ data }) => {
        console.log("Delete tax : " + JSON.stringify(data));
        if (data) {
          Alert.alert(
            'Invoease',
            'TAX destory successfully.',
            [
              { text: 'OK' },
            ],
            { cancelable: false },
          );

        }
      })

  };
  checkEmpty()
  {
    this.setState({flag : true})
  }
  renderTaxes() {
    return (
      <View>
        <SearchInput
          onChangeText={(term) => { this.searchUpdated(term) }}
          style={styles.searchInput}
          placeholder="Search Taxes"
          clearIcon={<IconFont name={'search'} size={20} color={'#373f51'} style={{ paddingRight: 5 }} />}
          returnKeyType="search"
        />
        <List containerStyle={{ marginBottom: 20 }}>
          <Query query={GET_TAXES}>
            {({ loading, error, data }) => {
              if (loading) return <Text>Loading...</Text>;
              if (error) {
                console.log("TEST ERR =>" + error.graphQLErrors.map(x => x.message));
                console.log(" ERR =>" + error);
                return <Text>Error :)</Text>;
              }

              console.log("taxxx", data.taxes)

              if (data.taxes.length == 0) {
                console.log("array is empty")
                this.checkEmpty()
              }

              console.log("Taxeess  data ====> ", data);
              console.log(
                data.taxes.map(({ name, rate }) => [
                  name, rate
                ])
              );

              filteredTaxes = data.taxes.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))

              let swipeBtns = [{
                text: 'Delete',
                backgroundColor: 'red',
                // underlayColor: 'rgba(0, 0, 0, 1, 0.6)',
                onPress: () => { this.deleteRow(filteredTaxes[this.state.rowID]) }
              }];

              return (
                <View>
                  {
                    filteredTaxes.map((item, i) => (
                      <Swipeout right={swipeBtns}
                        autoClose={true}
                        backgroundColor='transparent'
                        rowID={i}
                        key={i}
                        onOpen={(i) => {
                          console.log('VALUE OF ITEM :' + i)
                          this.setState({ rowItem: item })
                        }}>
                        <TouchableOpacity onPress={() => this._gotoEditTaxScreen(i)}>
                          <ListItem
                            key={i}
                            title={item.name}
                            subtitle={item.rate}
                          />
                        </TouchableOpacity>
                      </Swipeout>
                    ))

                  }
                </View>)
            }}
          </Query>
        </List>
      </View>

    )
  }

  handleSearch = (text) => {
    console.log("text", text)

    const newData = this.arrayholder.filter(invoice => {
      const itemData = '${invoices.artist.toUpperCase()} ${invoices.title.toUpperCase()}';
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });

    this.setState({ invoices: newData });
  };


  UNSAFE_componentWillMount() {
    console.log(`Tax screen ` + JSON.stringify(this.props.navigation));
    console.log(this.props.navigation)
    this.props.navigation.setParams({ gotoAddTax: this._gotoAddTax });
  }

  render() {
    return (
      <View>
        {
          (this.state.flag == true
            ?
            <View style={{ height: 100, backgroundColor: '#EAE9EF', marginTop: 25 }}>
              <Image source={require('../assets/images/tax.png')}
                style={{ width: 288, height: 71, position: 'absolute', right: 28, top: 5, }}></Image>
            </View>
            :
            <ScrollView>
              {/* <SearchBar lightTheme placeholder='Type Here...' onChangeText={this.handleSearch} />
            {this.renderTaxes()} */}
              {this.renderTaxes()}
            </ScrollView>

          )}
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  searchInput: {
    paddingTop: 1,
    marginTop: 10,
    paddingLeft: 10,
    marginLeft: 10,
    marginRight: 10,
    borderColor: '#CCC',
    height: 40,
    borderWidth: 1,
    backgroundColor: 'white',
    borderRadius: 5,
  }
  // searchInput:{
  //   marginTop:10,
  //   paddingTop:10,
  //   paddingBottom:5,
  //   marginLeft:10,
  //   marginRight:10,
  //   borderColor: '#CCC',
  //   borderWidth: 1,
  //   backgroundColor: 'lightgray',
  //   borderRadius:5,
  // }
});
export default graphql(DESTORY_TAX,
  {
    props: ({ mutate }) => ({
      destroy_tax: (id) =>
        mutate({ variables: { id } })
    }),
    options: {
      refetchQueries: [{
        query: GET_TAXES
      }
      ],
    }

  }
)(TaxScreen)
