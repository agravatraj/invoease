import React from 'react';

import {
  StyleSheet, AsyncStorage, View, SafeAreaView, StatusBar, Keyboard, KeyboardAvoidingView, TouchableWithoutFeedback, TouchableOpacity,
  Text, Image, TextInput, ScrollView, ImageBackground, Alert
} from 'react-native';
import { Button } from 'react-native-elements'

import Icon from 'react-native-vector-icons/FontAwesome';

import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import { Formik } from "formik";
import * as Yup from "yup";
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";

import { SIGN_UPUSER } from "../graphqlQueries";
import { graphql } from 'react-apollo';

// import { LinearGradient } from 'expo';
import { LinearGradient } from 'expo-linear-gradient';

const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

const initialState = {
  firstname: "",
  lastname: "",
  email: "",
  companyname: "",
  password: "",
  confirmpassword: "",
};

const MyInput = compose(handleTextInput, withNextInputAutoFocusInput)(TextField);

const Form = withNextInputAutoFocusForm(ScrollView, {
  submitAfterLastInput: false
});

const validationSchema = Yup.object().shape({
  firstname: Yup.string().required(),
  lastname: Yup.string().required(),
  company: Yup.string().required(),
  email: Yup.string()
    .required()
    .email(),
  password: Yup.string()
    .required()
    .min(6, "Too short"),
  confirmPassword: Yup.string().oneOf(
    [Yup.ref('password')],
    'Passwords do not match',
  ),
  star: Yup.boolean()
    .required()
    .oneOf([true])
});

class SignupScreen extends React.Component {

  static navigationOptions = {
    title: '',
    header: null
  };

  constructor(props) {
    super(props);
    this.state = { ...initialState };
  }

  _back = async () => {
    this.props.navigation.goBack();
  };


  render() {
    return (
      // <ImageBackground style={styles.imgBackground} source={require('../assets/images/login_bg.png')} resizeMode='cover'>
      <View style={styles.container}>
        <SafeAreaView style={styles.container}>
          <StatusBar barStyle="light-content" />
          <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={Platform.select({ios: 0, android: 80})} style={styles.container}>
            <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>
              <View style={styles.container}>

                <View>
                  <Text h2 style={styles.loginText}>Register</Text>
                </View>
                <View style={styles.infoContainer}>
                  {/* <Formik
                      onSubmit={values => alert(JSON.stringify(values, null, 2))}
                      validationSchema={validationSchema}
                      initialValues={{ star: true }}>
                      <Form> */}
                  <View style={styles.searchSection}>
                    <Icon raised style={styles.searchIcon} name='user' type='font-awesome' color='#000' size={20} />
                    <TextInput
                      style={styles.input}
                      label="First Name"
                      name="firstname"
                      type="firstname"
                      id="firstname"
                      placeholder="First Name"
                      underlineColorAndroid="transparent"
                      value={this.state.firstname}
                      onChangeText={(firstname) => this.setState({ firstname })}
                      keyboardType='default'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtLastname.focus()}
                    />
                  </View>
                  <View style={{ height: 1, backgroundColor: '#eee' }}></View>
                  <View style={styles.searchSection}>
                    <Icon raised style={styles.searchIcon} name='user' type='font-awesome' color='#000' size={20} />
                    <TextInput
                      style={styles.input}
                      label="Last Name"
                      name="lastname"
                      type="lastname"
                      id="lastname"
                      placeholder="Last Name"
                      underlineColorAndroid="transparent"
                      value={this.state.lastname}
                      onChangeText={(lastname) => this.setState({ lastname })}
                      keyboardType='default'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtEmail.focus()}
                      ref={"txtLastname"}
                    />
                  </View>
                  <View style={{ height: 1, backgroundColor: '#eee' }}></View>
                  <View style={styles.searchSection}>
                    <Icon raised style={styles.searchIcon} name='envelope' type='font-awesome' color='#000' size={20} />
                    <TextInput
                      style={styles.input}
                      label="Email"
                      name="email"
                      type="email"
                      id="email"
                      placeholder="Email"
                      underlineColorAndroid="transparent"
                      value={this.state.email}
                      onChangeText={(email) => this.setState({ email })}
                      keyboardType='email-address'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtCompany.focus()}
                      ref={"txtEmail"}
                    />
                  </View>
                  <View style={{ height: 1, backgroundColor: '#eee' }}></View>
                  <View style={styles.searchSection}>
                    <Icon raised style={styles.searchIcon} name='building' type='font-awesome' color='#000' size={20} />
                    <TextInput
                      style={styles.input}
                      label="Company"
                      name="company"
                      type="company"
                      id="company"
                      placeholder="Company"
                      underlineColorAndroid="transparent"
                      value={this.state.companyname}
                      onChangeText={(companyname) => this.setState({ companyname })}
                      keyboardType='default'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtPassword.focus()}
                      ref={"txtCompany"}
                    />
                  </View>
                  <View style={{ height: 1, backgroundColor: '#eee' }}></View>
                  <View style={styles.searchSection}>
                    <Icon raised style={styles.searchIcon} name='lock' type='font-awesome' color='#000' size={20} />
                    <TextInput
                      style={styles.input}
                      label="Password"
                      name="password"
                      type="password"
                      id="password"
                      placeholder="Password"
                      underlineColorAndroid="transparent"
                      value={this.state.password}
                      onChangeText={(password) => this.setState({ password })}
                      returnKeyType='next'
                      secureTextEntry={true}
                      autoCorrect={false}
                      ref={"txtPassword"}
                    />
                  </View>
                  <View style={{ height: 1, backgroundColor: '#eee' }}></View>
                  <View style={styles.searchSection}>
                    <Icon raised style={styles.searchIcon} name='lock' type='font-awesome' color='#000' size={20} />
                    <TextInput
                      style={styles.input}
                      label="Confirm Password"
                      name="confirmPassword"
                      type="confirmPassword"
                      placeholder="Confirm Password"
                      underlineColorAndroid="transparent"
                      value={this.state.confirmpassword}
                      onChangeText={(confirmpassword) => this.setState({ confirmpassword })}
                      returnKeyType='go'
                      secureTextEntry={true}
                      autoCorrect={false}
                      ref={"txtConfirmPassword"}
                    />
                  </View>
                  <LinearGradient colors={['#0dc7ca', '#1ddab7', '#1eea9d']} style={styles.actionButon}>
                    <View>
                      <TouchableOpacity onPress={this._signUpAsync}>
                        <Icon raised style={styles.actionRight} name='check' type='font-awesome' color='#fff' size={22} />
                      </TouchableOpacity>
                    </View>
                  </LinearGradient>

                  {/* </Form>
                    </Formik> */}
                </View>
                <View style={{ flex: 1, flexDirection: 'row', marginTop: 15 }}>
                  <Button title="Login" buttonStyle={styles.registerButton} color={'#2089dc'} onPress={() => this.props.navigation.goBack()} />
                </View>
              </View>
            </TouchableWithoutFeedback>
          </KeyboardAvoidingView>
        </SafeAreaView>
      </View>
      // </ImageBackground>
    );
  }


  _signUpAsync = async () => {
    //await AsyncStorage.setItem('userToken', 'abc');
    //this.props.navigation.navigate('App');

    if (this.state.firstname == '') {
      Alert.alert("Please enter firstname.");
      return
    }

    if (this.state.lastname == '') {
      Alert.alert("Please enter lastname.");
      return
    }


    if (reg.test(this.state.email) === false) {
      Alert.alert("Please enter valid Email.");
      return
    }

    if (this.state.companyname == '') {
      Alert.alert("Please enter company name.");
      return
    }

    if (this.state.password.length < 6) {
      Alert.alert("Please enter minimum six character in Password.");
      return
    }

    if(this.state.confirmpassword.length==0)
    {
      Alert.alert("please enter confirm password. ");
      return
    }
    else if(this.state.password.match(this.state.confirmpassword)!=this.state.password)
    {
      Alert.alert("confirm password must match with password");
      return
    }


    console.log("Passing Data =======> : " + this.state.firstname, this.state.lastname, this.state.email, this.state.password, this.state.companyname);
    this.props.
      SignUpUser(this.state.firstname, this.state.lastname, this.state.email, this.state.password, this.state.companyname)
      .then(({ data }) => {
        console.log("new token : " + JSON.stringify(data));
        if (data) {
          AsyncStorage.setItem('token', data.SignUpUser.authentication_token)
            .then(() => {
              this.props.navigation.navigate('App');
            })
          console.log(data.SignUpUser.user.account)
          //AsyncStorage.setItem('account', JSON.stringify(data.signIn));
          // this._storageTest(data)

        }
      }).catch((error) => {
        console.log('my error :' + error);
      })
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#FDFDFD',
    flexDirection: 'column'
  },
  logoContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    width: 202,
    height: 38
  },
  title: {
    color: '#F7C744',
    fontSize: 18,
    textAlign: 'center',
    marginTop: 5,
    opacity: 0.9
  },
  infoContainer: {
    borderRadius: 50,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    backgroundColor: '#fff',
    width: '90%',
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowRadius: 7,
  },
  containerButton: {
    flexDirection: 'row'
  },
  buttonContainer: {
    backgroundColor: '#373F51',
    marginTop: 15,
  },
  buttonForgotContainer: {
    marginTop: 15,
  },
  buttonSignupContainer: {
    marginTop: 30,
    backgroundColor: '#423231',
  },
  buttonText: {
    textAlign: 'center',
    color: 'rgb(32,53,70)',
    fontWeight: 'bold',
    fontSize: 18
  },
  searchSection: {
    flexDirection: 'row',
    borderTopColor: 'transparent',
    borderRightColor: 0,
    borderLeftColor: 0,
    borderBottomColor: 0,
    borderColor: '#ccc',
    borderWidth: 1
  },
  searchIcon: {
    padding: 15,
    position: 'relative'
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    color: '#424242',
  },
  imgBackground: {
    flex: 1
  },
  actionButon: {
    position: 'absolute',
    top: '42%',
    right: -20,
    borderRadius: 25,
    backgroundColor: '#2089DC',
    borderColor: 0,
    width: 50,
    height: 50,
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 15,
    //textAlign: 'center',
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowRadius: 3,
  },
  actionRight: {
    width: '100%'
  },
  forgotTxt: {
    width: '100%',
    color: '#666',
    textAlign: 'right',
    paddingTop: 20,
    paddingRight: 35,
    fontSize: 15
  },
  loginButton: {
    fontSize: 20,
    paddingHorizontal: 25,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowRadius: 7,
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
    marginRight: -15,
    backgroundColor: '#fff',
  },
  registerButton: {
    //fontSize: 20,
    paddingHorizontal: 25,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowRadius: 7,
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    marginLeft: -15,
    backgroundColor: '#fff',
  },
  loginText: {
    fontSize: 32,
    color: '#333',
    fontWeight: 'bold',
    width: '100%',
    textAlign: 'center',
    marginBottom: 20,
    marginTop: 20
  },
  content: {
    alignSelf: 'center',
    marginTop: 100,
    width: 200,
    overflow: 'hidden', // for hide the not important parts from circle
    margin: 10,
    height: 100,
  },
  background: { // this shape is a circle
    borderRadius: 400, // border borderRadius same as width and height
    width: 400,
    height: 400,
    marginLeft: -100, // reposition the circle inside parent view
    position: 'absolute',
    bottom: 0, // show the bottom part of circle
    overflow: 'hidden', // hide not important part of image
  },
  image: {
    height: 100, // same width and height for the container
    width: 200,
    position: 'absolute', // position it in circle
    bottom: 0, // position it in circle
    marginLeft: 100, // center it in main view same value as marginLeft for circle but positive
  }
});

export default graphql(SIGN_UPUSER,
  {
    props: ({ mutate }) => ({
      SignUpUser: (first_name, last_name, email, password, company_name) => mutate({ variables: { first_name, last_name, email, password, company_name } })
    }),

  }
)(SignupScreen)