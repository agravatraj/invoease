import React from 'react';
import { StyleSheet, View, Image, Text,AsyncStorage, SafeAreaView, Keyboard, KeyboardAvoidingView,ImageBackground, StatusBar, TouchableWithoutFeedback, TextInput, ScrollView,TouchableOpacity , Alert} from 'react-native';
import { Button } from 'react-native-elements'

import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import { Formik } from "formik";
import * as Yup from "yup";
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";

import { CHANGE_PASSWORD } from "../graphqlQueries";
import { graphql } from 'react-apollo';

import Icon from 'react-native-vector-icons/FontAwesome';
import { LinearGradient } from 'expo';

var FloatingLabel = require('react-native-floating-labels');

const MyInput = compose(handleTextInput, withNextInputAutoFocusInput)(TextField);

const Form = withNextInputAutoFocusForm(ScrollView, {
  submitAfterLastInput: false
});

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .required()
    .email(),
  password: Yup.string()
    .required()
    .min(6, "Too short"),
  star: Yup.boolean()
    .required()
    .oneOf([true])
});


const initialState = {
  oldPassword: '',
  newPassword: '',
  confirmPassword: '',
  authentication_token: 'WER5SkhFQUtBUktVMko3NzZUQmhJWVBEVVA3akdMWkdtTHhGSGtHSFpqVT0tLXJHQ3JNSTlRZW1IYXJ6WVZ4RXlIc2c9PQ==--bc22b7d1a372ebc8e70f8f19a7ef091e45874a07',
};

class ChangePassword extends React.Component {

  // static navigationOptions = {
  //   title: 'Change Password',
  //   headerRight: <TouchableOpacity onPress={this._changePasswordAsync}><View style={{marginRight:20}}><Text style={{color: '#000', fontSize: 18,}}>Save</Text></View></TouchableOpacity>
  // };

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Change Password',
      headerTitle: 'Change Password',
      headerRight: <TouchableOpacity onPress={navigation.getParam('changePasswordAsync')}><View style={{marginRight:20}}><Text style={{color: '#000', fontSize: 18,}}>Save</Text></View></TouchableOpacity>
    };
  };

  _back = async () => {
    this.props.navigation.goBack();
  };

  constructor(props) {
    super(props);
    this.state = { ...initialState };

    console.log("CHANGE PASSWORD")
    //this._retrieveAuthentication_token()
  }

  UNSAFE_componentWillMount(){
    this.props.navigation.setParams({ changePasswordAsync: this._changePasswordAsync });    
  }

  _retrieveAuthentication_token = async () => {
    try {
      const value = await AsyncStorage.getItem('authenticationToken');
      if (value !== null) {
        // We have data!!
        console.log('We have data! CHANGE PASSWORD');
        console.log(value.rawData)
        let data = JSON.parse(value)
        //console.log(data)
        this.setState( {authentication_token:data} ) 
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  componentDidMount() {

    const params = {
      right: (
        <Button
          onPress={() => this._back}
        >
        </Button>
      ),
    };
    this.props.navigation.setParams(params);
  }
  render() {
    return (
      <View style={styles.container}>
                <SafeAreaView style={styles.container}>
                    <StatusBar barStyle="light-content" />
                    <KeyboardAvoidingView behavior='padding' style={styles.container}>
                        <TouchableOpacity style={styles.container} onPress={Keyboard.dismiss}>
                            <View style={styles.container}>
                                <Formik
                                    onSubmit={values => alert(JSON.stringify(values, null, 2))}
                                    validationSchema={validationSchema}
                                    initialValues={{ star: true }}>
                                    <Form style={styles.infoContainer} keyboardShouldPersistTaps='always'>
                                        <FloatingLabel
                                          labelStyle={styles.labelInput}
                                          inputStyle={styles.input}
                                          style={styles.formInput}
                                          underlineColorAndroid="transparent"
                                          returnKeyType='next'
                                          value={this.state.oldPassword}
                                          onChangeText={(oldPassword) => this.setState({ oldPassword })}
                                          secureTextEntry={true}
                                          autoCorrect={false}
                                          ref={"txtPassword"}
                                          onBlur={this.onBlur}>
                                          
                                          Old Password
                                        </FloatingLabel>
                                        
                                        <FloatingLabel
                                          labelStyle={styles.labelInput}
                                          inputStyle={styles.input}
                                          style={styles.formInput}
                                          underlineColorAndroid="transparent"
                                          returnKeyType='next'
                                          value={this.state.newPassword}
                                          onChangeText={(newPassword) => this.setState({ newPassword })}
                                          secureTextEntry={true}
                                          autoCorrect={false}
                                          ref={"txtPassword"}
                                          onBlur={this.onBlur}>
                                          
                                          New Password
                                        </FloatingLabel>
                                        
                                        <FloatingLabel
                                          labelStyle={styles.labelInput}
                                          inputStyle={styles.input}
                                          style={styles.formInput}
                                          underlineColorAndroid="transparent"
                                          returnKeyType='go'
                                          value={this.state.confirmPassword}
                                          onChangeText={(confirmPassword) => this.setState({ confirmPassword })}
                                          secureTextEntry={true}
                                          autoCorrect={false}
                                          ref={"txtPassword"}
                                          onBlur={this.onBlur}>
                                          
                                          Confirm Password
                                        </FloatingLabel>
                                        
                                         {/*<Button raised
                                            buttonStyle={styles.buttonContainer}
                                            title='Update'
                                            onPress={this._changePasswordAsync} />*/}
                                    </Form>
                                </Formik>
                            </View>
                        </TouchableOpacity>
                    </KeyboardAvoidingView>
                </SafeAreaView>
            </View>
    );
  }

  _changePasswordAsync = async () => {

    if (this.state.oldPassword == '') {
      Alert.alert("Please enter Old password.");
      return
    }

    if (this.state.password == '') {
      Alert.alert("Please enter Password.");
      return
    }

    if (this.state.confirmPassword == '') {
      Alert.alert("Please enter confirm password.");
      return
    }

    if (this.state.confirmPassword == this.state.password) {
      Alert.alert("Password and confirm password are not matched.");
      return
    }

    console.log(this.state)

    this.props.
    change_password(String(this.state.oldPassword), String(this.state.password), String(this.state.confirmPassword), String(this.state.authentication_token))
      .then(({ data }) => {

        if(data.password_confirmation.error){
          
        }

        if (data) {
          console.log(data)
          //console.log(data.forgotPassword.message)
          alert(data.changePassword.message)
          this._back();

          //  AsyncStorage.setItem('token', data.signIn.authentication_token)
          //   .then(() => {
          //     this.props.navigation.navigate('App');
          //   })
        }
      }).catch((error) => {
        console.log('my error :' + error);
      })
  };


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(255, 255, 255)'
  },
  logoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  logo: {
    width: 266,
    height: 50
  },
  title: {
    color: '#F7C744',
    fontSize: 18,
    textAlign: 'center',
    marginTop: 5,
    opacity: 0.9
  },
  infoContainer: {
    flexDirection: 'column',
    flex: 1,
  },
  buttonContainer: {
    backgroundColor: '#1FA2FF',
    marginTop: 15,
  },
  buttonText: {
    textAlign: 'center',
    color: 'rgb(32,53,70)',
    fontWeight: 'bold',
    fontSize: 18
  },
  labelInput: {
    color: '#ccc',
    fontSize: 15,
    paddingLeft: 15
  },
  formInput: {    
    borderBottomWidth: 1,
    paddingLeft: 5,
    borderColor: '#ccc'
  },
  input: {
    borderWidth: 0
  }
});

export default graphql(CHANGE_PASSWORD,
  {
    props: ({ mutate }) => ({
      change_password: (old_password,password,password_confirmation,authentication_token) => mutate({ variables: { old_password,password,password_confirmation,authentication_token} })
    }),

  }
)(ChangePassword)