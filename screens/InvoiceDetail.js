import React from 'react'
import { Text, View, Image} from 'react-native'
import Card from './Card';
import CardSection from './CardSection'

const InvoiceDetail = (props) => {
    return(
        <Card>
          <CardSection>
            <View style={styles.imageContainerStyle}>
              <Image
              style = {styles.imageStyle}
                source={{uri: props.record.image}} //props.record.thumbnail_image
              />
            </View>
            <View style={styles.headerContentStyle}>
              <Text style={styles.headerTextStyle}>{props.record.title}</Text>
              <Text>{props.record.artist}</Text>
            </View>
          </CardSection>
        </Card>

    );
};

const styles = {
  headerContentStyle: {
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  headerTextStyle: {
    fontSize: 18
  },
  imageStyle:{
    height: 50,
    width: 50
  },
  imageContainerStyle:{
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10
  },
  centerImageStyle:{
    height: 300,
    flex: 1,
    width: null
  }
};
export default InvoiceDetail;
