import React from 'react';
import { View, AsyncStorage, TouchableOpacity, ScrollView, Text } from 'react-native';
import { Button } from 'react-native-elements'
import { List, ListItem } from 'react-native-elements'
import IconFont from 'react-native-vector-icons/FontAwesome';
import storage from '../helper/storage';
import ApolloClient from "apollo-boost";
import { apiConfig } from "../config/apiConfig";

const client = new ApolloClient( apiConfig.DEV_API_CONFIG);

export default class SettingsScreen extends React.Component {
  // static navigationOptions = {
  //   title: 'Settings',
  //   headerTitle: 'Settings',
  //   // headerRight: (
  //   //   <Button 
  //   //   icon={{name: 'power-off', type: 'font-awesome', color: 'black'}}
  //   //   title=''
  //   //   backgroundColor = 'transparent'
  //   //   onPress={async () => {
  //   //     await AsyncStorage.removeItem('token');
  //   //     //this.props.navigation.navigate('Auth');
  //   //     this._logout();
  //   //   }}/>
  //   // )
  // };

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Settings',
      headerTitle: 'Settings',
      headerRight: (
        <Button
          icon={{ name: 'power-off', type: 'font-awesome', color: 'black' }}
          title=''
          backgroundColor='transparent'
          onPress={navigation.getParam('logout')} />
      ),
    };
  };

  componentDidMount() {
    this.props.navigation.setParams({ logout: this._logout });
  }
  _logout = async () => {

    const client = new ApolloClient(apiConfig.DEV_API_CONFIG);
    try{
      if (client && typeof client.resetStore === 'function') {
        console.log("resetstore inside logout function");
        client.resetStore()
      }
    }
    catch(e){
      console.error('err client', e)
    }

    await AsyncStorage.removeItem('token').then(
      () => {
        console.log('Logout success')
        this.props.navigation.navigate('Auth');
        storage.clearMap();
      },
      () => {
        console.log('rejected')
        this.props.navigation.navigate('Auth');
      }
    )

  };


  _gotoAccountSettingScreen = async () => {
    console.log("Go To Account setting screen")
    this.props.navigation.navigate('AccountSettings')
    // this.props.navigation.navigate('IntroScreen')
    // this.props.navigation.navigate('IntroScreen')
  };
  _gotoProfileScreen = async () => {
    console.log("Go To ProfileScreen")
    this.props.navigation.navigate('ProfileScreen')
  };

  _gotoServicesScreen = async () => {
    console.log("Go To ServicesScreen")
    this.props.navigation.navigate('ServicesScreen')
  };

  _gotoTaxScreen = async () => {
    console.log("Go To TermsScreen")
    this.props.navigation.navigate('TaxScreen')
  };

  _gotoProductScreen = async () => {
    console.log("Go To Product Screen")
    //this.props.navigation.navigate('ProductScreen')

    this.props.navigation.navigate('ProductScreen', {
      comeFromCreateInvoice:"false"
    })
  };

  _gotoTermsScreen = async () => {
    console.log("Go To Terms Screen")
    this.props.navigation.navigate('TermsScreen')
  };
  _gotoAddPayment = async () => {
    console.log("Go To Add Payment Screen")
    this.props.navigation.navigate('AddPayment')
  };
  _gotoInvoiceEmail = async () => {
    console.log("Go To Invoice Email")
    this.props.navigation.navigate('InvoiceEmail')
  };

  _gotoInvoiceView = async () => {
    console.log("Go To Invoice view")
    this.props.navigation.navigate('InvoiceView')
  };
  _gotoInvoeaseNotification = async () => {
    console.log("Go To Add Invoease Notification Screen")
    this.props.navigation.navigate('InvoiceNotification')
  };
  _gotoNotification = async () => {
    console.log("Go To Add Notification Screen")
    this.props.navigation.navigate('Notification')
  };
  _gotoSeachInvoice = async () => {
    console.log("Go To Add SeachInvoice Screen")
    this.props.navigation.navigate('SeachInvoice')
  };
  _gotoProductAddScreen = async () => {
    console.log("Go To Product Add Screen")
    this.props.navigation.navigate('ProductAddScreen')
  };

  _gotoSortScreen = async () => {
    console.log("Go To Sort Screen")
    this.props.navigation.navigate('SortBy')
  };

  _gotoEditItem = async () => {
    console.log("Go To Edit Item")
    this.props.navigation.navigate('EditItem')
  };
  _gotoIntroScreen = async () => {
    this.props.navigation.navigate('IntroScreen')
  }
  _gotoUserList = async () => {
    this.props.navigation.navigate('UserList')
  }

  _gotoChangePassword = async () => {
    this.props.navigation.navigate('ChangePassword')
  }

  render() {
    return (
      <ScrollView>
        <View>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{ flex: 1 }}>
              <Text style={{ fontSize: 16, paddingLeft: 20, marginTop: 20 }}>PROFILE</Text>
            </View>
          </View>
          <List containerStyle={{
            marginBottom: 20,
            backgroundColor: 'transparent', borderColor: 'transparent',
            paddingLeft: 10, paddingRight: 10
          }}>

            <TouchableOpacity onPress={() => this._gotoProfileScreen()}>
              <ListItem
                roundAvatar
                title='My Profile'
                titleStyle={{ paddingLeft: 10 }}
                leftIcon={<IconFont name={'user-circle-o'} size={20} color={'#373f51'} style={{marginRight:10, width:28, textAlign: 'center'}} />}
                containerStyle={{
                  backgroundColor: '#fff', borderRadius: 15,
                  borderBottomColor: 'transparent',
                  borderBottomWidth: 0,
                }}
              />
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this._gotoAccountSettingScreen()}>
              <ListItem
                roundAvatar
                title='Account Settings'
                titleStyle={{ paddingLeft: 10 }}
                leftIcon={<IconFont name={'id-card-o'} size={20} color={'#373f51'} style={{marginRight:10, width:28, textAlign: 'center'}} />}
                containerStyle={{
                  backgroundColor: '#fff', borderRadius: 15, marginTop: 5,
                  borderBottomColor: 'transparent',
                  borderBottomWidth: 0,
                }}
              />
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this._gotoChangePassword()}>
            <ListItem
                roundAvatar
                title='Change Password'
                titleStyle={{ paddingLeft: 10 }}
                leftIcon={<IconFont name={'lock'} size={20} color={'#373f51'} style={{marginRight:10, width:28, textAlign: 'center'}} />}
                containerStyle={{
                  backgroundColor: '#fff', borderRadius: 15, marginTop: 5,
                  borderBottomColor: 'transparent',
                  borderBottomWidth: 0,
                }}
              />
              </TouchableOpacity>

            <TouchableOpacity onPress={() => this._gotoNotification()}>
              <ListItem
                roundAvatar
                title='Notification'
                titleStyle={{ paddingLeft: 10 }}
                leftIcon={<IconFont name={'bell'} size={20} color={'#575757'} style={{marginRight:10, width:28, textAlign: 'center'}} />}
                containerStyle={{
                  backgroundColor: '#fff', borderRadius: 15, marginTop: 5,
                  borderBottomColor: 'transparent',
                  borderBottomWidth: 0,
                }}
              />
            </TouchableOpacity>

            <ListItem
              roundAvatar
              title='Payment'
              titleStyle={{ paddingLeft: 10 }}
              leftIcon={<IconFont name={'money'} size={20} color={'#575757'} style={{marginRight:10, width:28, textAlign: 'center'}} />}
              containerStyle={{ backgroundColor: '#fff', borderRadius: 15, marginTop: 5 ,
              borderBottomColor: 'transparent',
              borderBottomWidth: 0,}}

            />


            <View style={{ flex: 1, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={{ fontSize: 16, paddingLeft: 20, marginTop: 20, marginBottom: 15 }}>GENERAL SETTINGS</Text>
              </View>
            </View>
            
            <TouchableOpacity onPress={() => this._gotoProductScreen()}>
              <ListItem
                roundAvatar
                title='Products'
                titleStyle={{ paddingLeft: 10 }}
                leftIcon={<IconFont name={'bars'} size={20} color={'#373f51'} style={{marginRight:10, width:28, textAlign: 'center'}} />}
                containerStyle={{
                  backgroundColor: '#fff', borderRadius: 15, marginTop: 5,
                  borderBottomColor: 'transparent',
                  borderBottomWidth: 0,
                }}

              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._gotoServicesScreen()}>
              <ListItem
                roundAvatar
                title='Services'
                titleStyle={{ paddingLeft: 10 }}
                leftIcon={<IconFont name={'briefcase'} size={20} color={'#575757'} style={{marginRight:10, width:28, textAlign: 'center'}} />}
                containerStyle={{
                  backgroundColor: '#fff', borderRadius: 15, marginTop: 5,
                  borderBottomColor: 'transparent',
                  borderBottomWidth: 0,
                }}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._gotoTaxScreen()}>
              <ListItem
                roundAvatar
                title='Taxes'
                titleStyle={{ paddingLeft: 10 }}
                leftIcon={<IconFont name={'money'} size={20} color={'#575757'} style={{marginRight:10, width:28, textAlign: 'center'}} />}
                containerStyle={{
                  backgroundColor: '#fff', borderRadius: 15, marginTop: 5,
                  borderBottomColor: 'transparent',
                  borderBottomWidth: 0
                }}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._gotoTermsScreen()}>
              <ListItem
                roundAvatar
                title='Terms'
                titleStyle={{ paddingLeft: 10 }}
                leftIcon={<IconFont name={'tag'} size={20} color={'#575757'} style={{marginRight:10, width:28, textAlign: 'center'}} />}
                containerStyle={{
                  backgroundColor: '#fff', borderRadius: 15, marginTop: 5,
                  borderBottomColor: 'transparent',
                  borderBottomWidth: 0,
                }}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._gotoUserList()}>
              <ListItem
                roundAvatar
                title='Users'
                titleStyle={{ paddingLeft: 10 }}
                leftIcon={<IconFont name={'users'} size={20} color={'#575757'} style={{marginRight:10, width:28, textAlign: 'center'}} />}
                containerStyle={{
                  backgroundColor: '#fff', borderRadius: 15, marginTop: 5,
                  borderBottomColor: 'transparent',
                  borderBottomWidth: 0,
                }}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._gotoIntroScreen()}>
              <ListItem
                roundAvatar
                title='Introduction'
                titleStyle={{ paddingLeft: 10 }}
                leftIcon={<IconFont name={'bicycle'} size={20} color={'#575757'} style={{marginRight:10, width:28, textAlign: 'center'}} />}
                containerStyle={{
                  backgroundColor: '#fff', borderRadius: 15, marginTop: 5,
                  borderBottomColor: 'transparent',
                  borderBottomWidth: 0,
                }}
              />
            </TouchableOpacity>

            {/* <TouchableOpacity onPress={() => this._gotoInvoeaseNotification()}>
              <ListItem
                roundAvatar
                title='Invoease Notification'
                titleStyle={{ paddingLeft: 10 }}
                leftIcon={<IconFont name={'bell'} size={25} color={'#575757'} />}
                containerStyle={{ backgroundColor: '#fff', borderRadius: 15, marginTop: 25 }}
              />
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this._gotoEditItem()}>
              <ListItem
                roundAvatar
                title='EditItem'
                titleStyle={{ paddingLeft: 10 }}
                leftIcon={<IconFont name={'user-circle-o'} size={25} color={'#575757'} />}
                containerStyle={{ backgroundColor: '#fff', borderRadius: 15, marginTop: 5 }}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._gotoProductAddScreen()}>
              <ListItem
                roundAvatar
                title='ProductAdd'
                titleStyle={{ paddingLeft: 10 }}
                leftIcon={<IconFont name={'bicycle'} size={25} color={'#575757'} />}
                containerStyle={{ backgroundColor: '#fff', borderRadius: 15, marginTop: 5 }}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._gotoSortScreen()}>
              <ListItem
                roundAvatar
                title='SortBy'
                titleStyle={{ paddingLeft: 10 }}
                leftIcon={<IconFont name={'bicycle'} size={25} color={'#575757'} />}
                containerStyle={{ backgroundColor: '#fff', borderRadius: 15, marginTop: 5 }}
              />
            </TouchableOpacity> */}
          </List>
        </View>
      </ScrollView>
    );
  }
}