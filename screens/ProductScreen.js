import React from 'react';
import { Platform, ScrollView, StyleSheet, Text, View, Button, TouchableOpacity, Image, TouchableWithoutFeedback, Alert } from 'react-native';
import { SearchBar, List, ListItem } from 'react-native-elements'
import InvoiceDetail from './InvoiceDetail';
import { Query } from "react-apollo";
import { GET_PRODUCTS } from "../graphqlQueries";
import Icon from 'react-native-vector-icons/FontAwesome';
import storage from '../helper/storage';
import IconFont from 'react-native-vector-icons/FontAwesome';
import Swipeout from 'react-native-swipeout';
import { DESTORY_PRODUCT } from "../graphqlQueries";
import { graphql } from 'react-apollo';



const listSelectItem = [];
import SearchInput, { createFilter } from 'react-native-search-filter';
const KEYS_TO_FILTERS = ['name'];
 class ProductScreen extends React.Component {

  //arrayholder = [{"title":"Taylor Swift","artist":"Taylor Swift","url":"https://www.amazon.com/Taylor-Swift/dp/B0014I4KH6","image":"https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Fearless","artist":"Taylor Swift","url":"https://www.amazon.com/Fearless-Enhanced-Taylor-Swift/dp/B001EYGOEM","image":"https://images-na.ssl-images-amazon.com/images/I/51qmhXWZBxL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Speak Now","artist":"Taylor Swift","url":"https://www.amazon.com/Speak-Now-Taylor-Swift/dp/B003WTE886","image":"https://images-na.ssl-images-amazon.com/images/I/51vlGuX7%2BFL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Red","artist":"Taylor Swift","url":"https://www.amazon.com/Red-Taylor-Swift/dp/B008XNZMOU","image":"https://images-na.ssl-images-amazon.com/images/I/41j7-7yboXL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"1989","artist":"Taylor Swift","url":"https://www.amazon.com/1989-Taylor-Swift/dp/B00MRHANNI","image":"https://images-na.ssl-images-amazon.com/images/I/717DWgRftmL._SX522_.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"}];
  //state = { invoices: [{"title":"Taylor Swift","artist":"Taylor Swift","url":"https://www.amazon.com/Taylor-Swift/dp/B0014I4KH6","image":"https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Fearless","artist":"Taylor Swift","url":"https://www.amazon.com/Fearless-Enhanced-Taylor-Swift/dp/B001EYGOEM","image":"https://images-na.ssl-images-amazon.com/images/I/51qmhXWZBxL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Speak Now","artist":"Taylor Swift","url":"https://www.amazon.com/Speak-Now-Taylor-Swift/dp/B003WTE886","image":"https://images-na.ssl-images-amazon.com/images/I/51vlGuX7%2BFL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Red","artist":"Taylor Swift","url":"https://www.amazon.com/Red-Taylor-Swift/dp/B008XNZMOU","image":"https://images-na.ssl-images-amazon.com/images/I/41j7-7yboXL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"1989","artist":"Taylor Swift","url":"https://www.amazon.com/1989-Taylor-Swift/dp/B00MRHANNI","image":"https://images-na.ssl-images-amazon.com/images/I/717DWgRftmL._SX522_.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"}]};

  // static navigationOptions = {
  //   title: '',
  //   headerTitle: 'Products',
  // };


  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Products',
      headerTitle: 'Products',
      headerRight: <TouchableOpacity onPress={navigation.getParam('gotoAddProduct')}><View style={{ marginRight: 20 }}><IconFont name={'plus'} size={22} color={'#373f51'} style={{ paddingRight: 10 }} /></View></TouchableOpacity>
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      searchTerm: '',
      // comeFromCreateInvoice: "false",
      comeFromCreateInvoice: this.props.navigation.state.params.comeFromCreateInvoice ? this.props.navigation.state.params.comeFromCreateInvoice : "false",
      flag: false,
      rowItem: null,
      filteredProducts:[],
              
    }
  }

  handleOnNavigateBack = (item) => {
    console.log("handleOnNavigateBack" , filteredProducts.length);
       this.setState({flag : false})
  }

  _setScreenState() {
    if ("comeFromCreateInvoice set screen state : " , this.props.navigation.state.params.comeFromCreateInvoice) {
      console.log("Inside setScrenstate");

      this.setState({ comeFromCreateInvoice: this.props.navigation.state.params.comeFromCreateInvoice }, () => {
        console.log("comeFromCreateInvoice New", this.state.comeFromCreateInvoice)

      });
    }

  }
 

  _gotoAddProduct = async () => {
    console.log("dfsd", this.state.comeFromCreateInvoice)
    this.props.navigation.navigate('ProductAddScreen', 
    {
      onNavigateBack: this.handleOnNavigateBackFromAddProduct.bind(this),
      comeFromCreateInvoice: this.state.comeFromCreateInvoice,
    })
  };

  handleOnNavigateBackFromAddProduct = (item) => {
    console.log("handleOnNavigationBack performs...")
    if(this.state.comeFromCreateInvoice == "true"){
      console.log("If la")
      this.props.navigation.state.params.onNavigateBack(item)
      this.props.navigation.pop()
    }
    else {
      console.log("ras la")
      this.setState({flag : false})
    }
  }
  
  searchUpdated(term) {
    this.setState({ searchTerm: term })
  }

  handleOnNavigateBackFromEditProduct = (item) => {

    console.log("PRODUCT BACK" , this.state.comeFromCreateInvoice)
    
    if(this.state.comeFromCreateInvoice == "true"){
      this.props.navigation.state.params.onNavigateBack(item)
      this.props.navigation.pop()
    }
    else {
      
    }

    // this.props.navigation.state.params.onNavigateBack(item)
    // this.props.navigation.pop()

  }

  _gotoEditServiceScreen = async (i) => {


  };

  actionOnRow(item) 
  {

     console.log("comeFromCreateInvoice p =",this.state.comeFromCreateInvoice)
      console.log("item-->",item)
    this.props.navigation.navigate('EditProduct', {
      selectedItem: item,
      selectedProduct: item,
      comeFromCreateInvoice: this.state.comeFromCreateInvoice,
      onNavigateBack: this.handleOnNavigateBackFromEditProduct.bind(this)
    })

    // this.props.navigation.navigate('EditProduct', {selectedItem:item,
    //   selectedProduct:item})

    //PRAS 

    // this.props.navigation.state.params.onNavigateBack(item)
    // this.props.navigation.pop()
  }

  deleteRow(item) {
    //console.log(filteredServices)
    //console.log(JSON.stringify(item))
    console.log('Select ROW :' +  JSON.stringify(this.state.rowItem) )

    Alert.alert(
      'Are you sure?',
      'You want to delete this product',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => 
          this.deleteProduct()
      },
      ],
      {cancelable: false},
    );

  }

  deleteProduct() {

    console.log(JSON.stringify(this.state.rowItem.id)) 

        this.props.
        destroy_product(this.state.rowItem.id)
      .then(({ data }) => {
        console.log("Delete product : " + JSON.stringify(data));
        if (data) {
          Alert.alert(
            'Product',
            'product deleted successfully.',
            [
              {text: 'OK'},
            ],
            {cancelable: false},
          );
        }
      })
  };

  checkEmpty()
  {
    this.setState({flag : true})
  }
  renderProducts() {
    return (

      <View>

        <SearchInput
          onChangeText={(term) => { this.searchUpdated(term) }}
          style={styles.searchInput}
          placeholder="Search products"
          clearIcon={<IconFont name={'search'} size={20} color={'#373f51'} style={{ paddingRight: 5 }} />}
          returnKeyType="search"
        />

        <List containerStyle={{ marginBottom: 20 }}>
          <Query query={GET_PRODUCTS}>
            {/* <Query query={GET_PRODUCTS}> */}
            {({ loading, error, data }) => {
              console.log("get products loaded.........")
              if (loading) return <Text>Loading...</Text>;
              if (error) {
                console.log("TEST ERR =>" + error.graphQLErrors.map(x => x.message));
                return <Text>Error :)</Text>;
              }
              //console.log("aaaaaarrrrr",data)

              if (data.products.length == 0) {
                this.checkEmpty()
              }
              // console.log(
              //   data.clients.map(({ company_name, contact } ) => [
              //     company_name, contact =>{return '${contact.first_name} ${contact.last_name}'}
              //   ]) 
              // );
              console.log("data -->> ", data) 
              filteredProducts = data.products.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))

              let swipeBtns = [{
                text: 'Delete',
                backgroundColor: 'red',
                //underlayColor: 'rgba(0, 0, 0, 1, 0.6)',
                onPress: () => { this.deleteRow(filteredProducts[this.state.rowID]) }
              }];

              return (
                <View>
                  {
                    // data.products.map((item, i) => (
                    //   <TouchableWithoutFeedback onPress={ () => this.actionOnRow(item)}>
                    //     <ListItem
                    //     key={i}
                    //     title={item.name}
                    //     subtitle={item.price}
                    //   />  
                    // </TouchableWithoutFeedback>
                    // ))

                    filteredProducts.map((item, i) => (

                      <Swipeout right={swipeBtns}
                      autoClose={true}
                      backgroundColor='transparent'
                      rowID={i}
                      key={i}
                      onOpen={(i) => {
                        console.log('VALUE OF ITEM :' + i)
                        this.setState({rowItem: item})
                      }}
                      style={styles.swipeData}>
                      <TouchableWithoutFeedback onPress={() => this.actionOnRow(item)} key={i}>
                        <ListItem
                          key={i}
                          title={item.name}
                          subtitle={item.unit_cost}
                        />
                      </TouchableWithoutFeedback>
                      </Swipeout>
                    ))

                  }
                </View>)
            }}
          </Query>
        </List>
      </View>

    )
  }

  handleSearch = (text) => {


    const newData = this.arrayholder.filter(invoice => {
      const itemData = '${invoices.artist.toUpperCase()} ${invoices.title.toUpperCase()}';
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });

    this.setState({ invoices: newData });
  };


  UNSAFE_componentWillMount() {
    console.log("product screen ... will mount ..")
    this.props.navigation.setParams({ gotoAddProduct: this._gotoAddProduct });
    this._setScreenState()
  }

  render() {
    return (
      <View>
        {
          (this.state.flag == true
            ?
            <View style={{ height: 100, backgroundColor: '#EAE9EF', marginTop: 25 }}>
              <Image source={require('../assets/images/create_product.png')} style={{ width: 280, height: 60, position: 'absolute', right: 28, top: 5, }}></Image>
            </View>
            :
            <ScrollView>
              {/* <SearchBar lightTheme placeholder='Type Here...' onChangeText={this.handleSearch} />
           {this.renderProducts()} */}
              {this.renderProducts()}
            </ScrollView>

          )
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  swipeoutSide: {
    backgroundColor: '#f00',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius:15,
    height: 80,
    marginRight: 10,
    padding: 0
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  searchInput: {
    paddingTop: 1,
    marginTop: 10,
    paddingLeft: 10,
    marginLeft: 10,
    marginRight: 10,
    borderColor: '#CCC',
    height: 40,
    borderWidth: 1,
    backgroundColor: 'white',
    borderRadius: 5,
  },
  // searchInput:{
  //   marginTop:10,
  //   paddingTop:10,
  //   paddingBottom:5,
  //   marginLeft:10,
  //   marginRight:10,
  //   borderColor: '#CCC',
  //   borderWidth: 1,
  //   backgroundColor: 'lightgray',
  //   borderRadius:5,
  // }
});

export default graphql(DESTORY_PRODUCT,
  {
    props: ({ mutate }) => ({
      destroy_product: (id) =>
        mutate({ variables: { id } })
    }),
    options: {
      refetchQueries: [{
        query: GET_PRODUCTS
      }
      ],
    }

  }
)(ProductScreen)


