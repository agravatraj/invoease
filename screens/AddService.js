import React from 'react';

import { StyleSheet,Text,TouchableOpacity, AsyncStorage, View, SafeAreaView,Alert, StatusBar, Keyboard, KeyboardAvoidingView, TouchableWithoutFeedback, Image, TextInput, ScrollView } from 'react-native';
import { Button } from 'react-native-elements'

import { CREATE_SERVICE } from "../graphqlQueries";
import { GET_SERVICES } from "../graphqlQueries";
import { graphql } from 'react-apollo';


import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import { Formik } from "formik";
import * as Yup from "yup";
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";

var FloatingLabel = require('react-native-floating-labels');

const MyInput = compose(handleTextInput, withNextInputAutoFocusInput)(TextField);

const Form = withNextInputAutoFocusForm(ScrollView, {
  submitAfterLastInput: false
});

const validationSchema = Yup.object().shape({
  name: Yup.string().required(),
  email: Yup.string()
    .required()
    .email(),
  password: Yup.string()
    .required()
    .min(6, "Too short"),
  confirmPassword: Yup.string().oneOf(
      [Yup.ref('password')],
      'Passwords do not match',
    ),
  star: Yup.boolean()
    .required()
    .oneOf([true])
});


const initialState = {
    name: "",
    description: "",
    price: "",
    billable: true,
    hours: "",
    account_id: ""
  };


class AddService extends React.Component {
 
  // static navigationOptions = ({ navigation }) => {
  //   return {
  //     title: '',
  //     headerTitle: 'Add Service',
  //     headerRight: <TouchableOpacity onPress={this._CreateService}><View style={{marginRight:20}}><Text style={{color: '#000', fontSize: 18,}}>Save</Text></View></TouchableOpacity>
  //   };
  // };

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Add Service',
      headerTitle: 'Add Service',
      headerRight: <TouchableOpacity onPress={navigation.getParam('CreateService')}><View style={{marginRight:20}}><Text style={{color: '#000', fontSize: 18,}}>Save</Text></View></TouchableOpacity>
    };
  };

  constructor(props) {
    super(props);
    this.state = { ...initialState };
    console.log("ADD SERVICE")
    this._retrieveAccountData()
  }

  UNSAFE_componentWillMount(){
    this.props.navigation.setParams({ CreateService: this._CreateService });    
  }

  _retrieveAccountData = async () => {
    try {
      const value = await AsyncStorage.getItem('account');
      if (value !== null) {
        // We have data!!
        console.log('We have data! Add Services');
        let data = JSON.parse(value)
        this.setState({
            account_id:data[0].id 
        }) 
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView style={styles.container}>
          <StatusBar barStyle="light-content" />
          <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={Platform.select({ios: 0, android: 80})} style={styles.container}>
            <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>
              <View style={styles.container}>
                <Formik
                  onSubmit={values => alert(JSON.stringify(values, null, 2))}
                  validationSchema={validationSchema}
                  initialValues={{ star: true }}>
                  <Form style={styles.infoContainer} keyboardShouldPersistTaps='always' >
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.name}
                      onChangeText={(name) => this.setState({ name })}
                      keyboardType='default'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtPrice.focus()}
                      ref={"txtName"}
                      onBlur={this.onBlur}>

                      Name
                    </FloatingLabel>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.price}
                      onChangeText={(price) => this.setState({ price })}
                      keyboardType='numeric'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtHours.focus()}
                      ref={"txtPrice"}
                      onBlur={this.onBlur}>

                      Price
                    </FloatingLabel>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.hours}
                      onChangeText={(hours) => this.setState({ hours })}
                      keyboardType='numeric'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.TxtDescription.focus()}
                      ref={"txtHours"}
                      onBlur={this.onBlur}>

                      Hours
                    </FloatingLabel>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.description}
                      onChangeText={(description) => this.setState({ description })}
                      keyboardType='default'
                      returnKeyType='go'
                      autoCorrect={false}
                      ref={"TxtDescription"}
                      onBlur={this.onBlur}>

                      Description
                    </FloatingLabel>
                    {/*<Button raised
                      buttonStyle={styles.buttonContainer}
                      //icon={{ name: 'fingerprint' }}
                      title='Save'
                      onPress={this._CreateService} />*/}
                  </Form>
              </Formik>
              </View>
            </TouchableWithoutFeedback>
          </KeyboardAvoidingView>
        </SafeAreaView>

      </View>
    );
  }

  goBack(amount)
  {
    console.log("Selected amount :", amount);
    this.props.navigation.state.params.onNavigateBack(amount)
    this.props.navigation.pop()
  }

  _CreateService = async () => {

    console.log(this.state);

        this.props.
        create_service(this.state.name, this.state.description, Number(this.state.price), Boolean(this.state.billable), 
                        Number(this.state.hours), Number(this.state.account_id))
      .then(({ data }) => {
        console.log("create service : " + JSON.stringify(data));
        if (data) {
          Alert.alert(
            'Invoease',
            'Service added successfully.',
            [
              {text: 'OK', onPress: () => this.goBack("ok1")},
            ],
            {cancelable: false},
          );

        }
      })

  };
}

const styles = StyleSheet.create({
  container: {
        flex: 1,
        backgroundColor: 'rgb(255, 255, 255)',
        flexDirection: 'column'
    },
    logoContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    logo: {
        width: 266,
        height: 50
    },
    title: {
        color: '#f7c744',
        fontSize: 18,
        textAlign: 'center',
        marginTop: 5,
        opacity: 0.9
    },
    infoContainer: {
        flexDirection: 'column',
        flex: 1,
    },
    buttonContainer: {
        backgroundColor: '#1FA2FF',
        marginTop: 15,
    },
    buttonText: {
        textAlign: 'center',
        color: 'rgb(32,53,70)',
        fontWeight: 'bold',
        fontSize: 18
    },

    labelInput: {
      color: '#ccc',
      fontSize: 15,
      paddingLeft: 15,
    },
    formInput: {    
      borderBottomWidth: 1, 
      paddingLeft: 5,
      borderColor: '#ccc',       
    },
    input: {
      borderWidth: 0
    }
});


export default graphql(CREATE_SERVICE,
    {
      props: ({ mutate }) => ({
        create_service: (name, description, price, billable, hours, account_id) => 
        mutate({ variables: {name, description, price, billable, hours, account_id } })
      }),
      options: {
        refetchQueries: [{
            query:GET_SERVICES
        }
        ],
      }
  
    }
  )(AddService)