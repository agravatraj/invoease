//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ImageBackground, TouchableOpacity, ScrollView, TouchableHighlight,KeyboardAvoidingView } from 'react-native';
import { Button, ThemeProvider, Avatar, Input} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import { Formik } from "formik";
import * as Yup from "yup";

import SimplePicker from 'react-native-simple-picker';

import { compose } from "recompose";
// import { ImagePicker } from 'expo';
// import { ImagePicker } from 'expo-image-picker';
// import { Permissions } from 'expo-permissions';

import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';

import { Contacts } from 'expo';
import { TextField } from "react-native-material-textfield";

import { UPDATE_ACCOUNT,GET_LANGUAGES, GET_CURRENCIES } from "../graphqlQueries";
import { graphql } from 'react-apollo';
import ApolloClient from "apollo-boost";
import { apiConfig } from "../config/apiConfig";
import gql from "graphql-tag";
import { createFilter } from  'react-native-search-filter';

import ReactNativePickerModule from 'react-native-picker-module';


var FloatingLabel = require('react-native-floating-labels');
const KEYS_TO_FILTERS = ['name'];

const MyInput = compose(handleTextInput, withNextInputAutoFocusInput)(TextField);

const Form = withNextInputAutoFocusForm(ScrollView, {
  submitAfterLastInput: false
});

const client = new ApolloClient( apiConfig.DEV_API_CONFIG);

const LanguageOptions = ['English', 'Dutch', 'French', 'German', 'Greek'];
const LanguageLabels = ['English', 'Dutch', 'French', 'German', 'Greek'];

const CurrencyOptions = ['US Dollar', 'Kenyan Shilling', 'Kina', 'Indian Rupee', 'Dobra'];
const CurrencyLabels = ['US Dollar', 'Kenyan Shilling', 'Kina', 'Indian Rupee', 'Dobra'];

const USER_SERVICE_URL = 'http://192.168.1.36:3000/api/languages';
const USER_SERVICE_URL2 = 'http://192.168.1.36:3000/api/currencies';

const validationSchema = Yup.object().shape({
  company: Yup.string().required(),
  name: Yup.string().required(),
  email: Yup.string()
    .required()
    .email(),
  password: Yup.string()
    .required()
    .min(6, "Too short"),
  confirmPassword: Yup.string().oneOf(
      [Yup.ref('password')],
      'Passwords do not match',
    ),
  star: Yup.boolean()
    .required()
    .oneOf([true])
});

// create a component
class AccountSettings extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Account Settings',
      headerTitle: 'Account Settings',
      headerRight: <TouchableOpacity onPress={navigation.getParam('updateAccountAsync')}><View style={{marginRight:20}}><Text style={{color: '#000', fontSize: 18,}}>Save</Text></View></TouchableOpacity>
    };
  };

    state = {
        image: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
        hasCameraPermission: null,
        contact:null,
        company:'',
        rate:'',
        fax : '',
        phone : '',
        language:'',
        currency:'',
        
      };
    
      constructor(props) {
        super(props)
      
        this.state = {          
          selectedLanguage: 'English',
          selectedLanguageID: '',
          selectedCurrency:'USD',
          selectedCurrencyID: '',
          image: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
          companyname:'',
          fax : '',
          phone : '',
          language:'',
          currency:'',
          account_id: '',
          arrLanguages:[],
          arrCurrencies:[],
          searchTerm: '',
          filteredLanguages:[],
          filteredCurrencies:[],
          arrUsers:[],
          selectedValue: null,
          dataLanguage: [
            "Javascript",
            "Go",
            "Java",
            "Kotlin",
            "C++",
            "C#",
            "PHP"
          ],
          dataCurrency: [
            "Dollar",
            "Rs",
            "Yen",
            "Euro",
            "AUD"
          ]
        };

        this._retrieveUserData()
        // this._retrieveTotalLanguages()
        // this._retrieveTotalCurrencies()
      }

    contactButtonPress = async () =>{
        //console.log('contact button');
        let data =  await Contacts.getContactsAsync({
            fields: [Contacts.Fields.Emails],
          }). then((contactResponse) => {
           // console.log('contact' + JSON.stringify(contactResponse));
           // console.log('data' + JSON.stringify(contactResponse.data));
            const contact = contactResponse.data[0];
            console.log(contact);
            this.setState({contact:contact})
          })
    }

    _pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
          allowsEditing: true,
          aspect: [4, 3],
        });
    
        console.log(result);
    
        if (!result.cancelled) {
          this.setState({ image: result.uri });
        }
      };
    async componentDidMount() {
        // const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        // this.setState({ hasCameraPermission: status === 'granted' });
        this._retrieveTotalLanguages()
        this._retrieveTotalCurrencies()
    }
    
    // componentDidMount()
    // {
    //     ImagePicker.launchImageLibraryAsync('mediaTypes:Images')
    // }

    _retrieveUserData = async () => {
      console.log('In User Data Function')

        storage.load({
        key: 'usersdata',
        autoSync: true,
        syncInBackground: true,
     
        // you can pass extra params to the sync method
        // see sync example below
        syncParams: {
          extraFetchOptions: {
            // blahblah
          },
          someFlag: true
        }
      })
      .then(ret => {
        // found data go to then()
         console.log('we found user data');
         console.log(ret);

         this.setState({ arrUsers: ret }, () => {  
          console.log("state arrUsers fax",this.state.arrUsers.account[0].fax)
          this.setState({fax :ret.account[0].fax})
          this.setState({companyname :ret.account[0].name})
          this.setState({currency :ret.address[0].name})
          this.setState({language :ret.account[0].language[0].name})
          this.setState({phone :ret.account[0].phone})

          console.log("hiiiiiiiiiiiiiiiiii")
        });
         
        
      })
      .catch(err => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case 'NotFoundError':
            // TODO;
    
            break;
          case 'ExpiredError':
            // TODO
            break;
        }
      });
     
    };


    _retrieveTotalLanguages = async () => {
      console.log('In Language Function')

        storage.load({
        key: 'totalLanguages',
        autoSync: true,
        syncInBackground: true,
     
        // you can pass extra params to the sync method
        // see sync example below
        syncParams: {
          extraFetchOptions: {
            // blahblah
          },
          someFlag: true
        }
      })
      .then(ret => {
        // found data go to then()
        console.log('we found languages');
        //console.log(ret);

        this.setState({ arrLanguages: ret }, () => {  
          //console.log("state arrlang",this.state.arrLanguages)

          this.state.arrLanguages.languages.map((item, i) => (
            
            this.state.filteredLanguages.push(item.name) 
        ))
        });

      })
      .catch(err => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case 'NotFoundError':
            // TODO;
    
            break;
          case 'ExpiredError':
            // TODO
            break;
        }
      });
     
    };

    _retrieveTotalCurrencies = async () => {
      console.log('In retrive Currencies Function')

        storage.load({
        key: 'totalCurrencies',
        autoSync: true,
        syncInBackground: true,
     
        // you can pass extra params to the sync method
        // see sync example below
        syncParams: {
          extraFetchOptions: {
            // blahblah
          },
          someFlag: true
        }
      })
      .then(ret => {
        // found data go to then()
        

        this.setState({ arrCurrencies: ret }, () => {  
          //console.log("state arrCurrencies",this.state.arrCurrencies)

          this.state.arrCurrencies.currencies.map((item, i) => (
            
            this.state.filteredCurrencies.push(item.name)
        ))
        });
        
      
      })
      .catch(err => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case 'NotFoundError':
            // TODO;
    
            break;
          case 'ExpiredError':
            // TODO
            break;
        }
      });
     
    };
    
    render() {
      //console.log("arr languages lllll = ",this.state.arrLanguages[0].name)
      //filteredProducts = this.state.arrLanguages.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))
      

        let { image,contact } = this.state;
        return (
          <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={Platform.select({ios: 0, android: 80})} style={styles.container}>
          <ScrollView>
            {/* </ScrollView><ImageBackground style={styles.imgBackground} source={{ uri: '' }} resizeMode='cover'> */}
            {/* <ImageBackground style={styles.imgBackground} resizeMode='cover'> */}
          
            <View style={{ flex: 1, backgroundColor:'#fff', paddingTop: 15, paddingRight: 15, paddingBottom: 15, paddingLeft: 15,
      borderTopLeftRadius: 40, borderTopRightRadius: 40, marginTop: 150 }}>
              
              <View style={{ flex: 1, flexDirection: 'row', position: 'relative' }}>
                <View style={{ width: '50%' }}>
                  <View style={styles.logo}>
                    <Avatar rounded xlarge source={{ uri: this.state.image }} showEditButton
                    onPress={() => this._pickImage()} />
                  </View>
                </View>

                <View style={{ width: '50%' }}>
                  <View style={styles.description}>
                    <Text style={styles.company}>{this.state.companyname}</Text>  
                    <View style={{flex: 1, flexDirection: 'row'}}>
                      <Icon style={{marginTop: 2, marginRight: 5}} name='map-marker' type='font-awesome' color='#ccc' size={15} />
                      <Text style={styles.address}></Text>
                    </View>
                  </View>
                </View> 
              </View>

              <View style={{ flex: 1, flexDirection: 'row', marginTop: 40 }}>
                <Text h2 style={styles.accountSetting}>Account Setting</Text>               
              </View>
              <View style={{ borderBottomColor: '#ccc', borderBottomWidth: 1, marginBottom: 10, marginTop: 5 }}></View>
              
              <View>
              <Formik
                  onSubmit={values => alert(JSON.stringify(values, null, 2))}
                  validationSchema={validationSchema}
                  initialValues={{ star: true }}>
                  <Form style={styles.infoContainer} keyboardShouldPersistTaps='always'>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.companyname}
                      onChangeText={(companyname) => this.setState({ companyname })}
                      keyboardType='default'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtlanguage.focus()}
                      forwardRef={"txtCompany"}
                      onBlur={this.onBlur}>

                      Company
                    </FloatingLabel>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.phone}
                      onChangeText={(phone) => this.setState({ phone })}
                      keyboardType='numeric'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtfax.focus()}
                      forwardRef={"txtphone"}
                      onBlur={this.onBlur}>

                      Phone
                    </FloatingLabel>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.fax}
                      onChangeText={(fax) => this.setState({ fax })}
                      keyboardType='numeric'
                      returnKeyType='next'
                      autoCorrect={false}
                      forwardRef={"txtfax"}
                      onBlur={this.onBlur}>

                      Fax
                    </FloatingLabel>
                      
                    <View style={styles.paymenttype}>
                      <Text style={styles.amounttitle}>Language</Text>
                      <View>
                        <Text style={styles.amount} onPress={() => { this.refs.language.show(); }}>{this.state.selectedLanguage}</Text>
                      </View>
                      <SimplePicker
                        ref={'language'}
                        options={this.state.filteredLanguages}
                        // labels={LanguageLabels}
                        labels={this.state.filteredLanguages}
                        itemStyle={{
                          color: '#444',
                          fontSize: 20
                        }}
                        onSubmit={(LanguageOptions) => {
                          this.setState({ selectedLanguage: LanguageOptions, language: LanguageOptions});
                        }}
                      />

                      {/* <Text style={styles.pickerTitle} onPress={() => {this.pickerRef.show()}}>{this.state.selectedLanguage}</Text>
                      <ReactNativePickerModule
                        pickerRef={e => this.pickerRef = e}
                        value={this.state.selectedValue}
                        title={"Select a language"}
                        items={this.state.filteredLanguages}
                        onValueChange={(LanguageOptions) => {
                          this.setState({ selectedLanguage: LanguageOptions, language: LanguageOptions});
                        }}
                      /> */}
                    </View>

                    <View style={styles.paymenttype}>

                    <Text style={styles.amounttitle}>currency</Text>
                      <View>
                        <Text style={styles.amount} onPress={() => { this.refs.currencyy.show(); }}>{this.state.selectedCurrency}</Text>
                      </View>
                      <SimplePicker
                        ref={'currencyy'}
                        options={this.state.filteredCurrencies}
                        // labels={LanguageLabels}
                        labels={this.state.filteredCurrencies}
                        itemStyle={{
                          color: '#444',
                          fontSize: 20
                        }}
                        onSubmit={(CurrencyOptions) => {
                          this.setState({selectedCurrency:CurrencyOptions, currency:CurrencyOptions});
                        }}
                      />

                      {/* <Text style={styles.pickerTitle} onPress={() => {this.currencyRef.show()}}>{this.state.selectedCurrency}</Text>
                      <ReactNativePickerModule
                        pickerRef={e => this.currencyRef = e}
                        value={this.state.selectedValue}
                        title={"Select a currency"}
                        items={this.state.filteredCurrencies}
                        onValueChange={(CurrencyOptions) => {
                          this.setState({ selectedCurrency: CurrencyOptions, currency: CurrencyOptions});
                        }}
                      /> */}
                    </View>
                    
                    {/*<View style={styles.inputsContainer}>
                      <TouchableHighlight style={styles.fullWidthButton} onPress={this._updateAccountAsync}>
                        <Text style={styles.fullWidthButtonText}>Save</Text>
                      </TouchableHighlight>
                    </View>*/}

                  </Form>
                </Formik>
              </View>

            </View>
          {/* </ImageBackground> */}

          </ScrollView>
          </KeyboardAvoidingView>
        );
    }

    UNSAFE_componentWillMount() {
      // this.keyboardDidShowListener.remove();
      // this.keyboardDidHideListener.remove();
  
      this.setState({
        visibleHeight: this.windowHeight,
        hideKA: true,
        opacity: 0
      });

      this.props.navigation.setParams({ updateAccountAsync: this._updateAccountAsync });
    }

    _updateAccountAsync = async () => {
      console.log("test ssss");

      console.log("s",
         Number(this.state.selectedLanguageID))
      console.log("s2",
         Number(this.state.selectedCurrencyID))

      this.props.
      updateAccount(this.state.account_id, 
        this.state.companyname,
         this.state.phone, this.state.fax, 
         Number(this.state.selectedLanguageID), Number(this.state.selectedCurrencyID))
        .then(({ data }) => {
          console.log("new data : " + JSON.stringify(data));
          if (data) {
            console.log(data)
            .then(() => {
              //this.props.navigation.navigate('App');
            })
            //console.log(data.signIn.user.account)
            //AsyncStorage.setItem('account', JSON.stringify(data.signIn));
           //this._storeageUser(data) 
            
          }
        }).catch((error) =>{
          console.log('my error :'+ error);
        })
    };    
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor:'#fff',
      borderTopLeftRadius: 40,
      borderTopRightRadius: 40
    },
    imgBackground: {
      flex: 1,
      backgroundColor: '#1FA2FF'
    },
    logo: {
      flex: 1,
      flexDirection: 'column',
      position: 'absolute',
      top: -80
    },
    description: {
      flex: 1,
      flexDirection: 'column'
    },
    company: {
      fontSize: 18,
      marginBottom: 5,
      color: 'black'
    },
    address: {
      fontSize: 15,
      marginBottom: 5,
      color: '#ccc'
    },
    accountSetting: {
      fontSize: 20,
      marginBottom: 5,
      color: '#333'
    },
    inputsContainer: {
      flex: 1
    },
    fullWidthButton: {
      backgroundColor: '#2089dc',
      height: 50,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 10,
      marginTop: 20
    },
    fullWidthButtonText: {
      fontSize: 20,
      color: 'white'
    },
    paymenttype: {
      borderWidth: 1,
      borderLeftWidth: 0,
      borderTopWidth: 0,
      borderRightWidth: 0,
      borderBottomWidth: 1,
      borderColor: '#ccc',
      marginBottom: 0
    },
    payamounttitle: {
      borderWidth: 1,
      borderColor: '#999',
      backgroundColor: '#fff',
      paddingTop: 15,
      paddingRight: 15,
      paddingBottom: 15,
      paddingLeft: 0,
    },
    amounttitle: {
      color: '#ccc',
      fontSize: 16
    },
    amount: {
      marginTop: 5,
      color: '#444',
      fontSize: 21
    },
    labelInput: {
      color: '#ccc',
      fontSize: 15,
    },
    formInput: {    
      borderBottomWidth: 1, 
      borderColor: '#ccc'
    },
    input: {
      borderWidth: 0
    },

    pickerTitle: {
      color: '#ccc',
      fontSize: 14,
      paddingVertical: 15,
      paddingHorizontal: 10
    }
});

//make this component available to the app
//export default AccountSettings;

export default graphql(UPDATE_ACCOUNT,
  {
    props: ({ mutate }) => ({
      updateAccount: (id, name, phone, fax, account_language_id, account_currency_id ) => mutate({ variables: { id, name, phone, fax, account_language_id, account_currency_id } })
    }),

  }
)(AccountSettings)