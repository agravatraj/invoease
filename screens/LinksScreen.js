import React from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import HeaderComponent from './HeaderComponent';

export default class LinksScreen extends React.Component {
  static navigationOptions = {
    title: 'Estimate',
  };

  render() {
    return (
      <View>
        <HeaderComponent {...this.props} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
