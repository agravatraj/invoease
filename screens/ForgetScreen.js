import React, { Component } from 'react';
import { View, Text, TextInput, StyleSheet, Image, ImageBackground, TouchableOpacity, ScrollView, TouchableHighlight } from 'react-native';
import { Button, ThemeProvider, Avatar, Input} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

class ForgetScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {  };
    }
    render() {
        return (
            <View style={styles.container}>
            <View style={styles.logoContainer}>
              <Image style={styles.logo}
                source={require('../assets/images/logo.png')}>
              </Image>
            </View>
    
            <View>
              <Text h2 style={styles.loginText}>Forgot Password?</Text>
            </View>
    
            <View style={styles.infoContainer}>
              <View style={styles.searchSection}>
                <Icon raised style={styles.searchIcon} name='envelope' type='font-awesome' color='#000' size={20} />
                <TextInput
                    style={styles.input}
                    placeholder="Email"
                    onChangeText={(searchString) => {this.setState({searchString})}}
                    underlineColorAndroid="transparent"
                />
              </View>
              <View style={styles.actionButon}>
                <Icon raised style={styles.actionRight} name='repeat' type='font-awesome' color='#fff' size={22} />
              </View>
            </View>
                
            <View style={{flex: 1, flexDirection: 'row', marginTop: 50}}>
              <Button title="Login" buttonStyle={styles.registerButton} color={'#2089dc'} />
            </View>
            
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fdfdfd',
      flexDirection: 'column'
    },
    logoContainer: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    },
    logo: {
      width: 266,
      height: 50
    },
    title: {
      color: '#f7c744',
      fontSize: 18,
      textAlign: 'center',
      marginTop: 5,
      opacity: 0.9
    },
    infoContainer: {
      borderRadius: 50,
      borderTopLeftRadius: 0,
      borderBottomLeftRadius: 0,
      backgroundColor: '#fff',
      width: '90%',
      shadowOffset: { width: 0, height: 0 },
      shadowColor: 'black',
      shadowOpacity: 0.2,
      shadowRadius: 7,
    },
    // input: {
    //   height: 40,
    //   backgroundColor: 'rgba(0,0,0,0.2)',
    //   color: '#fff',
    //   marginBottom: 20,
    //   paddingHorizontal: 10
    // },
    containerButton: {
      flexDirection: 'row'
    },
    buttonContainer: {
      backgroundColor: '#373f51',
      marginTop: 15,
    },
    buttonForgotContainer: {
      marginTop: 15,
    },
    buttonSignupContainer: {
      marginTop: 30,
      backgroundColor: '#423231',
    },
    buttonText: {
      textAlign: 'center',
      color: 'rgb(32,53,70)',
      fontWeight: 'bold',
      fontSize: 18
    },
    searchSection: {
      flexDirection: 'row',
      borderTopColor: 'transparent',
      borderRightColor: 0,
      borderLeftColor: 0,
      borderBottomColor: 0,
      borderColor: '#ccc',
      borderWidth: 1
    },
    searchIcon: {
      padding: 15,
      position: 'relative'
    },
    input: {
      paddingTop: 10,
      paddingRight: 10,
      paddingBottom: 10,
      paddingLeft: 0,
      color: '#424242',
    },
    imgBackground: {
      flex: 1
    },
    actionButon: {
      position: 'absolute',
      top: 1,
      right: -20,
      borderRadius: 100,
      backgroundColor: '#2089dc',
      width: 50,
      height: 50,
      paddingTop: 15,
      paddingRight: 15,
      paddingBottom: 15,
      paddingLeft: 15,
      textAlign: 'center'
    },
    actionRight: {
      width: '100%'
    },
    forgotTxt: {
      width: '100%',
      color: '#666',
      textAlign: 'right',
      paddingTop: 20,
      paddingRight: 20,
      fontSize: 15
    },
    loginButton: {
      fontSize: 20,
      paddingHorizontal: 25,
      shadowOffset: { width: 0, height: 0 },
      shadowColor: 'black',
      shadowOpacity: 0.2,
      shadowRadius: 7,
      borderTopLeftRadius: 20,
      borderBottomLeftRadius: 20,
      marginRight: -15,
      backgroundColor: '#fff',
    },
    registerButton: {
      fontSize: 20,
      paddingHorizontal: 25,
      shadowOffset: { width: 0, height: 0 },
      shadowColor: 'black',
      shadowOpacity: 0.2,
      shadowRadius: 7,
      borderTopRightRadius: 20,
      borderBottomRightRadius: 20,
      marginLeft: -15,
      backgroundColor: '#fff',
    },
    loginText:{
      fontSize: 32,
      color: '#333',
      fontWeight: 'bold',
      width: '100%',
      textAlign: 'center',
      marginBottom: 20
    }
  });

export default ForgetScreen;