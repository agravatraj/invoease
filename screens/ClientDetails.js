import React from 'react';

import {
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TouchableWithoutFeedback,
  StatusBar,
  TextInput,
  SafeAreaView,
  Keyboard,
  KeyboardAvoidingView,
  Alert,
  ScrollView
} from 'react-native';

import { LinearGradient } from 'expo';

import { WebBrowser } from 'expo';
import DeviceInfo from 'react-native-device-info';
import { SIGN_IN } from "../graphqlQueries";
import { Button, Avatar, List, ListItem } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';
import IconFont from 'react-native-vector-icons/FontAwesome';
import { graphql } from 'react-apollo';
import { CREATE_CLIENT, GET_CLIENTS } from "../graphqlQueries";

import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import { Formik } from "formik";
import * as Yup from "yup";
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";

class ClientDetails extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Client Detail',
      headerTitle: 'Client Detail'

    };
  };
  constructor(props) {
    // console.log("selectedClient d" , this.state.selectClient)
    // console.log("selectedClient first _ name" , this.state.selectClient.contacts[0].first_name)
    // console.log("selectedClient last _ name" , this.state.selectClient.contacts[0].last_name)
    super(props);
    console.log("selected client" , this.props.navigation.state.params.selectClient)
    this.state = {
      selectClient: this.props.navigation.state.params.selectClient,
      renderRows: [],
      checkClientContacts: false,
    };
    

    renderRows = this.state.selectClient.invoices
  }
  handleOnNavigateBack = () =>
   {
     console.log("handle on navigation is called********************")
     this.props.navigation.state.params.onNavigateBack()
      this.props.navigation.pop()
  }

  _createInvoice = async () => {
    console.log("create invoice called....",this.state.selectClient)

    this.props.navigation.navigate('InvoiceDetailScreen', {
      selectClient: this.state.selectClient,
      onNavigateBack: this.handleOnNavigateBack.bind(this)
    })
  }

  render() {
    console.log("its client detail screen.....")
    console.log("renderRows" , renderRows)
    
      if(this.state.selectClient.contacts.length === 0)
      {
        this.state = {
          checkClientContacts: true,
        };

        <View>
          <Text>No client information available</Text>
        </View>
      }
              
        console.log("ELSE part executed");
        return (
          
          <ScrollView>
            {
              !this.state.checkClientContacts &&
              <View style={styles.container}>
              <View style={styles.clientDetails}>
                <View style={{ flex: 2, flexDirection: 'column', alignItems: 'center' }}>
                  <IconFont style={styles.iconsEPA} name={'envelope'} size={18} color={'#575757'} />
                  {this.state.selectClient.contacts[0].mobile &&
                   <IconFont style={styles.iconsEPA} name={'phone'} size={18} color={'#575757'} />
                  }
                  <IconFont style={styles.iconsEPA} name={'map-marker'} size={18} color={'#575757'} />
                </View>
                <View style={{ flex: 8, flexDirection: 'column', justifyContent: 'center' }}>
                  <Text style={styles.clientEPA}>{this.state.selectClient.contacts[0].email}</Text>
                  {this.state.selectClient.contacts[0].mobile &&
                    <Text style={styles.clientEPA}>{this.state.selectClient.contacts[0].mobile}</Text>
                  }
                  <Text style={styles.clientEPA}>{this.state.selectClient.address[0].address_1 + " " + this.state.selectClient.address[0].address_2}</Text>
                </View>
              </View>
            </View>
            }
            
            <View style={{ flex: 1, marginTop: 20 }}>
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <View style={{ flex: 1 }}>
                  <Text style={{ fontSize: 16, paddingLeft: 20 }}>INVOICES</Text>
    
                </View>
                <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end', }}>
                  <TouchableOpacity onPress={() => this._createInvoice()}><View style={{ marginRight: 20 }}><IconFont name={'plus'} rounded size={22} color={'#373f51'} style={{ paddingRight: 10 }} /></View></TouchableOpacity>
                </View>
    
              </View>
              <List containerStyle={{
                marginBottom: 20,
                backgroundColor: 'transparent', borderColor: 'transparent',
                paddingLeft: 10, paddingRight: 10
              }}>
                <View>{
    
                  renderRows.map((item, i) => (
                    <TouchableOpacity>
                      <ListItem
                        title={item.invoice_number + " (" + item.inv_date + ")"}
                        titleStyle={{ fontWeight: 'normal', fontSize: 16, color: '#525252',  width: '130%', marginLeft: 0 }}
                        subtitle={item.invoice_status}
                        subtitleStyle={{marginLeft: 0}}
                        containerStyle={{ backgroundColor: '#fff', borderRadius: 3 }}
    
                        rightTitle={item.due_amount.toString()}
                        rightTitleStyle={{ fontWeight: 'bold', color: '#000', marginRight: 0, fontSize: 18 }}
                        hideChevron
                      />
                    </TouchableOpacity>
                  ))
                }</View>
                {/* <TouchableOpacity>
                  <ListItem
    
                    title='#000003 (3 Apr, 2019)'
                    titleStyle={{ fontWeight: 'normal', fontSize: 16, color: '#525252' }}
                    subtitle='DRAFT'
                    containerStyle={{ backgroundColor: '#fff', borderRadius: 3 }}
    
                    rightTitle='$350.00'
                    rightTitleStyle={{ fontWeight: 'bold', color: '#000', marginRight: 10, fontSize: 18 }}
                    hideChevron
                  />
                </TouchableOpacity> */}
              </List>
            </View>
          </ScrollView>
        );
      }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    shadowOffset: { width: 0, height: 1 },
    shadowColor: 'black',
    shadowOpacity: 0.6,
    flexDirection: 'column',
    // marginLeft: 20,
    // marginRight: 20,
  },
  clientTitle: {
    flex: 1,
    flexDirection: 'row',
    margin: 15,
    paddingBottom: 15,
    borderColor: '#ddd',
    borderBottomWidth: 1,
    // marginLeft: 10,
    // marginRight: 20,
    // paddingLeft: 10,
    // paddingRight: 10,
    // backgroundColor:'#f00'
  },
  clientName: {
    color: '#333',
    fontSize: 18,
    fontWeight: 'bold',
  },
  clientDetails: {
    flex: 1,
    flexDirection: 'row',
    margin: 15,
    marginTop: 0,
    paddingBottom: 15,
    // marginLeft: 10,
    // marginRight: 20,
    // paddingLeft: 10,
    // paddingRight: 10,
    // backgroundColor:'#f00'
  },
  iconsEPA: {
    color: '#666',
    paddingTop: 21
  },
  clientEPA: {
    color: '#666',
    fontSize: 18,
    paddingTop: 15
  },
});

//export default ClientDetails;

export default graphql(CREATE_CLIENT,
  {
    props: ({ mutate }) => ({
      create_client: (companyname, phone, currency_id, language_id, delivery_method, address_id, account_id, firstname, lastname, email, mobile, client_id,
        is_primary_contact, address1, address2, city, state, country_id, zipcode) => mutate({
          variables: {
            companyname, phone, currency_id, language_id, delivery_method, address_id, account_id, firstname, lastname, email, mobile, client_id,
            is_primary_contact, address1, address2, city, state, country_id, zipcode
          }
        })
    }),
    options: {
      refetchQueries: [{
        query: GET_CLIENTS
      }
      ],
    }
  }

)(ClientDetails)