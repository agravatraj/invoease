import React from 'react';

import { StyleSheet, AsyncStorage, View, SafeAreaView, StatusBar, Keyboard, KeyboardAvoidingView, TouchableWithoutFeedback, Image, TextInput, ScrollView, Alert, Text, TouchableOpacity, Animated, TouchableHighlight, SectionList } from 'react-native';
import { Button , List ,ListItem,} from 'react-native-elements'

import { Query } from "react-apollo";
import { GET_TAXES } from "../graphqlQueries";

import { CREATE_PRODUCT, GET_PRODUCTS } from "../graphqlQueries";
import { graphql } from 'react-apollo';

import DateTimePicker from 'react-native-modal-datetime-picker';

import { UPDATE_PRODUCT } from "../graphqlQueries";
import SimplePicker from 'react-native-simple-picker';

import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import { Formik } from "formik";
import * as Yup from "yup";
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";

import { createFilter } from 'react-native-search-filter';
const KEYS_TO_FILTERS = ['name'];

//import DoneButton from 'react-native-keyboard-done-button';
const math = require('mathjs')

import SwitchToggle from 'react-native-switch-toggle';
var listSelectItemTax = [];

const MyInput = compose(handleTextInput, withNextInputAutoFocusInput)(TextField);

const options = ['Bank Transfer', 'PayPal', 'Stripe', 'Credit Card', 'Debit Card', 'Check', 'Cash', 'Other'];
const labels = ['Bank Transfer', 'PayPal', 'Stripe', 'Credit Card', 'Debit Card', 'Check', 'Cash', 'Other'];

const Form = withNextInputAutoFocusForm(ScrollView, {
  submitAfterLastInput: false
});

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .required()
    .email(),
  password: Yup.string()
    .required()
    .min(6, "Too short"),
  confirmPassword: Yup.string().oneOf(
    [Yup.ref('password')],
    'Passwords do not match',
  ),
  star: Yup.boolean()
    .required()
    .oneOf([true])
});

const initialState = {
  id: "",
  name: "",
  description: "",
  price: "",
  quantity: "",
  inventory: "",
  account_id: ""
};

class ProductAddScreen extends React.Component {
  static navigationOptions = {
    title: 'ADD PRODUCT',
  };


  onPress1 = () => {
    this.setState({ switchOn1: !this.state.switchOn1 });
  }
  onPress2 = () => {
    this.setState({ switchOn2: !this.state.switchOn2 });
  }
  onPress3 = () => {
    this.setState({ switchOn3: !this.state.switchOn3 });
  }
  onPress4 = () => {
    this.setState({ switchOn4: !this.state.switchOn4 });
  };

  constructor(props) {
    super(props);

    this.state = {
      ...initialState,
      switchOn1: false,
      switchOn2: false,
      switchOn4: false,
      valueArray: [],
      taxes: [],
    };


    listSelectItemTax = [];
    this.index = 0;
    this.animatedValue = new Animated.Value(0);

    this._retrieveAccountData()
  }

  componentDidMount() {
  }

  UNSAFE_componentWillMount() {

    const { navigation } = this.props;
    const item = navigation.getParam('item', 'NO-ITEM');

    // if (item !== null)
    // {
    //   this.setState({ id: item.id.toString() })
    //   this.setState({ name: item.name })
    //   this.setState({ price: item.price.toString() })
    //   this.setState({ quantity: item.quantity.toString() })
    //   this.setState({ inventory: item.inventory.toString() })
    //   this.setState({ account_id: item.account_id.toString() })

    //   const isFromUpdate = navigation.getParam('isRecordUpdate', 'NO-UPDATE-ITEM');
    //   console.log(isFromUpdate)
    // }


    console.log('In User Data Function')

    storage.load({
      key: 'user',
      autoSync: true,
      syncInBackground: true,

      // you can pass extra params to the sync method
      // see sync example below
      syncParams: {
        extraFetchOptions: {
          // blahblah
        },
        someFlag: true
      }
    })
      .then(ret => {
        // found data go to then()
        console.log('we found data');
        console.log("first", ret.first_name);
        console.log("last", ret.last_name);
        console.log(ret.account[0].name)
        console.log(ret.account[0].phone)
        console.log("ret =", ret)
        console.log("taxes =[=[= ", ret.taxes)
        this.setState({ taxes: ret.taxes })


      })
      .catch(err => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case 'NotFoundError':
            // TODO;

            break;
          case 'ExpiredError':
            // TODO
            break;
        }
      });

  }

  handleOnNavigateBack = (item) => {

    listSelectItemTax.push(item)
    console.log("ProductAdd Screen &&&&&&&&&&&& ", listSelectItemTax)
    storage.save({
      key: 'selectItemTax', // Note: Do not use underscore("_") in key!
      data: listSelectItemTax,

      // if expires not specified, the defaultExpires will be applied instead.
      // if set to null, then it will never expire.
      expires: null //1000 * 3600
    });
    console.log('handleOnNavigateBack call product list')
    console.log(listSelectItemTax)

    this.setState({ newSubtotal: Number(0.00) });

    console.log("listSelectItemTax length", listSelectItemTax.length)
    subtotalCount = 0.0

    listSelectItemTax.map((item, key) => {
      console.log("Q * P", math.eval(Number(item.quantity) * Number(item.unit_cost)))
      //subtotalCount += math.eval(Number(item.quantity) * Number(item.price));
      subtotalCount += Number(item.quantity) * Number(item.unit_cost);
      console.log("subtotal", subtotalCount)
    })
    console.log("subtotal count", subtotalCount)


    this.setState({ newSubtotal: Number(subtotalCount) });
    console.log(this.state.newSubtotal)

    this.addMore()
  }

  gotoAddTax = () => {
    console.log('In ADD ProductScreen screen');
    this.props.navigation.navigate('AddTax', {
      onNavigateBack: this.handleOnNavigateBack.bind(this)
    })
  };

  addMore = () => {
    console.log('Add More Call')
    this.animatedValue.setValue(0);

    let newlyAddedValue = { index: this.index }

    console.log(newlyAddedValue)

    storage.load({
      key: 'selectItemTax',

      autoSync: true,

      syncInBackground: true,

      syncParams: {
        extraFetchOptions: {
          // blahblah
        },
        someFlag: true
      }
    })
      .then(ret => {
        // found data go to then()
        console.log('we found data Invoease Detail');
        console.log(ret);


        this.setState({ disabled: true, valueArray: [...this.state.valueArray, ret] }, () => {
          Animated.timing(
            this.animatedValue,
            {
              toValue: 1,
              duration: 500,
              useNativeDriver: true
            }
          ).start(() => {
            this.index = this.index + 1;
            this.setState({ disabled: false });

            //console.log(...this.state.valueArray)

          });
        });

        //console.log(ret.states);

        // var mystates = []
        // ret.states.map(state => mystates.push(state.name) );
        // this.setState({ stateLabel : mystates, stateOptions: mystates  });
      })
      .catch(err => {
        // any exception including data not found
        // goes to catch()
        console.log('error home' + err);
        console.warn(err.message);
        switch (err.name) {
          case 'NotFoundError':
            // TODO;
            break;
          case 'ExpiredError':
            // TODO
            break;
        }
      });

  }

  _handleSwitch = (item) => {
    this.state.isFocusNodeSelected = true;
    //console.log(item);
    //console.log(this.state.isFocusNodeSelected);
  }

  render() {
    let { name, description, price, quantity, inventory, account_id } = this.state;

    const animationValue = this.animatedValue.interpolate(
      {
        inputRange: [0, 1],
        outputRange: [-59, 0]
      });


    console.log("value array Taxes => ", JSON.stringify(this.state.taxes));

    // let newArray = this.state.taxes.map((item, key) => {
    //   console.log("newArray Tax => ", JSON.stringify(item));

    //   //'task' => 'TEST Devloper','description' => 'TEST Development','rate' => '100.00','tax_name' => 'C-GST','tax' => '10.00','qty' => '1.00','total' => '110.00'

    //   if ((key) == this.index) {
    //     return (
    //       <Animated.View key={key} style={[styles.viewHolder1, { opacity: this.animatedValue, transform: [{ translateY: animationValue }] }]}>
    //         {/* <Text style={styles.text}>Row {item.index}</Text> */}

    //         <View style={styles.payamounttitle}>
    //           <View style={{ flex: 3, flexDirection: 'column' }}>
    //             <Text style={styles.amounttitle}>{item.name}</Text>
    //             <Text style={styles.tax}>{item.rate}%</Text>
    //           </View>
    //           <View style={{ flex: 1, flexDirection: 'column' }}>
    //             <SwitchToggle
    //               switchOn={this.state.switchOn1}
    //               onPress={this.onPress1}
    //             />
    //           </View>
    //         </View>
    //       </Animated.View>
    //     );
    //   }
    //   else {
    //     return (
    //       <View key={key} style={styles.payamounttitle}>
    //         {/* <Text style={styles.text}>Row {item.index}</Text>  1 at 1.00,00 */}

    //         <View style={{ flex: 3, flexDirection: 'column' }}>
    //           <Text style={styles.amounttitle}>{item.name}</Text>
    //           <Text style={styles.tax}>{item.rate}%</Text>
    //         </View>
    //         <View style={{ flex: 1, flexDirection: 'column' }}>
    //           <SwitchToggle
    //             switchOn={this.state.switchOn1}
    //             onPress={this.onPress1}
    //           />
    //         </View>
    //       </View>
    //     );
    //   }
    // });

    return (

      <SafeAreaView style={styles.container}>
        <KeyboardAvoidingView behavior='padding' style={styles.container}>
          <StatusBar barStyle="light-content" />
          <ScrollView style={styles.container}>
            <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>
              <View style={styles.container}>
                {/* <View style={styles.logoContainer}>
                  <Image style={styles.logo}
                    source={require('../assets/images/logo.png')}>
                  </Image>
                </View> */}
                {/* New view */}
                <View style={styles.container}>
                  <Formik
                    onSubmit={values => alert(JSON.stringify(values, null, 2))}
                    validationSchema={validationSchema}
                    initialValues={{ star: true }}>
                    <Form style={styles.infoContainer} keyboardShouldPersistTaps='always'>
                      <Text style={styles.paytext}>Item Name</Text>
                      <TextInput
                        style={styles.paytitle}
                        value={this.state.name}
                        //onChange={(name) => this.setState({name})}
                        onChangeText={(name) => this.setState({ name })}
                        placeholder="Design Consulting"
                      />
                      <View style={styles.payamounttitle}>
                        {/* <Text style={styles.amounttitle}>Description</Text> */}
                        <TextInput
                          style={styles.amount}
                          value={this.state.description}
                          onChangeText={(description) => this.setState({ description })}
                          placeholder="Description"
                        />
                      </View>
                      <View style={{ flexDirection: 'row' }}>
                        <View style={styles.paymenttype}>
                          <Text style={styles.amounttitle}>Rate</Text>
                          <View>
                            {/* <Text style={styles.amount} onPress={() => { this.refs.picker2.show(); }}>{this.state.selectedState}</Text> */}
                            <TextInput
                              style={styles.amount}
                              value={this.state.price}
                              onChangeText={(price) => this.setState({ price })}
                              keyboardType="number-pad"
                              returnKeyType='done'
                              placeholder="Rate"
                            />
                            {/* <DoneButton
                              title="Done!" //not required, default value = `Done`
                              style={{ backgroundColor: 'red' }}  //not required
                              doneStyle={{ color: 'green' }}  //not required
                            /> */}
                          </View>
                        </View>
                        <View style={styles.paymentdate}>
                          <Text style={styles.amounttitle}>Quantity</Text>
                          <View style={{ flexWrap: 'wrap', alignItems: 'flex-start', flexDirection: 'row', }}>
                            <TouchableOpacity>
                              {/* <Text style={styles.amount}>15</Text> */}
                              <TextInput
                                style={styles.amount}
                                value={this.state.quantity}
                                onChangeText={(quantity) => this.setState({ quantity })}
                                placeholder="Quantity"
                                returnKeyType='done'
                                keyboardType="number-pad"
                              />
                              {/* <DoneButton
                                title="Done!"   //not required, default value = `Done`
                                style={{ backgroundColor: 'red' }}  //not required
                                doneStyle={{ color: 'green' }}  //not required
                              /> */}
                            </TouchableOpacity>
                            {/* <DateTimePicker isVisible={this.state.isDateTimePickerVisible} onConfirm={this._handleDatePicked} onCancel={this._hideDateTimePicker} /> */}
                          </View>
                        </View>
                      </View>
                      <View style={styles.payamounttitle}>
                        {this.renderTaxes()}
                      </View>

                      <View style={styles.payamounttitle}>
                        <Text style={styles.amounttitle}>Line Total</Text>
                        <Text style={styles.amount}>0.00</Text>
                      </View>

                      {/* <View style={styles.payamounttitle}>
                        <View style={{ flex: 3, flexDirection: 'column' }}>
                          <Text style={styles.amounttitle}>Tax</Text>
                          <Text style={styles.tax}>9% - 9%</Text>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'column' }}>
                          <SwitchToggle
                            switchOn={this.state.switchOn1}
                            onPress={this.onPress1}
                          />
                        </View>
                      </View>

                      <View style={styles.payamounttitle}>
                        <View style={{ flex: 3, flexDirection: 'column' }}>
                          <Text style={styles.tax}>50% - 50%</Text>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'column' }}>
                          <SwitchToggle
                            switchOn={this.state.switchOn2}
                            onPress={this.onPress2}
                          />
                        </View>
                      </View>

                      <View style={styles.payamounttitle}>
                        <View style={{ flex: 3, flexDirection: 'column' }}>
                          <Text style={styles.tax}>GST - 3%</Text>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'column' }}>
                          <SwitchToggle
                            switchOn={this.state.switchOn3}
                            onPress={this.onPress3}
                          />
                        </View>
                      </View> */}

                      

                      {/* <View style={{ backgroundColor: 'red' }}>
                        {
                          newArray
                        }

                      </View> */}
                      
                      
                      {/* <Text style={styles.addTaxes}> Add Taxes</Text> */}
                      <TouchableOpacity activeOpacity={0.8} disabled={this.state.disabled} onPress={this.gotoAddTax}>
                        <View>
                          <Text h3 style={styles.addTaxes}> Add Taxes</Text>
                        </View>
                      </TouchableOpacity>
                      <Button raised
                        buttonStyle={styles.buttonContainer}
                        icon={{ name: 'fingerprint' }}
                        title='SUBMIT'
                        //onPress={this._productUpdateAsync} />
                        onPress={this._productAddAsync} />
                    </Form>
                  </Formik>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </ScrollView>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }

  renderTaxes() {
    return (
      <View>
        {/* <SearchInput
          onChangeText={(term) => { this.searchUpdated(term) }}
          style={styles.searchInput}
          placeholder="Search Taxes"
          clearIcon={<IconFont name={'search'} size={20} color={'#373f51'} style={{ paddingRight: 5 }} />}
          returnKeyType="search"
        /> */}
        <List styles={{marginTop: 0}}>
          <Query query={GET_TAXES}>
            {({ loading, error, data }) => {
              if (loading) return <Text>Loading...</Text>;
              if (error) {
                //  console.log("TEST ERR =>"+ error.graphQLErrors.map(x => x.message));
                return <Text>Error :)</Text>;
              }
              // console.log(
              //   data.clients.map(({ company_name, contact } ) => [
              //     company_name, contact =>{return '${contact.first_name} ${contact.last_name}'}
              //   ])
              // );
              console.log(
                data.taxes.map(({ name, rate }) => [
                  name, rate
                ])
              );

              // filteredTaxes = data.taxes.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))

              // let swipeBtns = [{
              //   text: 'Delete',
              //   backgroundColor: 'red',
              //   underlayColor: 'rgba(0, 0, 0, 1, 0.6)',
              //   onPress: () => { this.deleteRow(filteredTaxes[this.state.rowID]) }
              // }];

              return (
                <View>
                  {
                  data.taxes.map((item, i) => (
                      // <Swipeout right={swipeBtns}
                      //   autoClose='true'
                      //   backgroundColor='transparent'
                      //   rowID={i}
                      //   onOpen={(i) => {
                      //     console.log('VALUE OF ITEM :' + i)
                      //     this.setState({ rowItem: item })
                      //   }}>
                        <TouchableOpacity onPress={() => this._gotoEditTaxScreen(i)}>
                          <ListItem
                            key={i}
                            title={item.name}
                            subtitle={item.rate}
                            // containerStyle={{
                            //   borderWidth: 0.5,
                            //   borderColor: '#bbb',
                            //   borderStyle: 'solid',
                            //   borderBottomColor: '#bbb',
                            //   borderBottomWidth: StyleSheet.hairlineWidth,
                            //   borderTopColor: '#bbb',
                            //   borderTopWidth: StyleSheet.hairlineWidth
                            // }}
                            switchButton
                            hideChevron
                            switched={item.isSelected}
                            switchOnTintColor={'#00BCD4'}
                            onSwitch={(value) => {
                              let focusNodes = [...this.state.focusNodes];
                              focusNodes[index].isSelected = value;
                              this.setState({ focusNodes, isFocusNodeSelected: value });
                            }}
                           
                          />
                        </TouchableOpacity>
                      // </Swipeout>
                    ))
                  }
                </View>)
            }}
          </Query>
        </List>
      </View>

    )
  }

  _productAddAsync = async () => {

    console.log("abc ======>", this.state.name, this.state.description, Number(this.state.price), Number(this.state.quantity), Number(this.state.inventory), Number(this.state.account_id));
 //create_product(this.state.name, this.state.description, Number(this.state.price), Number(this.state.quantity), Number(this.state.inventory), Number(this.state.account_id))
    this.props.
    create_product(this.state.name, this.state.description, Number(this.state.price), Number(this.state.quantity),Number(this.state.inventory), Number(this.state.account_id))    
      //create_product(this.state.name, this.state.description, 10, 5,1, 1)
      .then(({ data }) => {
        console.log("create product : " + JSON.stringify(data));
        if (data) {
          //AsyncStorage.setItem('token', data.signIn.authentication_token);
          //AsyncStorage.setItem('account',data.signIn.user.account)
          //this.props.navigation.navigate('App');
          //alert('Product added successfully.')

          Alert.alert(
            'Invoease',
            'Product added successfully.',
            [
              { text: 'OK', onPress: () => this.props.navigation.pop() },
            ],
            { cancelable: false },
          );

        }
      })
  };

  _productUpdateAsync = async () => {

    console.log("this state =====> ", this.state);

    this.props.update_product(Number(this.state.id), this.state.name, this.state.description, Number(this.state.price), Number(this.state.quantity), Number(this.state.inventory), Number(this.state.account_id))
      .then(({ data }) => {
        console.log("update product : " + JSON.stringify(data));
        if (data) {

          Alert.alert(
            'Invoease',
            'Product update successfully.',
            [
              { text: 'OK', onPress: () => this.props.navigation.pop() },
            ],
            { cancelable: false },
          );

        }
      })
  };


  _retrieveAccountData = async () => {
    try {
      const value = await AsyncStorage.getItem('account');
      if (value !== null) {
        // We have data!!
        console.log('We have data!');
        console.log(JSON.parse(value));
        let data = JSON.parse(value)
        console.log(data[0]);
        console.log("accountID ==========>>>>>>>>" , data[0].id);
        this.setState({
          account_id: data[0].id
        })
        console.log('Account ID');
        console.log(this.state);

      }
      else{
        console.log("No Account records foundddddd")
      }
    } catch (error) {
      // Error retrieving data
    }
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(255, 255, 255)',
    flexDirection: 'column'
  },
  logoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    //backgroundColor: 'red',
  },
  logo: {
    width: 266,
    height: 50
  },
  title: {
    color: '#f7c744',
    fontSize: 18,
    textAlign: 'center',
    marginTop: 5,
    opacity: 0.9
  },
  infoContainer: {
    flexDirection: 'column',
    flex: 1,
    bottom: 0,
    //height: 200,
    padding: 15,
  },
  input: {
    height: 40,
    backgroundColor: 'rgba(0,0,0,0.2)',
    color: '#fff',
    marginBottom: 20,
    paddingHorizontal: 10
  },
  buttonContainer: {
    backgroundColor: '#373f51'
  },
  buttonText: {
    textAlign: 'center',
    color: 'rgb(32,53,70)',
    fontWeight: 'bold',
    fontSize: 18
  },
  paytitle: {
    color: '#666',
    fontSize: 19,
    fontWeight: '500',
    paddingTop: 0,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 0
  },
  paytext: {
    color: '#999',
    fontSize: 18,
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 10,
    paddingLeft: 0
  },
  addTaxes: {
    color: 'skyblue',
    fontSize: 18,
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 10,
    paddingLeft: 10
  },
  payamounttitle: {
    // flex: 1,
    // flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#999',
    backgroundColor: '#fff',
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 15,
  },
  amounttitle: {
    color: '#666',
    fontSize: 18
  },
  amount: {
    marginTop: 5,
    color: '#444',
    fontSize: 21
  },
  tax: {
    marginTop: 5,
    color: '#ccc',
    fontSize: 17,
    flexWrap: 'wrap'
  },
  paymenttype: {
    width: '50%',
    borderWidth: 1,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 0,
    borderColor: '#999',
    backgroundColor: '#fff',
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 15
  },
  paymentdate: {
    width: '50%',
    borderWidth: 1,
    borderTopWidth: 0,
    borderBottomWidth: 0,
    borderColor: '#999',
    backgroundColor: '#fff',
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 15
  }
});

// export default graphql(UPDATE_PRODUCT,
//   {
//     props: ({ mutate }) => ({
//       update_product: (id, name, description, price, quantity, inventory, account_id) => mutate({ variables: { id, name, description, price, quantity, inventory, account_id } })
//     }),

//   }
// )(ProductAddScreen)

export default graphql(CREATE_PRODUCT,
  {
    props: ({ mutate }) => ({
      // create_product: (name, description, price, quantity, inventory, account_id) => mutate({ variables: { name, description, price, quantity, inventory, account_id } })
      create_product: (name, description, price, quantity, inventory, account_id) => mutate({ variables: { name, description, price, quantity, inventory, account_id } })
    }),
    options: {
      refetchQueries: [{
          query:GET_PRODUCTS
      }
      ],
    }
  }
)(ProductAddScreen)