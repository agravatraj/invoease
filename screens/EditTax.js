import React from 'react';

import { StyleSheet, AsyncStorage, Alert, View, SafeAreaView, StatusBar, Keyboard, KeyboardAvoidingView, TouchableWithoutFeedback, TextInput, ScrollView, TouchableOpacity, Text } from 'react-native';
import { Button } from 'react-native-elements'

import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import { Formik } from "formik";
import * as Yup from "yup";
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";

import { UPDATE_TAXES } from "../graphqlQueries";
import { GET_TAXES } from "../graphqlQueries";
import { graphql } from 'react-apollo';
import DoneButton from 'react-native-keyboard-done-button';

var FloatingLabel = require('react-native-floating-labels');

const math = require('mathjs')
const MyInput = compose(handleTextInput, withNextInputAutoFocusInput)(TextField);

const Form = withNextInputAutoFocusForm(ScrollView, {
    submitAfterLastInput: false
});

const validationSchema = Yup.object().shape({
    name: Yup.string().required(),
    email: Yup.string()
        .required()
        .email(),
    password: Yup.string()
        .required()
        .min(6, "Too short"),
    confirmPassword: Yup.string().oneOf(
        [Yup.ref('password')],
        'Passwords do not match',
    ),
    star: Yup.boolean()
        .required()
        .oneOf([true])
});

const initialState = {
    taxID: "",
    name: "",
    rate: "",
    number: "",
    compound: "",
    account_id: ""
};

class EditTax extends React.Component {
    // static navigationOptions = {
    //   title: 'Edit Tax',
    //   headerRight: <TouchableOpacity onPress={this._UpdateTaxes}><View style={{marginRight:20}}><Text style={{color: '#000', fontSize: 18,}}>Save</Text></View></TouchableOpacity>
    // };

    static navigationOptions = ({ navigation }) => {
      return {
        title: 'Edit Tax',
        headerTitle: 'Edit Tax',
        headerRight: <TouchableOpacity onPress={navigation.getParam('UpdateTaxes')}><View style={{marginRight:20}}><Text style={{color: '#000', fontSize: 18,}}>Save</Text></View></TouchableOpacity>
      };
    };

    constructor(props) {
        super(props);
        // this.state = { ...initialState };

        this.state = {

            taxID: "",
            name: "",
            rate: 0,
            number: 0,
            compound: "",
            account_id: "",
            selectTax: this.props.navigation.state.params.selectTax,
        };

        console.log("EDIT TAXES")

        this._retrieveAccountData()
    }

    componentDidMount() {
        console.log("ComponentDidMount called")
    }

    UNSAFE_componentWillMount() {
      this.props.navigation.setParams({ UpdateTaxes: this._UpdateTaxes });
    }

    _retrieveAccountData = async () => {
        console.log("_retrieveAccountData called" , this.state.selectTax)
        

            console.log("state =====> " , "taxID:", this.state.taxID ,"name:", this.state.name, "rate:", this.state.rate,
            "number:", this.state.number, "compound:", this.state.compound)

        try {
            const value = await AsyncStorage.getItem('account');
            if (value !== null) {
                // We have data!!
                console.log('We have data! Add TAXES');
                let data = JSON.parse(value)
                this.setState({
                    account_id: data[0].id
                })
            }

            this.setState({taxID: this.state.selectTax.id ,name: this.state.selectTax.name, rate: this.state.selectTax.rate,
                number: this.state.selectTax.number, compound: this.state.selectTax.compound})
    
            console.log(this.state.selectTax)
            console.log(this.state.selectTax.name)
        } catch (error) {
            // Error retrieving data
        }
    };

    render() {
        console.log("Render called")
        console.log("taxID:", this.state.selectTax.id ,"name:", this.state.selectTax.name, "rate:", this.state.selectTax.rate,
            "number:", this.state.selectTax.number, "compound:", this.state.selectTax.compound)
        return (
            <View style={styles.container}>
                <SafeAreaView style={styles.container}>
                    <StatusBar barStyle="light-content" />
                    <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={Platform.select({ios: 0, android: 80})} style={styles.container}>
                        <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>
                            <View style={styles.container}>
                                {/* <View style={styles.logoContainer}>
                  <Image style={styles.logo}
                    source={require('../assets/images/logo.png')}>
                  </Image>
                </View> */}
                                <Formik
                                    onSubmit={values => alert(JSON.stringify(values, null, 2))}
                                    validationSchema={validationSchema}
                                    initialValues={{ star: true }}>
                                    <Form style={styles.infoContainer} keyboardShouldPersistTaps='always'>
                                        <FloatingLabel
                                          labelStyle={styles.labelInput}
                                          inputStyle={styles.input}
                                          style={styles.formInput}
                                          value={this.state.name}
                                          onChangeText={(name) => this.setState({ name })}
                                          keyboardType='default'
                                          returnKeyType='next'
                                          autoCorrect={false}
                                          onSubmitEsiting={() => this.refs.txtRate.focus()}
                                          ref={"txtName"}
                                          onBlur={this.onBlur}>
                                          
                                          Name
                                        </FloatingLabel>
                                        <DoneButton
                                            title="Next!"   //not required, default value = `Done`
                                            doneStyle={{ color: 'green' }}  //not required
                                        />
                                        <FloatingLabel
                                          labelStyle={styles.labelInput}
                                          inputStyle={styles.input}
                                          style={styles.formInput}
                                          value={String(this.state.rate)}
                                          onChangeText={(rate) => this.setState({ rate })}
                                          keyboardType='numeric'
                                          returnKeyType='next'
                                          autoCorrect={false}
                                          onSubmitEsiting={() => this.refs.txtNumber.focus()}
                                          ref={"txtRate"}
                                          onBlur={this.onBlur}>
                                          
                                          Rate
                                        </FloatingLabel>
                                        <DoneButton
                                            title="Next!" //not required, default value = `Done`
                                            doneStyle={{ color: 'green' }}  //not required
                                        />
                                        <FloatingLabel
                                          labelStyle={styles.labelInput}
                                          inputStyle={styles.input}
                                          style={styles.formInput}
                                          value={String(this.state.number)}
                                          onChangeText={(number) => this.setState({ number })}
                                          keyboardType='numeric'
                                          returnKeyType='next'
                                          autoCorrect={false}
                                          onSubmitEsiting={() => this.refs.txtCompound.focus()}
                                          ref={"txtNumber"}
                                          onBlur={this.onBlur}>
                                          
                                          Number
                                        </FloatingLabel>
                                        <DoneButton
                                            title="Next!"   //not required, default value = `Done`
                                            doneStyle={{ color: 'green' }}  //not required
                                        />
                                        <FloatingLabel
                                          labelStyle={styles.labelInput}
                                          inputStyle={styles.input}
                                          style={styles.formInput}
                                          value={String(this.state.compound)}
                                          onChangeText={(compound) => this.setState({ compound })}
                                          keyboardType='numeric'
                                          returnKeyType='go'
                                          autoCorrect={false}
                                          onSubmitEsiting={() => this.refs.txtZipCode.focus()}
                                          ref={"txtCompound"}
                                          onBlur={this.onBlur}>
                                          
                                          Compound
                                        </FloatingLabel>
                                        <DoneButton
                                            title="Next!"   //not required, default value = `Done`
                                            doneStyle={{ color: 'green' }}  //not required
                                        />
                                        {/*<Button raised
                                            buttonStyle={styles.buttonContainer}
                                            //icon={{ name: 'fingerprint' }}
                                            title='Update'
                                            onPress={this._UpdateTaxes} />*/}
                                    </Form>
                                </Formik>
                            </View>
                        </TouchableWithoutFeedback>
                    </KeyboardAvoidingView>
                </SafeAreaView>
            </View>
        );
    }

    _goBack(data) {
        this.props.navigation.state.params.onNavigateBack(data)
        this.props.navigation.pop()
    }
    _UpdateTaxes = async () => {

        console.log(this.state);

        this.props.
            update_tax(this.state.taxID, this.state.name, Number(this.state.rate), String(this.state.number), Number(this.state.compound))
            .then(({ data }) => {
                //console.log("update tax : " + JSON.stringify(data.createTax.Tax));
                if (data) {
                    Alert.alert(
                        'Invoease',
                        'Tax update successfully.',
                        [
                            //{ text: 'OK', onPress: () => this._goBack(data.createTax.Tax) },
                        ],
                        { cancelable: false },
                    );
                }
            })
    };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(255, 255, 255)'
  },
  logoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  logo: {
    width: 266,
    height: 50
  },
  title: {
    color: '#F7C744',
    fontSize: 18,
    textAlign: 'center',
    marginTop: 5,
    opacity: 0.9
  },
  infoContainer: {
    flexDirection: 'column',
    flex: 1,
  },
  buttonContainer: {
    backgroundColor: '#1FA2FF',
    marginTop: 15,
  },
  buttonText: {
    textAlign: 'center',
    color: 'rgb(32,53,70)',
    fontWeight: 'bold',
    fontSize: 18
  },
  labelInput: {
    color: '#ccc',
    fontSize: 15,
    paddingLeft: 15
  },
  formInput: {    
    borderBottomWidth: 1,
    paddingLeft: 5,
    borderColor: '#ccc'
  },
  input: {
    borderWidth: 0
  }
});

export default graphql(UPDATE_TAXES,
    {
        props: ({ mutate }) => ({
            update_tax: (id, name, rate, number, compound) =>
                mutate({ variables: { id, name, rate, number, compound } })
        }),
        options: {
            refetchQueries: [{
                query:GET_TAXES
            }
            ],
          }
    }
)(EditTax)
