import React from 'react';

import { View, Text, StyleSheet, TextInput, ScrollView, Dimensions, TouchableOpacity, Animated, Image ,Alert,Keyboard , InputAccessoryView, TouchableWithoutFeedback, KeyboardAvoidingView} from 'react-native';
import { Icon, CheckBox, Input, Button, ListItem, SearchBar, Divider, Header, ButtonGroup, Card } from 'react-native-elements';


import DateTimePicker from 'react-native-modal-datetime-picker';
import DateTimeDuePicker from 'react-native-modal-datetime-picker';

import { CREATE_INVOICE } from "../graphqlQueries";
import { GET_INVOICE } from "../graphqlQueries";

import { graphql } from 'react-apollo';

import Swipeout from 'react-native-swipeout';

//const inputAccessoryViewID = 'inputAccessoryView1';

// import { ImagePicker } from 'expo';
import { ImagePicker } from 'expo-image-picker';
import { Permissions } from 'expo-permissions';
import IconFont from 'react-native-vector-icons/FontAwesome';

var listSelectItem = [];
const math = require('mathjs')

// const fs = RNFetchBlob.fs;
// let imagePath = null;

var subtotalCount = 0
var sum=0
 class InvoiceDetailScreen extends React.Component 
 {
  
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Create Invoice',
      headerTitle: 'Create Invoice',
      headerRight: <TouchableOpacity onPress={navigation.getParam('createInvoiceAsync')}><View style={{marginRight:20}}><IconFont name={'check'} size={22} color={'#373f51'} style={{paddingRight:10}}/></View></TouchableOpacity>
    };
  };
  // static navigationOptions = ({ navigation }) => {
  //   return {
  //     title: 'app.json',
  //     headerTitle: 'Invoice Detail',
  //     headerRight: <TouchableOpacity onPress={navigation.getParam('createInvoiceAsync')}><Image source={require('../assets/images/add.png')} style={{marginTop: 5, marginLeft:5, marginRight:10}} /></TouchableOpacity>
  //   };
  // };

  constructor(props) 
  {
    super(props)
    
    console.log("Create invoice CONSTRUCTOR Called..........");

    var today = new Date();
    var todayDay =today.getDate();
    var todayMonth = today.getMonth() + 1;
    var todayYear = today.getFullYear();
    //date=today.getDate() + "/"+ parseInt(today.getMonth()+1) +"/"+ today.getFullYear();
    
    // var date = new Date().getDay();
    // var month = new Date().getMonth();
    // var year = new Date().getFullYear();
 
    console.log("date is = ", todayDay)
    console.log("Month is = ", todayMonth)
    console.log("Year is = ", todayYear)
    //Alert.alert(date + '-' + month + '-' + year);

    console.disableYellowBox = true;
    this.state = 
    {
      sum:0,
      selectedIndex: 1,

      isDateTimePickerVisible: false,
      selectDay: todayDay,
      selectMonth: todayMonth,
      selectYear: todayYear,
      selectedDate: todayMonth+'/'+todayDay+'/'+todayYear,
      //selectedDate:'03/10/1990',

      LineInputArr:[],

      isDateTimePickerDueVisible: false,

      selectDueDay: todayDay,
      selectDueMonth: todayMonth,
      selectDueYear: todayYear,
      selectedDueDate: todayMonth+'/'+todayDay+'/'+todayYear,

      valueArray: [],
      disabled: false,

      logoimage: null,
      hasCameraPermission: null,
      newSubtotal: '',
      Tax: '',
      selectClient: this.props.navigation.state.params.selectClient ? this.props.navigation.state.params.selectClient : null,
      notes:'',

      first_name: '',
      last_name : '', 
      address1: '' , 
      address2 : '' ,
      city : '' , 
      state : '' , 
      country : '',
      zipcode : '', 
      client_id : '', 
      notes : '', 
      terms : '', 
      po_number : '', 
      invoice_number : '',
      invoice_date : '',
      invoice_due_date : '',
      total_line_count : '2',
      //line : "{'line' => {'1' => {'task' => 'TEST Devloper','description' => 'TEST Development','rate' => '100.00','tax_name' => 'C-GST','tax' => '10.00','qty' => '1.00','total' => '110.00'},'2' => {'task' => 'TEST2 Developer','description' => 'TEST2 Development','rate' => '100.00','tax_name' => 'S-GST','tax' => '10.00','qty' => '1.00','total' => '110.00'}}}",
      //line : '',
      amount_paid : '',
      total_amount : '',
      outstanding_amount : '',
      discount_type : 'amount',
      discount_flag : 'false',
      discount_percentage : '',
      discount_amount : '',

    }

    this.updateIndex = this.updateIndex.bind(this)
    console.log("===constructor of invoice detail called===")
    console.log("select Client ---" , this.state.selectClient);
    listSelectItem = [];
    this.index = 0;
    this.animatedValue = new Animated.Value(0);

    //this.props.navigation.setParams({ createInvoiceAsync: this._createInvoiceAsync });
    this._createInvoiceAsync = this._createInvoiceAsync.bind(this);
    
  }

  updateIndex (selectedIndex) {
    this.setState({selectedIndex})
  }
  
  goBack(amount)
    {
      console.log("Selected amount :", amount);
      this.props.navigation.state.params.onNavigateBack(amount)
      this.props.navigation.pop()
    }

  _createInvoiceAsync = async () =>{
    //console.log("invoice object" + JSON.stringify(this.state));

    // this.props.
    // create_invoice(Number(0), this.state.currency_id,
    //   this.state.first_name, this.state.last_name, this.state.address1, this.state.address2,this.state.city, this.state.state, this.state.country,
    //   this.state.zipcode, this.state.client_id, this.state.notes, this.state.terms, this.state.po_number, this.state.invoice_number,
    //   this.state.invoice_date,this.state.invoice_due_date,this.state.total_line_count,this.state.line,this.state.total_amount,
    //   this.state.outstanding_amount,this.state.discount_type,this.state.discount_flag,this.state.discount_percentage,this.state.discount_amount)
    //console.log("my selectClient" + JSON.stringify(this.state.selectClient));

    console.log("total line =>" , this.state.total_amount)
    console.log("outstanding line g=>" , this.state.outstanding_amount)
    
    console.log("this.state.newSubtotal" , this.state.newSubtotal)
    console.log("this.state.Tax" , this.state.Tax)


    //this.setState({total_amount : Number(this.state.newSubtotal) + Number(this.state.Tax)})
    this.state.total_amount = Number(this.state.newSubtotal) + Number(this.state.Tax);
    console.log("this.state.newSubtotal" , this.state.newSubtotal)
    //this.setState({outstanding_amount : Number(this.state.total_amount) - Number(this.state.amount_paid)})
    this.state.outstanding_amount =  Number(this.state.total_amount) - Number(this.state.amount_paid);
    //console.log("this.state.total_amount" , this.state.total_amount)
    console.log("Length  =>" , this.state.LineInputArr.length)
    console.log("LineInputArr Value =>" , JSON.stringify(this.state.LineInputArr))

    //console.log("selectedClient contacts =>" , this.state.selectClient.contacts[0].first_name)

    // console.log(this.state.selectClient.currency[0].id,this.state.selectClient.contacts[0].first_name, this.state.selectClient.contacts[0].last_name, this.state.selectClient.address[0].address_1,
    //   this.state.selectClient.address[0].address_2,this.state.selectClient.address[0].city,this.state.selectClient.address[0].state, this.state.selectClient.address[0].country[0].name,
    //   this.state.selectClient.address[0].zipcode, this.state.selectClient.id, this.state.notes, this.state.terms, this.state.po_number, this.state.invoice_number,
    //   this.state.selectedDate,this.state.selectedDueDate,this.state.LineInputArr.length,this.state.LineInputArr,this.state.total_amount,
    //   this.state.outstanding_amount,this.state.discount_type,this.state.discount_flag,this.state.discount_percentage,this.state.discount_amount)

    // this.props.
    // create_invoice(Number(this.state.selectClient.currency[0].id),
    //   this.state.selectClient.contacts[0].first_name, this.state.selectClient.contacts[0].last_name, this.state.selectClient.address[0].address_1, 
    //   this.state.selectClient.address[0].address_2,this.state.selectClient.address[0].city,this.state.selectClient.address[0].state, this.state.selectClient.address[0].country[0].name,
    //   this.state.selectClient.address[0].zipcode, Number(this.state.selectClient.id), this.state.notes, this.state.terms, this.state.po_number, this.state.invoice_number,
    //   this.state.selectedDate,this.state.selectedDueDate,this.state.LineInputArr.length,JSON.stringify(this.state.LineInputArr),this.state.total_amount,
    //   Number(this.state.outstanding_amount),Number(this.state.discount_type),this.state.discount_flag,Number(this.state.discount_percentage),Number(this.state.discount_amount))

    if (this.state.selectClient && Object.keys(this.state.selectClient.contacts).length > 0 )
    {
      var fName = this.state.selectClient.contacts[0].first_name ? this.state.selectClient.contacts[0].first_name : "";
      var lName = this.state.selectClient.contacts[0].last_name ? this.state.selectClient.contacts[0].last_name : "";
      
        this.state.first_name = fName;
        this.state.last_name = lName;

        console.log("fn" , fName);
        console.log("ln", lName);

    }
    else {
      console.log("else part runn")
        this.state.first_name = "";
        this.state.last_name = "";

        Alert.alert(
          'Alert',
          "Please select client",
          [
            {text: 'OK'},
          ],
          {cancelable: false},
        );
    }
     
    // console.log("this.state.LineInputArr.length", this.state.LineInputArr.length)
    //console.log("this.state.LineInputArr.arrray", JSON.stringify(this.state.LineInputArr))
    
      this.props.
    create_invoice(Number(this.state.selectClient.currency[0].id),
    this.state.first_name, this.state.last_name, this.state.selectClient.address[0].address_1, 
      this.state.selectClient.address[0].address_2,this.state.selectClient.address[0].city,this.state.selectClient.address[0].state, this.state.selectClient.address[0].country[0].name,
      this.state.selectClient.address[0].zipcode, Number(this.state.selectClient.id), this.state.notes, this.state.terms, this.state.po_number, this.state.invoice_number,
      this.state.selectedDate,this.state.selectedDueDate,this.state.LineInputArr.length,JSON.stringify(this.state.LineInputArr),this.state.total_amount,
      Number(this.state.outstanding_amount),Number(this.state.discount_type),this.state.discount_flag,Number(this.state.discount_percentage),Number(this.state.discount_amount))

  .then(({ data}) => {
    console.log("Create Invoice 2445421 data: " + JSON.stringify(data)); 

    if(data.CreateInvoice.errors)
    {
      Alert.alert(
        'Invoease',
        data.CreateInvoice.errors,
        [
          {text: 'OK'},
        ],
        {cancelable: false},
      );
    }

    else if ({ data }) {
        console.log("Data found",data)

        Alert.alert(
          'Invoease',
          'Create Invoice successfully.',
          [
            {text: 'OK', onPress: () => this.goBack(data)},
          ],
          {cancelable: false},
        );
    }
  }).catch((error) => {
    console.log('my error :' + error);
    Alert.alert(
      'Invoease',
      error.message,
      [
        
      ],
      {cancelable: false},
    );
  })
  };

  componentWillMount() {
    newSubtotal = 0.0
    subtotalCount = 0.0
    this.props.navigation.setParams({ createInvoiceAsync: this._createInvoiceAsync });
  }

  storeState = () => {
      console.log("store state called")
      this.setState({total_amount : Number(this.state.newSubtotal) + Number(this.state.Tax)})
      this.setState({outstanding_amount : Number(this.state.total_amount) - Number(this.state.amount_paid)})
  }

  addMore1 = () => {
    console.log('Add More11111 Call')
    this.animatedValue.setValue(0);

    let newlyAddedValue = { index: this.index }

    console.log(newlyAddedValue)

    storage.load({
      key: 'selectItem',

      autoSync: true,

      syncInBackground: true,

      syncParams: {
        extraFetchOptions: {
          // blahblah
        },
        someFlag: true
      }
    })
      .then(ret => {
        // found data go to then()
        // console.log('we found data Invoease Detail');
        //  console.log("ret" , ret);

        //this.setState({ disabled: true, valueArray: [...this.state.valueArray, ret] }, () => {
        this.setState({ valueArray:[]});
        this.setState({ disabled: true, valueArray: ret }, () => {  
          Animated.timing(
            this.animatedValue,
            {
              toValue: 1,
              duration: 500,
              useNativeDriver: true
            }
          ).start(() => {
            this.index = this.index + 1;
            this.setState({ disabled: false });

            //console.log(...this.state.valueArray)

          });
        });
        
        // console.log("addMore1 array length ============= ",  this.state.valueArray.length)
        //console.log(ret.states);

        // var mystates = []
        // ret.states.map(state => mystates.push(state.name) );
        // this.setState({ stateLabel : mystates, stateOptions: mystates  });
      })
      .catch(err => {
        // any exception including data not found
        // goes to catch()
        console.log('error home' + err);
        console.warn(err.message);
        switch (err.name) {
          case 'NotFoundError':
            // TODO;
            break;
          case 'ExpiredError':
            // TODO
            break;
        }
      });
  }

  addMore = () => {
    console.log('Add More Call')
    this.animatedValue.setValue(0);

    let newlyAddedValue = { index: this.index }

    console.log(newlyAddedValue)

    storage.load({
      key: 'selectItem',

      autoSync: true,

      syncInBackground: true,

      syncParams: {
        extraFetchOptions: {
          // blahblah
        },
        someFlag: true
      }
    })
    
      .then(ret => {
        // found data go to then()
        console.log('we found data Invoease Detail');
         console.log("ret" , ret);


        //this.setState({ disabled: true, valueArray: [...this.state.valueArray, ret] }, () => {
        this.setState({ disabled: true, valueArray: ret }, () => {  
          Animated.timing(
            this.animatedValue,
            {
              toValue: 1,
              duration: 500,
              useNativeDriver: true
            }
          ).start(() => {
            // this.index = this.index + 1;
            this.setState({ disabled: false });

            //console.log(...this.state.valueArray)

          });
        });
        //console.log("addMore array length ============= ",  this.state.valueArray.length)
        //console.log(ret.states);

        // var mystates = []
        // ret.states.map(state => mystates.push(state.name) );
        // this.setState({ stateLabel : mystates, stateOptions: mystates  });
      })
      .catch(err => {
        // any exception including data not found
        // goes to catch()
        console.log('error home' + err);
        console.warn(err.message);
        switch (err.name) {
          case 'NotFoundError':
            // TODO;
            break;
          case 'ExpiredError':
            // TODO
            break;
        }
      });
  }

  _moveToEditProductScreen = async (i) => {

    // console.log("i" , i)
    // console.log("item p1 whole array =",this.state.valueArray)
    // console.log("item p1 =",this.state.valueArray[i][i])
    // this.setState(selectedIndex: parseInt(i));
    this.state.selectedIndex = i
    console.log("iii =" ,this.state.valueArray[i])
    this.props.navigation.navigate('EditProduct', {selectedItem:this.state.valueArray[i],
      selectedProduct:this.state.valueArray[i],
      comeFromCreateInvoice:"insideLine",
      onNavigateBack: this.handleOnNavigateBackEDIT.bind(this)
      // onNavigateBack: this.handleOnNavigateBackFromEditProduct.bind(this)
    })
  }  

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
  _showDateTimeDuePicker = () => this.setState({ isDateTimePickerDueVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });
  _hideDateTimeDuePicker = () => this.setState({ isDateTimePickerDueVisible: false });

  _handleDatePicked = (date) => {

    console.log('A date has been Date of issue picked: ', date);

    var Day = date.getDate()
    var Month = date.getMonth() + 1
    var Year = date.getFullYear()

    console.log('A date has been picked: ' + Day);
    console.log('A date has been picked: ' + Month);
    console.log('A date has been picked: ' + Year);

    if (Day < 10) {
      Day = '0' + Day
    }
    if (Month < 10) {
      Month = '0' + Month 
    }

    this.setState({ selectMonth: Month })
    this.setState({ selectDay: Day })
    this.setState({ selectYear: Year })
    this.setState({ selectedDate: Month+"/"+Day+"/"+Year })
    this._hideDateTimePicker();
  };
  _handleDueDatePicked = (date) => {

    console.log('A date has been Due date picked: ', date);

    var Day = date.getDate()
    var Month = date.getMonth() + 1
    var Year = date.getFullYear()

    console.log('A date has been picked: ' + Day);
    console.log('A date has been picked: ' + Month);
    console.log('A date has been picked: ' + Year);

    if (Day < 10) {
      Day = '0' + Day
    }
    if (Month < 10) {
      Month = '0' + Month
    }

    this.setState({ selectDueMonth: Month })
    this.setState({ selectDueDay: Day })
    this.setState({ selectDueYear: Year })

    this.setState({ selectedDueDate: Month+"/"+Day+"/"+Year })

    this._hideDateTimeDuePicker();
  };


  handleOnNavigateBackEDIT = (item) => 
  {
    console.log("handleOnNavigateBackEDIT",item);
    // console.log("item invoice detail",item)
    //input {name: "product 3", description: undefined, rate: 20, tax_name: "", tax: "0", …}
    //input {name: "product 3", description: undefined, rate: 20, tax_name: "", tax: "0", …}
    console.log("selected Index " , this.state.selectedIndex)

    listSelectItem[this.state.selectedIndex] = item
    //console.log("listSelectItem after" , listSelectItem)

    //listSelectItem[0] = item

    //console.log("listSelectItem",listSelectItem)
    storage.save({
      key: 'selectItem', // Note: Do not use underscore("_") in key!
      data: listSelectItem,
     
      // if expires not specified, the defaultExpires will be applied instead.
      // if set to null, then it will never expire.
      expires: null //1000 * 3600
    });
    // console.log('handleOnNavigateBack call product list')
    // console.log(listSelectItem)

    this.setState({ newSubtotal: Number(0.00) });

    // console.log("listSelectItem length" , listSelectItem.length)
    subtotalCount = 0.0

    // console.log("quantity = ",item.quantity)
    // console.log("Unit cost = ",item.price)

    listSelectItem.map((item, key) => {
      // console.log("Q * P" , math.eval(Number(item.quantity) * Number(item.price)))
      //subtotalCount += math.eval(Number(item.quantity) * Number(item.price));
      subtotalCount += Number(item.quantity) * Number(item.unit_cost);
      // console.log("subtotal" , subtotalCount)
    })
    // console.log("subtotal count" , subtotalCount)
  
    this.setState({ newSubtotal: Number(subtotalCount)});

    this.state.total_amount = Number(newSubtotal)
    this.setState({ total_amount: Number(newSubtotal)});
    
    // console.log(this.state.newSubtotal)

    this.addMore1()
  }

  handleOnNavigateBack = (item) => {
    console.log("handleOnNavigateBack",item);
    // console.log("item invoice detail",item)
    //input {name: "product 3", description: undefined, rate: 20, tax_name: "", tax: "0", …}
    //input {name: "product 3", description: undefined, rate: 20, tax_name: "", tax: "0", …}

    listSelectItem.push(item)
    console.log("listSelectItem ha" , item)
    // console.log("listSelectItem",listSelectItem)
    storage.save({
      key: 'selectItem', // Note: Do not use underscore("_") in key!
      data: listSelectItem,
     
      // if expires not specified, the defaultExpires will be applied instead.
      // if set to null, then it will never expire.
      expires: null //1000 * 3600
    });
    // console.log('handleOnNavigateBack call product list')
    // console.log(listSelectItem)

    this.setState({ newSubtotal: Number(0.00) });

    //console.log("listSelectItem length" , listSelectItem.length)
    subtotalCount = 0.0
    sum=0

    // console.log("quantity = ",item.quantity)
    // console.log("Unit cost = ",item.price)

    listSelectItem.map((item, key) => {
      //console.log("Q * P" , math.eval(Number(item.quantity) * Number(item.price)))
      //subtotalCount += math.eval(Number(item.quantity) * Number(item.price));
      subtotalCount += Number(item.quantity) * Number(item.unit_cost);
      sum=sum+(Number(item.unit_cost)*Number(item.quantity)*Number(item.tax1_amt))/100;
      //console.log("subtotal" , subtotalCount)
    })

    
    // console.log("subtotal count" , subtotalCount)
    console.log("new sub total is------>",subtotalCount)
    console.log("new sum of tax is------>",sum)

    this.setState({ newSubtotal: Number(subtotalCount)});
    this.setState({Tax:sum});

    this.state.total_amount = Number(newSubtotal)
    this.setState({ total_amount: Number(newSubtotal)});
    
    // console.log(this.state.newSubtotal)

    this.addMore()
  }
 
  gotoProductList = () => {
    console.log('In ProductScreen screen');
    this.props.navigation.navigate('ProductScreen', {
      comeFromCreateInvoice:"true",
      onNavigateBack: this.handleOnNavigateBack.bind(this)
    })
  };

  handleOnNavigateBackFromClient = (listSelectItem) => {
    console.log('handleOnNavigateBackFromClient call')
    console.log("select client is ====>>> " , listSelectItem)
    console.log("select client end")
    this.setState({ selectClient: listSelectItem });
  }

  gotoClientList = () => {
    console.log('Client List function call');

    this.props.navigation.navigate('ClientScreen', {
      onNavigateBack: this.handleOnNavigateBackFromClient.bind(this),
      isFromInvoiceDetail: "TRUE",
    })
  };

  doSomethingWith(base64String)
  {
    console.log("base64String")
  }

  _pickImage = async () => {
    console.log('pick image called');
    // if(!this.state.logoimage)
    // {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
      base64:true,
    });

    console.log("result uri" , JSON.stringify(result.uri));
    console.log("result base64" , JSON.stringify(result.base64));


    if (!result.cancelled) {
      this.setState({ logoimage: result.base64});
      
      // ImgToBase64.getBase64String('file://youfileurl')
      // .then(base64String => doSomethingWith(base64String))
      // .catch(err => doSomethingWith(err));

    }
     
      // ImgToBase64.getBase64String(result.uri)
      //     .then(base64String => {
           
      //       console.log(base64String)
      //       //this.navigateToResult(shareImageBase64);
      //     })
      //     .catch(err => console.log("Error in ImgToBase64", err));
      // }
      
    }

  async componentDidMount() {
    console.log('componentDidMount CALL')
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    this.setState({ newSubtotal: 0 });
    this.setState({ hasCameraPermission: status === 'granted' });
  }

_openKeyBoard()
{
  
}

updateState(e)
{
  console.log('tax' + JSON.stringify(e));
  var newSubTotal1 = Number(this.state.newSubtotal) + Number(e);
  console.log('new subtotal 1 : ' + Number(newSubTotal1));
  this.setState({Tax:e, total_amount:Number(newSubTotal1)}); 
  //this.setState({Tax} , (total_amount) => this.setState(Number(this.state.newSubtotal) + Number(this.state.Tax)))
}

collectTax(mytax)
{
  console.log("this is mytax",mytax)
  
}



calculateTax(tax,linetotal)
{
  console.log("tax is -> ",tax ,"line total is=>",linetotal)
  this.state.sum=this.state.sum+((linetotal*tax)/100)
  console.log("taxxxxxxxx---->",this.state.sum)

}

  render() {
    
    console.log("Invoice Detail Screen => RENDER METHOD CALLED")
    const buttons = ['%', '$']
    const { selectedIndex } = this.state

    // console.log('newSubtotal state : ', this.state.newSubtotal);
    // console.log("this.state.valueArray" , this.state.valueArray)
    let { logoimage, selectClient } = this.state;

    const animationValue = this.animatedValue.interpolate(
      {
        inputRange: [0, 1],
        outputRange: [-59, 0]
      });
      
    this.state.LineInputArr = []
    //  console.log("array length ============= ",  this.state.valueArray.length)
    //  console.log("array value ============= ",  this.state.valueArray)
    let newArray = this.state.valueArray.map((item, key) => 
    {
       console.log("item value ============= ",  item) 
      //'task' => 'TEST Devloper','description' => 'TEST Development','rate' => '100.00','tax_name' => 'C-GST','tax' => '10.00','qty' => '1.00','total' => '110.00'
      

      //var input = {"task":item[key].name, "description": "","rate":item[key].price,"tax_name":"GST","tax":"0","qty":item[key].quantity,"total": item[key].quantity * item[key].price};

      // var input = {"task":item.name, "description": item.description,"rate":item.unit_cost,"tax_name":"GST","tax":item.tax1_amt,"qty":item.quantity,"total": item.quantity * item.unit_cost};

      var input = {"task":item.name, "description": item.description,"rate":item.unit_cost,"tax_name":"c-GST","tax":"10","qty":item.quantity,"total": item.quantity * item.unit_cost};

      var result = {};

      //console.log("key =",key)
      this.state.LineInputArr.push(input)

      if ((key) == this.index) {
        return (
          <Animated.View key={key} style={[styles.viewHolder1, { opacity: this.animatedValue, transform: [{ translateY: animationValue }] }]}>
            {/* <Text style={styles.text}>Row {item.index}</Text> */}

            <View style={styles.description}>
              <View style={{ flex: 1 }}>
                <Text style={styles.descriptiontext1}>{item.name} </Text>
                <Text h5 style={styles.unitprice}> {item.quantity} at {item.unit_cost}.00 </Text>
              </View>

              <TouchableOpacity onPress={ () => this._moveToEditProductScreen(key)} key={key}>
                <View style={{ marginTop: 10, backgroundColor: 'white', borderColor: 'grey', borderRadius: 5, borderWidth: 2, borderStyle: 'solid', dashGap:0,width:50,height:50,justifyContent:'center', alignItems:'center'}}>
                  <Text h3 style={styles.addline1}> {item.tax1_amt} </Text>
                  {this.collectTax(item.tax1_amt)}
                </View>

              </TouchableOpacity>
              <View style={{ flex: 1 }}>
                <Text style={styles.descriptiontext2}>{math.eval(Number(item.quantity) * Number(item.unit_cost))}</Text>
              </View>
            </View>
            {this.calculateTax(item.tax1_amt,math.eval(Number(item.quantity) * Number(item.unit_cost)))}
          </Animated.View>
        );
      }
      else {
        return (
          <View key={key} style={styles.viewHolder1}> 
            {/* <Text style={styles.text}>Row {item.index}</Text>  1 at 1.00,00 */}
            <View style={styles.description}>
              <View style={{ flex: 1 }}>
                <Text style={styles.descriptiontext1}> {item.name} </Text>
                <Text h5 style={styles.unitprice}> {item.quantity} at {item.unit_cost}.00 </Text>
              </View>
              
              {/* <View style={{ flex: 1 }}>
                <TextInput style={styles.descriptiontext2}> {item[key].tax1_id} </TextInput>
              </View> */} 
              
              <TouchableWithoutFeedback onPress={ () => this._moveToEditProductScreen(key)} key={key}>
                <View style={{ marginTop: 10, backgroundColor: 'white', borderColor: 'grey', borderRadius: 5, borderWidth:2, borderStyle: 'solid', dashGap:0,width:50,height:50 ,justifyContent:'center',alignItems:'center'}}  >
                  <Text h3 style={styles.addline1}>{item.tax1_amt}</Text>
                </View>
              </TouchableWithoutFeedback>

              <View style={{ flex: 1 }}>
                <Text style={styles.descriptiontext2}>{math.eval(Number(item.quantity) * Number(item.unit_cost))}</Text>
              </View>
            </View>
          </View>
        );
      }
    });

    return (
     
      <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={Platform.select({ios: 0, android: 80})} style={styles.container}>
        <ScrollView>
          <View style={{ flex: 1, backgroundColor: '#fff', paddingLeft: 10, paddingRight: 10 }}>
            {/*<View style={styles.container}>*/}
            {/* <TouchableOpacity onPress={() => this._pickImage()}>
              {!logoimage &&
                <View style={styles.logo}>
                  <Text h3 style={styles.text} style={{ alignItems : 'center' }}> + Add a Logo</Text>
                </View>
              }
              {logoimage &&
                <View style={styles.logo}>
                  <Image source={{ uri: logoimage }} style={{ width: 180, height: 50 }} />
                </View>
              }
            </TouchableOpacity> */}
            <View style={{ flex:1, flexDirection: 'row' }}>
              <View style={{ width: '55%', }}>
                <View style={styles.description}>
                  <View style={{ marginTop: 20, flex:1 }}>
                    <Text style={styles.welcome1}>Billed To</Text>

                    <TouchableOpacity onPress={() => this.gotoClientList()} style={{ flex: 1 }}>
                      {!selectClient &&
                        <View style={{ flex:1,alignItems:'flex-start', marginTop: 10, marginLeft: 0, backgroundColor: 'white', borderColor: 'grey', borderRadius: 5, borderWidth:2 , borderStyle: 'dashed', dashGap: 3 , width: 180, justifyContent: 'center', }}>
                          <Text h3 style={styles.text} style={{ alignItems : 'center' }}> + Add a Client</Text>
                        </View>
                      }
                      {selectClient &&
                        <View style={{ flex:1 , marginTop: 10, marginLeft: 0, backgroundColor: 'white', width: 180, alignItems:'flex-start',justifyContent:'flex-start', }}>
                          <Text h3 style={styles.text} style={{ alignItems : 'center', fontSize: 16, color: 'black' }}> {selectClient.company_name}</Text>

                          {selectClient.address[0].address_1 !== "" &&
                            <Text h3 style={styles.text} style={{ alignItems : 'center', fontSize: 16, color: 'black' }}> {selectClient.address[0].address_1}</Text>
                          }
                          {selectClient.address[0].address_2 !== "" &&
                            <Text h3 style={styles.text} style={{ alignItems : 'center', fontSize: 16, color: 'black' }}> {selectClient.address[0].address_2}</Text>
                          }
                          {selectClient.address[0].city !== "" &&
                            <Text h3 style={styles.text} style={{ alignItems : 'center', fontSize: 16, color: 'black' }}> {selectClient.address[0].city}</Text>
                          }
                          {selectClient.address[0].country[0].name  !== "" &&
                            <Text h3 style={styles.text} style={{ alignItems : 'center', fontSize: 16, color: 'black' }}> {selectClient.address[0].country[0].name}</Text>
                          }
                        </View>
                      }
                    </TouchableOpacity>



                  </View>
                </View>
              </View>

              <View style={{ width: '5%', }} />

              <View style={{ width: '40%' }}>
                <View style={styles.description}>
                  <View style={{ marginTop: 20, flex:1 }}>
                    <Text style={styles.welcome2}>Invoice Number</Text>
                    <TextInput
                      style={{ fontSize: 16, textAlign: 'right', color: 'black' }}
                      onChangeText={(invoice_number) => this.setState({invoice_number})}
                      value={this.state.invoice_number}
                      placeholder="0000001"
                    />
                    
                  </View>
                </View>

                <View style={{ marginTop: 10 }}>
                  <Text style={styles.welcome}>Date of Issue</Text>
                  {/* <TextInput
                    style={{ fontSize: 16, textAlign: 'right', color: 'black' }}
                    placeholder="Mar 12, 2019"
                  /> */}

                  <TouchableOpacity onPress={this._showDateTimePicker}>
                    <Text style={{ fontSize: 16, textAlign: 'right', color: 'black' }}>{this.state.selectDay}/{this.state.selectMonth}/{this.state.selectYear}</Text>
                  </TouchableOpacity>
                  <DateTimePicker isVisible={this.state.isDateTimePickerVisible} onConfirm={this._handleDatePicked} onCancel={this._hideDateTimePicker} />

                </View>

                <View style={{ marginTop: 10 }}>
                  <Text style={styles.welcome}>Due Date</Text>
                  {/* <TextInput
                    style={{ fontSize: 16, textAlign: 'right', color: 'black' }}
                    placeholder="Apr 11, 2019"
                  /> */}
                  <TouchableOpacity onPress={this._showDateTimeDuePicker}>
                    <Text style={{ fontSize: 16, textAlign: 'right', color: 'black' }}>{this.state.selectDueDay}/{this.state.selectDueMonth}/{this.state.selectDueYear}</Text>
                  </TouchableOpacity>
                  <DateTimeDuePicker isVisible={this.state.isDateTimePickerDueVisible} onConfirm={this._handleDueDatePicked} onCancel={this._hideDateTimeDuePicker} />
                </View>

              </View>

            </View>

            <View style={{ marginTop: 10 }}>
              <Text style={styles.welcome}>Reference</Text>
              <TextInput
                style={{ fontSize: 16, textAlign: 'right', color: 'black' }}
                onChangeText={(po_number) => this.setState({po_number})}
                value={this.state.po_number}
                placeholder="Add Ref. (e.g. PO #)"
              />
            </View>

            <View style={{ borderBottomColor: 'green', borderBottomWidth: 3, marginBottom: 10, marginTop: 20 }}></View>
            <View style={styles.description}>
              <View style={{ flex: 1 }}>
                <Text style={styles.welcome1}> Description </Text>
              </View>
              <View style={{flex:1}}>
                <Text style={styles.welcome3}> Tax </Text>
              </View>
              <View style={{ flex: 1 }}>
                <Text style={styles.welcome2}> Line Total </Text>
              </View>
            </View>

            <View style={{ flex: 1, padding: 4 }}>
              {
                newArray
              }
            </View>
            
            <TouchableOpacity activeOpacity={0.8} disabled={this.state.disabled} onPress={this.gotoProductList}>
              <View style={{ marginTop: 10, backgroundColor: 'white', borderColor: 'grey', borderRadius: 5, borderWidth:2, borderStyle: 'dashed', dashGap:3 }} >
                <Text h3 style={styles.addline}> + Add a Line</Text>
              </View>
            </TouchableOpacity>

            <View style={{ flex: 1, flexDirection: 'row', borderBottomColor: '#999', borderBottomWidth: 1, paddingBottom: 10 }}>
              <View style={{ width: '30%', }} />

              <View style={{ width: '70%', }} >
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{ width: '60%', }}>
                    <View style={{ marginTop: 10 }}>
                      <Text style={{ fontSize: 16, textAlign: 'right' }}>Subtotal</Text>
                    </View>
                  </View>
                  <View style={{ width: '40%', }}>
                    <View style={{ marginTop: 10 }}>
                      <Text style={{ fontSize: 16, textAlign: 'right', color: 'black' }}>{this.state.newSubtotal}</Text>
                      {/* <TextInput
                        style={{ fontSize: 16, textAlign: 'right', color: 'black' }}
                        placeholder="0.00"
                      /> */}
                    </View>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{ width: '60%', }}>
                    <View style={{ marginTop: 7}}>
                    {/* <Button title = 'Add a Discount' buttonStyle={styles.registerButton} color={'blue'} onPress={this._openKeyBoard()} /> */}
                     <Text style={styles.registerButton} onPress={this._openKeyBoard()}>Add a Discount</Text>   
                    </View>
                  </View>
                  <View style={{ width: '40%', }}>
                    <View style={{ marginTop: 10 }}>
                      {/* <Text style={{ fontSize: 16, textAlign: 'right', color: 'black' }}>{0.00}</Text> */}
                      {/* <TextInput
                        keyboardType = 'numeric'
                        style={{ fontSize: 16, textAlign: 'right', color: 'black' }}
                        // ref={(input) => { this.addDiscountTextInput = input; }}
                        placeholder="0.00"
                      /> */}
              

                <View>
                    <TouchableOpacity onPress={() => {}}>
                      {/* <TextInput style={{ fontSize: 16, textAlign: 'right' }} inputAccessoryViewID={inputAccessoryViewID} keyboardType = 'numeric'>0.00</TextInput> */}
                      <TextInput
                      style={{ fontSize: 16, textAlign: 'right', color: 'black' }}
                        onChangeText={(discount_amount) => this.setState({discount_amount})}
                        // inputAccessoryViewID={inputAccessoryViewID}
                        keyboardType = 'numeric'
                        value={this.state.discount_amount}
                        placeholder="0.00">
                      </TextInput>
                    </TouchableOpacity>
                  
                    {/* <TextInput
                    style={styles.default}
                    inputAccessoryViewID={inputAccessoryViewID}
                    onChangeText={(text) => this.setState({text})}
                    value={0.00}
                  /> */}

                  {/* <InputAccessoryView nativeID={inputAccessoryViewID}>
                   <View style={{backgroundColor: 'white', flex: 1, flexDirection: 'row', justifyContent: 'space-between', padding: 5}}>
                     <ButtonGroup
                       onPress={this.updateIndex}
                       selectedIndex={selectedIndex}
                       buttons={buttons}
                       containerStyle={{height: 40, width: '30%', borderColor: 'blue', borderWidth: 1}}
                       textStyle={{color: 'black'}}
                       selectedButtonStyle={{backgroundColor: 'blue'}}
                       selectedTextStyle={{color: 'white'}}
                     />
                     <View style={{justifyContent: 'center', marginRight: 15}}>
                       <TouchableOpacity onPress={this._showDateTimePicker}>
                         <Text style={{ fontSize: 16, textAlign: 'right', color: 'blue' }}>Done</Text>
                       </TouchableOpacity>
                     </View>
                   </View>
                 </InputAccessoryView> */}

                  </View>
                    </View>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{ width: '60%', }}>
                    <View style={{ marginTop: 7 }}>
                      <Text style={{ fontSize: 16, textAlign: 'right' }}>Tax</Text>
                    </View>
                  </View>
                  <View style={{ width: '40%', }}>
                    <View style={{ marginTop: 7 }}>
                     
                      <TextInput
                        style={{ fontSize: 16, textAlign: 'right', color: 'black' }}
                       // onChangeText={(Tax) => this.setState({Tax} , (total_amount) => this.setState(Number(this.state.newSubtotal) + Number(this.state.Tax)))}
                        onChangeText={(Tax)=> this.setState({Tax})}
                        value={this.state.Tax.toString()}
                        keyboardType = 'numeric'
                        placeholder="0.00"
                      />
                    </View>
                  </View>
                </View>
              </View>
            </View>

            <View style={{ flex: 1, flexDirection: 'row', borderBottomColor: '#999', borderBottomWidth: 2, paddingBottom: 10 }}>
              <View style={{ width: '30%', }} />

              <View style={{ width: '70%', }} >
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{ width: '60%', }}>
                    <View style={{ marginTop: 10 }}>
                      <Text style={{ fontSize: 16, textAlign: 'right' }}>Total</Text>
                    </View>
                  </View>
                  <View style={{ width: '40%', }}>
                    <View style={{ marginTop: 10 }}>
                      <Text style={{ fontSize: 16, textAlign: 'right', color: 'black' }}>{ Number(this.state.newSubtotal) - Number(this.state.discount_amount) + Number(this.state.Tax)}
                      </Text>
                      
                      {/* <TextInput
                        editable={false} selectTextOnFocus={false} 
                        style={{ fontSize: 16, textAlign: 'right', color: 'black' }}
                        onChangeText={(total_amount) => this.setState(total_amount)}
                        value={Number(this.state.newSubtotal) + Number(this.state.Tax)}
                        placeholder="0.00"
                      /> */}
                    </View>
                  </View>
                </View>

                {/* <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{ width: '63%', height: 30 }}>
                    <Text style={styles.discount}>Request a Deposit</Text>
                  </View>
                </View> */}

                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{ width: '60%', }}>
                    <View style={{ marginTop: 0 }}>
                      <Text style={{ fontSize: 16, textAlign: 'right' }}>Amount Paid</Text>
                    </View>
                  </View>
                  <View style={{ width: '40%', }}>
                    <View style={{ marginTop: 0 }}>
                      <TextInput
                        style={{ fontSize: 16, textAlign: 'right', color: 'black' }}
                        onChangeText={(amount_paid) => this.setState({amount_paid})}
                        value={this.state.amount_paid}
                        keyboardType = 'numeric'
                        placeholder="0.00"      
                      />
                    </View>
                  </View>
                </View>
              </View>
            </View>
                
            <View style={{ flex: 1, flexDirection: 'row', paddingBottom: 15 }}>
              <View style={{ width: '25%', }} />

              <View style={{ width: '75%', }}>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{ width: '62%', }}>
                    <View style={{ marginTop: 10 }}>
                      <Text style={{ fontSize: 16, color: 'green', textAlign: 'right' }}>Amount Due (INR)</Text>
                    </View>
                  </View>
                  <View style={{ width: '38%', }}>
                    <View style={{ marginTop: 10 }}>
                      <Text style={{ fontSize: 16, textAlign: 'right', color: 'black' }}>{Number(this.state.newSubtotal) - Number(this.state.discount_amount) + Number(this.state.Tax) - Number(this.state.amount_paid)}</Text>
                      
                      {/* <TextInput
                        editable={false} selectTextOnFocus={false} 
                        style={{ fontSize: 16, fontWeight: 'bold', textAlign: 'right', color: 'black' }}
                        onChangeText={(outstanding_amount) => this.setState({outstanding_amount})}
                        value={this.state.total_amount - this.state.amount_paid}
                        placeholder="0.00"
                      /> */}
                    </View>
                  </View>
                </View>
              </View>
            </View>
            
            <View style={{ flex: 1, flexDirection: 'row', paddingBottom: 15 }}>
              <View style={{ width: '100%', }}>
                <View style={{ marginTop: 10 }}>
                  <Text style={{ fontSize: 16, color: 'green', textAlign: 'left' }}>Notes</Text>
                </View>
                <View style={{ marginTop: 10 }}>
                  <TextInput
                    style={{ fontSize: 17, fontWeight: 'bold', textAlign: 'left', color: 'black' }}
                    onChangeText={(notes) => this.setState({notes})}
                    value={this.state.notes}
                    placeholder="Add Notes"
                  />
                </View>
              </View>
            </View>

            <View style={{ flex: 1, flexDirection: 'row', paddingBottom: 15 }}>
              <View style={{ width: '100%', }}>
                <View style={{ marginTop: 10 }}>
                  <Text style={{ fontSize: 16, color: 'green', textAlign: 'left' }}>Terms</Text>
                </View>
                <View style={{ marginTop: 10 }}>
                  <TextInput
                    style={{ fontSize: 17, fontWeight: 'bold', textAlign: 'left', color: 'black' }}
                    onChangeText={(terms) => this.setState({terms})}
                    value={this.state.terms}
                    placeholder="Add Terms"
                  />
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingLeft: 10,
    paddingRight: 10,
  },
  logo: {
    flex: 1,
    marginTop: 10,
    marginLeft: 'auto',
    marginRight: 'auto',
    backgroundColor: 'white',
    borderColor: 'grey',
    borderRadius: 5,
    borderWidth: 2,
    borderStyle: 'dashed',
    // dashGap: 3, 
    // alignItems: 'center', 
    width: 180,
    textAlign: 'left',
    justifyContent: 'center',
    height: 50
  },
  h5: {
    color: '#666',
    fontSize: 13,
  },
  text: {
    flex: 1,
    color: '#333',
    fontSize: 15,
  },
  welcome: {
    fontSize: 15,
    textAlign: 'right',
    marginTop: 10,
    marginBottom: 5,
    color: 'green',
  },
  input: {
    height: 50,
    borderColor: '#f00',
    borderWidth: 1,
    borderBottomColor: '#fff',
    color: '#333',
  },
  description: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 0,
    flexWrap: 'wrap',
  },
  welcome1: {
    fontSize: 15,
    textAlign: 'left',
    alignSelf: 'stretch',
    marginTop: 10,
    marginBottom: 5,
    color: 'green',
  },
  welcome2: {
    fontSize: 15,
    textAlign: 'right',
    alignSelf: 'stretch',
    marginTop: 10,
    marginBottom: 5,
    color: 'green',
  },
  addline: {
    flex: 1,
    padding:15,
    color: '#333',
    fontSize: 15,
    textAlign: 'left',
  },
  addline2: {
    flex: 1,
    color: '#333',
    fontSize: 15,
    textAlign: 'center',
  },
  discount: {
    fontSize: 16,
    textAlign: 'right',
    color: 'blue',
    marginTop: 5,
    marginBottom: 0
  },
  viewHolder:
  {
    height: 55,
    backgroundColor: '#26A69A',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 4
  },
  description: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
    marginTop: 0,
    flexWrap: 'wrap',
  },
  descriptiontext1: {
    fontSize: 17,
    textAlign: 'left',
    marginTop: 25,
    marginBottom: 5,
    color: '#333',
  },
  descriptiontext2: {
    fontSize: 17,
    textAlign: 'right',
    marginTop: 25,
    marginBottom: 5,
    color: '#333',
  },
  unitprice: {
    fontSize: 15,
    textAlign: 'left',
    alignSelf: 'stretch',
    marginBottom: 5,
    color: '#666',
  },
  registerButton: {
    // color: 'blue',
    fontSize: 16,
    padding: 0,
    marginRight:0,
    margin: 0,
    textAlign: 'right',
    backgroundColor: '#fff'
  },
  welcome3: {
    fontSize: 15,
    textAlign: 'center',
    alignSelf: 'stretch',
    marginTop: 10,
    marginBottom: 5,
    color: 'green',
  },
  // buttons: {
  //   color: 'red'
  // }
});


export default graphql(CREATE_INVOICE,
  {
    props: ({ mutate }) => ({
      create_invoice: ( currency_id,first_name,last_name,address1,address2,city,state,country,zipcode,client_id,notes,terms,po_number,invoice_number,invoice_date,invoice_due_date,total_line_count,line,total_amount,outstanding_amount,discount_type,discount_flag,discount_percentage,discount_amount) => mutate({ variables: {
         currency_id,first_name,last_name,address1,address2,city,state,country,zipcode,client_id,notes,terms,po_number,invoice_number,invoice_date,invoice_due_date,total_line_count,line,total_amount,outstanding_amount,discount_type,discount_flag,discount_percentage,discount_amount } })
    }),

    options: {
      refetchQueries: [{
          query:GET_INVOICE
      }
      ],
    }
  }
)(InvoiceDetailScreen)
