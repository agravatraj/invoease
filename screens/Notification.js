import React from 'react';

import { View, Text, StyleSheet, TextInput, ScrollView, Dimensions, TouchableOpacity, TouchableHighlight } from 'react-native';
import { Icon, CheckBox, Input, Button, ListItem, SearchBar, Divider, Header, ButtonGroup, Card } from 'react-native-elements';

export default class SNotification extends React.Component {
    
  static navigationOptions = {
    title: 'app.json',
    headerTitle: 'Invoice Email'
  };
  // constructor(){
  //   super();
  //   console.disableYellowBox = true;
  // }

  constructor () {
    super()
    this.state = {
      selectedEmailsms: 0,
      selectedEstimatepay: 0
    }
    this.updateEmailsms = this.updateEmailsms.bind(this)
    this.updateEstimatepay = this.updateEstimatepay.bind(this)
  }
  
  updateEmailsms (selectedEmailsms) {
    this.setState({selectedEmailsms})
  }
  updateEstimatepay (selectedEstimatepay) {
    this.setState({selectedEstimatepay})
  }

  render() {
    const emailsms = ['Email', 'SMS'];
    const estimatepay = ['Invoease', 'Estimate', 'Payment'];

    const { selectedEmailsms, selectedEstimatepay } = this.state
    
    return (
        <View style={styles.container}>
          <View style={{elevation: 4, backgroundColor: '#fff', shadowOffset: { width: 0, height: 1 }, shadowColor: 'black', shadowOpacity: 0.6, paddingTop: 20, paddingBottom: 20}}>
            <ButtonGroup
              onPress={this.updateEmailsms}
              selectedIndex={selectedEmailsms}
              buttons={emailsms}
              textStyle={{fontSize: 16}}
              selectedTextStyle={{color: '#fff'}}
              containerStyle={{height: 40, backgroundColor: '#fff', borderRadius: 10}}
              selectedButtonStyle={{backgroundColor: '#4285f4'}}
            />
            </View>
            <View style={{flex: 1, paddingTop: 20, paddingBottom: 10}}>
            <ButtonGroup
                onPress={this.updateEstimatepay}
                selectedIndex={selectedEstimatepay}
                buttons={estimatepay}
                textStyle={{fontSize: 16}}
                selectedTextStyle={{color: '#fff'}}
                containerStyle={{height: 40, backgroundColor: '#fff', borderRadius: 10}}
                selectedButtonStyle={{backgroundColor: '#4285f4'}}
              />
              <Text style={styles.notificationSubject}>Hello, We have generated an invoice for you amounting. To view your invoice, or to download a PDF copy for your records, click the link below: We value your relationship with us. For any questions, feel free to get in touch with us.</Text>
              {/* <Button title="Save" style={styles.saveButton} buttonStyle={{paddingTop: 8, paddingRight: 8, paddingBottom: 8, paddingLeft: 8}} /> */}
          </View>

          <TouchableOpacity style={styles.registerButton}>
            <Text style={styles.registerbtn}>Save</Text>
          </TouchableOpacity>

        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  notificationSubject: {
    color: '#666',
    fontSize: 18,
    padding: 15
  },
  registerButton: {
    alignItems: 'flex-end',
    flexDirection: 'row',
    marginBottom: 20
  },
  registerbtn: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#4285f4',
    padding: 15,
    margin: 15,
    marginBottom: 5,
    color: '#fff',
    fontSize: 24,
    textAlign: 'center',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    overflow: 'hidden'
  }
});