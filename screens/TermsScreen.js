import React from 'react';
import { Platform, ScrollView, StyleSheet, Text, View,Button,TouchableOpacity,Alert ,Image} from 'react-native';
import { SearchBar, List, ListItem } from 'react-native-elements'
import InvoiceDetail from './InvoiceDetail';
import { Query } from "react-apollo";
import { GET_TERMS } from "../graphqlQueries";
import { DESTORY_TERMS } from "../graphqlQueries";
import { graphql } from 'react-apollo';
//import Icon from 'react-native-vector-icons/FontAwesome';
import IconFont from 'react-native-vector-icons/FontAwesome';
import Swipeout from 'react-native-swipeout';
import SearchInput, { createFilter } from  'react-native-search-filter';
const KEYS_TO_FILTERS = ['name'];

class TermsScreen extends React.Component {

  //arrayholder = [{"title":"Taylor Swift","artist":"Taylor Swift","url":"https://www.amazon.com/Taylor-Swift/dp/B0014I4KH6","image":"https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Fearless","artist":"Taylor Swift","url":"https://www.amazon.com/Fearless-Enhanced-Taylor-Swift/dp/B001EYGOEM","image":"https://images-na.ssl-images-amazon.com/images/I/51qmhXWZBxL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Speak Now","artist":"Taylor Swift","url":"https://www.amazon.com/Speak-Now-Taylor-Swift/dp/B003WTE886","image":"https://images-na.ssl-images-amazon.com/images/I/51vlGuX7%2BFL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Red","artist":"Taylor Swift","url":"https://www.amazon.com/Red-Taylor-Swift/dp/B008XNZMOU","image":"https://images-na.ssl-images-amazon.com/images/I/41j7-7yboXL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"1989","artist":"Taylor Swift","url":"https://www.amazon.com/1989-Taylor-Swift/dp/B00MRHANNI","image":"https://images-na.ssl-images-amazon.com/images/I/717DWgRftmL._SX522_.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"}];
  //state = { invoices: [{"title":"Taylor Swift","artist":"Taylor Swift","url":"https://www.amazon.com/Taylor-Swift/dp/B0014I4KH6","image":"https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Fearless","artist":"Taylor Swift","url":"https://www.amazon.com/Fearless-Enhanced-Taylor-Swift/dp/B001EYGOEM","image":"https://images-na.ssl-images-amazon.com/images/I/51qmhXWZBxL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Speak Now","artist":"Taylor Swift","url":"https://www.amazon.com/Speak-Now-Taylor-Swift/dp/B003WTE886","image":"https://images-na.ssl-images-amazon.com/images/I/51vlGuX7%2BFL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"Red","artist":"Taylor Swift","url":"https://www.amazon.com/Red-Taylor-Swift/dp/B008XNZMOU","image":"https://images-na.ssl-images-amazon.com/images/I/41j7-7yboXL.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"},{"title":"1989","artist":"Taylor Swift","url":"https://www.amazon.com/1989-Taylor-Swift/dp/B00MRHANNI","image":"https://images-na.ssl-images-amazon.com/images/I/717DWgRftmL._SX522_.jpg","thumbnail_image":"https://i.imgur.com/K3KJ3w4h.jpg"}]};
  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Terms',
      headerTitle: 'Terms',
      headerRight: <TouchableOpacity onPress={navigation.getParam('gotoAddTerms')}><View style={{marginRight:20}}><IconFont name={'plus'} size={22} color={'#373f51'} style={{paddingRight:10}}/></View></TouchableOpacity>
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      searchTerm: '',
      rowItem: null,
      filteredTerms:[],
      flag:false
    }
  }

  handleOnNavigateBack = () => {
    console.log("handleOnNavigateBack" , filteredTerms.length);
       this.setState({flag : false})
  }

  _gotoAddTerms = async () => {
    this.props.navigation.navigate('AddTerms', {
      onNavigateBack: this.handleOnNavigateBack.bind(this)
    });
  };

  _gotoEditTermsScreen = async (i) => {
    console.log(filteredTerms[i])
    this.props.navigation.navigate('EditTerms',{selectTerms:filteredTerms[i]})
  };
  deleteRow(item) {
    //console.log(filteredServices)
    //console.log(JSON.stringify(item))
    console.log('Select ROW :' +  JSON.stringify(this.state.rowItem) )

    Alert.alert(
      'Are you sure?',
      'You want to delete this Term',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => 
          this.deleteTerms()
      },
      ],
      {cancelable: false},
    );

  }

  deleteTerms() {

    console.log(JSON.stringify(this.state.rowItem.id)) 

        this.props.
        destroy_term(this.state.rowItem.id)
      .then(({ data }) => {
        console.log("Delete Terms : " + JSON.stringify(data));
        if (data) {
          Alert.alert(
            'Invoease',
            'Term destory successfully.',
            [
              {text: 'OK'},
            ],
            {cancelable: false},
          );
        }
      })
  };
  checkEmpty()
  {
    this.setState({flag : true})
  }

  renderTerms() {
    return (
      <View style={{flex:1,}}>
        <SearchInput 
        onChangeText={(term) => { this.searchUpdated(term) }} 
        style={styles.searchInput}
        placeholder="Search Terms"
        clearIcon={<IconFont name={'search'} size={20} color={'#373f51'} style={{paddingRight:5}}/>}
        returnKeyType="search"
        />
        <ScrollView>      
            <List containerStyle={{ marginTop:0 }}>
              <Query query={GET_TERMS}>
                {({ loading, error, data }) => {
                  if (loading) return <Text>Loading...</Text>;
                  if (error) {
                    //  console.log("TEST ERR =>"+ error.graphQLErrors.map(x => x.message));
                    return <Text>Error :)</Text>;
                  }
                  if (data.terms.length == 0) 
                  {
                    console.log("array is empty")
                   this.checkEmpty()
                  }
                 
                  // console.log(
                  //   data.clients.map(({ company_name, contact } ) => [
                  //     company_name, contact =>{return '${contact.first_name} ${contact.last_name}'}
                  //   ])
                  // );

                  // console.log(  
                  //   data.terms.map(({ name }) => [
                  //     name
                  //   ])
                  // );

                  filteredTerms = data.terms.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))

                  let swipeBtns = [{
                    text: 'Delete',
                    backgroundColor: 'red',
                    //underlayColor: 'rgba(0, 0, 0, 1, 0.6)',
                    onPress: () => { this.deleteRow(filteredTerms[this.state.rowID]) }
                  }];

                  return (
                    <View>
                      {
                        filteredTerms.map((item, i) => (
                          <Swipeout right={swipeBtns}
                              // autoClose='true'
                              backgroundColor='transparent'
                              rowID={i}
                              key={i}
                              onOpen={(i) => {
                                console.log('VALUE OF ITEM :' + i)
                                this.setState({ rowItem: item })
                              }}>
                          <TouchableOpacity onPress={() => this._gotoEditTermsScreen(i)}>
                            <ListItem
                            key={i}
                            title={item.name}
                          />  
                          </TouchableOpacity>
                          </Swipeout>
                        ))
                      }
                    </View>)
                }}
              </Query>
            </List>
        </ScrollView>
      </View>
    )
  }

  handleSearch = (text) => {
    console.log("text", text)

    const newData = this.arrayholder.filter(invoice => {
      const itemData = '${invoices.artist.toUpperCase()} ${invoices.title.toUpperCase()}';
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });

    this.setState({ invoices: newData });
  };

  UNSAFE_componentWillMount() {
    console.log(`ServicesScreen screen ` + JSON.stringify(this.props.navigation));
    console.log(this.props.navigation)
    this.props.navigation.setParams({ gotoAddTerms: this._gotoAddTerms });
  }

  render() {

    console.log("Terms render method called.")

    return (
      <View style={styles.container}>
      {
        (this.state.flag == true
          ?
          <View style={{ height: 100, backgroundColor: '#EAE9EF',marginTop:25,}}>
            <Image source={require('../assets/images/term.png')} 
            style={{ width: 283, height: 66, position: 'absolute', right: 28, top: 5, }}></Image>
          </View>
          :         
            <View style={{flex: 1,}}>
              {/* <SearchBar lightTheme placeholder='Type Here...' onChangeText={this.handleSearch} />
          {this.renderTaxes()} */}
              {this.renderTerms()}
            </View>        
        )}
    </View>

      // <View style={{flex: 1}}>
      //   <View style={{flex: 0.4, backgroundColor: 'ccc'}}>
      //     {/* <SearchBar lightTheme placeholder='Type Here...' onChangeText={this.handleSearch} /> */}
      //     <SearchInput 
      //       onChangeText={(term) => { this.searchUpdated(term) }} 
      //       style={styles.searchInput}
      //       placeholder="Search Terms"
      //       clearIcon={<IconFont name={'search'} size={20} color={'#373f51'} style={{paddingRight:5}}/>}
      //       returnKeyType="search"
      //       />
      //   </View>
      //   <View style={{flex: 3.6, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center'}}>
      //     {this.renderTerms()}
      //   </View>
      // </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  // pendingbox: {
  //   flex: 1,
  //   flexDirection: 'row',
  //   borderBottomWidth: 1,
  //   borderColor: '#ccc'
  // },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  searchInput: {
    margin:10,
    paddingTop:1,
    paddingLeft:10,
    borderColor: '#CCC',
    height:40,
    borderWidth: 1,
    backgroundColor: 'white',
    borderRadius: 5,
  },
});

export default graphql(DESTORY_TERMS,
  {
    props: ({ mutate }) => ({
      destroy_term: (id) =>
        mutate({ variables: { id } })
    }),
    options: {
      refetchQueries: [{
        query: GET_TERMS
      }
      ],
    }

  }
)(TermsScreen)

