import React from 'react';
import { StyleSheet, View, Image, Text,AsyncStorage, SafeAreaView, Keyboard, KeyboardAvoidingView,ImageBackground, StatusBar, TouchableWithoutFeedback, TextInput, ScrollView,TouchableOpacity , Alert} from 'react-native';
import { Button } from 'react-native-elements'

import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import { Formik } from "formik";
import * as Yup from "yup";
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";

import { CHANGE_PASSWORD } from "../graphqlQueries";
import { graphql } from 'react-apollo';

import Icon from 'react-native-vector-icons/FontAwesome';

import { LinearGradient } from 'expo';

const MyInput = compose(handleTextInput, withNextInputAutoFocusInput)(TextField);

const Form = withNextInputAutoFocusForm(ScrollView, {
  submitAfterLastInput: false
});

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .required()
    .email(),
  password: Yup.string()
    .required()
    .min(6, "Too short"),
  star: Yup.boolean()
    .required()
    .oneOf([true])
});

const initialState = {
  oldPassword: '',
  newPassword: '',
  confirmPassword: '',
  authentication_token: 'WER5SkhFQUtBUktVMko3NzZUQmhJWVBEVVA3akdMWkdtTHhGSGtHSFpqVT0tLXJHQ3JNSTlRZW1IYXJ6WVZ4RXlIc2c9PQ==--bc22b7d1a372ebc8e70f8f19a7ef091e45874a07',
};
class ChangePassword extends React.Component {

  static navigationOptions = {
    title: 'Change Password',
    headerTitle: 'Change Password',
    header: null,
    //   header: ({ state }) => ({
    //     left: <Button title={"Save"} onPress={state.params.handleBack} />
    // })
  };

  _back = async () => {
    this.props.navigation.goBack();
  };

  constructor(props) {
    super(props);
    this.state = { ...initialState };

    console.log("CHANGE PASSWORD")
    //this._retrieveAuthentication_token()

  }

  _retrieveAuthentication_token = async () => {
    try {
      const value = await AsyncStorage.getItem('authenticationToken');
      if (value !== null) {
        // We have data!!
        console.log('We have data! CHANGE PASSWORD');
        console.log(value.rawData)
        let data = JSON.parse(value)
        //console.log(data)
        this.setState( {authentication_token:data} ) 
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  componentDidMount() {

    const params = {
      right: (
        <Button
          onPress={() => this._back}
        >
        </Button>
      ),
    };
    this.props.navigation.setParams(params);
  }
  render() {
    return (
      <ImageBackground style={styles.imgBackground} source={require('../assets/images/login_bg.png')} resizeMode='cover'>
      <View style={styles.container}>
        <SafeAreaView style={styles.container}>
          <StatusBar barStyle="light-content" />
          
          <KeyboardAvoidingView behavior='padding' style={styles.container}>
            <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>
              <View style={styles.container}>
                <View style={styles.logoContainer}>
                  <Image style={styles.logo}
                    source={require('../assets/images/logo.png')}>
                  </Image>
                </View>

                <View>
                  <Text h2 style={styles.loginText}>Change Password?</Text>
                </View>

                <View style={styles.infoContainer}>
                    <View style={styles.searchSection}>
                      <Icon raised style={styles.searchIcon} name='lock' type='font-awesome' color='#000' size={20} />
                      <TextInput
                          style={styles.input}
                          placeholder="Old Password"
                          underlineColorAndroid="transparent"
                          returnKeyType='next'
                          value={this.state.oldPassword}
                          onChangeText={(oldPassword) => this.setState({ oldPassword })}
                          secureTextEntry={true}
                          autoCorrect={false}
                          ref={"txtPassword"}
                      />
                    </View>
                    <View style={{ height: 1, backgroundColor: '#eee' }}></View>
                    <View style={styles.searchSection}>
                    <Icon raised style={styles.searchIcon} name='lock' type='font-awesome' color='#000' size={20} />
                    <TextInput
                        style={styles.input}
                        placeholder="New Password"
                        underlineColorAndroid="transparent"
                        returnKeyType='next'
                        value={this.state.newPassword}
                        onChangeText={(newPassword) => this.setState({ newPassword })}
                        secureTextEntry={true}
                        autoCorrect={false}
                        ref={"txtPassword"}
                    />
                    </View>
                    <View style={{height: 1, backgroundColor: '#eee'}}></View>
                    <View style={styles.searchSection}>
                        <Icon raised style={styles.searchIcon} name='lock' type='font-awesome' color='#000' size={20} />
                        <TextInput
                          style={styles.input}
                          placeholder="Confirm Password"
                          underlineColorAndroid="transparent"
                          returnKeyType='go'
                          value={this.state.confirmPassword}
                          onChangeText={(confirmPassword) => this.setState({ confirmPassword })}
                          secureTextEntry={true}
                          autoCorrect={false}
                          ref={"txtPassword"}
                      />
                    </View>
                    <View style={styles.actionButon}>
                      <Icon raised style={styles.actionRight} name='chevron-right' type='font-awesome' color='#fff' size={17} />
                        </View>
                            
                            <View>
                                <TouchableOpacity onPress={this._changePasswordAsync}>
                                <Icon raised style={styles.actionRight} name='check' type='font-awesome' color='#fff' size={22} />
                                </TouchableOpacity>
                            </View>
                            
                        </View>

                <View style={{ flex: 1, flexDirection: 'row', marginTop: 50 }}>
                  <Button title="Back" buttonStyle={styles.registerButton} color={'#2089dc'} onPress={() => this.props.navigation.pop()} />
                </View>

                <View style={{ flex: 1, flexDirection: 'row', marginTop: 50 }}>
                  <Button title="Forwared" buttonStyle={styles.registerButton} color={'#2089dc'} onPress={this._changePasswordAsync} />
                </View>

              </View>
            </TouchableWithoutFeedback>
          </KeyboardAvoidingView>
          
        </SafeAreaView>
      </View>
      </ImageBackground>
    );
  }

  _changePasswordAsync = async () => {

    if (this.state.oldPassword == '') {
      Alert.alert("Please enter Old password.");
      return
    }

    if (this.state.password == '') {
      Alert.alert("Please enter Password.");
      return
    }

    if (this.state.confirmPassword == '') {
      Alert.alert("Please enter confirm password.");
      return
    }

    if (this.state.confirmPassword == this.state.password) {
      Alert.alert("Password and confirm password are not matched.");
      return
    }

    console.log(this.state)

    this.props.
    change_password(String(this.state.oldPassword), String(this.state.password), String(this.state.confirmPassword), String(this.state.authentication_token))
      .then(({ data }) => {

        if(data.password_confirmation.error){
          
        }

        if (data) {
          console.log(data)
          //console.log(data.forgotPassword.message)
          alert(data.changePassword.message)
          this._back();

          //  AsyncStorage.setItem('token', data.signIn.authentication_token)
          //   .then(() => {
          //     this.props.navigation.navigate('App');
          //   })
        }
      }).catch((error) => {
        console.log('my error :' + error);
      })
  };


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //backgroundColor: '#fdfdfd',
    flexDirection: 'column'
  },
  imgBackground: {
    flex: 1
  },
  logoContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    width: 266,
    height: 50
  },
  title: {
    color: '#f7c744',
    fontSize: 18,
    textAlign: 'center',
    marginTop: 5,
    opacity: 0.9
  },
  infoContainer: {
    elevation: 4,
    borderRadius: 50,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    backgroundColor: '#fff',
    width: '90%',
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowRadius: 7,
  },  
  // input: {
  //   height: 40,
  //   backgroundColor: 'rgba(0,0,0,0.2)',
  //   color: '#fff',
  //   marginBottom: 20,
  //   paddingHorizontal: 10
  // },
  containerButton: {
    flexDirection: 'row'
  },
  buttonContainer: {
    backgroundColor: '#373f51',
    marginTop: 15,
  },
  buttonForgotContainer: {
    marginTop: 15,
  },
  buttonSignupContainer: {
    marginTop: 30,
    backgroundColor: '#423231',
  },
  buttonText: {
    textAlign: 'center',
    color: 'rgb(32,53,70)',
    fontWeight: 'bold',
    fontSize: 18
  },
  searchSection: {
    flexDirection: 'row',
    borderTopColor: 'transparent',
    borderRightColor: 0,
    borderLeftColor: 0,
    borderBottomColor: 0,
    // borderColor: '#ccc',
    // borderWidth: 1
  },
  searchIcon: {
    padding: 15,
    position: 'relative'
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    color: '#424242',
  },
  imgBackground: {
    flex: 1
  },
  actionButon: {
    position: 'absolute',
    top: '34%',
    right: -20,
    borderRadius: 100,
    backgroundColor: '#2089dc',
    width: 50,
    height: 50,
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 15,
    //textAlign: 'center'
  },
  actionRight: {
    width: '100%'
  },
  forgotTxt: {
    width: '100%',
    color: '#666',
    textAlign: 'right',
    paddingTop: 20,
    paddingRight: 20,
    fontSize: 15
  },
  loginButton: {
    fontSize: 20,
    paddingHorizontal: 25,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowRadius: 7,
    borderTopLeftRadius: 20,
    borderBottomLeftRadius: 20,
    marginRight: -15,
    backgroundColor: '#fff',
  },
  registerButton: {
    elevation: 3,
    //fontSize: 20,
    paddingHorizontal: 25,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowRadius: 7,
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    marginLeft: -15,
    backgroundColor: '#fff',
  },
  loginText: {
    fontSize: 32,
    color: '#333',
    fontWeight: 'bold',
    width: '100%',
    textAlign: 'center',
    marginBottom: 20
  }
});

export default graphql(CHANGE_PASSWORD,
  {
    props: ({ mutate }) => ({
      change_password: (old_password,password,password_confirmation,authentication_token) => mutate({ variables: { old_password,password,password_confirmation,authentication_token} })
    }),

  }
)(ChangePassword)