import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  AsyncStorage,
  View,
  TouchableWithoutFeedback,
} from 'react-native';
import { WebBrowser } from 'expo';

import { List, ListItem, Button, SearchBar, Avatar } from 'react-native-elements';

import { Query } from "react-apollo";
import { GET_ACCOUNT_USERS } from "../graphqlQueries";
import DropdownAlert from 'react-native-dropdownalert';

import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator
} from 'react-native-indicators';

import SearchInput, { createFilter } from 'react-native-search-filter';
import IconFont from 'react-native-vector-icons/FontAwesome';

const KEYS_TO_FILTERS = ['first_name', 'last_name'];

export default class UserList extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Users',
      headerTitle: 'Users',
      headerRight: <TouchableOpacity onPress={navigation.getParam('gotoAddClient')}><View style={{ marginRight: 20 }}><IconFont name={'plus'} size={22} color={'#373f51'} style={{ paddingRight: 10 }} /></View></TouchableOpacity>
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      searchTerm: '',
      flag:false
    }

    const token = async () => {
      try {
        const value = await AsyncStorage.getItem('token');
        if (value !== null) {
          // We have data!!
          console.log('value : ' + value);
        }
      } catch (error) {
        // Error retrieving data
        console.log('error' + error);
      }
    }
    console.log('token client' + token);
  }

  handleOnNavigateBack = () => {
        this.setState({flag : false})
  }


  itemAction(item) {
    console.log('in item action')
    switch (item.type) {
      case 'close':
        this.forceClose();
        break;
      default:
        const random = Math.floor(Math.random() * 1000 + 1);
        const title = item.type + ' #' + random;
        this.dropdown.alertWithType(item.type, title, item.message);
    }
  }
  forceClose() {
    this.dropdown.close();
  }
  onClose(data) {
    console.log(data);
  }
  onCancel(data) {
    console.log(data);
  }
  _gotoAddClient = async () =>
   {
    this.props.navigation.navigate('AddUser', {
      onNavigateBack: this.handleOnNavigateBack.bind(this)
    });
  }

  UNSAFE_componentWillMount() {
    this.props.navigation.setParams({ gotoAddClient: this._gotoAddClient });
  }

  searchUpdated(term) {
    this.setState({ searchTerm: term })
  }

  //   actionOnSelectClient(item) {

  //     const { navigation } = this.props;
  //     const isFromInvoiceDetail = navigation.getParam('isFromInvoiceDetail', 'NO-UPDATE-ITEM');
  //     console.log(isFromInvoiceDetail)

  //     if (isFromInvoiceDetail === "TRUE")
  //     {
  //       console.log('Selected Item :', item);
  //       this.props.navigation.state.params.onNavigateBack(item)
  //       this.props.navigation.pop()
  //     }
  //   }

  actionOnSelectUser(item) {

    const { navigation } = this.props;
    const isFromInvoiceDetail = navigation.getParam('isFromInvoiceDetail', 'NO-UPDATE-ITEM');
    console.log("sdf", item)

    if (isFromInvoiceDetail === "TRUE") {
      console.log('Selected Item :', item);
      this.props.navigation.state.params.onNavigateBack(item)
      this.props.navigation.pop()
    }
    else {
      //console.log('go to client detail screen :', item);
      this.props.navigation.navigate('EditUser', { selectedUser: item })
      //this.props.navigation.navigate('ClientDetail')
    }
  }
  checkEmpty()
  {
    this.setState({flag : true})
  }
  renderUser() {

    //const filteredEmails = emails.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))

    return (
      
      <View>
          <SearchInput
          onChangeText={(term) => { this.searchUpdated(term) }}
          style={styles.searchInput}
          placeholder="Search users"
          clearIcon={<IconFont name={'search'} size={20} color={'#373f51'} style={{ paddingRight: 5 }} />}
          returnKeyType="search"
          />

        <ScrollView style={{ marginBottom: 40, marginTop: 10 }}>
          <List containerStyle={{ marginBottom: 20, marginTop: 0, backgroundColor: 'transparent', borderColor: 'transparent' }}>
            <Query query={GET_ACCOUNT_USERS}>
              {({ loading, error, data }) => {
                //this.dropdown.alertWithType('error', 'Error', "Please check log");
                if (loading) return <PulseIndicator color='#4235cc' />;
                if (error) {

                  this.dropdown.alertWithType('error', 'Error', "Please check log");
                  //console.log("TEST ERR =>" + error.graphQLErrors.map(x => x.message));
                  return <Text>Error :)</Text>;
                }
                // console.log("heyyyyy",data)

                if (data.account_users.length == 0) {
                    this.checkEmpty()
                }

                //console.log('Data '+ JSON.stringify(data))
                filteredEmails = data.account_users.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))
                filteredEmails.map((item, i) => (
                  console.log('hello ' + JSON.stringify(item))
                ));
                return (
                  <View>
                    {
                      //data.clients.map((item, i) => (
                      filteredEmails.map((item, i) => (
                        // <TouchableOpacity onPress={() => console.log('yes')} key={i}>
                        <TouchableOpacity onPress={() => this.actionOnSelectUser(item)} key={i}>
                          <ListItem
                            key={i}
                            //title="test"
                            title={`${item.first_name} ${item.last_name}`}
                            titleStyle={{ fontSize: 18, fontWeight: 'bold' }}
                            subtitle={item.mobile}
                            wrapperStyle={{
                              backgroundColor: '#fff', alignItems: 'center',
                              borderRadius: 15, height: 80, paddingLeft: 14, paddingRight: 6

                            }}
                            containerStyle={{
                              marginBottom: 0, marginTop: 0,
                              paddingTop: 0, paddingBottom: 10,
                              borderBottomColor: 'transparent',
                              borderBottomWidth: 0,
                              shadowColor: '#999999',
                              shadowOffset: { width: 0, height: 2 },
                              shadowOpacity: 0.5,
                              shadowRadius: 3,
                            }}
                            noBorder
                            avatar={<Avatar
                              size="large"
                              width={55}
                              rounded
                              title={`${item.first_name.substring(0, 1)}${item.last_name.substring(0, 1)}`}
                              backgroundColor='#4f2'
                              onPress={() => console.log("Works!")}
                              activeOpacity={0.7}

                            />}
                          />
                        </TouchableOpacity>
                      ))
                    }
                  </View>)
              }}
            </Query>
          </List>
          <DropdownAlert ref={ref => this.dropdown = ref} />

        </ScrollView>
      </View>
    );
  }
  render() 
  {
    return (
      <View>
        {
          (this.state.flag == true
            ?
            <View style={{ height: 100, backgroundColor: '#EAE9EF' ,marginTop:25}}>
              <Image source={require('../assets/images/createuser.png')} 
              style={{ flex:1 ,width: 287, height: 67, position: 'absolute', right: 28, top: 5, }}></Image>
            </View>
            :
            <ScrollView>
              {/* <SearchBar lightTheme placeholder='Type Here...' onChangeText={this.handleSearch} />
              {this.renderProducts()} */}
              {this.renderUser()}
            </ScrollView>
          )
        }
      </View>
    );
  }

  _maybeRenderDevelopmentModeWarning() {
    if (__DEV__) {
      const learnMoreButton = (
        <Text onPress={this._handleLearnMorePress} style={styles.helpLinkText}>
          Learn more
        </Text>
      );

      return (
        <Text style={styles.developmentModeText}>
          Development mode is enabled, your app will be slower but you can use useful development
          tools. {learnMoreButton}
        </Text>
      );
    } else {
      return (
        <Text style={styles.developmentModeText}>
          You are not in development mode, your app will run at full speed.
        </Text>
      );
    }
  }

  _handleLearnMorePress = () => {
    WebBrowser.openBrowserAsync('https://docs.expo.io/versions/latest/guides/development-mode');
  };

  _handleHelpPress = () => {
    WebBrowser.openBrowserAsync(
      'https://docs.expo.io/versions/latest/guides/up-and-running.html#can-t-see-your-changes'
    );
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
  searchInput: {
    paddingTop: 1,
    marginTop: 10,
    paddingLeft: 10,
    marginLeft: 10,
    marginRight: 10,
    borderColor: '#CCC',
    height: 40,
    borderWidth: 1,
    backgroundColor: 'white',
    borderRadius: 5,
  }
});
