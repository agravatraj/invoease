import React from 'react';

import {
  StyleSheet, AsyncStorage, View, SafeAreaView, StatusBar, Keyboard, KeyboardAvoidingView,
  TouchableWithoutFeedback, Image, TextInput, ScrollView, Alert, Text, TouchableOpacity
} from 'react-native';
import { Button } from 'react-native-elements'

import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import { Formik } from "formik";
import * as Yup from "yup";
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";

import { UPDATE_USER } from "../graphqlQueries";
import { graphql } from 'react-apollo';

import * as Permissions from 'expo-permissions';

import SimplePicker from 'react-native-simple-picker';

import ReactNativePickerModule from 'react-native-picker-module';

var FloatingLabel = require('react-native-floating-labels');

const MyInput = compose(handleTextInput, withNextInputAutoFocusInput)(TextField);

const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

const Form = withNextInputAutoFocusForm(ScrollView, {
  submitAfterLastInput: false
});

const validationSchema = Yup.object().shape({

  firstname: Yup.string().required(),

  lastname: Yup.string().required(),

  username: Yup.string().required(),

  email: Yup.string()
    .required()
    .email(),
  password: Yup.string()
    .required()
    .min(6, "Too short"),
  confirmPassword: Yup.string().oneOf(
    [Yup.ref('password')],
    'Passwords do not match',
  ),
  star: Yup.boolean()
    .required()
    .oneOf([true])
});


//class ProfileScreen extends React.Component {
class ProfileScreen extends React.Component {
  // static navigationOptions = {
  //   title: 'My Profile',
  //   headerRight: <TouchableOpacity onPress={this._updateUserAsync}><View style={{marginRight:20}}><Text style={{color: '#000', fontSize: 18,}}>Save</Text></View></TouchableOpacity>
  // };

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'My Profile',
      headerTitle: 'My Profile',
      headerRight: <TouchableOpacity onPress={navigation.getParam('updateUserAsync')}><View style={{ marginRight: 20 }}><Text style={{ color: '#000', fontSize: 18, }}>Save</Text></View></TouchableOpacity>
    };
  };

  constructor(props) {
    super(props)

    this.state = {
      firstname: '',
      lastname: '',
      username: '',
      address1: '',
      address2: '',
      zipcode: '',
      country: '',
      country_id: '3',
      state1: '',
      email: '',
      timezone: '',
      timezone_id: '',
      language: '',
      language_id: '',
      arrLanguages: [],
      arrTimezones: [],
      arrCountries: [],
      selectedLanguageID: '',
      filteredLanguages: [],
      filteredTimezones: [],
      filteredCountries: [],
      selectedLanguage: 'English',
      selectedCountry: 'India',
      selectedTimeZone: 'IN',
      user_avatar:'',
      selectedValue: null,
      currency_id:''
    };
    this._retrieveUserData()
  }


  UNSAFE_componentWillMount() {
    this.props.navigation.setParams({ updateUserAsync: this._updateUserAsync });
    this._retrieveTotalLanguages()
    this._retrieveTotalTimezones()
    this._retrieveTotalCountries()
  }

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    this.setState({ hasCameraPermission: status === 'granted' });

  }

  _retrieveTotalLanguages = async () => {
    console.log('In Language Function')

    storage.load({
      key: 'totalLanguages',
      autoSync: true,
      syncInBackground: true,

      // you can pass extra params to the sync method
      // see sync example below
      syncParams: {
        extraFetchOptions: {
          // blahblah
        },
        someFlag: true
      }
    })
      .then(ret => {
        // found data go to then()
        console.log("retrive total languages called", ret);

        this.setState({ arrLanguages: ret }, () => {

          this.state.arrLanguages.languages.map((item, i) => (
            this.state.filteredLanguages.push(item.name)
          ))
        });

      })
      .catch(err => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case 'NotFoundError':
            // TODO;

            break;
          case 'ExpiredError':
            // TODO
            break;
        }
      });
  };

  _retrieveTotalTimezones = async () => {
    console.log('In Timezone Function')

    storage.load({
      key: 'totalTimezones',
      autoSync: true,
      syncInBackground: true,

      // you can pass extra params to the sync method
      // see sync example below
      syncParams: {
        extraFetchOptions: {
          // blahblah
        },
        someFlag: true
      }
    })
      .then(ret => {
        // found data go to then()
        console.log('retrive total timezone called',ret);
        //console.log(ret);

        this.setState({ arrTimezones: ret }, () => {
          this.state.arrTimezones.timezones.map((item, i) => (
            this.state.filteredTimezones.push(item.name)
          ))
        });

      })
      .catch(err => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case 'NotFoundError':
            // TODO;

            break;
          case 'ExpiredError':
            // TODO
            break;
        }
      });
  };

  _retrieveTotalCountries = async () => {
    console.log('In Contry Function')

    storage.load({
      key: 'totalCountries',
      autoSync: true,
      syncInBackground: true,

      // you can pass extra params to the sync method
      // see sync example below
      syncParams: {
        extraFetchOptions: {
          // blahblah
        },
        someFlag: true
      }
    })
      .then(ret => {
        // found data go to then()
        console.log('retrive total countries',ret);
        //console.log(ret);

        this.setState({ arrCountries: ret }, () => {
          this.state.arrCountries.countries.map((item, i) => (

            this.state.filteredCountries.push(item.name)
          ))
        });

      })
      .catch(err => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case 'NotFoundError':
            // TODO;

            break;
          case 'ExpiredError':
            // TODO
            break;
        }
      });

  };

  _retrieveUserData = async () => {
    console.log('In User Data Function')

    storage.load({ 
      key: 'usersdata',
      autoSync: true,
      syncInBackground: true,

      // you can pass extra params to the sync method
      // see sync example below
      syncParams: {
        extraFetchOptions: {
          // blahblah
        },
        someFlag: true
      }
    })
      .then(ret => {
        // found data go to then()
        console.log('retrive user data called',ret);
        console.log("timezone value:",ret.timezone[0].id)
        console.log("language value:",ret.account[0].language[0].id)
        console.log("currency value:",ret.account[0].currency[0].id)
      
         this.setState({
         // firstname:ret.first_name ? ret.first_name:'',
        //lastname:ret.last_name ? ret.last_name:'' , 
         // username:ret.username ? ret.username : '', 
         // timezone_id:ret.timezone[0].id ? ret.timezone[0].id : '',
          //language_id:ret.account[0].language[0].id ? ret.account[0].language[0].id : '',
         // currency_id:ret.account[0].currency[0].id ? ret.account[0].currency[0].id : '',
          // address1:ret.address[0].address_1 ? ret.address[0].address_1 : '',
          // address2:ret.address[0].address_2 ? ret.address[0].address_2: '',
          // zipcode:ret.address[0].zipcode?ret.address[0].zipcode:'',
          // country:ret.country?ret.country:'',
          // country_id:ret.country_id?ret.country_id:'',
          // state1:ret.address[0].state?ret.address[0].state:'',
          // email:ret.email?ret.email:'',
          // timezone:ret.timezone?ret.timezone:'',
          // language:ret.language?ret.language:'',

        })


      })
      .catch(err => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case 'NotFoundError':
            // TODO;

            break;
          case 'ExpiredError':
            // TODO
            break;
        }
      });
  };

  render() {
    // console.log("filteredLanguage ",this.state.filteredLanguages)

    return (
      <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={Platform.select({ios: 0, android: 80})} style={styles.container}>
        <SafeAreaView style={styles.container}>
          <StatusBar barStyle="light-content" />
          <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>
            <View style={styles.container}>
              <Formik
                onSubmit={values => alert(JSON.stringify(values, null, 2))}
                validationSchema={validationSchema}
                initialValues={{ star: true }}>

                <Form style={styles.infoContainer} keyboardShouldPersistTaps='always'>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={styles.paymentdate}>
                      <FloatingLabel
                        labelStyle={styles.labelInput}
                        inputStyle={styles.input}
                        style={styles.formInput}
                        value={this.state.firstname}
                        onChangeText={(firstname) => this.setState({ firstname })}
                        keyboardType='default'
                        returnKeyType='next'
                        autoCorrect={false}
                        onSubmitEsiting={() => this.refs.txtLastName.focus()}
                        ref={"txtFirstName"}
                        onBlur={this.onBlur}>

                        First Name
                                      </FloatingLabel>
                    </View>
                    <View style={styles.paymentdate}>
                      <FloatingLabel
                        labelStyle={styles.labelInput}
                        inputStyle={styles.input}
                        style={styles.formInput}
                        value={this.state.lastname}
                        onChangeText={(lastname) => this.setState({ lastname })}
                        keyboardType='default'
                        returnKeyType='next'
                        autoCorrect={false}
                        onSubmitEsiting={() => this.refs.txtUsername.focus()}
                        ref={"txtLastName"}
                        onBlur={this.onBlur}>

                        Last Name
                                      </FloatingLabel>
                    </View>
                  </View>

                  <View style={styles.payamounttitle}>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.username}
                      onChangeText={(username) => this.setState({ username })}
                      keyboardType='default'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtAddress.focus()}
                      ref={"txtUsername"}
                      onBlur={this.onBlur}>

                      User Name
                                    </FloatingLabel>
                  </View>

                  <View style={{ flexDirection: 'row' }}>
                    <View style={styles.paymenttype}>
                      <FloatingLabel
                        labelStyle={styles.labelInput}
                        inputStyle={styles.input}
                        style={styles.formInput}
                        value={this.state.address1}
                        onChangeText={(address1) => this.setState({ address1 })}
                        keyboardType='default'
                        returnKeyType='next'
                        autoCorrect={false}
                        onSubmitEsiting={() => this.refs.txtAddress2.focus()}
                        ref={"txtAddress"}
                        onBlur={this.onBlur}>

                        Address1
                                      </FloatingLabel>
                    </View>
                    <View style={styles.paymentdate}>
                      <FloatingLabel
                        labelStyle={styles.labelInput}
                        inputStyle={styles.input}
                        style={styles.formInput}
                        value={this.state.address2}
                        onChangeText={(address2) => this.setState({ address2 })}
                        keyboardType='default'
                        returnKeyType='next'
                        autoCorrect={false}
                        onSubmitEsiting={() => this.refs.txtZipCode.focus()}
                        ref={"txtAddress2"}
                        onBlur={this.onBlur}>

                        Address2
                                      </FloatingLabel>
                    </View>
                  </View>

                  <View style={styles.payamounttitle}>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.zipcode}
                      onChangeText={(zipcode) => this.setState({ zipcode })}
                      keyboardType='numeric'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtCountry.focus()}
                      ref={"txtZipCode"}
                      onBlur={this.onBlur}>

                      Zipcode
                                    </FloatingLabel>
                  </View>

                  {/* <View style={styles.payamounttitle}>
                                    <FloatingLabel
                                      labelStyle={styles.labelInput}
                                      inputStyle={styles.input}
                                      style={styles.formInput}
                                      value={this.state.country}
                                      onChangeText={(country) => this.setState({ country })}
                                      keyboardType='default'
                                      returnKeyType='next'
                                      items={this.state.filteredCountries}
                                      autoCorrect={false}
                                      onSubmitEsiting={() => this.refs.txtState.focus()}
                                      ref={"txtCountry"}
                                      onBlur={this.onBlur}>
                                      
                                      Country
                                    </FloatingLabel>
                                  </View> */}

                  <View style={styles.languagetitle}>
                    <Text style={styles.amounttitle}>Country</Text>

                    <View>
                      <Text style={styles.amount} onPress={() => { this.refs.country.show(); }}>{this.state.selectedCountry}
                      </Text>
                    </View>
                    <SimplePicker
                      ref={'country'}
                      options={this.state.filteredCountries}
                      labels={this.state.filteredCountries}
                      itemStyle={{
                        color: '#444',
                        fontSize: 20
                      }}
                      onSubmit={(CountryOptions) => {
                        this.setState({ selectedCountry: CountryOptions, country: CountryOptions });
                      }}
                    />

                    {/* <Text style={styles.pickerTitle} onPress={() => {this.countryRef.show()}}>{this.state.selectedCountry}</Text>
                                    <ReactNativePickerModule
                                      pickerRef={e => this.countryRef = e}
                                      value={this.state.selectedValue}
                                      title={"Select a Country"}
                                      items={this.state.filteredCountries}
                                      onValueChange={(CountryOptions) => {
                                        this.setState({ selectedCountry: CountryOptions, country: CountryOptions});
                                      }}
                                    /> */}
                  </View>

                  <View style={styles.payamounttitle}>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.state1}
                      onChangeText={(state1) => this.setState({ state1 })}
                      keyboardType='default'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtEmail.focus()}
                      ref={"txtState"}
                      onBlur={this.onBlur}>

                      State
                                    </FloatingLabel>
                  </View>

                  <View style={styles.payamounttitle}>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.email}
                      onChangeText={(email) => this.setState({ email })}
                      keyboardType='email-address'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtTimeZone.focus()}
                      ref={"txtEmail"}
                      onBlur={this.onBlur}
                      editable={false}>

                      Email
                      </FloatingLabel>
                  </View>


                  <View style={styles.languagetitle}>
                    <Text style={styles.amounttitle}>Timezone</Text>

                    <View>
                      <Text style={styles.amount} onPress={() => { this.refs.timezone.show(); }}>{this.state.selectedTimeZone}
                      </Text>
                    </View>
                    <SimplePicker
                      ref={'timezone'}
                      options={this.state.filteredTimezones}
                      labels={this.state.filteredTimezones}
                      itemStyle={{
                        color: '#444',
                        fontSize: 20
                      }}
                      onSubmit={(TimeZoneOptions) => {
                        this.setState({ selectedTimeZone: TimeZoneOptions, timezone: TimeZoneOptions });
                      }}
                    />

                    {/* <Text style={styles.pickerTitle} onPress={() => {this.timeZoneRef.show()}}>{this.state.selectedTimeZone}</Text>
                                    <ReactNativePickerModule
                                      pickerRef={e => this.timeZoneRef = e}
                                      value={this.state.selectedValue}
                                      title={"Select a Timezone"}
                                      items={this.state.filteredTimezones}
                                      onValueChange={(TimeZoneOptions) => {
                                        this.setState({ selectedTimeZone: TimeZoneOptions, timezone: TimeZoneOptions});
                                      }}
                                    /> */}
                  </View>

                  <View style={styles.languagetitle}>
                    <Text style={styles.amounttitle}>Language</Text>

                    <View>
                      <Text style={styles.amount} onPress={() => { this.refs.language.show(); }}>{this.state.selectedLanguage}
                      </Text>
                    </View>
                    <SimplePicker
                      ref={'language'}
                      options={this.state.filteredLanguages}
                      labels={this.state.filteredLanguages}
                      itemStyle={{
                        color: '#444',
                        fontSize: 20
                      }}
                      onSubmit={(LanguageOptions) => {
                        this.setState({ selectedLanguage: LanguageOptions, language: LanguageOptions });
                      }}
                    />

                    {/* <Text style={styles.pickerTitle} onPress={() => {this.pickerRef.show();}}>{this.state.selectedLanguage}</Text>
                                   
                                    <ReactNativePickerModule
                                      pickerRef={e=>this.pickerRef=e}
                                      value={this.state.selectedValue}
                                      title={"Select a language"}
                                      items={this.state.filteredLanguages}
                                      onValueChange={(LanguageOptions) => {
                                        this.setState({ selectedLanguage: LanguageOptions, language: LanguageOptions});
                                      }}
                                    /> */}
                  </View>
                  {/*<Button raised
                                    buttonStyle={styles.buttonContainer}
                                    //icon={{ name: 'fingerprint' }}
                                    title='Save'
                                    onPress={this._updateUserAsync} />*/}
                </Form>
              </Formik>
            </View>
          </TouchableWithoutFeedback>

        </SafeAreaView>
      </KeyboardAvoidingView>

    );
  }



  _updateUserAsync = async () => {

    if (this.state.firstname == '') {
      Alert.alert("Please enter firstname.");
      return
    }

    if (this.state.lastname == '') {
      Alert.alert("Please enter lastname.");
      return
    }

    if (this.state.username == '') {
      Alert.alert("Please enter username.");
      return
    }

    if (reg.test(this.state.email) === false) {
      Alert.alert("Please enter valid Email.");
      return
    }

    console.log("save press")
    console.log("zipcode" , this.state.zipcode)
    console.log("lleeeeeeeeee")

    // var data = {"task":"dfg", "description": "dfg","rate":"dfgdfg"};

    var data={"first_name":this.state.firstname, 
      "last_name":this.state.lastname, 
      "username":this.state.username,
     // "address_1":this.state.address1,
     // "address_2":this.state.address2,
     // "zipcode":this.state.zipcode,
     // "country":this.state.selectedCountry,
     // "country_id":this.state.country_id,
     // "state":this.state.state1,
     // "email":this.state.email,
     // "timezone":this.state.selectedTimeZone,
      "timezone_id":this.state.timezone_id,
      "language_id":this.state.language_id,
      "currency_id":this.state.currency_id,
      //"language":this.state.selectedLanguage
    }

    storage.save({
      key: 'usersdata', // Note: Do not use underscore("_") in key!
      data:data,
      
      // if expires not specified, the defaultExpires will be applied instead.
      // if set to null, then it will never expire.
      expires: 1000 * 3600
    });
    console.log("dddddddddddddd",data)
  
    //this._retrieveUserData()

    this.props.
        update_user(this.state.firstname,
            this.state.lastname,
            this.state.username,
            this.state.address1,
            this.state.address2,
            this.state.zipcode,
            Number(this.state.country_id),
            this.state.state1,
            this.state.email,
            Number(this.state.timezone_id),
            Number(this.state.language_id),
            this.state.user_avatar,

        )
        .then(({ data }) => {
            console.log("new data : " + JSON.stringify(data));
            if (data) {
                console.log("my new data" , data)

                Alert.alert(
                    'Invoease',
                    'Profile updated Successfully.',
                    [
                        { text: 'OK', onPress: () => this.props.navigation.pop() },
                    ],
                    { cancelable: false },
                );
            }
        }).catch((error) => {
            console.log('my error :' + error);
        })
  };

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f8f9'
  },
  paytitle: {
    color: '#333',
    fontSize: 26,
    fontWeight: '600',
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 15
  },
  paytext: {
    color: '#999',
    fontSize: 18,
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 15
  },
  payamounttitle: {
    borderWidth: 0.5,
    borderColor: '#ccc',
    backgroundColor: '#fff',
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 5,
  },
  languagetitle: {
    borderWidth: 0.5,
    borderColor: '#ccc',
    backgroundColor: '#fff',
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 15,
  },
  amounttitle: {
    color: '#ccc',
    fontSize: 16
  },
  amount: {
    marginTop: 5,
    color: '#444',
    fontSize: 21
  },
  paymenttype: {
    width: '50%',
    borderWidth: 0.5,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 0,
    borderColor: '#ccc',
    backgroundColor: '#fff',
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 5
  },
  paymentdate: {
    width: '50%',
    borderWidth: 0.5,
    borderTopWidth: 0,
    borderBottomWidth: 0,
    borderColor: '#ccc',
    backgroundColor: '#fff',
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 5
  },
  buttonContainer: {
    marginTop: 15,
    marginBottom: 15,
    backgroundColor: '#2089dc',
  },
  labelInput: {
    color: '#ccc',
    fontSize: 14,
    paddingLeft: 10
  },
  formInput: {
    // borderBottomWidth: 1, 
    paddingLeft: 0,
    borderColor: '#ccc'
  },
  input: {
    borderWidth: 0
  }
});

export default graphql(UPDATE_USER,
  {
    props: ({ mutate }) => ({
      update_user: (first_name, last_name, username, address_1, address_2, zipcode, country_id, state, email, timezone_id, language_id,user_avatar,currency_id) => mutate({ variables: { first_name, last_name, username, address_1, address_2, zipcode, country_id, state, email, timezone_id, language_id ,user_avatar,currency_id} })
    }),

  }
)(ProfileScreen)