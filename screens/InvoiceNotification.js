import React from 'react';

import { RefreshControl, View, Text, StyleSheet, TextInput, ScrollView, Dimensions } from 'react-native';
import { Icon, CheckBox, Input, Button, ListItem, SearchBar, Divider, Header, ButtonGroup, Card } from 'react-native-elements';

import Timeline from 'react-native-timeline-theme'
//import Icon from 'react-native-vector-icons/MaterialIcons';
import IconFont from 'react-native-vector-icons/FontAwesome';
import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator, 
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator
} from 'react-native-indicators';

import { Query } from "react-apollo";
//import { graphql } from 'react-apollo';
import { GET_INVOICE_NOTIFICATION } from "../graphqlQueries";


export default class InvoiceNotification extends React.Component {

  static navigationOptions = {
    title: 'app.json',
    headerTitle: 'Notifications'
  };
  constructor(props) {
    super(props);

    this.state = {
      waiting: 0,
      isRefreshing: 0,
      data: [],
      invoiceId: this.props.navigation.state.params.invoiceId,
      mydata: [],
      time:''
    }

    
    console.log("Invoease ID :" + this.state.invoiceId)

    this.data = [
      {
        time: '09:00', title: 'Created', description: 'Created by you.',
        renderIcon: () => <IconFont name={'envelope-open-o'} size={25} color={'#517fa4'} />,
        lineColor: '#517fa4',
        titleStyle: { color: '#517fa4' },
      },
      {
        time: '10:45', title: 'Payment', description: 'Payment added by Ronak Bhatt $ 100.0',
        renderIcon: () => <IconFont name={'money'} size={25} color={'#8c38'} />,
        lineColor: '#8c38',
        titleStyle: { color: '#8c38' },
      },
      {
        time: '12:00', title: 'Seen', description: 'Viewed by Ronak Bhatt.',
        renderIcon: () => <IconFont name={'eye'} size={25} color={'#006064'} />,
        lineColor: '#006064',
        titleStyle: { color: '#006064' },
      },
      {
        time: '14:00', title: 'Sent', description: 'Sent via email to  Ronak Bhatt  by you for 1100.0.',
        renderIcon: () => <IconFont name={'send'} size={25} color={'#6064'} />,
        lineColor: '#6064',
        titleStyle: { color: '#6064' },
      },
      {
        time: '16:30', title: 'Download', description: 'Download by Ronak Bhatt.',
        renderIcon: () => <IconFont name={'file-pdf-o'} size={25} color={'#4ecdc4'} />,
        lineColor: '#4ecdc4',
        titleStyle: { color: '#4ecdc4' },
      },
      {
        time: '18:30', title: 'Print', description: 'Printed by Ronak Bhatt.',
        renderIcon: () => <IconFont name={'print'} size={25} color={'#a770ef'} />,
        lineColor: '#a770ef',
        titleStyle: { color: '#a770ef' },
      },
      {
        time: '19:14', title: 'Update', description: 'Updated by you.',
        renderIcon: () => <IconFont name={'exchange'} size={25} color={'#7F7F7F'} />,
        lineColor: 'transparent',
        titleStyle: { color: '#7F7F7F' },
      }
    ]
  }

  onRefresh = () => {
    //set initial data
    console.log('On Refresh');
  }

  onEndReached = () => {
    //fetch next data
  }
  renderFooter = () => {
    //show loading indicator
    if (this.state.waiting) {
      return <WaveIndicator color='#4235cc' />;
    } else {
      return <Text>~</Text>;
    }
  }

  notificationDataArr(item) 
  {
    console.log("notificationDataArr called")
    console.log("item value",item)
    var input;

    if (item.activity_type == 'create') {
        input = {
          time: item.create_time , title: 'Created', description: item.activiy_text,
          renderIcon: () => <IconFont name={'envelope-open-o'} size={25} color={'#517fa4'} />,
          lineColor: '#517fa4',
          titleStyle: { color: '#517fa4' },
        }
    }
    else if (item.activity_type == 'payment') {
      input = {
        time: item.create_time, title: 'Payment', description: item.activiy_text,
        renderIcon: () => <IconFont name={'money'} size={25} color={'#8c38'} />,
        lineColor: '#8c38',
        titleStyle: { color: '#8c38' },
      }
    }
    else if (item.activity_type == 'seen') {
    
      input = {
        time: item.create_time, title: 'Seen', description: item.activiy_text,
        renderIcon: () => <IconFont name={'eye'} size={25} color={'#006064'} />,
        lineColor: '#006064',
        titleStyle: { color: '#006064' },
      }

    }
    else if (item.activity_type == 'sent') {
      input = {
          time: item.create_time, title: 'Sent', description: item.activiy_text,
          renderIcon: () => <IconFont name={'send'} size={25} color={'#6064'} />,
          lineColor: '#6064',
          titleStyle: { color: '#6064' },
        }
    }
    else if (item.activity_type == 'download') {
      input = {
        time: item.create_time , title: 'Download', description: item.activiy_text,
        renderIcon: () => <IconFont name={'file-pdf-o'} size={25} color={'#4ecdc4'} />,
        lineColor: '#4ecdc4',
        titleStyle: { color: '#4ecdc4' },
      }
    }
    else if (item.activity_type == 'print') {
      input = {
        time: item.create_time, title: 'Print', description: item.activiy_text,
        renderIcon: () => <IconFont name={'print'} size={25} color={'#a770ef'} />,
        lineColor: '#a770ef',
        titleStyle: { color: '#a770ef' },
      }

    }
    else if (item.activity_type == 'update') {
      input = {
        time: item.create_time, title: 'Update', description: item.activiy_text,
        renderIcon: () => <IconFont name={'exchange'} size={25} color={'#7F7F7F'} />,
        lineColor: 'transparent',
        titleStyle: { color: '#7F7F7F' },
      }
    }

    this.state.mydata.push(input)
  }

  render()
   {
     console.log("render method called")
    return (
      <View style={styles.container}>

        <Query query={GET_INVOICE_NOTIFICATION} variables={{ id: Number(this.state.invoiceId) }}>
          {({ loading, error, data }) => {
            if (loading) return <WaveIndicator color='#4235cc' />;
            if (error) {
              console.log("TEST ERR =>" + error.graphQLErrors.map(x => x.message));
              return <Text>{error.graphQLErrors.map(x => x.message)}Error :)</Text>;
            }

            console.log('notification data' + JSON.stringify(data));

            data.activity.map((item, i) => (

             this.notificationDataArr(item)

            ))
            
            console.log("length: ", this.state.mydata)

            if (this.state.mydata.length > 0) {
              return (
                <Timeline
                    data={this.state.mydata}
                    styleContainer={{ flex: 1, marginLeft: 10, paddingTop: 20 }}
                    timeContainerStyle={{ marginTop: 0, marginRight: 20 }}
                    timeStyle={{
                      textAlign: 'center', backgroundColor: '#e26a00', color: 'white',
                      padding: 5, borderRadius: 23

                    }}
                    descriptionStyle={{ color: 'gray', borderColor: 'white' }}
                    detailContainerStyle={{ paddingLeft: 10, marginLeft: 20 }}
                    flatListProps={{
                      style: {
                        paddingTop: 20, paddingLeft: 30,
                      }
                    }}
                    marginTopCircle={5}
                    dashLine={true}
                    isRenderSeperator={true}

                />
                )
            }
            else {
              return(
                <Text>No new Notifications </Text>
              )
            }
          }}
        </Query>  
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  create: {
    width: '68%',
    flex: 1,
    flexDirection: 'row',
    marginTop: 5,
    marginBottom: 5,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: '#eef0f1'
  },
  pending: {
    width: '69%',
    flex: 1,
    flexDirection: 'row',
    marginTop: 5,
    marginBottom: 5,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: '#f9f3e1'
  },
  success: {
    width: '69%',
    flex: 1,
    flexDirection: 'row',
    marginTop: 5,
    marginBottom: 5,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: '#e6f4d0'
  },
  createTitle: {
    fontSize: 16,
    textAlign: 'left',
    color: '#333'
  },
  createDate: {
    fontSize: 16,
    textAlign: 'center',
    marginTop: 1,
    marginLeft: 5,
    color: '#999'
  },
  verticalBorderCreate: {
    position: 'absolute',
    top: -20,
    left: '43%',
    backgroundColor: '#c4cdd3',
    width: 6,
    height: 42,
    zIndex: -123,
  },
  verticalBorderPending: {
    position: 'absolute',
    top: -20,
    left: '43%',
    backgroundColor: '#f6df92',
    width: 6,
    height: 42,
    zIndex: -123,
  },
  verticalBorderSuccess: {
    position: 'absolute',
    top: -20,
    left: '43%',
    backgroundColor: '#b5dc79',
    width: 6,
    height: 42,
    zIndex: -123,
  },
  horizontalBorderCreate: {
    position: 'absolute',
    top: '45%',
    left: 15,
    backgroundColor: '#c4cdd3',
    width: 42,
    height: 6,
    zIndex: -123,
  },
  horizontalBorderPending: {
    position: 'absolute',
    top: '45%',
    left: 15,
    backgroundColor: '#f6df92',
    width: 42,
    height: 6,
    zIndex: -123,
  },
  horizontalBorderSuccess: {
    position: 'absolute',
    top: '45%',
    left: 15,
    backgroundColor: '#b5dc79',
    width: 42,
    height: 6,
    zIndex: -123,
  },
});


// export default graphql(GET_INVOICE_NOTIFICATION, {
//   options: (props) => ({ variables: { id: 88 } })
// })( InvoiceNotification );