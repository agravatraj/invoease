//import liraries
import React, { Component } from 'react';
import { View, ImageBackground,Text, StyleSheet, AppRegistry, Alert,Image,Dimensions,AsyncStorage } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import AppIntroSlider from 'react-native-app-intro-slider';
import storage from '../helper/storage';

// create a component
class IntroScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
          title: '',
         header: null,
         tabBarHidden: true,
        };
      };

    state = {
        showRealApp: false
      }

    showAlert()
    {
        console.log('show from intro');
    }
    // UNSAFE_componentWillMount() {
    //     this.props.navigator.toggleTabs({
    //         to: 'hidden',
    //         animate: true,
    //     })
    // }
    _renderItem = (item) => {
        var {height, width} = Dimensions.get('window');
        return (
          <View style={styles.slide}>
            <ImageBackground source={item.image} style={{width: width, height: height, resizeMode: 'cover'}} >
            <Text style={styles.title}>{item.title}</Text>

            <Text style={styles.text}>{item.text}</Text>
            </ImageBackground>
          </View>
        );
      }
      _onDone = async () => {
        // User finished the introduction. Show real app through
        // navigation or simply by controlling state
        this.setState({ showRealApp: true });
        storage.save({
          key: 'introDisplayed', // Note: Do not use underscore("_") in key!
          data: 'yes',
         
          // if expires not specified, the defaultExpires will be applied instead.
          // if set to null, then it will never expire.
          expires: null //1000 * 3600
        });
        const userToken = await AsyncStorage.getItem('token');
        this.props.navigation.navigate(userToken ? 'App' : 'Auth');
       //this.props.navigation.pop();

      }

      _renderNextButton = () => {
        return (
          <View style={styles.buttonCircle}>
            <Ionicons
              name="md-arrow-round-forward"
              color="rgba(255, 255, 255, .9)"
              size={24}
              style={{ backgroundColor: 'transparent' }}
            />
          </View>
        );
      }
      _renderDoneButton = () => {
        return (
          <View style={styles.buttonCircle}>
            <Ionicons
              name="md-checkmark"
              color="rgba(255, 255, 255, .9)"
              size={24}
              style={{ backgroundColor: 'transparent' }}
            />
          </View>
        );
      }
    render() {
        
        const slides = [
            {
              key: '1',
              title: 'Title 1',
              text: 'Description.\nSay something cool',
              image: require('../assets/images/1.png'),
              backgroundColor: '#59b2ab',
            },
            {
              key: '2',
              title: 'Title 2',
              text: 'Other cool stuff',
              image: require('../assets/images/2.png'),
              backgroundColor: '#febe29',
            },
            {
              key: '3',
              title: 'Rocket guy',
              text: 'I\'m already out of descriptions\n\nLorem ipsum bla bla bla',
              image: require('../assets/images/3.png'),
              backgroundColor: '#22bcb5',
            }
          ];

          return (
            
            <AppIntroSlider 
            renderItem={this._renderItem} 
            slides={slides} 
            onDone={this._onDone} 
            renderDoneButton={this._renderDoneButton}
            renderNextButton={this._renderNextButton}/>
           
          );
        
    }
}

// define your styles
const styles = StyleSheet.create({
    mainContent: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
      },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
    slide: {
        flex: 1,
        alignItems: 'center',
       
        resizeMode: 'cover'
      },
      image: {
        width: 600,
        height: 800,
      },
      text: {
        color: 'rgba(255, 255, 255, 0.8)',
        backgroundColor: 'transparent',
        textAlign: 'center',
        paddingHorizontal: 16,
        marginBottom:40
      },
      title: {
        fontSize: 22,
        marginTop:40,
        color: 'white',
        backgroundColor: 'transparent',
        textAlign: 'center',
        marginBottom: 16,

      },
      buttonCircle: {
        width: 40,
        height: 40,
        backgroundColor: 'rgba(0, 0, 0, .2)',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
      },
      
});

//make this component available to the app
export default IntroScreen;
