import React, {Component} from 'react';
import {View,TouchableHighlight, Image, Text} from 'react-native';
import { DrawerActions } from 'react-navigation'

class HeaderComponent extends Component {

  static navigationOptions = ({ navigation }) => {
   let drawerLabel = 'Home';
   let drawerIcon = () => (
     <Image
       source={require('../Icons/add.png')}
       style={ {width:26, height:26, tintColor:backgroundColor} }
     />
   );
   return { drawerLabel, drawerIcon};
 }

  render(){
    console.log("in HeaderComponent123");
    return (
      <View style= {{flexDirection: 'row', backgroundColor: 'green',height:50, marginTop: 20}}>
        <TouchableHighlight style={ {marginLeft: 10, marginTop: 10  } }
          onPress={() => {
            console.log(`item left drawer pressed ` + JSON.stringify(this.props.navigation));
          }}>
          <Image
            style = { {width:25, height: 25} }
            source = {(require('../Icons/user.png'))}
          />
        </TouchableHighlight>

        <Text style= {{ fontSize: 20,
        textAlign: 'center',
        marginTop: 10,
        marginLeft: 100,
        justifyContent: 'center',
        color:'white',
        fontWeight: 'bold'}}>{this.props.navigation.state.key}
        </Text>
    </View>
    );
  }
}

export default HeaderComponent;
