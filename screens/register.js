import React from 'react';

import {
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  AsyncStorage,
  View,
  TouchableWithoutFeedback,
  StatusBar,
  TextInput,
  SafeAreaView,
  Keyboard,
  KeyboardAvoidingView,
  ScrollView
} from 'react-native';

import { WebBrowser } from 'expo';
import DeviceInfo from 'react-native-device-info'; //Todo
//https://github.com/rebeccahughes/react-native-device-info
import { SIGN_IN } from "../graphqlQueries";
//import { Mutation } from "react-apollo";
import { Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';
//import LoginMutation from '../components/LoginMutation';
import { graphql } from 'react-apollo';

import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import { Formik } from "formik";
import * as Yup from "yup";
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";

const MyInput = compose(handleTextInput, withNextInputAutoFocusInput)(TextField);

const Form = withNextInputAutoFocusForm(ScrollView, {
  submitAfterLastInput: false
});

const initialState = {
  email: "ronak@complitech.us",
  password: "02021994"
};

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .required()
    .email(),
  password: Yup.string()
    .required()
    .min(6, "Too short"),
  star: Yup.boolean()
    .required()
    .oneOf([true])
});

class LogInScreen extends React.Component {
  static navigationOptions = {
    title: '',
    header: null
  };

  constructor(props) {
    super(props);
    this.state = { ...initialState };
  }

  render() {
    console.log("Register screen loaded....")
    let { email, password, error } = this.state;
    return (
      // <Mutation mutation={SIGN_IN} variables={{ email, password }}>
      //  {(login, { error, data, loading }) => {
      //       // if (loading) return "Loading...";
      //       if (error) console.log(error.message);

      //       return (

    <ImageBackground style={styles.imgBackground} source={require('../assets/images/login_bg.png')} resizeMode='cover'>

      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Image style={styles.logo}
            source={require('../assets/images/logo.png')}>
          </Image>
        </View>

        <View style={styles.infoContainer}>
          <View style={styles.searchSection}>
            <Icon raised style={styles.searchIcon} name='user' type='font-awesome' color='#000' size={20} />
            <TextInput
                style={styles.input}
                placeholder="Username"
                onChangeText={(searchString) => {this.setState({searchString})}}
                underlineColorAndroid="transparent"
            />
          </View>
          <View style={{height: 1, backgroundColor: '#ccc'}}></View>
          <View style={styles.searchSection}>
            <Icon raised style={styles.searchIcon} name='lock' type='font-awesome' color='#000' size={20} />
            <TextInput
                style={styles.input}
                placeholder="Password"
                onChangeText={(searchString) => {this.setState({searchString})}}
                underlineColorAndroid="transparent"
            />
          </View>
          <View style={{height: 1, backgroundColor: '#ccc'}}></View>
          <View style={styles.searchSection}>
            <Icon raised style={styles.searchIcon} name='envelope' type='font-awesome' color='#000' size={20} />
            <TextInput
                style={styles.input}
                placeholder="Email"
                onChangeText={(searchString) => {this.setState({searchString})}}
                underlineColorAndroid="transparent"
            />
          </View>
          <View style={styles.actionButon}>
            <Icon raised style={styles.actionRight} name='chevron-right' type='font-awesome' color='#fff' size={17} />
          </View>
        </View>
        
        <View>
          <Text h4 style={styles.forgotTxt}>Forgot ?</Text>
        </View>

        <View style={{flex: 1, flexDirection: 'row', marginTop: 50}}>
          {/* <Button title="Register" buttonStyle={styles.registerButton} color={'#2089dc'} /> */}
          <Button title="Regi" color={'#2089dc'} />
        </View>
        
      </View>
    </ImageBackground>

    );
  }

  onEmailChange = email => {
    
 //   const email = e.target.value;
    
    this.setState(email => ({
      email
    }));
  };
  onPasswordChange = password => {
    //const password = e.target.value;
    this.setState(password => ({
      password
    }));
  };
  _gotoSignupScreen = async () => {
    this.props.navigation.navigate('SignupScreen')
  };
  _signInAsync = async () => {
    //await AsyncStorage.setItem('userToken', 'abc');
    //this.props.navigation.navigate('App');
    this.props.
      signIn(this.state.email, this.state.password)
      .then(({ data }) => {
        
        if (data) {
         AsyncStorage.setItem('token', data.signIn.authentication_token)
          .then(() => {
            this.props.navigation.navigate('App');
          })
          
          //AsyncStorage.setItem('account', JSON.stringify(data.signIn));
         // this._storageTest(data)
          
        }
      }).catch((error) =>{
        console.log('my error :'+ error);
      })
  };

  _storageTest = async (data) => {

    await AsyncStorage.setItem('account', JSON.stringify(data.signIn.user.account))
            .then(() => {
              console.log('It was saved successfully')
            })
            .catch(() => {
              console.log('There was an error saving the account')
            })
  }


  // _signInAsync = async(login)   => {
  //   console.log('here we go');
  //   login().then(({ data: { signIn: { authentication_token } } }) => {
  //      console.log(authentication_token);
  //     // localStorage.setItem("token", authentication_token);
  //     // this.resetState();
  //     // this.props.history.push("/dashboard");
  //     console.log('before store token');
  //     AsyncStorage.setItem('token', authentication_token);
  //     this.props.navigation.navigate('App');  
  //   });

  // };

  _gotoForgetPassword = async () => {
    this.props.navigation.navigate('ForgetPasswordScreen')
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fdfdfd',
    flexDirection: 'column'
  },
  logoContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    width: 266,
    height: 50
  },
  title: {
    color: '#f7c744',
    //fontSize: 18,
    textAlign: 'center',
    marginTop: 5,
    opacity: 0.9
  },
  infoContainer: {
    borderRadius: 50,
    borderTopLeftRadius: 0,
    borderBottomLeftRadius: 0,
    backgroundColor: '#fff',
    width: '90%',
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowRadius: 7,
  },
  // input: {
  //   height: 40,
  //   backgroundColor: 'rgba(0,0,0,0.2)',
  //   color: '#fff',
  //   marginBottom: 20,
  //   paddingHorizontal: 10
  // },
  containerButton: {
    flexDirection: 'row'
  },
  buttonContainer: {
    backgroundColor: '#373f51',
    marginTop: 15,
  },
  buttonForgotContainer: {
    marginTop: 15,
  },
  buttonSignupContainer: {
    marginTop: 30,
    backgroundColor: '#423231',
  },
  buttonText: {
    textAlign: 'center',
    color: 'rgb(32,53,70)',
    fontWeight: 'bold',
    //fontSize: 18
  },
  searchSection: {
    flexDirection: 'row',
    borderTopColor: 'transparent',
    borderRightColor: 0,
    borderLeftColor: 0,
    borderBottomColor: 0,
    borderColor: '#ccc',
    borderWidth: 1
  },
  searchIcon: {
    padding: 15,
    position: 'relative'
  },
  input: {
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    color: '#424242',
  },
  actionButon: {
    position: 'absolute',
    top: '25%',
    right: -20,
    borderRadius: 100,
    backgroundColor: '#2089dc',
    width: 50,
    height: 50,
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 15,
    textAlign: 'center'
  },
  imgBackground: {
    flex: 1    
  },
  actionRight: {
    width: '100%'
  },
  forgotTxt: {
    width: '100%',
    color: '#666',
    textAlign: 'right',
    paddingTop: 20,
    paddingRight: 20,
    //fontSize: 15
  },
  registerButton: {
    //fontSize: 20,
    paddingHorizontal: 25,
    shadowOffset: { width: 0, height: 0 },
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowRadius: 7,
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    marginLeft: -15,
    backgroundColor: '#fff',
  },
});

export default graphql(SIGN_IN,
  {
    props: ({ mutate }) => ({
      signIn: (email, password) => mutate({ variables: { email, password } })
    }),

  }
)(LogInScreen)
