import React from 'react';

import { StyleSheet, TouchableOpacity, Alert, AsyncStorage, View, Text, SafeAreaView, StatusBar, Keyboard, KeyboardAvoidingView, TouchableWithoutFeedback, Image, TextInput, ScrollView, TouchableHighlight } from 'react-native';
import { Button } from 'react-native-elements'

import { CREATE_CLIENT, GET_CLIENTS } from "../graphqlQueries";
import { graphql } from 'react-apollo';

import SimplePicker from 'react-native-simple-picker';

import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import { Formik, Field } from "formik";
import * as Yup from "yup";
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";

const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

import makeInput, {
  KeyboardModal,
  withPickerValues
} from "react-native-formik";
import IconFont from 'react-native-vector-icons/FontAwesome';

var FloatingLabel = require('react-native-floating-labels');

const MyInput = compose(handleTextInput, withNextInputAutoFocusInput)(TextField);

const Form = withNextInputAutoFocusForm(ScrollView, {
  submitAfterLastInput: false
});

const LanguageOptions = ['English', 'Dutch', 'French', 'German', 'Greek'];
const LanguageLabels = ['English', 'Dutch', 'French', 'German', 'Greek'];

const CurrencyOptions = ['US Dollar', 'Kenyan Shilling', 'Kina', 'Indian Rupee', 'Dobra'];
const CurrencyLabels = ['US Dollar', 'Kenyan Shilling', 'Kina', 'Indian Rupee', 'Dobra'];

const MyPicker = compose(
  makeInput,
  withPickerValues
)(TextInput);

const myvalidationSchema = Yup.object().shape({

  firstname: Yup.string().required(),

  lastname: Yup.string().required(),

  company: Yup.string().required(),

  city: Yup.string().required(),

  email: Yup.string()
    .required()
    .email(),
  password: Yup.string()
    .required()
    .min(6, "Too short"),
  confirmPassword: Yup.string().oneOf(
    [Yup.ref('password')],
    'Passwords do not match',
  ),
  star: Yup.boolean()
    .required()
    .oneOf([true])
});


const initialState = {
  companyname: "",
  phone: "",
  currency_id: "1",
  language_id: "2",
  delivery_method: "",
  address_id: "1",
  account_id: "",
  firstname: "",
  lastname: "",
  email: "",
  mobile: "1234567890",
  client_id: "5",
  is_primary_contact: true,
  address1: "",
  address2: "",
  city: "",
  state: "",
  country: "",
  country_id: "1",
  zipcode: "",
  selectedCountry: 'India',
  arrCountries: [],
  filteredCountries: [],
  filteredLanguages:[],
};

class AddClient extends React.Component {

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Add Client',
      headerTitle: 'Add Client',
      headerRight: <TouchableOpacity onPress={navigation.getParam('addClientInAsync')}><View style={{ marginRight: 20 }}><IconFont name={'check'} size={22} color={'#373f51'} style={{ paddingRight: 10 }} /></View></TouchableOpacity>
    };
  };

  constructor(props) {
    super(props);
    this.state = { ...initialState };
    console.log("ADD CLIENT")
    this._retrieveAccountData()

    this._addClientInAsync = this._addClientInAsync.bind(this);
  }

  UNSAFE_componentWillMount() {
    this.props.navigation.setParams({ addClientInAsync: this._addClientInAsync });
    this._retrieveTotalCountries()
  }

  _retrieveTotalCountries = async () => {
    console.log('In Contry Function')

    storage.load({
      key: 'totalCountries',
      autoSync: true,
      syncInBackground: true,

      // you can pass extra params to the sync method
      // see sync example below
      syncParams: {
        extraFetchOptions: {
          // blahblah
        },
        someFlag: true
      }
    })
      .then(ret => {
        // found data go to then()
        console.log('we found data');
        //console.log(ret);

        this.setState({ arrCountries: ret }, () => {
          this.state.arrCountries.countries.map((item, i) => (

            this.state.filteredCountries.push(item.name)
            
          ))
        });
        console.log("*****************",this.state.filteredCountries)
      })
      .catch(err => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case 'NotFoundError':
            // TODO;

            break;
          case 'ExpiredError':
            // TODO
            break;
        }
      });

  };

  _retrieveAccountData = async () => {
    try {
      console.log('We ient');
      const value = await AsyncStorage.getItem('account');
      if (value !== null) {
        // We have data!!
        console.log('We have data! Add Client');
        let data = JSON.parse(value)
        this.setState({
          account_id: data[0].id
        })
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  render() {
    return (
      <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={Platform.select({ios: 0, android: 80})} style={styles.container}>
        <SafeAreaView style={styles.container}>
          <StatusBar barStyle="light-content" />
          <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>
            <View style={styles.container}>
              {/* <View style={styles.logoContainer}>
                            <Image style={styles.logo}
                              source={require('../assets/images/logo.png')}>
                            </Image>
                          </View> */}
              <Formik
                onSubmit={(values) => alert(JSON.stringify(values))}
                validationSchema={myvalidationSchema}
              >
                {
                    <Form style={styles.infoContainer} keyboardShouldPersistTaps='always'>
                      <View style={{ flexDirection: 'row' }}>
                        <View style={styles.paymenttype}>
                          <FloatingLabel
                            labelStyle={styles.labelInput}
                            inputStyle={styles.input}
                            style={styles.formInput}
                            value={this.state.firstname}
                            onChangeText={(firstname) => this.setState({ firstname })}
                            autoCorrect={false}
                            keyboardType='default'
                            returnKeyType='next'
                            autoCorrect={false}
                            onSubmitEsiting={() => this.refs.txtlanguage.focus()}
                            ref='txtFirstName'
                            onBlur={this.onBlur}
                          >
                            First Name
                          </FloatingLabel>

                        </View>
                        <View style={styles.paymentdate}>
                          <FloatingLabel
                            labelStyle={styles.labelInput}
                            inputStyle={styles.input}
                            style={styles.formInput}
                            value={this.state.lastname}
                            onChangeText={(lastname) => this.setState({ lastname })}
                            autoCorrect={false}
                            keyboardType='default'
                            returnKeyType='next'
                            autoCorrect={false}
                            onSubmitEsiting={() => this.refs.txtlanguage.focus()}
                            ref='txtLastName'
                            onBlur={this.onBlur}
                          >
                            Last Name
                          </FloatingLabel>
                        </View>
                      </View>

                      <View style={styles.payamounttitle}>
                        <FloatingLabel
                          labelStyle={styles.labelInput}
                          inputStyle={styles.input}
                          style={styles.formInput}
                          value={this.state.companyname}
                          onChangeText={(companyname) => this.setState({ companyname })}
                          autoCorrect={false}
                          keyboardType='default'
                          returnKeyType='next'
                          autoCorrect={false}
                          onSubmitEsiting={() => this.refs.txtlanguage.focus()}
                          ref='txtCompany'
                          onBlur={this.onBlur}>

                          Company (Optional)
                                </FloatingLabel>
                      </View>

                      <View style={{ flexDirection: 'row' }}>
                        <View style={styles.paymenttype}>
                          <FloatingLabel
                            labelStyle={styles.labelInput}
                            inputStyle={styles.input}
                            style={styles.formInput}
                            value={this.state.address1}
                            onChangeText={(address1) => this.setState({ address1 })}
                            autoCorrect={false}
                            keyboardType='default'
                            returnKeyType='next'
                            autoCorrect={false}
                            onSubmitEsiting={() => this.refs.txtlanguage.focus()}
                            ref='txtAddress1'
                            onBlur={this.onBlur}>

                            Address1
                                  </FloatingLabel>
                        </View>
                        <View style={styles.paymentdate}>
                          <FloatingLabel
                            labelStyle={styles.labelInput}
                            inputStyle={styles.input}
                            style={styles.formInput}
                            value={this.state.address2}
                            onChangeText={(address2) => this.setState({ address2 })}
                            keyboardType='default'
                            returnKeyType='next'
                            autoCorrect={false}
                            onSubmitEsiting={() => this.refs.txtlanguage.focus()}
                            ref='txtAddress2'
                            onBlur={this.onBlur}>

                            Address2
                                  </FloatingLabel>
                        </View>
                      </View>

                      <View style={{ flexDirection: 'row' }}>
                        <View style={styles.paymenttype}>
                          <FloatingLabel
                            labelStyle={styles.labelInput}
                            inputStyle={styles.input}
                            style={styles.formInput}
                            value={this.state.city}
                            onChangeText={(city) => this.setState({ city })}
                            autoCorrect={false}
                            keyboardType='default'
                            returnKeyType='next'
                            autoCorrect={false}
                            onSubmitEsiting={() => this.refs.txtlanguage.focus()}
                            ref='txtCity'
                            onBlur={this.onBlur}>

                            City
                                  </FloatingLabel>
                        </View>
                        <View style={styles.paymentdate}>
                          <FloatingLabel
                            labelStyle={styles.labelInput}
                            inputStyle={styles.input}
                            style={styles.formInput}
                            value={this.state.zipcode}
                            onChangeText={(zipcode) => this.setState({ zipcode })}
                            autoCorrect={false}
                            keyboardType='numeric'
                            returnKeyType='next'
                            autoCorrect={false}
                            onSubmitEsiting={() => this.refs.txtlanguage.focus()}
                            ref='txtZipcode'
                            onBlur={this.onBlur}>

                            Zip Code
                                  </FloatingLabel>
                        </View>
                      </View>

                    <View style={styles.languagetitle}>
                    <Text style={styles.amounttitle}>Country</Text>

                    <View>
                      <Text style={styles.amount} onPress={() => {this.refs.currencyy.show()}}>{this.state.selectedCountry}
                      </Text>
                    </View>
                    <SimplePicker
                      ref={'currencyy'}
                      options={this.state.filteredCountries}
                      labels={this.state.filteredCountries}
                      itemStyle={{
                        color: '#444',
                        fontSize: 20
                      }}
                      onSubmit={(CurrencyOptions) => {
                        this.setState({ selectedCountry: CurrencyOptions, country: CurrencyOptions });
                      }}
                    />
                    </View>

                      <View style={styles.payamounttitle}>
                        <FloatingLabel
                          labelStyle={styles.labelInput}
                          inputStyle={styles.input}
                          style={styles.formInput}
                          value={this.state.email}
                          onChangeText={(email) => this.setState({ email })}
                          autoCorrect={false}
                          keyboardType='email-address'
                          returnKeyType='next'
                          autoCorrect={false}
                          onSubmitEsiting={() => this.refs.txtlanguage.focus()}
                          ref='txtEmail'
                          onBlur={this.onBlur}>

                          Email
                            </FloatingLabel>
                      </View>

                      <View style={styles.payamounttitle}>
                        <FloatingLabel
                          labelStyle={styles.labelInput}
                          inputStyle={styles.input}
                          style={styles.formInput}
                          value={this.state.phone}
                          onChangeText={(phone) => this.setState({ phone })}
                          autoCorrect={false}
                          keyboardType='numeric'
                          returnKeyType='next'
                          autoCorrect={false}
                          onSubmitEsiting={() => this.refs.txtfax.focus()}
                          ref='txtphone'
                          onBlur={this.onBlur}>

                          Phone Number
                                </FloatingLabel>
                      </View>
                    
                    </Form>
                }
              </Formik>
            </View>
          </TouchableWithoutFeedback>
        </SafeAreaView>
      </KeyboardAvoidingView>
    );

  }
  goBack(amount) {
    console.log("Selected amount :", amount);
    this.props.navigation.state.params.onNavigateBack(amount)
    this.props.navigation.pop()
  }

  _addClientInAsync = async () => {
    console.log("all client states ==???", this.state);


    if (this.state.firstname == '') {
      Alert.alert("Please enter firstname.");
      return
    }

    if (this.state.lastname == '') {
      Alert.alert("Please enter lastname.");
      return
    }

    if (this.state.address1 == '') {
      Alert.alert("Please enter Address1.");
      return
    }

    if (this.state.address2 == '') {
      Alert.alert("Please enter Address2.");
      return
    }

    if (this.state.city == '') {
      Alert.alert("Please enter city.");
      return
    }

    if (this.state.zipcode.length < 6) {
      Alert.alert("Please enter valid zipcode.");
      return
    }

    if (this.state.country == '') {
      Alert.alert("Please enter country.");
      return
    }

    // if (this.state.state == '') {
    //   Alert.alert("Please enter State.");
    //   return
    // }

    // if (this.state.email == '') {
    //   Alert.alert("Please enter Email.");
    //   return
    // }

    if (reg.test(this.state.email) === false) {
      Alert.alert("Please enter valid Email.");
      return
    }

    if ((this.state.phone.length < 10) || (this.state.phone.length > 15)) {
      Alert.alert("Please enter valid phone number.");
      return
    }

    console.log("this is acc id", this.state.account_id)
    this.props.
      create_client(this.state.companyname, this.state.phone, Number(this.state.currency_id), Number(this.state.language_id), this.state.delivery_method,
        Number(this.state.address_id), Number(this.state.account_id), this.state.firstname, this.state.lastname, this.state.email, this.state.mobile, Number(this.state.client_id), Boolean(this.state.is_primary_contact),
        this.state.address1, this.state.address2, this.state.city, this.state.state, Number(this.state.country_id), this.state.zipcode)
      .then(({ data }) => {
        console.log("create product : " + JSON.stringify(data));
        console.log("error product : " + JSON.stringify(data.error));
        if (data) {
          Alert.alert(
            'Invoease',
            'Client added successfully.',
            [
              { text: 'OK', onPress: () => this.goBack("ok1") },
            ],
            { cancelable: false },
          );
        }
      })
      .catch((error) => {
        console.log('Add client error :' + error);
      })

  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(255, 255, 255)'
  },
  logoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  logo: {
    width: 266,
    height: 50
  },
  title: {
    color: '#F7C744',
    fontSize: 18,
    textAlign: 'center',
    marginTop: 5,
    opacity: 0.9
  },
  infoContainer: {
    flexDirection: 'column',
    flex: 1,
  },
  input: {
    // fontSize: 18,
    // borderColor: '#ccc',
    // borderWidth: 1,
    // borderStyle: 'solid',
    // height: 30,
  },
  buttonContainer: {
    backgroundColor: '#1FA2FF',
    marginTop: 15,
  },
  buttonText: {
    textAlign: 'center',
    color: 'rgb(32,53,70)',
    fontWeight: 'bold',
    fontSize: 18
  },
  paytitle: {
    color: '#333',
    fontSize: 26,
    fontWeight: '600',
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 15
  },
  paytext: {
    color: '#999',
    fontSize: 18,
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 15
  },
  payamounttitle: {
    borderWidth: 0.5,
    borderColor: '#ccc',
    backgroundColor: '#fff',
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 0
  },
  amounttitle: {
    color: '#ccc',
    fontSize: 16
  },
  amount: {
    marginTop: 5,
    color: '#444',
    fontSize: 21
  },
  paymenttype: {
    width: '50%',
    borderWidth: 0.5,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 0.5,
    borderColor: '#ccc',
    backgroundColor: '#fff',
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 0
  },
  paymentdate: {
    width: '50%',
    borderWidth: 0.5,
    borderTopWidth: 0,
    borderBottomWidth: 0.5,
    borderColor: '#ccc',
    backgroundColor: '#fff',
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 0
  },
  languagetitle: {
    borderWidth: 0.5,
    borderColor: '#ccc',
    backgroundColor: '#fff',
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 15,
  },

  labelInput: {
    color: '#ccc',
    fontSize: 15,
    paddingLeft: 15
  },
  formInput: {
    // borderBottomWidth: 1, 
    paddingLeft: 5,
    borderColor: '#ccc'
  },
  input: {
    borderWidth: 0
  }
});

export default graphql(CREATE_CLIENT,
  {
    props: ({ mutate }) => ({
      create_client: (companyname, phone, currency_id, language_id, delivery_method, address_id, account_id, firstname, lastname, email, mobile, client_id,
        is_primary_contact, address1, address2, city, state, country_id, zipcode) => mutate({
          variables: {
            companyname, phone, currency_id, language_id, delivery_method, address_id, account_id, firstname, lastname, email, mobile, client_id,
            is_primary_contact, address1, address2, city, state, country_id, zipcode
          }
        })
    }),
    options: {
      refetchQueries: [{
        query: GET_CLIENTS
      }
      ],
    }
  }

)(AddClient)