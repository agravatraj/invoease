import React from 'react';

import { StyleSheet, AsyncStorage, View, SafeAreaView, StatusBar, Keyboard, KeyboardAvoidingView, Text, TouchableWithoutFeedback, Image, TextInput, ScrollView, TouchableOpacity } from 'react-native';
import { Button } from 'react-native-elements'

import { GET_ACCOUNT_USERS, CREATE_USER } from "../graphqlQueries";

import { graphql } from 'react-apollo';

import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import { Formik } from "formik";
import * as Permissions from 'expo-permissions';
import * as Yup from "yup";
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";

import SimplePicker from 'react-native-simple-picker';

import ReactNativePickerModule from 'react-native-picker-module';

var FloatingLabel = require('react-native-floating-labels');

const MyInput = compose(handleTextInput, withNextInputAutoFocusInput)(TextField);

const Form = withNextInputAutoFocusForm(ScrollView, {
  submitAfterLastInput: false
});

const validationSchema = Yup.object().shape({
  name: Yup.string().required(),
  email: Yup.string()
    .required()
    .email(),
  password: Yup.string()
    .required()
    .min(6, "Too short"),
  confirmPassword: Yup.string().oneOf(
    [Yup.ref('password')],
    'Passwords do not match',
  ),
  star: Yup.boolean()
    .required()
    .oneOf([true])
  });

class EditUser extends React.Component {
  // static navigationOptions = {
  //   title: 'Edit User',
  //   headerRight: <TouchableOpacity onPress={this._CreateUser}><View style={{marginRight:20}}><Text style={{color: '#000', fontSize: 18,}}>Save</Text></View></TouchableOpacity>
  // };

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Edit User',
      headerTitle: 'Edit User',
      headerRight: <TouchableOpacity onPress={navigation.getParam('CreateUser')}><View style={{marginRight:20}}><Text style={{color: '#000', fontSize: 18,}}>Save</Text></View></TouchableOpacity>
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      firstname: "Dipak",
      lastname: "Baraiya",
      timezone: "1",
      email: "dipak@complitech.net",
      password: "dipak123",
      confirmPassword: "dipak123",
      language_id: 2,
      timezone_id: 101,
      mobile: "1234567890",
      phone: "1234567890",
      address_1: "Vadava Near ST Stand",
      address_2: "Bhavnagar",
      city: "Bhavnagar",
      state: "Gujarat",
      country_id: 1,
      zipcode: "364001",
      account_id: "1",
      user_avatar: "",
      app_version: "1.0",
      device_type: 1,

      selectedIndex: 1,
      selectedState: 'Cash',
      selectedUser : this.props.navigation.state.params.selectedUser,

      filteredCountries:[],
      arrLanguages:[],
      arrCountries:[],
      filteredLanguages:[],

      selectedLanguages: 'English',
      selectedCountries: 'India',
      selectedValue: null  
    };

    console.log("selectedUser/ ",this.state.selectedUser)

    this._retrieveAccountData()
  }

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    this.setState({ hasCameraPermission: status === 'granted' });
    this._retrieveTotalLanguages()
    this._retrieveTotalCountries()
  }

  UNSAFE_componentWillMount(){
    this.props.navigation.setParams({ CreateUser: this._CreateUser });    
  }

  _retrieveUserData = async () => {
     console.log('In User Data Function')

      storage.load({
          key: 'user',
          autoSync: true,
          syncInBackground: true,

          // you can pass extra params to the sync method
          // see sync example below
          syncParams: {
              extraFetchOptions: {
                  // blahblah
              },
              someFlag: true
          }
      })
      .then(ret => {
          // found data go to then()
      console.log('we found data');
      console.log(ret);

      this.setState({ arrLanguages: ret }, () => {  
        console.log("state arrlang",this.state.arrLanguages)

        this.state.arrLanguages.languages.map((item, i) => (
          
          this.state.filteredLanguages.push(item.name) 
      ))
      });

    })
      .catch(err => {
          // any exception including data not found
          // goes to catch()
          console.warn(err.message);
          switch (err.name) {
              case 'NotFoundError':
                  // TODO;

                  break;
              case 'ExpiredError':
                  // TODO
                  break;
          }
      });
    };

    _retrieveTotalLanguages = async () => {
      console.log('In Language Function')

        storage.load({
        key: 'totalLanguages',
        autoSync: true,
        syncInBackground: true,
     
        // you can pass extra params to the sync method
        // see sync example below
        syncParams: {
          extraFetchOptions: {
            // blahblah
          },
          someFlag: true
        }
      })
      .then(ret => {
        // found data go to then()
        console.log('we found data');
        //console.log(ret);

        this.setState({ arrLanguages: ret }, () => {  
          console.log("state arrlang",this.state.arrLanguages)

          this.state.arrLanguages.languages.map((item, i) => (
            
            this.state.filteredLanguages.push(item.name) 
        ))
        });

      })
      .catch(err => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case 'NotFoundError':
            // TODO;
    
            break;
          case 'ExpiredError':
            // TODO
            break;
        }
      });     
    };

    _retrieveTotalCountries = async () => {
      console.log('In Contry Function')

        storage.load({
        key: 'totalCountries',
        autoSync: true,
        syncInBackground: true,
     
        // you can pass extra params to the sync method
        // see sync example below
        syncParams: {
          extraFetchOptions: {
            // blahblah
          },
          someFlag: true
        }
      })
      .then(ret => {
        // found data go to then()
        console.log('we found data');
        //console.log(ret);

        this.setState({ arrCountries: ret }, () => {  
          console.log("state arrlang",this.state.arrCountries)

          this.state.arrCountries.countries.map((item, i) => (
            
            this.state.filteredCountries.push(item.name) 
        ))
        });

      })
      .catch(err => {
        // any exception including data not found
        // goes to catch()
        console.warn(err.message);
        switch (err.name) {
          case 'NotFoundError':
            // TODO;
    
            break;
          case 'ExpiredError':
            // TODO
            break;
        }
      });
     
    };

  _retrieveAccountData = async () => {
    try {
      const value = await AsyncStorage.getItem('account');
      if (value !== null) {
        // We have data!!
        console.log('We have data! Add Client');
        let data = JSON.parse(value)
        this.setState({
          account_id: data[0].id
        })
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  _CreateUser = async () => {

    this.props.navigation.pop()
    return

    console.log(this.state);
    console.log(Number(this.state.device_type))

    this.props.
      create_account_user(this.state.email, this.state.password, this.state.confirmPassword, this.state.firstname, this.state.lastname, Number(this.state.language_id),
        Number(this.state.timezone_id), this.state.address_1, this.state.address_2, this.state.city, this.state.state, Number(this.state.country_id),
        this.state.zipcode, this.state.phone, this.state.mobile, this.state.user_avatar, this.state.app_version.toString())
      .then(({ data }) => {
        console.log("create user : " + JSON.stringify(data));
        if (data) {
          Alert.alert(
            'Invoease',
            'User added successfully.',
            [
              { text: 'OK', onPress: () => this.props.navigation.pop() },
            ],
            { cancelable: false },
          );
        }
      })
  };

  render() {
    console.log("countries list lkjk ", this.state.filteredCountries);
    return (
      <View style={styles.container}>
        <SafeAreaView style={styles.container}>
          <StatusBar barStyle="light-content" />
          <KeyboardAvoidingView behavior='padding' style={styles.container}>
            <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>
              <View style={styles.container}>
                <Formik
                  onSubmit={values => alert(JSON.stringify(values, null, 2))}
                  validationSchema={validationSchema}
                  initialValues={{ star: true }}>
                  <Form style={styles.infoContainer} keyboardShouldPersistTaps='always'>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.selectedUser.first_name}
                      onChangeText={(firstname) => this.setState({ firstname })}
                      keyboardType='default'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtLastName.focus()}
                      ref={"txtFirstName"}
                      onBlur={this.onBlur}>

                      First Name
                    </FloatingLabel>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.selectedUser.last_name}
                      onChangeText={(lastname) => this.setState({ lastname })}
                      keyboardType='default'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtTimeZone.focus()}
                      ref={"txtLastName"}
                      onBlur={this.onBlur}>

                      Last Name
                    </FloatingLabel>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      keyboardType='phone-pad'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtEmail.focus()}
                      ref={"txtTimeZone"}
                      onBlur={this.onBlur}>

                      Time Zone
                    </FloatingLabel>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.selectedUser.email}
                      onChangeText={(email) => this.setState({ email })}
                      keyboardType='email-address'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtPassword.focus()}
                      ref={"txtEmail"}
                      onBlur={this.onBlur}>

                      Email
                    </FloatingLabel>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.password}
                      onChangeText={(password) => this.setState({ password })}
                      keyboardType='email-address'
                      returnKeyType='next'
                      autoCorrect={false}
                      secureTextEntry={true}
                      onSubmitEsiting={() => this.refs.txtConfirmPassword.focus()}
                      onBlur={this.onBlur}>

                      Password
                    </FloatingLabel>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.confirmPassword}
                      onChangeText={(confirmPassword) => this.setState({ confirmPassword })}
                      keyboardType='email-address'
                      returnKeyType='next'
                      autoCorrect={false}
                      secureTextEntry={true}
                      onSubmitEsiting={() => this.refs.txtLanguage.focus()}
                      ref={"txtConfirmPassword"}
                      onBlur={this.onBlur}>

                      Confirm Password
                    </FloatingLabel>
                    {/*<FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      keyboardType='default'
                      returnKeyType='go'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtMobile.focus()}
                      ref={"txtLanguage"}
                      onBlur={this.onBlur}>

                      
                    </FloatingLabel>*/}

                    <View style={styles.paymenttype}>
                      <Text style={styles.pickerTitle}>Language</Text>
                      <View>
                        <Text style={styles.pickerDesc} onPress={() => {this.languageRef.show()}}>{this.state.selectedLanguages}</Text>
                        <ReactNativePickerModule
                          pickerRef={e => this.languageRef = e}
                          value={this.state.selectedValue}
                          title={"Select a Language"}
                          items={this.state.filteredLanguages}
                          onValueChange={(LanguageOptions) => {
                            this.setState({ selectedLanguages: LanguageOptions, language: LanguageOptions});
                          }}
                        />
                      </View>
                    </View>

                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.selectedUser.mobile}
                      onChangeText={(mobile) => this.setState({ mobile })}
                      keyboardType='email-address'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtPhone.focus()}
                      ref={"txtMobile"}
                      onBlur={this.onBlur}>

                      Mobile
                    </FloatingLabel>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.selectedUser.phone}
                      onChangeText={(phone) => this.setState({ phone })}
                      keyboardType='default'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtAddress.focus()}
                      ref={"txtPhone"}
                      onBlur={this.onBlur}>

                      Phone
                    </FloatingLabel>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.selectedUser.address[0].address_1}
                      onChangeText={(address_1) => this.setState({ address_1 })}
                      keyboardType='default'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtAddress.focus()}
                      ref={"txtAddress"}
                      onBlur={this.onBlur}>

                      Address1
                    </FloatingLabel>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.selectedUser.address[0].address_2}
                      onChangeText={(address_2) => this.setState({ address_2 })}
                      keyboardType='default'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtCity.focus()}
                      ref={"txtAddress"}
                      onBlur={this.onBlur}>

                      Address2
                    </FloatingLabel>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.city}
                      onChangeText={(city) => this.setState({ city })}
                      keyboardType='default'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtState.focus()}
                      ref={"txtCity"}
                      onBlur={this.onBlur}>

                      City
                    </FloatingLabel>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.selectedUser.address[0].state}
                      onChangeText={(state) => this.setState({ state })}
                      keyboardType='default'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtZipCode.focus()}
                      ref={"txtState"}
                      onBlur={this.onBlur}>

                      State
                    </FloatingLabel>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.selectedUser.address[0].zipcode}
                      onChangeText={(zipcode) => this.setState({ zipcode })}
                      keyboardType='numeric'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtCountry.focus()}
                      ref={"txtZipCode"}
                      onBlur={this.onBlur}>

                      Zip Code
                    </FloatingLabel>

                    {/*<FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.zipcode}
                      keyboardType='default'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtState.focus()}
                      onPress={() => { this.refs.picker2.show(); }}
                      ref={"txtCountry"}
                      onBlur={this.onBlur}>

                      Country
                    </FloatingLabel>*/}

                    <View style={styles.paymenttype}>
                      <Text style={styles.pickerTitle}>Country</Text>
                      <View>
                        <Text style={styles.pickerDesc} onPress={() => {this.pickerRef.show()}}>{this.state.selectedCountries}</Text>
                        <ReactNativePickerModule
                          pickerRef={e => this.pickerRef = e}
                          value={this.state.selectedValue}
                          title={"Select a Country"}
                          items={this.state.filteredCountries}
                          onValueChange={(CountryOptions) => {
                            this.setState({ selectedCountries: CountryOptions, country: CountryOptions});
                          }}
                        />
                      </View>
                    </View>

                    {/*<Button raised
                      buttonStyle={styles.buttonContainer}
                     // icon={{ name: 'fingerprint' }}
                      title='Save'
                      onPress={this._CreateUser} />*/}
                  </Form>
                </Formik>
              </View>
            </TouchableWithoutFeedback>
          </KeyboardAvoidingView>
        </SafeAreaView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(255, 255, 255)',
    flexDirection: 'column'
  },
  logoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  logo: {
    width: 266,
    height: 50
  },
  title: {
    color: '#f7c744',
    fontSize: 18,
    textAlign: 'center',
    marginTop: 5,
    opacity: 0.9
  },
  infoContainer: {
    flexDirection: 'column',
    flex: 1
  },
  buttonContainer: {
    backgroundColor: '#1FA2FF',
    marginTop: 15,
  },
  buttonText: {
    textAlign: 'center',
    color: 'rgb(32,53,70)',
    fontWeight: 'bold',
    fontSize: 18
  },
  paymenttype:{
    borderBottomWidth: 1, 
    borderColor: '#ccc',
  },

  labelInput: {
    color: '#ccc',
    fontSize: 15,
    paddingLeft: 15,
  },
  formInput: {    
    borderBottomWidth: 1, 
    paddingLeft: 5,
    borderColor: '#ccc',       
  },
  input: {
    borderWidth: 0
  },

  pickerTitle: {
    marginTop: 5,
    color: '#ccc',
    fontSize: 15,
    paddingHorizontal: 15
  },
  pickerDesc: {
    marginTop: 5,
    color: '#444',
    fontSize: 21,
    paddingHorizontal: 15
  }
});


export default graphql(CREATE_USER,
  {
    props: ({ mutate }) => ({
      create_account_user: (email, password, password_confirmation, first_name, last_name, language_id, timezone_id, address_1, address_2, city, state, country_id, phone, mobile, user_avatar, app_version, device_type) =>
        mutate({ variables: { email, password, password_confirmation, first_name, last_name, language_id, timezone_id, address_1, address_2, city, state, country_id, phone, mobile, user_avatar, app_version } })
    }),
    options: {
      refetchQueries: [{
        query: GET_ACCOUNT_USERS
      }
      ],
    }
  }
)(EditUser)