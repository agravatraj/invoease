import React from 'react';
import {Platform,StyleSheet,Text,AsyncStorage,View,ScrollView,SafeAreaView,StatusBar,Image,CameraRoll} from 'react-native';
import TabBarIcon from '../components/TabBarIcon';
import { Button } from 'react-native-elements';
import { Dimensions } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph
} from 'react-native-chart-kit'

export default class Dashboard extends React.Component {
  

static navigationOptions = ({ navigation }) => {
  return {
    headerTitle: 'Dashboard',
    // headerRight: (
    //   <Button
    //     onPress={navigation.getParam('profileScreen')}
    //     title="+1"
    //     color={Platform.OS === 'ios' ? '#fff' : null}
    //   />
    // ),
    // headerLeft: (
    //   <Button 
    //   icon={{name: 'user-circle', type: 'font-awesome', color: 'black'}}
    //   title=''
    //   backgroundColor = 'transparent'
    //   onPress={navigation.getParam('profileScreen')}/>
    // ),
  };
};


UNSAFE_componentWillMount() {
  this.props.navigation.setParams({ profileScreen: this._profileScreen });
}

_profileScreen = async () => {
  console.log('PROFILE');
  this.props.navigation.navigate('Profile');
};

_openGalleryPress = () => {
  CameraRoll.getPhotos({
      first: 20,
      assetType: 'Photos',
    })
    .then(r => {
      this.setState({ photos: r.edges });
    })
    .catch((err) => {
       //Error Loading Images
    });
  };

  render() {

    const piedata = [
      { name: 'Draft', invoices: 18, color: '#a770ef', legendFontColor: '#7F7F7F', legendFontSize: 15 },
      { name: 'Sent', invoices: 21, color: '#00c3ff', legendFontColor: '#7F7F7F', legendFontSize: 15 },
      { name: 'Viewed', invoices: 45, color: '#ff7e5f', legendFontColor: '#7F7F7F', legendFontSize: 15 },
      { name: 'Partial Paid', invoices: 77, color: '#4ecdc4', legendFontColor: '#7F7F7F', legendFontSize: 15 },
      { name: 'Paid', invoices: 76, color: '#64f38c', legendFontColor: '#7F7F7F', legendFontSize: 15 }
    ]
    const data = {
      labels: ['January', 'February', 'March', 'April', 'May', 'June'],
      datasets: [{
        data: [ 20, 45, 28, 80, 99, 43 ]
      }]
    }
    const chartConfig = {
      backgroundGradientFrom: '#00c3ff',
      backgroundGradientTo: '#ffff1c',
      
      color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
      strokeWidth: 2, // optional, default 3
      style: {
        borderRadius: 16
      }
    }
    const screenWidth = Dimensions.get('window').width - 20
    return (
      <View style={styles.container}>
        <SafeAreaView style = {styles.container}>
          <StatusBar barStyle= "light-content"/>
          <ScrollView>
          <View style={{marginLeft:10, marginRight:10}}>
          
  <LineChart
    data={{
      labels: ['January', 'February', 'March', 'April', 'May', 'June'],
      datasets: [{
        data: [
          Math.random() * 100,
          Math.random() * 100,
          Math.random() * 100,
          Math.random() * 100,
          Math.random() * 100,
          Math.random() * 100
        ]
      }]
    }}
    width={screenWidth} // from react-native
    height={220}
    chartConfig={{
      backgroundColor: '#e26a00',
      backgroundGradientFrom: '#fb8c00',
      backgroundGradientTo: '#ffa726',
      decimalPlaces: 2, // optional, defaults to 2dp
      color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
      style: {
        borderRadius: 16
      }
    }}
    bezier
    style={{
      marginVertical: 8,
      borderRadius: 16
    }}
  />

<BarChart
  style={{borderRadius:16}}
  data={data}
  width={screenWidth}
  height={220}
  chartConfig={chartConfig}
/>
<PieChart
  data={piedata}
  width={screenWidth}
  height={220}
  chartConfig={chartConfig}
  accessor="invoices"
  backgroundColor="transparent"
  paddingLeft="15"
/>

          </View>
          </ScrollView>
        </SafeAreaView>
      </View>
    );
    
  }

  _maybeRenderDevelopmentModeWarning() {
    if (__DEV__) {
      const learnMoreButton = (
        <Text onPress={this._handleLearnMorePress} style={styles.helpLinkText}>
          Learn more
        </Text>
      );

      return (
        <Text style={styles.developmentModeText}>
          Development mode is enabled, your app will be slower but you can use useful development
          tools. {learnMoreButton}
        </Text>
      );
    } else {
      return (
        <Text style={styles.developmentModeText}>
          You are not in development mode, your app will run at full speed.
        </Text>
      );
    }
  }

  _handleLearnMorePress = () => {
    WebBrowser.openBrowserAsync('https://docs.expo.io/versions/latest/guides/development-mode');
  };

  _handleHelpPress = () => {
    WebBrowser.openBrowserAsync(
      'https://docs.expo.io/versions/latest/guides/up-and-running.html#can-t-see-your-changes'
    );
  };
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: 'rgb(255, 255, 255)',
    flexDirection: 'column'
  },
  mainContainer:{
    alignItems: 'center',
    justifyContent: 'center',
    //flex: 1,
    height: 200,
    //backgroundColor: 'blue',
  },
  userImage:{
    width: 150,
    height: 150
  },
  infoContainer: {
    flexDirection: 'column',
    flex: 1,
    // position: 'absolute',
    // left: 0,
    // right: 0,
     //bottom: 0,
     //height: 100,
     //padding: 30,
    //backgroundColor: 'red'
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});
