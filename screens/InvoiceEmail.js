import React from 'react';

import { View, Text, StyleSheet, TextInput, ScrollView,TouchableOpacity, Dimensions,Image,Alert } from 'react-native';
import { Icon, CheckBox, Input, Button, ListItem, SearchBar, Divider, Header, ButtonGroup, Card } from 'react-native-elements';

import { SEND_EMAIL_TO_CLIENT } from "../graphqlQueries";
import { graphql } from 'react-apollo';
import { Formik, Field } from "formik";
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";
import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import * as Yup from "yup";

import EmailChip from 'react-native-email-chip';

const Form = withNextInputAutoFocusForm(ScrollView, {
  submitAfterLastInput: false
});
const validationSchema = Yup.object().shape({
  
});

const initialState = {
  cc: "",
  bcc: "",
  id: ""
};

class InvoiceView extends React.Component {    
  static navigationOptions = ({ navigation }) => {
    return{
    title: 'app.json',
    headerTitle: 'Invoice Email',
    //headerRight: <TouchableOpacity onPress={navigation.getParam('gotoCreateInvoiceScreen')}><Text style={{marginTop: 5, marginLeft:5, marginRight:10}}>Send</Text></TouchableOpacity>
    headerRight: <TouchableOpacity onPress={navigation.getParam('gotoEmailInvoice')}>
    <Icon containerStyle={{marginRight: 15,}} name='paper-plane' type='font-awesome' color='#333' />
    </TouchableOpacity>
  };
  };

  UNSAFE_componentWillMount() {
    this.props.navigation.setParams({ gotoEmailInvoice: this._gotoEmailInvoice });
  }
  
  _gotoEmailInvoice = async () => {
    console.log("state" + this.state);
    console.log("cc" + this.state.cc);
    console.log("bcc" + this.state.bcc);
    console.log("id" + Number(this.state.id));
    console.log("subject" + this.state.subject);

  this.props.
  send_email_to_client(this.state.cc, this.state.bcc, Number(this.state.id),this.state.subject)
 .then(({ data }) => {
  console.log("Email invoice : " + JSON.stringify(data));
  if (data) {
    Alert.alert(
      'Invoease',
      'Email sent successfully.',
      [
        {text: 'OK', onPress: () => this.props.navigation.pop()},
      ],
      {cancelable: false},
    );
  }
})
};

  constructor(props) {
    super(props)
    //console.log("client ",this.props.navigation.state.params.myInvoice);
    if (this.props.navigation.state.params.myInvoice.client[0].contacts.length === 0) {
      console.log("if executes")
      this.state = { ...initialState ,
        cc : ""
      };
    }
    else{
      console.log("else executes")
      console.log("d email",this.props.navigation.state.params.myInvoice.client[0].contacts[0].email)
      this.state = { ...initialState ,
        
        cc : this.props.navigation.state.params.myInvoice.client[0].contacts[0].email,
      };
    }

    this.state = { ...initialState ,
      myInvoice : this.props.navigation.state.params.myInvoice,
      // cc : this.props.navigation.state.params.myInvoice.client[0].contact[0].email,
      bcc : '',
      emails:[],

      id : this.props.navigation.state.params.myInvoice.id,  
      companyname:'',
      firstname:'',
      lastname:'',
      invoiceNumber:this.props.navigation.state.params.myInvoice.invoice_number,
      dueAmount:this.props.navigation.state.params.myInvoice.due_amount,
      dueDate:this.props.navigation.state.params.myInvoice.inv_due_date,
      subject:'[' + this.props.navigation.state.params.myInvoice.account[0].name + ']' + ' has sent you an invoice ' + this.props.navigation.state.params.myInvoice.invoice_number,
      currency:this.props.navigation.state.params.myInvoice.account[0].currency[0].symbol,
    };
     this._retrieveUserData()
    // console.log("Invoice Email email id of client  = ", this.state.myInvoice.client[0].contact[0].email)
    console.log("emails" , this.state.emails);
  }

  _retrieveUserData = async () => {
    console.log('In User Data Function')

      storage.load({
      key: 'user',
      autoSync: true,
      syncInBackground: true,
   
      // you can pass extra params to the sync method
      // see sync example below
      syncParams: {
        extraFetchOptions: {
          // blahblah
        },
        someFlag: true
      }
      
    })
    .then(ret => {
      // found data go to then()
      console.log('we found data');
      console.log("first" , ret.first_name);
      console.log("last" , ret.last_name);
      console.log(ret.account[0].name)
      console.log(ret.account[0].phone)
      this.setState({account_id:ret.account[0].id , firstname:ret.first_name, lastname:ret.last_name, image: ret.account[0].account_image_url, companyname: ret.account[0].name, fax: ret.account[0].fax, phone: ret.account[0].phone, selectedLanguage: ret.account[0].language[0].name, selectedCurrency: ret.account[0].currency[0].name, selectedLanguage: ret.account[0].language[0].id, selectedCurrencyID: ret.account[0].currency[0].id })
      this.setState({cc :this.props.navigation.state.params.myInvoice.client[0].contacts[0].email})
      console.log("cc after set state ", this.state.cc)

    })
    .catch(err => {
      // any exception including data not found
      // goes to catch()
      console.warn(err.message);
      switch (err.name) {
        case 'NotFoundError':
          // TODO;
  
          break;
        case 'ExpiredError':
          // TODO
          break;
      }
    });

   
  };

onChange = (emails) => {
  this.setState({ emails })
}

  render() {
    return (
    <ScrollView style={styles.container}>
      <View style={{flex: 1}}>

        <View style={{flex: 1, flexDirection: 'row', paddingLeft: 10, paddingRight: 10}}>
          <View style={{width: '100%',}}>
            {/* <Text h3 style={styles.toperson}>To:</Text> */}
            {/* <Text h1 style={styles.toemail}>admin@complitech.net</Text> */}
            {/* <TextInput
              style={styles.toemail}
              onChangeText={(emails) => this.setState({emails})}
              value={this.state.emails}
            /> */}             
            <EmailChip
              emails={this.state.emails}
              onChange={this.onChange}
              chipContainerStyle={{backgroundColor:'#87cefa',paddingTop:7,paddingBottom:7}}
              invalidChipContainerStyle={{backgroundColor:'red'}}
              chipTextStyle={{color:'#000'}}
              invalidChipTextStyle = {{color: 'red'}}
              textInputStyles={{borderBottomWidth: 0}}
            />
          </View>
          {/* <View style={{width: '10%', justifyContent: 'center'}}>
            <Icon name='plus' type='font-awesome' color='#666' size={20} />
          </View> */}
        </View>

        <View style={styles.subject}>
          <Text h3 style={styles.subjecttitle}>Subject:</Text>
          <TextInput editable = {false} style={styles.subjecttext} multiline={true}>            {this.state.firstname} {this.state.lastname} has sent you an invoice
          </TextInput>  
          <Text h3 style={styles.subjecttext}>{this.state.invoiceNumber}</Text>
        </View>

        <View style={styles.emaildetails}>
          <Text h2 style={styles.emaildesc}><Text style={{fontWeight: '600'}}>{this.state.firstname} {this.state.lastname}</Text> has sent you an invoice {this.state.invoiceNumber} for <Text style={{fontWeight: '600'}}>{this.state.currency}{this.state.dueAmount}</Text> that's due on <Text style={{fontWeight: '600'}}>{this.state.dueDate}</Text></Text>
        </View>

        {/* <View style={styles.personalmsg}>
          <Text h3 style={{textAnchor: 'middle', fontSize: 18, color: '#666'}}> + Add a personal message</Text>
        </View> */}
        
      </View>
    </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingLeft: 10,
    paddingRight: 10
  },
  logo: {
   flex: 1, 
   marginTop: 10,
   marginLeft: 'auto', 
   marginRight: 'auto', 
   textAlign: 'center',
   justifyContent: 'center'
  },
  h5: {
    color: '#666',
    fontSize: 13
  },
  toperson: {
    fontSize: 15,
    textAlign: 'left',
    alignSelf: 'stretch',
    marginTop: 15,
    marginBottom: 5,
    color: '#666'
  },
  toemail: {
    width: '100%',
    fontSize: 18,
    textAlign: 'center',
    alignSelf: 'flex-start',
    marginBottom: 5,
    marginLeft: 5,
    color: '#666',
    backgroundColor:'#c9e3f9',
    borderRadius: 14,
    borderWidth: 1,
    borderColor: '#fff',
    paddingTop: 4,
    paddingRight: 8,
    paddingBottom: 4,
    paddingLeft: 8,
    //overflow: 'hidden'
  },
  addemail: {
    fontSize: 15,
    textAlign: 'right',
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 5,
    color: '#666',
    paddingTop: 4,
    paddingRight: 8,
    paddingBottom: 4,
    paddingLeft: 8
  },
  iconplus: {
    paddingTop: 25,
    marginRight: 0,
    marginBottom: 0,
    marginLeft: 0
  },
  subject: {
    borderTopWidth: 2,
    borderRightWidth: 0,
    borderBottomWidth: 1,
    borderLeftWidth: 0,
    borderColor: '#ccc',
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 10
  },
  subjecttitle: {
    fontSize: 15,
    color: '#666'
  },
  subjecttext: {
    fontSize: 17,
    color: '#ccc'
  },
  emaildetails: {
    width: '85%',
    textAlign: 'center',
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    marginTop: 15,
    marginRight: 'auto',
    marginBottom: 15,
    marginLeft: 'auto',
  },
  emaildesc: {
    fontSize: 18,
    color: '#666',
    textAlign: 'center'
  },
  personalmsg: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    height: 150,
    borderWidth: 2,
    borderColor: '#ccc',
    borderStyle: 'dashed',
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    marginTop: 15,
    marginRight: 15,
    marginBottom: 15,
    marginLeft: 15
  }
});



export default graphql(SEND_EMAIL_TO_CLIENT,
  {
    props: ({ mutate }) => ({
      send_email_to_client: (cc, bcc, id,subject) => mutate({ variables: {
            cc, bcc, id , subject} })
    }),

  }
)(InvoiceView)