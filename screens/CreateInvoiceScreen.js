import React from 'react';
import {View,Text,FlatList, Dimensions,StyleSheet,Image,TouchableOpacity} from 'react-native';


const numColumns = 3;
const data = [{key: 'A', image: 'https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg'}, 
    {key: 'B', image: 'https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg'},
    {key: 'C', image: 'https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg'},
    {key: 'D', image: 'https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg'}, 
    {key: 'E', image: 'https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg'}, 
    {key: 'F', image: 'https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg'},
    {key: 'G', image: 'https://images-na.ssl-images-amazon.com/images/I/61McsadO1OL.jpg'}];

var formatData = (data, numColumns) => {
    const numberOfFullRows = Math.floor(data.length/numColumns)
    let numberOfElementsLastRow = data.length - (numberOfFullRows*numColumns);
    while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
        data.push({ key: 'blank-${numberOfElementsLastRow}', empty: true});
        numberOfElementsLastRow = numberOfElementsLastRow + 1;
    }
    formatData = data
}


export default class CreateInvoiceScreen extends React.Component {

  static navigationOptions = {
    title: 'app.json',
    headerTitle: 'Select Templete'
  };

  gotoInvoiceDetail = (item) => {
    console.log('In Create Invoice screen');
    console.log(item.image);
    console.log(item.key);
    this.props.navigation.navigate('InvoiceDetailScreen', {
        item: item,
      })
  };

  renderItem = ({ item, index }) => {
      if (item.empty === true){
          return <View style={[styles.item, styles.itemInvisible]} />
      }
    return(
        <TouchableOpacity  style={styles.item} onPress={() => this.gotoInvoiceDetail(item)}>
            <View style={styles.item}  key={index}>
                <Image source={{uri: item.image}} style={styles.image} />
                <Text style={styles.itemText}>{item.key}</Text>
            </View>
        </TouchableOpacity>
    );
  };

  UNSAFE_componentWillMount(){
    console.log(`Create Invoice screen ` + JSON.stringify(this.props.navigation));
    console.log(this.props.navigation)
  }

  render() {
    const { navigation } = this.props;
    return (
      <FlatList 
        //data= {formatData(data, numColumns)}
        data= {data}
        style={styles.container}
        renderItem={this.renderItem}
        numColumns={numColumns}
      />
    );
  }
  
}

const styles = StyleSheet.create({
    container:{
      flex: 1,
      backgroundColor: 'rgb(0, 255, 255)',
      marginVertical: 20,
    },
    item: {
        backgroundColor : '#373f51',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        margin: 1,
        height: Dimensions.get('window').width/numColumns,
    },
    itemInvisible:{
        backgroundColor: 'transparent'
    },
    itemText:{
        color: '#fff'
    },
    image:{
        width: 100,
        height: Dimensions.get('window').width/numColumns - 30,
        resizeMode: 'stretch'
      },
  });



