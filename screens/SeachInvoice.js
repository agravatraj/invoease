import React from 'react';

import { View, Text, Image, StyleSheet, TextInput, ScrollView, Dimensions } from 'react-native';
import { Icon, CheckBox, Input, Button, ListItem, SearchBar, Divider, Header, ButtonGroup, Card } from 'react-native-elements';

export default class SeachInvoice extends React.Component {
    
  static navigationOptions = {
    title: 'app.json',
    headerTitle: 'Search Invoice'
  };
  constructor(){
    super();
    console.disableYellowBox = true;
	}
	 
  render() {    
		const users = [
			{
					name: 'brynn',
					avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/brynn/128.jpg'
			},	
		]
    return (
			<ScrollView style={styles.container}>
				<View style={{flex: 1}}>

					 <View style={styles.pendingBox}>          
							<View style={styles.pendingleftBorder}></View>
								<View style={{width: '48%', justifyContent: 'center'}}>
									<View style={styles.vmiddle}>
										<Text style={styles.pendingTitle}>Tsd</Text>
										<Text style={styles.pendingMemo}>#000003</Text>
										<Text style={styles.pendingStatus}>PARTIAL</Text>
									</View>
								</View>
								<View style={{width: '48%', justifyContent: 'center'}}>
									<View style={styles.vmiddle}>									
										<Text style={styles.pendingDate}>Mar 15, 2019</Text>
										<Text style={styles.pendingAmount}>1.100,00</Text>
									</View>
							</View>
					</View>

					<View style={styles.pendingBox}>          
							<View style={styles.createleftBorder}></View>
								<View style={{width: '48%', justifyContent: 'center'}}>
									<View style={styles.vmiddle}>
										<Text style={styles.pendingTitle}>Tsd</Text>
										<Text style={styles.pendingMemo}>#000002</Text>
										<Text style={styles.createStatus}>DRAFT</Text>
									</View>
								</View>
								<View style={{width: '48%', justifyContent: 'center'}}>
									<View style={styles.vmiddle}>									
										<Text style={styles.pendingDate}>Mar 15, 2019</Text>
										<Text style={styles.pendingAmount}>2.500,00</Text>
									</View>
							</View>
					</View>

					<View style={styles.pendingBox}>          
							<View style={styles.successleftBorder}></View>
								<View style={{width: '48%', justifyContent: 'center'}}>
									<View style={styles.vmiddle}>
										<Text style={styles.pendingTitle}>Apple Inc</Text>
										<Text style={styles.pendingMemo}>#000001</Text>
										<Text style={styles.successStatus}>PAID</Text>
									</View>
								</View>
								<View style={{width: '48%', justifyContent: 'center'}}>
									<View style={styles.vmiddle}>									
										<Text style={styles.pendingDate}>Mar 15, 2019</Text>
										<Text style={styles.pendingAmount}>25,00</Text>
									</View>
							</View>
					</View> 
					 {/* <View style={styles.pendingBox}>							
						<View style={{width: '48%', justifyContent: 'center'}}>
							<View style={styles.vmiddle}>									
								<Text style={styles.pendingDate}>Mar 15, 2019</Text>
								<Text style={styles.pendingAmount}>1.100,00</Text>
							</View>
						</View>
						<View style={{width: '48%', justifyContent: 'center'}}>
							<View style={styles.vmiddle}>
								<Text style={styles.pendingTitle}>Tsd</Text>
								<Text style={styles.pendingMemo}>#000003</Text>
								<Text style={styles.pendingStatus}>PARTIAL</Text>
							</View>
						</View>
					</View>  */}

			
				
				</View>
			</ScrollView>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff'
		},
		pendingBox:{
			flex: 1,
			flexDirection: 'row',
			borderBottomWidth: 1,
			borderColor: '#ccc'
		},
		pendingleftBorder: {
			width: '4%',
			borderLeftWidth: 8,
			borderColor: '#f6df92'
		},
    pendingTitle: {   
      fontSize: 18,
      textAlign: 'left',
			color: '#333',
			marginTop: 10,
			marginBottom: 5,
      marginLeft: 5
		},
		pendingMemo: {   
      fontSize: 15,
      textAlign: 'left',
			color: '#ccc',
			marginTop: 10,
			marginBottom: 5,
      marginLeft: 5
		},
		pendingStatus: {   
      fontSize: 18,
			textAlign: 'left',
			fontWeight: 'bold',
			color: '#f6df92',
			marginTop: 10,
			marginBottom: 10,
      marginLeft: 5
    },
    pendingDate: {
      fontSize: 14,
			textAlign: 'right',
			color: '#999',
			marginTop: 10,
			marginRight: 10
		},
		pendingAmount: {
      fontSize: 18,
			textAlign: 'right',
			color: '#333',
			marginBottom: 10,
			marginRight: 10
		},
		vmiddle: {
			flex: 1,
			flexDirection: 'column',
			justifyContent: 'space-between'
		},
		createleftBorder: {
			width: '4%',
			borderLeftWidth: 8,
			borderColor: '#c4cdd3'
		},
		createStatus:{
			fontSize: 18,
			textAlign: 'left',
			fontWeight: 'bold',
			color: '#c4cdd3',
			marginTop: 10,
			marginBottom: 10,
      marginLeft: 5
		},
		successleftBorder:{
			width: '4%',
			borderLeftWidth: 8,
			borderColor: '#b5dc79'
		},
		successStatus:{
			fontSize: 18,
			textAlign: 'left',
			fontWeight: 'bold',
			color: '#b5dc79',
			marginTop: 10,
			marginBottom: 10,
      marginLeft: 5
		}
  });