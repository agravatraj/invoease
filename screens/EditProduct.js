import React from 'react';

import { StyleSheet, AsyncStorage, View, SafeAreaView, StatusBar, Keyboard, KeyboardAvoidingView, TouchableWithoutFeedback, Image, TextInput, ScrollView, Alert , Text , TouchableOpacity , Animated} from 'react-native';
import { Button,List } from 'react-native-elements'

import { CREATE_PRODUCT, GET_PRODUCTS } from "../graphqlQueries";
import { graphql } from 'react-apollo';

import DateTimePicker from 'react-native-modal-datetime-picker';

import { Query } from "react-apollo";
import { UPDATE_PRODUCT } from "../graphqlQueries";
import { GET_TAXES } from "../graphqlQueries";
import SimplePicker from 'react-native-simple-picker';
import { Dropdown } from "react-native-material-dropdown";

import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import { Formik } from "formik";
import * as Yup from "yup";
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";
import DoneButton from 'react-native-keyboard-done-button';
const math = require('mathjs')
import IconFont from 'react-native-vector-icons/FontAwesome';

import SwitchToggle from 'react-native-switch-toggle';
var listSelectItemTax = [];

var FloatingLabel = require('react-native-floating-labels');

const MyInput = compose(handleTextInput, withNextInputAutoFocusInput)(TextField);

const options = ['Bank Transfer', 'PayPal', 'Stripe','Credit Card', 'Debit Card', 'Check','Cash','Other'];
const labels = ['Bank Transfer', 'PayPal', 'Stripe','Credit Card', 'Debit Card', 'Check','Cash','Other'];

const Form = withNextInputAutoFocusForm(ScrollView, {
  submitAfterLastInput: false
});

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .required()
    .email(),
  password: Yup.string()
    .required()
    .min(6, "Too short"),
  confirmPassword: Yup.string().oneOf(
    [Yup.ref('password')],
    'Passwords do not match',
  ),
  star: Yup.boolean()
    .required()
    .oneOf([true])
});

const initialState = {
  id: "",
  name: "",
  description: "",
  price: 0,
  quantity: 0,
  inventory: "",
  account_id: "",
  tax1_id: "",
  tax1_amt: 0,
  tax2_amt: 0,
  tax2_id: "",
  comeFromCreateInvoice: "false",
  selectedTaxes:0
};

class EditProduct extends React.Component {
  // static navigationOptions = {
  //   title: 'EDIT PRODUCT',
  // };

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Edit Product',
      headerTitle: 'Edit Product',
      headerRight: <TouchableOpacity onPress={navigation.getParam('productUpdateAsync')}><View style={{marginRight:20}}><Text style={{color: '#000', fontSize: 18,}}>Save</Text></View></TouchableOpacity>
    };
  };

  onPress1 = () => {
    this.setState({ switchOn1: !this.state.switchOn1 });
  }
  onPress2 = () => {
    this.setState({ switchOn2: !this.state.switchOn2 });
  }
  onPress3 = () => {
    this.setState({ switchOn3: !this.state.switchOn3 });
  }
  onPress4 = () => {
    this.setState({switchOn4: !this.state.switchOn4});
  };

  constructor(props) {
    super(props);
    
    this.state = { ...initialState,
      switchOn1: false,
      switchOn2: false,
      switchOn4: false,
      valueArray: [],
      selectedItem : this.props.navigation.state.params.selectedItem,
      comeFromCreateInvoice  : this.props.navigation.state.params.comeFromCreateInvoice,
      mytaxname:""
    };
    console.log("selectedItem---> ",this.state.selectedItem)
    listSelectItemTax = [];
    this.index = 0;
    this.animatedValue = new Animated.Value(0);
  }

  componentDidMount(){
    this._retrieveAccountData()
  }

  UNSAFE_componentWillMount(){
    const { navigation } = this.props;
    const item = navigation.getParam('item', 'NO-ITEM');   
    
    this.props.navigation.setParams({ productUpdateAsync: this._productUpdateAsync });    
  }

  handleOnNavigateBack = (item) => {
    listSelectItemTax.push(item)
    console.log("ProductAdd Screen &&&&&&&&&&&& ",listSelectItemTax)
    storage.save({
      key: 'selectItemTax', // Note: Do not use underscore("_") in key!
      data: listSelectItemTax,
     
      // if expires not specified, the defaultExpires will be applied instead.
      // if set to null, then it will never expire.

      expires: null //1000 * 3600
    });
    console.log('handleOnNavigateBack call product list')
    console.log(listSelectItemTax)

    this.setState({ newSubtotal: Number(0.00) });

    console.log("listSelectItemTax length" , listSelectItemTax.length)
    subtotalCount = 0.0

    listSelectItemTax.map((item, key) => {
      console.log("Q * P" , math.eval(Number(item.quantity) * Number(item.unit_cost)))
      //subtotalCount += math.eval(Number(item.quantity) * Number(item.price));
      subtotalCount += Number(item.quantity) * Number(item.unit_cost);
      console.log("subtotal" , subtotalCount)
    })
    console.log("subtotal count" , subtotalCount)

  
    this.setState({ newSubtotal: Number(subtotalCount)});
    console.log(this.state.newSubtotal)

    this.addMore()
  }
  
  gotoAddTax = () => {
    console.log('In ProductScreen screen');
    this.props.navigation.navigate('AddTax', {
      onNavigateBack: this.handleOnNavigateBack.bind(this)
    })
  };

  addMore = () => {
    console.log('Add More Call')
    this.animatedValue.setValue(0);

    let newlyAddedValue = { index: this.index }

    console.log(newlyAddedValue)

    storage.load({
      key: 'selectItemTax',

      autoSync: true,

      syncInBackground: true,

      syncParams: {
        extraFetchOptions: {
          // blahblah
        },
        someFlag: true
      }
    })
      .then(ret => {
        // found data go to then()
        console.log('we found data Invoease Detail');
        console.log(ret);

       
        this.setState({ disabled: true, valueArray: [...this.state.valueArray, ret] }, () => {
          Animated.timing(
            this.animatedValue,
            {
              toValue: 1,
              duration: 500,
              useNativeDriver: true
            }
          ).start(() => {
            this.index = this.index + 1;
            this.setState({ disabled: false });
            //console.log(...this.state.valueArray)
          });
        });

        //console.log(ret.states);

        // var mystates = []
        // ret.states.map(state => mystates.push(state.name) );
        // this.setState({ stateLabel : mystates, stateOptions: mystates  });
      })
      .catch(err => {
        // any exception including data not found
        // goes to catch()
        console.log('error home' + err);
        console.warn(err.message);
        switch (err.name) {
          case 'NotFoundError':
            // TODO;
            break;
          case 'ExpiredError':
            // TODO
            break;
        }
      });

  }

  renderTaxesDropdown() {
    console.log("renderTaxesDropdown called");
    return (
      <View styles={{ marginTop: 0, marginBottom: 0 }}>
        <List
          containerStyle={{
            marginTop: 0,
            marginLeft: 0,
            paddingLeft: 0,
            borderTopWidth: 0
          }}
        >
          <Query query={GET_TAXES}>
            {({ loading, error, data }) => {
              if (loading) return <Text>Loading...</Text>;
              if (error) {
                return <Text>Error :)</Text>;
              } else {
              }

              console.log(this.state.focusNodes);
              console.log("this is data", data);
              this.state.mydropdowndata = data.taxes;
              console.log("mydata", this.state.mydropdowndata);
              return <View>
                {
                  this.dropdown()
                }
                </View>;
            }}
          </Query>
        </List>
      </View>
    );
  }

  dropdown() {
    console.log("dropdown called");
    let data = this.state.mydropdowndata;
    console.log("dataaaaa---->",data);
    //this for loop is used for copy the name field object of taxes array into value field because drop down fiels takes value from only value field
    var i;
    for (i = 0; i < data.length; i++) 
    {
          data[i].value = data[i]["name"] + " (" + data[i]["rate"] + "%)";
      //  delete data[i].name;
    }
    return (
      <Dropdown
        label="    Select Tax"
        data={data}
        style={{marginLeft:12}}
        //onChangeText={(value)=>{ this.setState({dropdownValue:value})}}
        //onChangeText={(value,index,data)=>{alert(data),console.log("dropdown dataa- >",data[index].id)}}
        onChangeText={(value,index,data)=>
          {
            console.log("aaaaaaaaaaaaaaaa---->>>>",data[index])
            this.setState({selectedTaxes:data[index]})
          }}
         value={this.state.mytaxname}
        
      />
    );
  }

  render() {
    let { name, description, price, quantity, inventory, account_id } = this.state;
    console.log("price qz  === " , this.state.price)
    console.log("price qz  === " , this.state.tax1_amt)
    console.log("price qz  === " , this.state.tax2_amt)
    const animationValue = this.animatedValue.interpolate(
      {
        inputRange: [0, 1],
        outputRange: [-59, 0]
      });

    this.state.LineInputArr = []
    console.log("value array tax => ", JSON.stringify(this.state.valueArray));
    
    return (
      <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={Platform.select({ios: 0, android: 80})} style={styles.container}>
      <ScrollView style={styles.container}>
        <SafeAreaView style={styles.container}>
          <StatusBar barStyle="light-content" />
            <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>
              <View style={styles.container}>
                <View style={styles.container}>
                  <Formik
                    onSubmit={values => alert(JSON.stringify(values, null, 2))}
                    validationSchema={validationSchema}
                    initialValues={{ star: true }}>
                    <Form style={styles.infoContainer} keyboardShouldPersistTaps='always'> 
                      <FloatingLabel
                        labelStyle={styles.labelInput}
                        inputStyle={styles.input}
                        style={styles.formInput}
                        value={this.state.name}
                        onChangeText={(name) => this.setState({ name })}
                        placeholder=""
                        onBlur={this.onBlur}>

                        Item Name
                      </FloatingLabel>
                      <View style={styles.payamounttitle}>
                        <FloatingLabel
                          labelStyle={styles.labelInput}
                          inputStyle={styles.input}
                          style={styles.formInput}
                          value={this.state.description}
                          onChangeText={(description) => this.setState({ description })}
                          onBlur={this.onBlur}>

                          Description
                        </FloatingLabel>
                      </View>
                      <View style={{flexDirection: 'row'}}>
                        <View style={styles.paymenttype}>
                          <FloatingLabel
                            labelStyle={styles.labelInput}
                            inputStyle={styles.input}
                            style={styles.formInput}
                            value={this.state.price.toString()}
                            onChangeText={(price) => this.setState({ price })}
                            keyboardType = "numeric"
                            returnKeyType='done' 
                            onBlur={this.onBlur}>

                            Rate
                          </FloatingLabel>
                          <DoneButton
                            title="Done!"   
                            style={{ backgroundColor: 'red' }} 
                            doneStyle={{ color: 'green' }}  
                          />                          
                        </View>
                        {/*<View style={styles.paymentdate}>
                          <Text style={styles.amounttitle}>Quantity</Text>
                          <View>
                            <TouchableOpacity>
                              <TextInput
                              style={styles.amount}
                              value={this.state.quantity.toString()}
                              onChangeText={(quantity) => this.setState({ quantity })}
                              placeholder="Quantity"
                              returnKeyType='done'
                              keyboardType = "number-pad"
                            />
                            <DoneButton
                                title="Done!"   
                                style={{ backgroundColor: 'red' }} 
                                doneStyle={{ color: 'green' }}  
                            />
                            </TouchableOpacity>
                          </View>
                        </View>*/}
                        
                        <View style={styles.paymentdate}>
                          <FloatingLabel
                            labelStyle={styles.labelInput}
                            inputStyle={styles.input}
                            style={styles.formInput}
                            value={this.state.quantity.toString()}
                            onChangeText={(quantity) => this.setState({ quantity })}
                            returnKeyType='done'
                            keyboardType = "number-pad"
                            onBlur={this.onBlur}>

                            Quantity
                          </FloatingLabel>
                          <DoneButton
                              title="Done!"   //not required, default value = `Done`
                              style={{ backgroundColor: 'red' }}  //not required
                              doneStyle={{ color: 'green' }}  //not required
                          />
                        </View>
                      </View>      
                      
                      {/* <View style={styles.payamounttitle}>
                        <View style={{flex: 3, flexDirection: 'column'}}>
                          <Text style={styles.amounttitle}>Tax</Text>
                          <Text style={styles.tax}>9% - 9%</Text>
                        </View>
                        <View style={{flex: 1, flexDirection: 'column'}}>
                        <SwitchToggle
                          switchOn={this.state.switchOn1}
                          onPress={this.onPress1}
                        />
                        </View>
                      </View> */}

                      {/* <View style={styles.payamounttitle}>
                        <View style={{flex: 3, flexDirection: 'column'}}>
                        <Text style={styles.tax}>50% - 50%</Text>
                      </View>
                      <View style={{flex: 1, flexDirection: 'column'}}>
                      <SwitchToggle
                          switchOn={this.state.switchOn2}
                          onPress={this.onPress2}
                      />
                      </View>
                      </View> */}
                       
                      <View >
                        {/* <View style={{flex: 1, flexDirection: 'column'}}>
                          <FloatingLabel
                            labelStyle={styles.labelInput}
                            inputStyle={styles.input}
                            style={styles.formInput}
                            value={this.state.tax1_amt.toString()}
                            onChangeText={(tax1_amt) => this.setState({ tax1_amt })}
                            returnKeyType='done'
                            keyboardType = "number-pad"
                            onBlur={this.onBlur}>

                            Tax
                          </FloatingLabel>
                        </View> */}

                        {this.renderTaxesDropdown()}
                      </View>

                      <View>
                        <View style={{flex: 1, flexDirection: 'column', paddingHorizontal: 15}}>
                          <Text style={styles.tax}>Line Total</Text>
                          <Text style={styles.amount}>{math.eval(Number(this.state.quantity) * Number(this.state.price))}</Text>
                        </View>
                      </View>
                      <View style={{height:0.5,backgroundColor:'#999'}}></View>

                      {/*<Button raised
                        buttonStyle={styles.buttonContainer}
                        icon={{ name: 'fingerprint' }}
                        title='SUBMIT'
                        onPress={this._productUpdateAsync} />*/}
                    </Form>
                  </Formik>
                </View>
              </View>
            </TouchableWithoutFeedback>
        </SafeAreaView>
      </ScrollView>
      </KeyboardAvoidingView>
    );
  }

  _productAddAsync = async () => {

    console.log("this is state from edit product",this.state);

    this.props.
    create_product(this.state.name, this.state.description, Number(this.state.price), Number(this.state.quantity), 
    Number(this.state.inventory), Number(this.state.account_id))
      .then(({ data }) => {
        console.log("create product : " + JSON.stringify(data));
        if (data) {
          //AsyncStorage.setItem('token', data.signIn.authentication_token);
          //AsyncStorage.setItem('account',data.signIn.user.account)
          //this.props.navigation.navigate('App');
          //alert('Product added successfully.')

          Alert.alert(
            'Invoease',
            'Product added successfully.',
            [
              {text: 'OK', onPress: () => this.props.navigation.pop()},
            ],
            {cancelable: false},
          );

        }
      })
  };

  handleOnNavigateBackFromEditProduct = (item) => {
  
    console.log("item passing ", item)
    this.props.navigation.state.params.onNavigateBack(item)
    this.props.navigation.pop()
  }
  moveBack()
  {
    var input = {"account_id":this.state.selectedItem.account_id, "description": this.state.description,"id":this.state.selectedItem.id,
    "inventory":this.state.selectedItem.inventory,"name":this.state.name,"price":this.state.price,
    "quantity": this.state.quantity,"tax1_id":this.state.selectedItem.tax1_id,"tax2_id":this.state.selectedItem.tax2_id,
    "tax1_amt":this.state.tax1_amt,"tax2_amt":this.state.tax2_amt};

    // this.props.navigation.state.params.onNavigateBack(input)
    this.props.navigation.pop()
  }

  _productUpdateAsync = async () => {
    console.log("its product update func.");
    console.log("account_id", this.state.selectedItem.account_id, "description", this.state.description,"id",this.state.selectedItem.id,
    "inventory",this.state.selectedItem.inventory,"name",this.state.name,"unit_cost",this.state.price,
    "quantity", this.state.quantity,"tax1_id",this.state.selectedTaxes.id,"tax2_id",this.state.selectedTaxes.id,
    "tax1_amt",this.state.selectedTaxes.rate?this.state.selectedTaxes.rate:this.state.tax1_amt,"tax2_amt",this.state.selectedTaxes.rate?this.state.selectedTaxes.rate:this.state.tax1_amt);

    var input = {"account_id":this.state.selectedItem.account_id, "description": this.state.description,"id":this.state.selectedItem.id,
    "inventory":this.state.selectedItem.inventory,"name":this.state.name,"unit_cost":this.state.price,
    "quantity": this.state.quantity,"tax1_id":this.state.selectedTaxes.id,"tax2_id":this.state.selectedTaxes.id,
    "tax1_amt":this.state.selectedTaxes.rate?this.state.selectedTaxes.rate:this.state.tax1_amt,"tax2_amt":this.state.selectedTaxes.rate?this.state.selectedTaxes.rate:this.state.tax1_amt
  };

    //this.props.navigation.state.params.onNavigateBack(this.state.selectedItem)

    if(this.state.comeFromCreateInvoice == "true" || this.state.comeFromCreateInvoice == "insideLine") {
      console.log("IF executed")
      console.log("input data------>>>>",input)
      this.handleOnNavigateBackFromEditProduct(input)
    }
    else {
      console.log("ELSE executed")
      console.log("input ===> ", input);
      //this.props.navigation.state.params.onNavigateBack(input)
      //this.props.navigation.pop()

      console.log("zzzzz->",Number(this.state.id), this.state.name, this.state.description, Number(this.state.price), 
      Number(this.state.quantity), Number(this.state.inventory),Number(this.state.selectedTaxes), Number(this.state.account_id));

      this.props.
      update_product(Number(this.state.id), this.state.name, this.state.description, Number(this.state.price), 
      Number(this.state.quantity), Number(this.state.inventory),Number(this.state.selectedTaxes), Number(this.state.account_id))
        .then(({ data }) => {
          console.log("update product : " + JSON.stringify(data));
          if (data) {
          
            Alert.alert(
              'Invoease',
              'Product update successfully.',
              [
                {text: 'OK', onPress: () => this.moveBack()},
              ],
              {cancelable: false},
            );

          }
        })

      }  
    
  };


  _retrieveAccountData = async () => {
    console.log("comeFromCreateInvoice" , this.state.comeFromCreateInvoice);
    //  console.log("selected item onoy = ",this.state.selectedItem)
    //  console.log("selected item priceeeee" , this.state.selectedItem.price)
    //  console.log("selected item quantityyyyyy" , this.state.selectedItem.quantity)
    
     //this.setState({ name: this.state.selectedItem.name })
     //name = this.state.selectedItem.name;
      
    //  this.state.name = this.state.selectedItem.name
    //  this.state.description = this.state.selectedItem.description
    //  this.state.price =  this.state.selectedItem.price
    //  this.state.quantity = this.state.selectedItem.quantity
    
    // console.log("selectedItem =>>>>> ",this.state.selectedItem);

    if(this.state.comeFromCreateInvoice == "insideLine") {
      console.log("insideline function...... selecteditem",this.state.selectedItem)
      this.setState({id: this.state.selectedItem.id,account_id:this.state.selectedItem.account_id, 
        name:this.state.selectedItem.name, description:this.state.selectedItem.description, 
        price: this.state.selectedItem.price, quantity:this.state.selectedItem.quantity, 
        tax1_id:this.state.selectedItem.tax1_id, tax2_id:this.state.selectedItem.tax2_id,
        tax1_amt:0,tax2_amt:0})
    }
    else {
    if(this.state.selectedItem.tax.length > 0)
     {
      console.log("come from product")
      console.log("raj--->",this.state.selectedItem.tax[0].rate)
      console.log("raj2--->",this.state.selectedItem)
      this.setState({id: this.state.selectedItem.id,account_id:this.state.selectedItem.account_id,
        name:this.state.selectedItem.name, description:this.state.selectedItem.description, 
        price: this.state.selectedItem.price, quantity:this.state.selectedItem.quantity, 
        tax1_id:this.state.selectedItem.tax1_id, tax2_id:this.state.selectedItem.tax2_id,
        tax1_amt:this.state.selectedItem.tax[0].rate,tax2_amt:this.state.selectedItem.tax[0].rate,
        mytaxname:this.state.selectedItem.tax[0].name +" ("+this.state.selectedItem.tax[0].rate +"%)"})
    }
    else {
      console.log("come from aaaaa")
      this.setState({id: this.state.selectedItem.id,account_id:this.state.selectedItem.account_id, 
        name:this.state.selectedItem.name, description:this.state.selectedItem.description, 
        price: this.state.selectedItem.price, quantity:this.state.selectedItem.quantity, 
        tax1_id:this.state.selectedItem.tax1_id, tax2_id:this.state.selectedItem.tax2_id,
        tax1_amt:0,tax2_amt:0})
        console.log("--------------",this.state)

    }
  }
    console.log("sele tax",this.state.selectedItem.tax);  
    console.log("Tax1 rate : " + this.state.tax1_amt);

    console.log("selectedItem ===??? " , this.state.name)

    try {
      console.log("selectedItem ===??? " , selectedItem)
      const value = await AsyncStorage.getItem('account');
      if (value !== null) {
        // We have data!!
        console.log('We have data!');
        console.log(JSON.parse(value));
        let data = JSON.parse(value)
        console.log(data[0]);
        console.log(data[0].id);
        this.setState({
          account_id:data[0].id ,
          name:this.state.selectedItem.name
        }) 
        console.log('Account ID');
        console.log("name", this.state.name);
        console.log("description", this.state.description);
      }
    } catch (error) {
      // Error retrieving data
    }
  };
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(255, 255, 255)',
    flexDirection: 'column'
  },
  logoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  logo: {
    width: 266,
    height: 50
  },
  title: {
    color: '#f7c744',
    fontSize: 18,
    textAlign: 'center',
    marginTop: 5,
    opacity: 0.9
  },
  infoContainer: {
    flexDirection: 'column',
    flex: 1
  },
  buttonContainer: {
    backgroundColor: '#373f51',
    marginTop: 10
  },
  buttonText: {
    textAlign: 'center',
    color: 'rgb(32,53,70)',
    fontWeight: 'bold',
    fontSize: 18
  },
  paytitle: {
    color: '#666',
    fontSize: 19,
    fontWeight: '500',
    paddingTop: 0,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 0
  },
  paytext: {
    color: '#999',
    fontSize: 18,
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 10,
    paddingLeft: 0
  },
  addTaxes: {
    color: 'skyblue',
    fontSize: 18,
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 10,
    paddingLeft: 10
  },
  payamounttitle: {
    //flex: 1,
    //flexDirection: 'row',
    borderWidth: 0.5,
    borderColor: '#999',
    backgroundColor: '#fff',
    paddingLeft: 0,
  },
  amounttitle: {
    color: '#666',
    fontSize: 18
  },
  amount: {
    marginTop: 5,
    color: '#444',
    fontSize: 21
  },
  tax: {
    marginTop: 5,
    color: '#ccc',
    fontSize: 17,
    flexWrap: 'wrap'
  },
  paymenttype: {
    width: '50%',
    borderWidth: 0.5,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 0.5,
    borderColor: '#999',
    backgroundColor: '#fff',
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 0
  },
  paymentdate: {
    width: '50%',
    borderWidth: 0.5,
    borderTopWidth: 0,
    borderBottomWidth: 0.5,
    borderColor: '#999',
    backgroundColor: '#fff',
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 0
  },
  labelInput: {
    color: '#ccc',
    fontSize: 15,
    paddingLeft: 15
  },
  formInput: {    
    // borderBottomWidth: 1,
    paddingLeft: 5,
    borderColor: '#ccc'
  },
  input: {
    borderWidth: 0
  }
});

export default graphql(UPDATE_PRODUCT,
  {
    props: ({ mutate }) => ({
      update_product: (id, name, description, price, quantity, inventory,tax1_id, account_id) => mutate({ variables: { id, name, description, price, quantity, inventory,tax1_id, account_id } })
    }),

    options: {
      refetchQueries: [{
          query:GET_PRODUCTS
      }
      ],
    }
  }
)(EditProduct)

// export default graphql(CREATE_PRODUCT,
//   {
//     props: ({ mutate }) => ({
//       create_product: (name, description, price, quantity, inventory, account_id) => mutate({ variables: { name, description, price, quantity, inventory, account_id } })
//     }),

//   }
// )(EditProduct)