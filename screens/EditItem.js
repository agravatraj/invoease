import React from 'react';
import { Platform, ScrollView, StyleSheet, Text, View,TouchableOpacity,Image,TextInput } from 'react-native';
import { SearchBar, List, ListItem } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome';
import { ADD_PAYMENT_FOR_INVOICE } from "../graphqlQueries";
import { graphql } from 'react-apollo';
import { AsyncStorage, SafeAreaView, StatusBar, Keyboard, KeyboardAvoidingView, TouchableWithoutFeedback } from 'react-native';
import { Button } from 'react-native-elements'
import { Formik, Field } from "formik";
import SimplePicker from 'react-native-simple-picker';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";
import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import * as Yup from "yup";
const Form = withNextInputAutoFocusForm(ScrollView, {
submitAfterLastInput: false
});
import makeInput, {
KeyboardModal,
withPickerValues
} from "react-native-formik";

import ToggleSwitch from "toggle-switch-react-native";

const MyInput = compose(handleTextInput, withNextInputAutoFocusInput)(TextField);

const options = ['Bank Transfer', 'PayPal', 'Stripe','Credit Card', 'Debit Card', 'Check','Cash','Other'];
const labels = ['Bank Transfer', 'PayPal', 'Stripe','Credit Card', 'Debit Card', 'Check','Cash','Other'];

const validationSchema = Yup.object().shape({ 

});

const initialState = {
  amount: "0.0",
  date: "",
  payment_method: "1",
  notes: "2",
  invoice_id: "",
  first_name: "pratik"
};

class EditItem extends React.Component {
constructor(props) {
super(props);
this.state = {  };
}
render() {
return (
<View style={styles.container}>
  <Formik
        onSubmit={values => alert(JSON.stringify(values, null, 2))}
        validationSchema={validationSchema}
        initialValues={{ star: true }}>
    <Form style={styles.infoContainer} keyboardShouldPersistTaps='always'> 
          <Text style={styles.paytext}> Item Name</Text>     
          <Text style={styles.paytitle}>Design Consulting</Text>
          <View style={styles.payamounttitle}>
            <Text style={styles.amounttitle}>Description</Text>
            <TextInput
              style={styles.amount}
              onChangeText={(amount) => this.setState({amount})}
              value={this.state.amount}
            />
          </View>
          <View style={{flexDirection: 'row'}}>
            <View style={styles.paymenttype}>
              <Text style={styles.amounttitle}>Rate</Text>
              <View>
                <Text style={styles.amount} onPress={() => { this.refs.picker2.show(); }}>{this.state.selectedState}</Text>
              </View>
              <SimplePicker
                ref={'picker2'}
                options={options}
                labels={labels}
                itemStyle={{
                  color: '#444',
                  fontSize: 21
                }}
                onSubmit={(option) => {
                  this.setState({ selectedState: option, });
                  this.setState({ State: option, });
                }}
              />
            </View>
            <View style={styles.paymentdate}>
              <Text style={styles.amounttitle}>Quantity</Text>
              <View style={{ flexWrap: 'wrap', alignItems: 'flex-start', flexDirection: 'row', }}>
                <TouchableOpacity>
                  <Text style={styles.amount}>15</Text>
                </TouchableOpacity>
                <DateTimePicker isVisible={this.state.isDateTimePickerVisible} onConfirm={this._handleDatePicked} onCancel={this._hideDateTimePicker} />
              </View>
            </View>
          </View>      
          <View style={styles.payamounttitle}>
            <View style={{flex: 3, flexDirection: 'column'}}>
              <Text style={styles.amounttitle}>Tax</Text>
              <Text style={styles.tax}>9% - 9%</Text>
            </View>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <ToggleSwitch
                isOn={false}
                onColor='green'
                offColor='#eee'
                labelStyle={{color: 'black', fontWeight: '900'}}
                size='medium'
                onToggle={ (isOn) => console.log('changed to : ', isOn) }
              />
            </View>
          </View>
          <View style={styles.payamounttitle}>
            <View style={{flex: 3, flexDirection: 'column'}}>
            <Text style={styles.tax}>50% - 50%</Text>
          </View>
          <View style={{flex: 1, flexDirection: 'column'}}>
            <ToggleSwitch
              isOn={false}
              onColor='green'
              offColor='#eee'
              labelStyle={{color: 'black', fontWeight: '900'}}
              size='medium'
              onToggle={ (isOn) => console.log('changed to : ', isOn) }
            />
          </View>
          </View>
          <View style={styles.payamounttitle}>
            <View style={{flex: 3, flexDirection: 'column'}}>
              <Text style={styles.tax}>GST - 3%</Text>
            </View>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <ToggleSwitch
                isOn={false}
                onColor='green'
                offColor='#eee'
                labelStyle={{color: 'black', fontWeight: '900'}}
                size='medium'
                onToggle={ (isOn) => console.log('changed to : ', isOn) }
              />
            </View>
          </View>
          <View style={styles.payamounttitle}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <Text style={styles.amounttitle}>Line Total</Text>
              <Text style={styles.amount}>0.00</Text>
            </View>
          </View>
          <View style={styles.payamounttitle}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <Text style={styles.amounttitle}>Line Total</Text>
              <Text style={styles.amount}>0.00</Text>
            </View>
          </View>
          <View style={styles.payamounttitle}>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <Text style={styles.amounttitle}>Line Total</Text>
              <Text style={styles.amount}>0.00</Text>
            </View>
          </View>
          <Text style={styles.addTaxes}> Add Taxes</Text>
          <Text style={styles.addTaxes}> Add Taxes</Text>
          <Text style={styles.addTaxes}> Add Taxes</Text>
          <Text style={styles.addTaxes}> Add Taxes</Text>
          <Button raised
                      buttonStyle={styles.buttonContainer}
                      //icon={{ name: 'fingerprint' }}
                      title='Update'
                      onPress={this._productUpdateAsync} />
        </Form>
      </Formik>
    </View>
);
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f8f9'
  },
  paytitle: {
    color: '#666',
    fontSize: 19,
    fontWeight: '500',
    paddingTop: 0,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 20
  },
  paytext: {
    color: '#999',
    fontSize: 18,
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 10,
    paddingLeft: 15
  },
  addTaxes: {
    color: 'skyblue',
    fontSize: 18,
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 10,
    paddingLeft: 10
  },
  payamounttitle: {
    flex: 1,
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#999',
    backgroundColor: '#fff',
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 15,
  },
  amounttitle: {
    color: '#666',
    fontSize: 18
  },
  amount: {
    marginTop: 5,
    color: '#444',
    fontSize: 21
  },
  tax: {
    marginTop: 5,
    color: '#ccc',
    fontSize: 17,
    flexWrap: 'wrap'
  },
  paymenttype: {
    width: '50%',
    borderWidth: 1,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderBottomWidth: 0,
    borderColor: '#999',
    backgroundColor: '#fff',
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 15
  },
  paymentdate: {
    width: '50%',
    borderWidth: 1,
    borderTopWidth: 0,
    borderBottomWidth: 0,
    borderColor: '#999',
    backgroundColor: '#fff',
    paddingTop: 15,
    paddingRight: 15,
    paddingBottom: 15,
    paddingLeft: 15
  }
});

export default EditItem;