import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  AsyncStorage,
  View,
  TouchableWithoutFeedback,
  Alert
} from 'react-native';
import { WebBrowser } from 'expo';

import { List, ListItem, Button, SearchBar,Avatar } from 'react-native-elements';

import { Query } from "react-apollo";
import { GET_CLIENTS } from "../graphqlQueries";
import { DESTORY_CLIENT } from "../graphqlQueries";
import Swipeout from 'react-native-swipeout';
import { graphql } from 'react-apollo';

import DropdownAlert from 'react-native-dropdownalert';

import {
  BallIndicator,
  BarIndicator,
  DotIndicator,
  MaterialIndicator,
  PacmanIndicator,
  PulseIndicator,
  SkypeIndicator,
  UIActivityIndicator,
  WaveIndicator
} from 'react-native-indicators';

import SearchInput, { createFilter } from  'react-native-search-filter';
import IconFont from 'react-native-vector-icons/FontAwesome';


const KEYS_TO_FILTERS = ['company_name'];

class ClientScreen extends React.Component {

  // static navigationOptions = {
  //   title: 'app.json',
  //   headerTitle: 'Client',
  //   headerLeft: (
  //     <Button
  //       icon={{ name: 'user-circle', type: 'font-awesome', color: 'black' }}
  //       title=''
  //       backgroundColor='transparent' />
  //   ),
  // };

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Client',
      headerTitle: 'Client',
      headerRight: <TouchableOpacity onPress={navigation.getParam('gotoAddClient')}><View style={{marginRight:20}}><IconFont name={'plus'} size={22} color={'#373f51'} style={{paddingRight:10}}/></View></TouchableOpacity>
    };
  };

  constructor(props) {
    super(props);
    console.log("Client Screen Constructor method callllllllllll")
    this.state = {
      searchTerm: '',
      rowItem: null,
      filteredClients:[],
      flag:false
    }

    const token = async () => {
      try {
        const value = await AsyncStorage.getItem('token');
        if (value !== null) {
          // We have data!!
          console.log('value : ' + value);
        }
      } catch (error) {
        // Error retrieving data
        console.log('error' + error);
      }
    }
    console.log('token client' + token);
  }


  itemAction( item) {
    console.log('in item action')
    switch (item.type) {
      case 'close':
        this.forceClose();
        break;
      default:
        const random = Math.floor(Math.random() * 1000 + 1);
        const title = item.type + ' #' + random;
        this.dropdown.alertWithType(item.type, title, item.message);
    }
  }
  handleOnNavigateBack = (item) => {
    console.log("handleOnNavigateBack" , filteredClients.length);
       this.setState({flag : false})
  }
  handleOnNavigationbackClientDetail=()=>
{
  console.log("back from client detail screen....")
  this.setState({flag:false})
}
  
  forceClose() {
    this.dropdown.close();
  }
  onClose(data) {
    console.log(data);
  }
  onCancel(data) {
    console.log(data);
  }
  _gotoAddClient = async () => {
   
    //App.showAlert();
   // IntroScreen.showAlert();
//    this.dropdown.alertWithType('success','title here','message here');
    //console.log(JSON.stringify(this.props));
    //this.props.showAlert();
    //this.itemAction({type:'info', message:'Message here', title:'title here'});
    
    //this.props.navigation.navigate('AddClient')


    console.log("gotoAdd client clicked")
    this.props.navigation.navigate('AddClient', {
     onNavigateBack: this.handleOnNavigateBack.bind(this)
    });

  };

  UNSAFE_componentWillMount() {
   this.props.navigation.setParams({ gotoAddClient: this._gotoAddClient });
  }

  searchUpdated(term) {
    this.setState({ searchTerm: term })
  }

  actionOnSelectClient(item) {

    const { navigation } = this.props;
    const isFromInvoiceDetail = navigation.getParam('isFromInvoiceDetail', 'NO-UPDATE-ITEM');
    console.log(isFromInvoiceDetail)

    if (isFromInvoiceDetail === "TRUE")
    {
      console.log('Selected Item :', item);
      this.props.navigation.state.params.onNavigateBack(item)
      this.props.navigation.pop()
    }
    else{
      //console.log('go to client detail screen :', item);
      this.props.navigation.navigate('ClientDetail',  {selectClient:item,
      //onNavigateBack: this.handleOnNavigationbackClientDetail.bind(this)
    })}
  }

  deleteRow(item) {
    //console.log(filteredServices)
    //console.log(JSON.stringify(item))
    console.log('Select ROW :' +  JSON.stringify(this.state.rowItem) )
  
    Alert.alert(
      'Are you sure?',
      'You want to delete this client',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: () => 
          this.deleteClient()
      },
      ],
      {cancelable: false},
    );
  }
  
  deleteClient() {
  
    console.log(JSON.stringify(this.state.rowItem.id)) 
  
        this.props.
        destroy_client(this.state.rowItem.id)
      .then(({ data }) => {
        console.log("Delete client : " + JSON.stringify(data));
        if (data) {
          Alert.alert(
            'Invoease',
            'Client destory successfully.',
            [
              {text: 'OK'},
            ],
            {cancelable: false},
          );
  
        }
      })
  
  };
  checkEmpty()
  {
    this.setState({flag : true})
  }
  render() 
  {

    //const filteredClients = emails.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))
    console.log("Client Screen render screen sdfsdf")
    return (
      (this.state.flag==true)
      ?
      <View style={{backgroundColor: 'white',flex:1 }}>
          <Image source={require('../assets/images/client.png')}
                 style={{ width: 292, height: 66, position: 'absolute', right: 28, top: 5,marginTop:25 }}>
          </Image>
      </View>
      :
      <View>
        {/* <SearchBar lightTheme placeholder='Type Here...' onChangeText={this.handleSearch} /> */}
        <SearchInput
          onChangeText={(term) => { this.searchUpdated(term) }}
          style={styles.searchInput}
          placeholder="Search clients"
          clearIcon={<IconFont name={'search'} size={20} color={'#373f51'} style={{paddingRight:5}}/>}
          returnKeyType="search"
        />
        <ScrollView style={{marginBottom:40,marginTop:10}}>
        <List containerStyle={{ marginBottom: 20, marginTop:0,backgroundColor:'transparent', borderColor:'transparent' }}>
          <Query query={GET_CLIENTS}>
            {({ loading, error, data }) => {
              //this.dropdown.alertWithType('error', 'Error', "Please check log");
              if (loading) return <PulseIndicator color='#4235cc'  />;
              if (error) {
                 
                 this.dropdown.alertWithType('error', 'Error', "Please check log");
                //console.log("TEST ERR =>" + error.graphQLErrors.map(x => x.message));
                return <Text>Error :)</Text>;
              }
              if(data)
              {
              // console.log(
              //   data.clients.map(({ company_name, contact } ) => [
              //     company_name, contact =>{return '${contact.first_name} ${contact.last_name}'}
              //   ])
              // );


              // console.log(
              //   data.clients.map(({ company_name }) => [
              //     company_name
              //   ])
              // );

                filteredClients = data.clients.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))
                console.log(JSON.stringify("GET_CLIENTS 3e ====",filteredClients))
                filteredClients.map((item, i) => (
                  console.log(JSON.stringify("GET_CLIENTS ====",item.company_name))
                ));
                }

                if(data.clients.length==0)
                {
                  this.checkEmpty()
                }

                let swipeBtns = [
                {
                  backgroundColor: 'transparent',
                  onPress: () => { this.deleteRow(filteredClients[this.state.rowID]) },                  
                  component: (
                    <View style={styles.swipeoutSide}>
                      
                      <IconFont name={'trash'} size={24} color={'#fff'} />
                    </View>

                  )
                },
              ];
              
              return (
                <View>
                  {
                    //data.clients.map((item, i) => (
                    filteredClients.map((item, i) => (
                      <Swipeout right={swipeBtns}
                        autoClose={true}
                        backgroundColor='transparent'
                        rowID={i}
                        key={i}
                        onOpen={(i) => {
                          console.log('VALUE OF ITEM :' + i)
                          this.setState({rowItem: item})
                        }}
                        style={styles.swipeData}>
                      <TouchableOpacity onPress={() => this.actionOnSelectClient(item)} key={i}>
                        <ListItem
                          key={i}
                          title={item.company_name}
                          titleStyle={{fontSize:18, fontWeight:'bold'}}
                          //subtitle={`${item.contacts[0].first_name} ${item.contacts[0].last_name}`}
                          wrapperStyle = {{backgroundColor:'#fff', alignItems:'center',
                          borderRadius:15, height:80, paddingLeft: 14,paddingRight:6
                         
                          }}
                          containerStyle={{marginBottom:0,marginTop:0,
                          paddingTop:0, paddingBottom:10,
                          borderBottomColor: 'transparent',
                          borderBottomWidth: 0,
                          shadowColor: '#999999',
                          shadowOffset: { width: 0, height: 2 },
                          shadowOpacity: 0.5,
                          shadowRadius: 3,
                          }}
                          noBorder
                          avatar={<Avatar
                            size="large"
                            width={55}
                            rounded
                            //title={`${item.contacts[0].first_name.substring(0, 1)}${item.contacts[0].last_name.substring(0, 1)}`}
                            backgroundColor='#4f2'
                            onPress={() => console.log("Works!")}
                            activeOpacity={0.7}
                           
                          />}
                        />
                      </TouchableOpacity>
                      </Swipeout>
                    ))
                  }
                </View>)
            }}
          </Query>
        </List>
        <DropdownAlert ref={ref => this.dropdown = ref} />
       
        </ScrollView>
        </View>
    );
  }

  _maybeRenderDevelopmentModeWarning() {
    if (__DEV__) {
      const learnMoreButton = (
        <Text onPress={this._handleLearnMorePress} style={styles.helpLinkText}>
          Learn more
        </Text>
      );

      return (
        <Text style={styles.developmentModeText}>
          Development mode is enabled, your app will be slower but you can use useful development
          tools. {learnMoreButton}
        </Text>
      );
    } else {
      return (
        <Text style={styles.developmentModeText}>
          You are not in development mode, your app will run at full speed.
        </Text>
      );
    }
  }

  _handleLearnMorePress = () => {
    WebBrowser.openBrowserAsync('https://docs.expo.io/versions/latest/guides/development-mode');
  };

  _handleHelpPress = () => {
    WebBrowser.openBrowserAsync(
      'https://docs.expo.io/versions/latest/guides/up-and-running.html#can-t-see-your-changes'
    );
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
  searchInput: {
    paddingTop:1,
    marginTop: 10,
    paddingLeft:10,
    marginLeft: 10,
    marginRight: 10,
    borderColor: '#CCC',
    height:40,
    borderWidth: 1,
    backgroundColor: 'white',
    borderRadius: 5,
  },
  swipeoutSide: {
    backgroundColor: '#f00',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius:15,
    height: 80,
    marginRight: 10,
    padding: 0
  }
});

export default graphql(DESTORY_CLIENT,
  {
    props: ({ mutate }) => ({
      destroy_client: (id) =>
        mutate({ variables: { id } })
    }),
    options: {
      refetchQueries: [{
        query: GET_CLIENTS
      }
      ],
    }

  }
)(ClientScreen)
