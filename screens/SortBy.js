import React from 'react';

import { StyleSheet, AsyncStorage, View, SafeAreaView, StatusBar, Keyboard, KeyboardAvoidingView, TouchableWithoutFeedback, Image, TextInput, ScrollView, Text, TouchableOpacity } from 'react-native';
import { Button, ListItem } from 'react-native-elements'
import { CREATE_SERVICE } from "../graphqlQueries";
import { graphql } from 'react-apollo';
import Icon from 'react-native-vector-icons/FontAwesome';
import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import { Formik } from "formik";
import * as Yup from "yup";
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";
import IconFont from 'react-native-vector-icons/FontAwesome';

const list = [
	{
		name: 'Client',
		icon: 'user'
	},
	{
		name: 'Invoice Number',
		icon: 'calculator'
	},
	{
		name: 'Status',
		icon: 'info'
	},
	{
		name: 'Amount',
		icon: 'dollar'
	},
	{
		name: 'Date of Issue',
		icon: 'calendar'
	},
	{
		name: 'Last Updated',
		icon: 'sort'
	},
]

class SortBy extends React.Component {


	static navigationOptions = ({ navigation }) => {
		return {
			title: 'Sort By',
			headerTitle: 'Sort By',
			headerRight: <TouchableOpacity onPress={navigation.getParam('sortAndReloadList')}><View style={{ marginRight: 20 }}><IconFont name={'check'} size={22} color={'#373f51'} style={{ paddingRight: 10 }} /></View></TouchableOpacity>
		};
	};

	_sortAndReloadList = async () => {

		this.props.navigation.state.params.onNavigateBack(this.state.checked)
		this.props.navigation.pop()
	}

	constructor(props) {
		super(props);
		this.state = {
			dataSource: {},
			terms: [],
			selectedItem: this.props.navigation.state.params.selectedItem[0] ? this.props.navigation.state.params.selectedItem[0] : null,
			checked: [],
			click: true
		};
	}

	checkItem = async (l) => {
		console.log('here;;', l);
		this.state.checked = [];
		console.log(l, 'Item added');

		if (!this.state.checked.includes(l)) {
			console.log(l, 'false');
			//this.setState({ checked: [...checked, l] });
			this.state.checked.push(l)
			this.setState({ terms: [] })
		}
		else {
			console.log(l, 'True');
			this.setState({ checked: checked.filter(a => a !== l) });
		}
		console.log("checked array after check item :", this.state.checked);
	};

	checkItem1 = async (l) => {
		console.log('here;;', l);
		//this.state.checked = [];
		console.log(l, 'Item added');

		if (!this.state.checked.includes(list[4])) {
			console.log(list[4], 'false');
			//this.setState({ checked: [...checked, list[4]] });
			this.state.checked.push(list[4])
		}
		else {
			console.log(l, 'True');
			this.setState({ checked: checked.filter(a => a !== l) });
		}
		console.log("checked array after check item :", this.state.checked);
	};

	
	UNSAFE_componentWillMount() {

		this.props.navigation.setParams({ sortAndReloadList: this._sortAndReloadList });
		console.log("component did mount selected item"+this.state.selectedItem)
		if (this.state.selectedItem) {
			this.checkItem(this.state.selectedItem);
		}
		else {
			this.checkItem1(this.state.selectedItem);
		}
		
		console.log("selectItem ", this.state.selectedItem);
	}
	

	renderTerms(l)
	 {
		 console.log("length is = "+this.state.checked)
		// if (checked && checked.length)
		// {
		// 	console.log("render items if exwcuted")
		// 	//this.setState({ checked: [...checked,l]})
		// }
		// else
		// {
		// 	console.log("render items else exwcuted")
		// 	this.setState({ checked: [...checked,l]})
		// }
		// console.log("Render Terms ITEM :" + l)
		
		 if (this.state.checked.includes(l))
		  {
			console.log("includes")
			return <ListItem
				onPress={() => this.checkItem(l)}
				key={l.name}
				title={l.name}
				leftIcon={<IconFont name={l.icon} size={20} color={'#373f51'} style={{ marginRight: 20, width: 20, textAlign: 'center' }} />}
				titleStyle={{ fontSize: 16, color: '#333' }}
				rightIcon={<IconFont name={'check'} size={20} color={'#188038'} style={{ paddingRight: 5 }} />}
			/>
		}
		else
		 {
			console.log("not includes")
			return <ListItem
				onPress={() => this.checkItem(l)}
				key={l.name}
				title={l.name}
				leftIcon={<IconFont name={l.icon} size={20} color={'#373f51'} style={{ marginRight: 20, width: 20, textAlign: 'center' }} />}
				titleStyle={{ fontSize: 16, color: '#333' }}
				//rightIcon={<IconFont name={'check'} size={20} color={'#188038'} style={{paddingRight:5}}/>}	
				hideChevron
			/>

		}
	}

	render() {
		console.log("CHECKED is = "+this.state.checked)
		return (
			<View style={styles.container}>
				<SafeAreaView style={styles.container}>
					<StatusBar barStyle="light-content" />
					<KeyboardAvoidingView behavior='padding' style={styles.container}>
						<TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>
							<View style={styles.container}>
								<View>
									{

										list.map((l) => (

											this.renderTerms(l)

											// <ListItem

											// 	onPress={() => this.checkItem(l)}  
											// 	key={i}

											// 	title={l.name}
											// 	titleStyle={{fontSize: 21, color: '#333'}}
											// 	hideChevron

											// />
										))
									}
								</View>
							</View>
						</TouchableWithoutFeedback>
					</KeyboardAvoidingView>
				</SafeAreaView>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: 'rgb(255, 255, 255)',
		flexDirection: 'column'
	},
	sortBy: {
		flexDirection: 'row',
		backgroundColor: '#fff',
		borderBottomWidth: 1,
		borderColor: '#ccc',
		padding: 15
	},
	sortTitle: {
		color: '#333',
		fontSize: 21
	},
	sortIcon: {
		flex: 0.5,
		flexDirection: 'column',
	},
});

export default SortBy;