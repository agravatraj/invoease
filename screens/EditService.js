import React from 'react';

import { StyleSheet, AsyncStorage, View, SafeAreaView, StatusBar, Keyboard, KeyboardAvoidingView, TouchableWithoutFeedback, Alert, TextInput,ScrollView, TouchableOpacity, Text } from 'react-native';
import { Button } from 'react-native-elements'
import { GET_SERVICES } from "../graphqlQueries";
import { UPDATE_SERVICE } from "../graphqlQueries";
import { graphql } from 'react-apollo';


import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import { Formik } from "formik";
import * as Yup from "yup";
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";

var FloatingLabel = require('react-native-floating-labels');

const MyInput = compose(handleTextInput, withNextInputAutoFocusInput)(TextField);

const Form = withNextInputAutoFocusForm(ScrollView, {
  submitAfterLastInput: false
});

const validationSchema = Yup.object().shape({
  name: Yup.string().required(),
  email: Yup.string()
    .required()
    .email(),
  password: Yup.string()
    .required()
    .min(6, "Too short"),
  confirmPassword: Yup.string().oneOf(
      [Yup.ref('password')],
      'Passwords do not match',
    ),
  star: Yup.boolean()
    .required()
    .oneOf([true])
});


const initialState = {
  name: "",
  description: "",
  price: 0,
  billable: true,
  hours: 0,
  account_id: ""
};

class EditService extends React.Component {
  // static navigationOptions = {
  //   title: 'Edit Service',
  //   headerRight: <TouchableOpacity onPress={this._UpdateService}><View style={{marginRight:20}}><Text style={{color: '#000', fontSize: 18,}}>Save</Text></View></TouchableOpacity>
  // };

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Edit Service',
      headerTitle: 'Edit Service',
      headerRight: <TouchableOpacity onPress={navigation.getParam('UpdateService')}><View style={{marginRight:20}}><Text style={{color: '#000', fontSize: 18,}}>Save</Text></View></TouchableOpacity>
    };
  };

  constructor(props) {
    super(props);
    //this.state = { ...initialState };
    console.log("Edit SERVICE")

    this.state = {
        serviceID: "",
        name: "",
        description: "",
        price: 0,
        billable: true,
        hours: 0,
        account_id: "",
        selectService : this.props.navigation.state.params.selectService,
    };

    console.log(this.state.selectService)
    console.log(this.state.selectService.price)
    console.log(this.state.selectService.hours)

    this._retrieveAccountData()
  }

  componentDidMount(){
    
    console.log("componentDidMount Called")
    this.setState({serviceID: this.state.selectService.id ,name: this.state.selectService.name, description: this.state.selectService.description, price: this.state.selectService.price, hours: this.state.selectService.hours})
    // console.log("id", this.state.selectService.id , "name ",this.state.name, "description", this.state.description, "price:" , this.state.price, "hours:", this.state.hours)
    console.log("id", this.state.selectService.id , "name ",this.state.name, "description", this.state.description, "price:" , this.state.price, "hours:", this.state.hours)

  }

  UNSAFE_componentWillMount(){
    this.props.navigation.setParams({ UpdateService: this._UpdateService });    
  }

  _retrieveAccountData = async () => {
    try {
      const value = await AsyncStorage.getItem('account');
      if (value !== null) {
        // We have data!!
        console.log('We have data! Add Services');
        let data = JSON.parse(value)
        this.setState({
            account_id:data[0].id 
        }) 
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  render() {
    console.log("render Called")
    return (
      <View style={styles.container}>
        <SafeAreaView style={styles.container}>
          <StatusBar barStyle="light-content" />
          <KeyboardAvoidingView behavior='padding' keyboardVerticalOffset={Platform.select({ios: 0, android: 80})} style={styles.container}>
            {/* <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}> */}
              <View style={styles.container}>
                {/* <View style={styles.logoContainer}>
                  <Image style={styles.logo}
                    source={require('../assets/images/logo.png')}>
                  </Image>
                </View> */}
                <Formik
                  onSubmit={values => alert(JSON.stringify(values, null, 2))}
                  validationSchema={validationSchema}
                  initialValues={{ star: true }}>
                  <Form style={styles.infoContainer} keyboardShouldPersistTaps='always'>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.name}
                      onChangeText={(name) => this.setState({ name })}
                      keyboardType='default'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtPrice.focus()}
                      forwardRef={"txtName"}
                      onBlur={this.onBlur}>

                      Name
                    </FloatingLabel>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={String(this.state.price)}
                      onChangeText={(price) => this.setState({ price })}
                      keyboardType='numeric'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.txtHours.focus()}
                      forwardRef={"txtPrice"}
                      onBlur={this.onBlur}>

                      Price
                    </FloatingLabel>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={String(this.state.hours)}
                      onChangeText={(hours) => this.setState({ hours })}
                      keyboardType='numeric'
                      returnKeyType='next'
                      autoCorrect={false}
                      onSubmitEsiting={() => this.refs.TxtDescription.focus()}
                      forwardRef={"txtHours"}
                      onBlur={this.onBlur}>

                      Hours
                    </FloatingLabel>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.description}
                      onChangeText={(description) => this.setState({ description })}
                      keyboardType='default'
                      returnKeyType='go'
                      autoCorrect={false}
                      //onSubmitEsiting={() => this.refs.txtZipCode.focus()}
                      forwardRef={"TxtDescription"}
                      onBlur={this.onBlur}>

                      Description
                    </FloatingLabel>
                    {/*<Button raised
                      buttonStyle={styles.buttonContainer}
                      //icon={{ name: 'fingerprint' }}
                      title='Update'
                      onPress={this._UpdateService} />*/}
                  </Form>
                </Formik>
              </View>
            {/* </TouchableWithoutFeedback> */}
          </KeyboardAvoidingView>
        </SafeAreaView>
      </View>
    );
  }


  _UpdateService = async () => {


    if (this.state.name == '') {
      Alert.alert("Please enter email.");
      return
    }

    if (this.state.price == '') {
      Alert.alert("Please enter price.");
      return
    }

    if (this.state.hours == '') {
      Alert.alert("Please enter hours.");
      return
    }

    if (this.state.email == '') {
      Alert.alert("Please enter email.");
      return
    }

    console.log(this.state);

        this.props.
        update_service(this.state.serviceID, this.state.name, this.state.description, Number(this.state.price), Boolean(this.state.billable), 
                        Number(this.state.hours))
      .then(({ data }) => {
        console.log("update service : " + JSON.stringify(data));
        if (data) {
          Alert.alert(
            'Invoease',
            'Service update successfully.',
            [
              {text: 'OK', onPress: () => this.props.navigation.pop()},
            ],
            {cancelable: false},
          );

        }
      })

  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(255, 255, 255)',
    flexDirection: 'column'
  },
  logoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    //backgroundColor: 'red',
  },
  logo: {
    width: 266,
    height: 50
  },
  title: {
    color: '#f7c744',
    fontSize: 18,
    textAlign: 'center',
    marginTop: 5,
    opacity: 0.9
  },
  infoContainer: {
    flexDirection: 'column',
    flex: 1
  },
  input: {
    height: 40,
    backgroundColor: 'rgba(0,0,0,0.2)',
    color: '#fff',
    marginBottom: 20,
    paddingHorizontal: 10
  },
  buttonContainer: {
    backgroundColor: '#373f51',
    marginTop: 15,
  },
  buttonText: {
    textAlign: 'center',
    color: 'rgb(32,53,70)',
    fontWeight: 'bold',
    fontSize: 18
  },

  labelInput: {
    color: '#ccc',
    fontSize: 15,
    paddingLeft: 15,
  },
  formInput: {    
    borderBottomWidth: 1, 
    paddingLeft: 5,
    borderColor: '#ccc',       
  },
  input: {
    borderWidth: 0
  }
});


export default graphql(UPDATE_SERVICE,
    {
      props: ({ mutate }) => ({
        update_service: (id, name, description, price, billable, hours, account_id) => 
        mutate({ variables: {id, name, description, price, billable, hours, account_id } })
      }),
      options: {
        refetchQueries: [{
            query:GET_SERVICES
        }
        ],
      }
    }
  )(EditService)

