import React from 'react';

import { StyleSheet, AsyncStorage, View, SafeAreaView, StatusBar, Keyboard, KeyboardAvoidingView, TouchableWithoutFeedback, Alert, TextInput,ScrollView, TouchableOpacity, Text } from 'react-native';
import { Button } from 'react-native-elements'

import { handleTextInput, withNextInputAutoFocusForm, withNextInputAutoFocusInput } from "react-native-formik";
import { Formik } from "formik";
import * as Yup from "yup";
import { compose } from "recompose";
import { TextField } from "react-native-material-textfield";

import { UPDATE_TERMS } from "../graphqlQueries";
import { GET_TERMS } from "../graphqlQueries";
import { graphql } from 'react-apollo';

var FloatingLabel = require('react-native-floating-labels');

const MyInput = compose(handleTextInput, withNextInputAutoFocusInput)(TextField);

const Form = withNextInputAutoFocusForm(ScrollView, {
  submitAfterLastInput: false
});

const validationSchema = Yup.object().shape({
  name: Yup.string().required(),
  email: Yup.string()
    .required()
    .email(),
  password: Yup.string()
    .required()
    .min(6, "Too short"),
  confirmPassword: Yup.string().oneOf(
      [Yup.ref('password')],
      'Passwords do not match',
    ),
  star: Yup.boolean()
    .required()
    .oneOf([true])
});

const initialState = {
  name: "",
  account_id: ""
};

class EditTerms extends React.Component {
  // static navigationOptions = {
  //   title: 'Edit Terms',
  //   headerRight: <TouchableOpacity onPress={this._UpdateTerms}><View style={{marginRight:20}}><Text style={{color: '#000', fontSize: 18,}}>Save</Text></View></TouchableOpacity>
  // };

  static navigationOptions = ({ navigation }) => {
    return {
      title: 'Edit Terms',
      headerTitle: 'Edit Terms',
      headerRight: <TouchableOpacity onPress={navigation.getParam('UpdateTerms')}><View style={{marginRight:20}}><Text style={{color: '#000', fontSize: 18,}}>Save</Text></View></TouchableOpacity>
    };
  };

  constructor(props) {
    super(props);
    //this.state = { ...initialState };

    this.state = {
        selectTerms: this.props.navigation.state.params.selectTerms,
        termsID: "",
        name: "",
        account_id: ""
    };

    console.log("EDIT TERMS")

    console.log(this.state.selectTerms)
    console.log(this.state.selectTerms.name)

    
    this._retrieveAccountData()
  }

  componentDidMount() {
    this.setState({termsID: this.state.selectTerms.id , name: this.state.selectTerms.name})
  }
  
  UNSAFE_componentWillMount(){
    this.props.navigation.setParams({ UpdateTerms: this._UpdateTerms });    
  }

  _retrieveAccountData = async () => {
    try {
      const value = await AsyncStorage.getItem('account');
      if (value !== null) {
        // We have data!!
        console.log('We have data! Add TERMS');
        let data = JSON.parse(value)
        this.setState({
            account_id:data[0].id 
        }) 
      }
    } catch (error) {
      // Error retrieving data
    }
  };


  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView style={styles.container}>
          <StatusBar barStyle="light-content" />
          <KeyboardAvoidingView behavior='padding' style={styles.container}>
            <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>
              <View style={styles.container}>
                {/* <View style={styles.logoContainer}>
                  <Image style={styles.logo}
                    source={require('../assets/images/logo.png')}>
                  </Image>
                </View> */}
                <Formik
                  onSubmit={values => alert(JSON.stringify(values, null, 2))}
                  validationSchema={validationSchema}
                  initialValues={{ star: true }}>
                  <Form style={styles.infoContainer} keyboardShouldPersistTaps='always'>
                    <FloatingLabel
                      labelStyle={styles.labelInput}
                      inputStyle={styles.input}
                      style={styles.formInput}
                      value={this.state.name}
                      onChangeText={(name) => this.setState({ name })}
                      keyboardType='default'
                      returnKeyType='go'
                      autoCorrect={false}
                      ref={"txtName"}
                      onBlur={this.onBlur}>
                      
                      Name
                    </FloatingLabel>
                    {/*<Button raised
                      buttonStyle={styles.buttonContainer}
                      //icon={{ name: 'fingerprint' }}
                      title='Update'
                      onPress={this._UpdateTerms} />*/}
                  </Form>
                </Formik>
              </View>
            </TouchableWithoutFeedback>
          </KeyboardAvoidingView>
        </SafeAreaView>

      </View>
    );
  }

  _UpdateTerms = async () => {
    
    console.log(this.state);

    if (this.state.name == '') {
      Alert.alert("Please enter Term.");
      return
    }
        this.props.
        update_term(this.state.termsID, this.state.name)
      .then(({ data }) => {
        console.log("update terms : " + JSON.stringify(data));
        if (data) {
          Alert.alert(
            'Invoease',
            'Terms update successfully.',
            [
              {text: 'OK', onPress: () => this.props.navigation.pop()},
            ],
            {cancelable: false},
          );

        }
      })
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(255, 255, 255)',
    flexDirection: 'column'
  },
  logoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    //backgroundColor: 'red',
  },
  logo: {
    width: 266,
    height: 50
  },
  title: {
    color: '#f7c744',
    fontSize: 18,
    textAlign: 'center',
    marginTop: 5,
    opacity: 0.9
  },
  infoContainer: {
    flexDirection: 'column',
    flex: 1
  },
  input: {
    height: 40,
    backgroundColor: 'rgba(0,0,0,0.2)',
    color: '#fff',
    marginBottom: 20,
    paddingHorizontal: 10
  },
  buttonContainer: {
    backgroundColor: '#1FA2FF',
    marginTop: 15,
  },
  buttonText: {
    textAlign: 'center',
    color: 'rgb(32,53,70)',
    fontWeight: 'bold',
    fontSize: 18
  },
  labelInput: {
    color: '#ccc',
    fontSize: 15,
    paddingLeft: 15
  },
  formInput: {    
    borderBottomWidth: 1,
    paddingLeft: 5,
    borderColor: '#ccc'
  },
  input: {
    borderWidth: 0
  }
});

export default graphql(UPDATE_TERMS,
  {
    props: ({ mutate }) => ({
        update_term: (id, name) => 
      mutate({ variables: {id, name } })
    }),
    options: {
        refetchQueries: [{
            query:GET_TERMS
        }
        ],
      }

  }
)(EditTerms)