import React from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import { AppLoading , Icon} from 'expo';
import AppNavigator from './navigation/AppNavigator';
import ApolloClient from "apollo-boost";

import { InMemoryCache } from 'apollo-cache-inmemory';

import { ApolloProvider } from "react-apollo";
import { apiConfig } from "./config/apiConfig";
import DropdownAlert from 'react-native-dropdownalert';

import * as Font from 'expo-font';
import { Asset } from 'expo-asset';
//import { Icon } from 'expo-Vector-icons';

const defaultOptions = {
  watchQuery: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'ignore',
  },
  query: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'all',
  },
}
const client = new ApolloClient( apiConfig.DEV_API_CONFIG);

// const getToken = async () => {
//   const token = await AsyncStorage.getItem('token')
//   console.log("tooken ==>>",token)
//   return token
// }
// const token = getToken()
// // Initiate apollo client
// const client = new ApolloClient({
  
//     //uri: "https://app.invoease.com/api",  // Live
//     uri: "https://beta.invoease.com/api", //live
//     //uri: "http://192.168.1.36:3000/api", // Local
//     // uri: "http://192.168.1.100:3000/api", // Local
//     credentials: "same-origin",
//     cache: new InMemoryCache(),
//     headers: {
//         authorization: token ? `Bearer ${token}` : ""
//     }
//   ,
//   cache: new InMemoryCache()
// })


export default class App extends React.Component {
  state = {
    isLoadingComplete: false,
  };

  constructor(props) {
    super(props);
    this.showAlert = this.showAlert.bind(this);
     
  }
  
  showAlert()
  {
    console.log('show alert in App.js');
  }
  render() {
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      );
    } else {
      return (
        <ApolloProvider client={client}>
        <View style={styles.container}>
          {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
          <AppNavigator />
          <DropdownAlert
          ref={ref => this.dropdown = ref}
          showCancel={true}
          onClose={data => this.onClose(data)}
          onCancel={data => this.onCancel(data)}
        />
        </View>
        </ApolloProvider>
      );
    }
  }

  componentDidMount() {
    // Font.loadAsync({
    //   //'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf'),
    //   'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
    // });

   
   
  }

  _loadResourcesAsync = async () => {
    console.disableYellowBox = true;
    return Promise.all([
      Asset.loadAsync([
        require('./assets/images/robot-dev.png'),
        require('./assets/images/robot-prod.png'),
      ]),

      // Font.loadAsync({
      //   // This is the font that we are using for our tab bar
      //   //...Icon.Ionicons.font,
      //   // We include SpaceMono because we use it in HomeScreen.js. Feel free
      //   // to remove this if you are not using it in your app
      //   'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
      // }),
      
    ]);
  };

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.log('Apollo error message');
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

export class DropDownHolder {
  static dropDown;
  static setDropDown(dropDown) {
      this.dropDown = dropDown;
  }
  static getDropDown() {
      return this.dropDown;
  }
}