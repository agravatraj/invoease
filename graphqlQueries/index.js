import gql from "graphql-tag";
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Remote debugger']);
// Queries

export const GET_TASKS = gql
  `query tasks {
    tasks {
      id
      name
      description
    }
  }
`;

export const GET_ITEMS = gql
  `query items {
    items {
      id
      name
      description
    }
  }
`;

export const GET_LANGUAGES = gql
  `query languages {
    languages{
      id
      name
      code
    }
  }
`;

export const GET_TIMEZONES = gql
  `query timezones {
    timezones{
      id
      name
      abbr
      UTC_offset
    }
  }
`;

export const GET_COUNTRIES = gql
  `query countries {
    countries{
      id
      name
      code
    }
  }
`;


export const GET_CURRENCIES = gql
  `query currencies {
    currencies{
      id
      name
      code
      symbol
    }
  }
`;


export const GET_PRODUCTS = gql
`query products{
  products{
    account_id
    id
    description
    inventory
    name
    price
    quantity
    tax1_id
    tax {
      rate
      name
    }
    tax2_id
  }
}`;

export const GET_INVOICE_NOTIFICATION = gql
`query get_invoice_activity($id: Int){
  activity(id: $id){
    activity_type
    activiy_text
    created_at
    create_time
  }
}
`;

export const GET_SERVICES = gql
`query services{
  services{
    account_id
    billable
    description
    hours
    id
    name
    price
    tax1_id
    tax2_id
    taxes_id
  }
}`;

export const GET_TERMS = gql
`query terms{
  terms{
    id
    name
  }
}`;

// export const GET_TAXES = gql
// `query taxes{
//   taxes{
//     id
//     name
//     account_id
//     rate
//     number
//     compound
//   }
// }`;

export const GET_TAXES = gql
`query taxes{
  taxes{
    id
    name
    account_id
    rate
    number
    compound
  }
}`;

export const GET_CLIENTS = gql
`query clients{
  clients{
    address_id
    company_name
    currency_id
    id
    language_id
    language
    {
      id
      name
    }
    address
    {
      id
      address_1
      address_2
      zipcode
      city
      country_id
      country{
        id
        name
        code
      }
      state
    }
    contacts {
      id
      first_name
      last_name
      email
      mobile
 
    }
    invoices{
      id
      invoice_number
      invoice_date
      inv_date
      due_amount
      invoice_status
    

    }
    currency{
      id
      name
    }
 
  }
 } 
`;

export const GET_INVOICE = gql
`query get_invoices{
  invoices{
    id,
    invoice_number,
    po_number,
    organization,
    first_name,
    last_name,
    address1,
    address2,
    city,
    country,
    zip,
    status,
    invoice_status,
    amount,
      invoice_lines{
       id
       name
       description
       quantity
       unit_cost
       line_total
       currency_symbol
       tax1_name
       tax1_percent
       line_tax_amount
    },
      client{
        id
        company_name
        phone
        contacts{
          email
          first_name
          last_name
        }
        currency{
          name
          code
          symbol
        }
        language{
          name
          code
        }
      address{
        address_1
        address_2
        city
        state
        zipcode
        country{
          name
          code
        }
      }
    }
    paid_amount,
    outstanding_amount,
    notes,
    terms,
    language,
      account{
        name
        slug
        account_image_url
          language{
            code
            name
          }
          currency{
            name
            code
            symbol
          }
        phone
        fax
      }
    inv_date,
    updated_at,
    created_at,
    inv_due_date,
    status,
    discount_type,
    discount,
    discount_amount_wise,
    total_invoice_amount,
    due_amount,
    }
  }
`;

export const GET_ACCOUNT_USERS = gql
`query get_account_users{
  account_users{
  	email
    first_name
    last_name
    timezone{
      name
      abbr
      UTC_offset
    }
    address{
       address_1
       address_2
       city
       state
       zipcode
       country{
         name
         code
       }
     }
    account{
      name      
    }
    language{
      id
      name
      code
    }
    currency{
      id
      name
      code
      symbol
    }
    user_image_url
    phone
    mobile
  }
}
`;




// Mutations
export const PRE_SIGN_UP = gql`
  mutation createProspect($email: String!) {
    createProspect(email: $email) {
      email
      created_at
    }
  }
`;

// mutation signIn($email: String!, $password: String!) {
//   signIn(email: $email, password: $password) {
//     authentication_token
//   }
// }


// export const SIGN_IN = gql`
//   mutation signIn($email: String!, $password: String!) {
//     signIn(input: { email: $email, password: $password }) {
//       authentication_token
//     }
//   }
// `;

// export const SIGN_IN = gql`
// mutation signIn{
//   signIn(input:{email: "admin@invoease.com",password: "12345678"}){
//     user{
//       email    
//     }
//     authentication_token
//     message
//     errors
    
//   }
// }
// `;

export const FORGOT_PASSWORD = gql`
mutation forgot_password($email: String!){
  forgotPassword(input:{
    email: $email
  }){
    message
    errors
  }
}
`;

export const CHANGE_PASSWORD = gql`
mutation change_password($old_password: String!, $password: String!, $password_confirmation: String!, $authentication_token: String! ){
  ChangePassword(input:{
    old_password: $old_password,
    password: $password,
    password_confirmation: $password_confirmation,
    authentication_token: $authentication_token
  }){
    message
    errors
  }
}
`;

export const SIGN_UPUSER = gql`
mutation SignUpUser($first_name: String!, $last_name: String!, $email: String!, $password: String!, $company_name: String!){
  SignUpUser(input:{
    first_name: $first_name,
    last_name: $last_name,
    email: $email,
    password: $password,
    company_name: $company_name
  }){
    user{
      email 
      first_name
      last_name   
    }
    authentication_token
    message
    errors
    
  }
}
`;

export const SIGN_IN = gql`
mutation signIn($email: String!, $password: String!){
  signIn(input:{email: $email,password: $password}){
    user{
      email,
      timezone {
        id
        name
      }
      taxes{
        id
        name
        rate
      },
      username,
      first_name,
      last_name,
      user_image_url
      address{
        id
        country{
          id
          name
        }
        state
        address_1
        address_2
        zipcode
        city
      }
      account {
        id,
        name,
        account_image_url
        currency {
          id
          code
          name,
          symbol
        },
        phone,
        fax,
        language{
          id
          code,
          name
        }
      }
    }
    authentication_token
    
    message
    errors
  
    
  }
}
`;

export const CREATE_PRODUCT = gql`
mutation create_product($name: String!, $description: String!, $price: Float!, $quantity: Float!, $inventory: Float!,$tax1_id: Int!, $tax2_id: Int!, $account_id: Int! ){
  createProduct(input:{
    name: $name,
    description: $description,
    price: $price,
    quantity: $quantity,
    inventory: $inventory,
    tax1_id:$tax1_id,
    tax2_id:$tax2_id,
    account_id: $account_id
    
  }){
    product{
      name
      description
      price
      quantity
      inventory
      tax1_id
      tax2_id
      account_id
    }
    message
    errors
  }
}`;

export const DESTORY_PRODUCT = gql`
mutation destroy_product($id: ID! ){
  destroyProduct(input:{
    id: $id
  }){
    product{
      name
    }
    message
    errors
  }
}`;

export const UPDATE_ACCOUNT = gql`
mutation update_account($id: ID!, $name: String!, $phone: String, $fax: String, $account_language_id: Int!, $account_currency_id: Int!){
  updateAccount(input:{
    id: $id,
    name: $name,
    phone: $phone,
    fax: $fax,
    account_language_id: $account_language_id,
    account_currency_id: $account_currency_id,
    
  }){
    user{
      email,
      user_image_url
      address{
        id
        country{
          id
          name
        }
        state
        address_1
        address_2
        zipcode
        city
      }
      account {
        id,
        name,
        account_image_url
        currency {
          id
          code
          name,
          symbol
        },
        phone,
        fax,
        language{
          id
          code,
          name
        }
      }
    }
    authentication_token
    
    message
    errors
  }
}`;


export const UPDATE_USER = gql`
mutation update_user($first_name: String, $last_name: String,$username: String, $address_1: String, $address_2: String, $zipcode: String, $country_id: Int,$state: String, $email: String, $timezone_id: Int, $language_id: Int,$user_avatar:String){
  UpdateUser(input:{
    first_name: $first_name,
    last_name: $last_name,
    username: $username,
    address_1: $address_1,
    address_2: $address_2,
    zipcode: $zipcode,
    country_id: $country_id,
    state: $state,
    email: $email,
    timezone_id: $timezone_id,
    language_id: $language_id,
    user_avatar:$user_avatar
    
  }){
    user{
      email,
      user_image_url
      address{
        id
        country{
          id
          name
        }
        state
        address_1
        address_2
        zipcode
        city
      }
      account {
        id,
        name,
        account_image_url
        currency {
          id
          code
          name,
          symbol
        },
        phone,
        fax,
        language{
          id
          code,
          name
        }
      }
    }
    
    message
    errors
  }
}`;


export const UPDATE_PRODUCT = gql`
mutation update_product($id: ID!, $name: String!, $description: String, $price: Int, $quantity: Int, $inventory: Int,$tax1_id:Int, $account_id: Int! ){
  updateProduct(input:{
    id: $id,
    name: $name,
    description: $description,
    price: $price,
    quantity: $quantity,
    inventory: $inventory,
    tax1_id: $tax1_id,
    account_id: $account_id
    
  }){
    product{
      name
      id
      inventory
      account_id
      tax1_id
    }
    message
    errors
  }
}`;

export const CREATE_CLIENT = gql`
mutation create_client($companyname: String!, $phone: String, $currency_id: Int, $language_id: Int, $delivery_method: String,$address_id: Int, $account_id: Int, $firstname: String, $lastname: String,
  $email: String, $mobile: String, $client_id: Int, $is_primary_contact: Boolean, $address1: String, $address2: String, $city: String, $state: String, $country_id: Int,
  $zipcode: String){
  createClient(input:{
    company_name: $companyname,
    phone: $phone,
    currency_id: $currency_id,
    language_id: $language_id,
    delivery_method: $delivery_method,
    address_id: $address_id,
    account_id: $account_id,
    first_name: $firstname,
    last_name: $lastname,
    email : $email,
    mobile: $mobile,
    client_id: $client_id,
    is_primary_contact: $is_primary_contact
    address_1: $address1,
    address_2: $address2,
    city: $city,
    state: $state,
    country_id: $country_id,
    zipcode: $zipcode
    
  }){
    client{
      company_name
        phone
      currency_id
      language_id
      delivery_method
      address_id
    }
    message
    errors
    
  }
}`;



export const DESTORY_CLIENT = gql`
mutation destroy_client($id: ID!){
  destroyClient(input:{
    id: $id,
  }){
    client{
      company_name
    }
    message
    errors
    
  }
}`;

export const CREATE_USER = gql`
mutation create_account_user(
  $email: String!,
  $password: String!, 
  $password_confirmation: String!, 
  $first_name: String!, 
  $last_name: String!,
  $language_id: Int!,
  $timezone_id: Int,
  $address_1: String,
  $address_2: String,
  $city: String!, 
  $state: String!,
  $country_id: Int!,
  $zipcode: String,
  $phone: String,
  $mobile: String , 
  $user_avatar: String,
  $app_version: String,
  ){
  AccountUserCreate(input:{
    email: $email,
    password: $password,
    password_confirmation: $password_confirmation,
    first_name: $first_name,
    last_name: $last_name,
    language_id: $language_id,
    timezone_id: $timezone_id,
    address_1: $address_1,
    address_2: $address_2,
    city: $city,
    state: $state,
    country_id: $country_id,
    zipcode: $zipcode,
    phone: $phone,
    mobile: $mobile,
    user_avatar: $user_avatar,
    app_version: $app_version,
  }){
    user{
      email
     roles{
       id
       name
     }
    }
    
    message
    errors
  }
}`;

export const CREATE_INVOICE = gql`
mutation create_invoice($currency_id: Int!,$first_name: String, $last_name: String, $address1: String, $address2: String,
  $city: String, $state: String, $country: String, $zipcode: String, $client_id: Int, $notes: String, $terms: String, $po_number: String, 
  $invoice_number: String,$invoice_date: String,$invoice_due_date: String,$total_line_count: Int,$line: String,
  $total_amount: Float!,$outstanding_amount: Float!,$discount_type: String,$discount_flag: String,$discount_percentage: Float!,
  $discount_amount: Float!){
  CreateInvoice(input:{
    currency_id: $currency_id,
    first_name: $first_name,
    last_name: $last_name,
    address1: $address1,
    address2: $address2,
    city: $city,
    state: $state,
    country:$country,
    zipcode: $zipcode,
    client_id: $client_id,
    notes: $notes,
    terms: $terms,
    po_number: $po_number,
    invoice_number: $invoice_number,
    invoice_date: $invoice_date,
    invoice_due_date: $invoice_due_date,
    total_line_count: $total_line_count,
    line:$line ,
  	total_amount: $total_amount,
    outstanding_amount: $outstanding_amount,
    discount_type: $discount_type,
    discount_flag: $discount_flag,
    discount_percentage: $discount_percentage,
    discount_amount: $discount_amount    
  }){
    errors
    message
  }
}`;

export const UPDATE_INVOICE = gql`
mutation update_invoice($id:Int,$first_name: String, $last_name: String, $address1: String, $address2: String,
  $city: String, $state: String, $country: String, $zipcode: String, $client_id: Int, $notes: String, $terms: String, $po_number: String, 
  $invoice_number: String,$invoice_date: String,$invoice_due_date: String,$total_line_count: Int,$line: String,
  $total_amount: Float!,$outstanding_amount: Float!,$discount_type: String,$discount_flag: String,$discount_percentage: Float!,
  $discount_amount: Float!){
  UpdateInvoice(input:{
    id: $id,
    first_name: $first_name,
    last_name: $last_name,
    address1: $address1,
    address2: $address2,
    city: $city,
    state: $state,
    country:$country,
    zipcode: $zipcode,
    client_id: $client_id,
    notes: $notes,
    terms: $terms,
    po_number: $po_number,
    invoice_number: $invoice_number,
    invoice_date: $invoice_date,
    invoice_due_date: $invoice_due_date,
    total_line_count: $total_line_count,
    line:$line ,
  	total_amount: $total_amount,
    outstanding_amount: $outstanding_amount,
    discount_type: $discount_type,
    discount_flag: $discount_flag,
    discount_percentage: $discount_percentage,
    discount_amount: $discount_amount    
  }){
    errors
    message
  }
}`;

export const CREATE_TERMS = gql`
mutation create_terms($name: String!, $account_id: Int!){
  createTerm(input:{
    name: $name,
    account_id: $account_id
  }){
    Term{
      name
      account_id
    }
    
    message
    errors
  }
}`;

export const UPDATE_TERMS = gql`
mutation update_term($id: ID! $name: String!){
  updateTerm(input:{
    id: $id,
    name: $name
  }){
    Term{
      name
      account_id
    }
    
    message
    errors
  }
}`;



export const DESTORY_TERMS = gql`
mutation destroy_term($id: ID!){
  destroyTerm(input:{
    id: $id,
  }){
    Term{
      name
    }
    
    message
    errors
  }
}`;

export const CREATE_TAXES = gql`
mutation create_taxes($name: String!, $rate: Float!, $number: String!, $compound: Float, $account_id: Int!){
  createTax(input:{
    name: $name,
    rate: $rate,
    number: $number,
    compound: $compound,
    account_id: $account_id
  }){
    Tax{
      name
      number
      rate
      compound
      account_id
    }
    
    message
    errors
  }
}`;


export const UPDATE_TAXES = gql`
mutation update_tax($id: ID!, $name: String!, $rate: Float!, $number: String!, $compound: Float){
  updateTax(input:{
    id: $id,
    name: $name,
    rate: $rate,
    number: $number,
    compound: $compound
  }){
    Tax{
      name
      number
      rate
      compound
      account_id
    }
    
    message
    errors
  }
}`;



export const DESTORY_TAX = gql`
mutation destroy_tax($id: ID!){
  destroyTax(input:{
    id: $id
  }){
    Tax{
      name
    }
    message
    errors
  }
}`;


export const CREATE_SERVICE = gql`
mutation create_service($name: String!, $description: String!, $price: Float!, $billable: Boolean, $hours: Float!, $account_id: Int!){
  createService(input:{
    name: $name,
    description: $description,
    price: $price,
    billable: $billable,
    hours: $hours,
    account_id: $account_id
  }){
    service{
      name
      description
      billable
      price
      hours
      tax1_id
      tax2_id
      account_id
    }
    
    message
    errors
  }
}`;

export const UPDATE_SERVICE = gql`
mutation update_service($id: ID!, $name: String!, $description: String!, $price: Float!, $billable: Boolean, $hours: Float!){
  updateService(input:{
    id: $id,
    name: $name,
    description: $description,
    price: $price,
    billable: $billable,
    hours: $hours,
  }){
    service{
      name
      description
      billable
      price
      hours
      tax1_id
      tax2_id
      account_id
    }
    
    message
    errors
  }
}`;

export const DESTORY_SERVICE = gql`
mutation destroy_service($id: ID!){
  DestroyService(input:{
    id: $id
  }){
    service{
      name
    }
    message
    errors
  }
}`; 

export const SEND_EMAIL_TO_CLIENT = gql`
mutation send_email_to_client($id: Int!, $cc: String, $bcc: String, $subject: String){
     SendInvoiceEmailToClient(input:{
       id: $id,
       cc: $cc,
       bcc: $bcc,
       subject : $subject,
     }){
       message
       errors
     }
}`;

export const ADD_PAYMENT_FOR_INVOICE = gql`
mutation add_payment_for_invoice($amount: Float!, $date: String!, $payment_method: String!, $notes: String, $invoice_id: Int!){
  AddPayment(input:{
    amount: $amount,
    date: $date,
    payment_method: $payment_method,
    notes: $notes,
    invoice_id: $invoice_id
  }){
    Payment{
      amount
      payment_method
      date
      notes
    },
    message
    errors    
  }
}`;


//
// mutation MySignIn {
//   SignIn(email: "admin@complitech.net", password: "password") {
//     authentication_token
//   }
// }
//
// query MyQuery($id: Int!) {
//   task(id: $id) {
//     id
//     name
//   }
// }
//
// query Tasks {
//   All_Tasks {
//     name
//     description
//   }
// }
//
// query Items {
//   All_Items {
//     name
//     description
//   }
// }
//
// mutation CreateTask {
//   CreateTask(name: "Android Development", description: "Coding Android application") {
//     id
//   }
// }
//
// mutation CreateItem {
//   CreateItem(name: "Web Design", description: "Designing stuff") {
//     id
//   }
// }
