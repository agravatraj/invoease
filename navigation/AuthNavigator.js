import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation'; // 1.0.0-beta.19

import Register from './Register';
import Login from './Login';
import Profile from './Profile';

const AuthStack = StackNavigator({
  Register: { screen: Register, navigationOptions: { headerTitle: 'Register' } },
  Login: { screen: Login, navigationOptions: { headerTitle: 'Login' } },
});

