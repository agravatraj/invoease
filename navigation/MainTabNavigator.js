import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import EstimateListScreen from '../screens/EstimateListScreen';
import EstimateCreateScreen from '../screens/EstimateCreateScreen';
import SettingsScreen from '../screens/SettingsScreen';
import Dashboard from '../screens/Dashboard';
import ClientScreen from '../screens/ClientScreen';
import ClientScreen1 from '../screens/ClientScreen';
import ClientDetail from '../screens/ClientDetails';
import EditInvoiceScreen from '../screens/EditInvoiceScreen';
import EditProduct from '../screens/EditProduct';
import EditUser from '../screens/EditUser';
import CreateInvoiceScreen from '../screens/CreateInvoiceScreen';
import InvoiceDetailScreen from '../screens/InvoiceDetailScreen';
//import TestAddnewitem from '../screens/TestAddnewitem';

import ServicesScreen from '../screens/ServicesScreen';
import TaxScreen from '../screens/TaxScreen';
import ProductScreen from '../screens/ProductScreen';
import ProductAddScreen from '../screens/ProductAddScreen';
import SortBy from '../screens/SortBy';
import TermsScreen from '../screens/TermsScreen';

import ProfileScreen from '../screens/ProfileScreen';

import AddClient from '../screens/AddClient';
import AddTax from '../screens/AddTax';
import AddTerms from '../screens/AddTerms';
import AddService from '../screens/AddService';
import AddPayment from '../screens/AddPayment';
import AddUser from '../screens/AddUser';

import InvoiceView from '../screens/InvoiceView';
import InvoiceEmail from '../screens/InvoiceEmail';
import InvoiceNotification from '../screens/InvoiceNotification';

import Notification from '../screens/Notification';
import SeachInvoice from '../screens/SeachInvoice';

import AccountSettings from '../screens/AccountSettings';
import IntroScreen from '../screens/IntroScreen';
import UserList from '../screens/UserList';
import ChangePassword from '../screens/ChangePassword';


import ForgetScreen from '../screens/ForgetScreen';
import EditItem from '../screens/EditItem';
import EditService from '../screens/EditService';
import EditTax from '../screens/EditTax';
import EditTerms from '../screens/EditTerms';


const DashboardStack = createStackNavigator({
  Dashboard: Dashboard,
});

DashboardStack.navigationOptions = {
  tabBarLabel: 'Dashboard',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-pie${focused ? '' : ''}`
          : 'ios-pie'
      }
    />
  ),
};

const HomeStack = createStackNavigator({
  Home: HomeScreen,
  CreateInvoiceScreen: CreateInvoiceScreen,
  InvoiceDetailScreen: InvoiceDetailScreen,
  ProductScreen: ProductScreen,
  SortBy: SortBy,
  AddTax: AddTax,
  EditProduct: EditProduct,
  // 
  EditInvoiceScreen: EditInvoiceScreen,
  ProductAddScreen: ProductAddScreen,
  InvoiceView:InvoiceView,
  ClientScreen: ClientScreen1,
  AddClient: AddClient,
  InvoiceEmail:InvoiceEmail,
  AddPayment : AddPayment,
  InvoiceNotification : InvoiceNotification,
  Notification : Notification,
  //TestAddnewitem:TestAddnewitem,
  EditItem : EditItem,
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Invoice',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-calculator${focused ? '' : ''}`
          : 'ios-calculator'
      }
    />
  ),
};

const EstimateStack = createStackNavigator({
  Estimate: EstimateListScreen,
  EstimateCreateScreen: EstimateCreateScreen,
});

EstimateStack.navigationOptions = {
  tabBarLabel: 'Estimate',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-calculator' : 'ios-calculator'}
    />
  ),
};

const ClientStack = createStackNavigator({
  Client: ClientScreen,
  AddClient: AddClient,
  ClientDetail:ClientDetail,
  HomeScreen: HomeScreen,
  CreateInvoiceScreen: CreateInvoiceScreen,
  InvoiceDetailScreen: InvoiceDetailScreen,
  InvoiceView:InvoiceView,
  EditInvoiceScreen: EditInvoiceScreen,
  ProductScreen: ProductScreen,
  ProductAddScreen: ProductAddScreen,
  EditProduct: EditProduct,
});

ClientStack.navigationOptions = {
  tabBarLabel: 'Client',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-man${focused ? '' : ''}`
          : 'ios-man'
      }
    />
  ),
};


const SettingsStack = createStackNavigator({
  Settings: SettingsScreen,
  ServicesScreen: ServicesScreen,
  TaxScreen: TaxScreen,
  ProductScreen: ProductScreen,
  AddTax: AddTax,
  ProductAddScreen: ProductAddScreen,
  TermsScreen: TermsScreen,
  AddTerms: AddTerms,
  ProfileScreen: ProfileScreen,
  AddService: AddService,
  AddPayment: AddPayment,
  InvoiceView:InvoiceView,
  InvoiceNotification:InvoiceNotification,
  Notification:Notification,
  SeachInvoice:SeachInvoice,
  AccountSettings:AccountSettings,
  IntroScreen:IntroScreen,
  ForgetScreen:ForgetScreen,
  EditItem:EditItem,
  UserList:UserList,
  EditService:EditService,
  EditTax:EditTax,
  EditUser: EditUser,
  EditTerms:EditTerms,
  EditProduct:EditProduct,
  AddUser:AddUser,
  AddTax: AddTax,
  ChangePassword:ChangePassword,
});

SettingsStack.navigationOptions = {
  tabBarLabel: 'Settings',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-settings' : 'ios-settings'}
    />
  ),
};

export default createBottomTabNavigator({
  DashboardStack,
  HomeStack,
  EstimateStack,
  ClientStack,
  SettingsStack,
});
